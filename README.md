# Japanese translation of Kare wa Kanojo

**Status of the translation**: 60%, based on LameKwK - Looking for translators and corrections...

### Notes about the japanese Translation

Since the action takes place in Japan, some details needs to be mentionned for the japanese adaptation of the texts.

- Don't forget the traditional 「 and 」 when people are speaking. Like, all the japanese visual novels use them so...
- Also use 『 and 』 for brackets that are: already inside 「 and 」s and dialog inside phones (you can remove `{i}` stuff there)
- Do not worry much for indentation. Ren'Py will manage them automatically thanks to the "japanese-strict" parameter inside the code.
- Names are spelled like this: Sakura: 咲良, Rika: 梨花, Nanami:七海, Taichi: 太一, Toshio: 利夫.
- Rika speaks with the Kansai dialect.
- Rika always addresses the player and her friends without any particle word. (Like, just "Taichi", never "Taichi-kun" or anything, for instance.) when she doesn't call him 街ネズミ of course. (city rat)
- Sakura speaks very polite but not too much. She's kind of a "desu" girl. I don't know if people from Kyoto have a specific dialect. But if it's the case...
- Sakura speaks to Nanami like a mother would do for her child.
- Sakura calls her father 父ゆえ (chichi-yue) but calls her mother 母さん. (kaa-san)
- Nanami speaks like a child, so, she often uses her name to refer herself. (like, 七海は太一にが大好き or "Nanami loves Taichi-nii") I don't know if there's a specific dialect for Okinawa...
- Nanami is often called by her friends (mostly by sakura) ななちゃん (Nana-chan)
- Sakura's mother is kind of the "ara ara" mom type.
- The player speaks like a dynamic boy, while not being too much of a nekketsu boy. Think of the average harem anime hero.
