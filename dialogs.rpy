﻿# TODO: Translation updated at 2018-04-25 09:34

# game/dialogs.rpy:3
translate japanese scene1_acf2102c:

    # centered "{size=+12}Summer of the 9th year of Heisei era\n(1997){/size}"
    centered "{size=+12}平成九年　夏{/size}"

# game/dialogs.rpy:6
translate japanese scene1_dd8c4dd5:

    # "Life has a way of changing. Often drastically without any warnings whatsoever."
    "人生とは唐突に変化する物だ。大抵は何の前触れも無く、ガラリと変わってしまう。"

# game/dialogs.rpy:7
translate japanese scene1_0497b746:

    # "At first, a day may seem like any other."
    "昨日はいつもと同じ様に過ごしていた。"

# game/dialogs.rpy:8
translate japanese scene1_1ab3b689:

    # "But by tomorrow, it can all change...{p}from where you live, to who you are as a person."
    "しかし今日、何もかもが変わってしまった…{p}住んでいた場所、環境…自分自身まで…何て事も珍しくない。"

# game/dialogs.rpy:9
translate japanese scene1_522a76b6:

    # "Recently, I've experienced such a large change."
    "最近、僕もそんな様な経験をした。"

# game/dialogs.rpy:10
translate japanese scene1_011ea937:

    # "My dad wanted to start his own grocery shop. So my parents and I moved to a small village near Osaka."
    "父は以前から百貨店を開業しようとしていたらしく、僕達家族は意を決して大阪の隣の小さな村まで引越して来たのだ。"

# game/dialogs.rpy:11
translate japanese scene1_2b371705:

    # "I thought I would have a hard time readjusting."
    "東京に居た僕等がここの環境に慣れるまでは大変だった。"

# game/dialogs.rpy:12
translate japanese scene1_a26a5f2b:

    # "The move from Tokyo to a small lost village, far from a big city with shops and fast-food restaurants..."
    "お店やファーストフードからずっと離れた小さな村への引越し…"

# game/dialogs.rpy:13
translate japanese scene1_fa8a1f18:

    # "Being a 18-year-old student, it would be difficult. I was used to the excitement of the big city."
    "18歳の少年にとってそれは中々過酷な環境だった。"

# game/dialogs.rpy:14
translate japanese scene1_8d6f1c40:

    # "However, even before my first day of school, my life would change."
    "ところが暫くして、そんな環境はまた変わり始めた。それは村の学校に初めて登校する日の事…"

# game/dialogs.rpy:19
translate japanese scene1_d7a7c4a2:

    # centered "{size=+35}CHAPTER 1\nWhere everything started{fast}{/size}"
    centered "{size=+35}第1章\nすべてが始まったところ{fast}{/size}"

# game/dialogs.rpy:26
translate japanese scene1_5bb0286e:

    # "It's sunday evening, after dinner."
    "It's sunday evening, after dinner."

# game/dialogs.rpy:27
translate japanese scene1_a651e12e:

    # "My stuff is all unpacked and placed in my new room."
    "My stuff is all unpacked and placed in my new room."

# game/dialogs.rpy:28
translate japanese scene1_54a8b155:

    # "I love this new look!"
    "I love this new look!"

# game/dialogs.rpy:29
translate japanese scene1_8bb96761:

    # "It sure is smaller than my previous bedroom, but it's not like I needed that much space."
    "It sure is smaller than my previous bedroom, but it's not like I needed that much space."

# game/dialogs.rpy:30
translate japanese scene1_acf53d85:

    # "As long as I have room to sleep, play games, watch anime, and geek out, it's all good."
    "As long as I have room to sleep, play games, watch anime, and geek out, it's all good."

# game/dialogs.rpy:31
translate japanese scene1_7d722416:

    # "I'm not sure what I should do before going to bed."
    "I'm not sure what I should do before going to bed."

# game/dialogs.rpy:32
translate japanese scene1_b2b74adf:

    # "I could study a bit, so I don't look too dumb at school tomorrow..."
    "I could study a bit, so I don't look too dumb at school tomorrow..."

# game/dialogs.rpy:33
translate japanese scene1_ad0d9f75:

    # "But I also want to rest and have a bit of fun..."
    "But I also want to rest and have a bit of fun..."

# game/dialogs.rpy:48
translate japanese scene1_e05b5690:

    # "I should play a short game, or I'll stay up too long. So I decide to play some {i}Tetranet online{/i}."
    "I should play a short game, or I'll stay up too long. So I decide to play some {i}Tetranet online{/i}."

# game/dialogs.rpy:49
translate japanese scene1_7170a1ff:

    # "It's a puzzle game with blocks. The online version lets you attack your opponents and flood them with their own blocks."
    "It's a puzzle game with blocks. The online version lets you attack your opponents and flood them with their own blocks."

# game/dialogs.rpy:50
translate japanese scene1_8dd01706:

    # "After a few minutes, I found myself in a game room. One of the players named 'NAMINAMI' had two stars in front of their name."
    "After a few minutes, I found myself in a game room. One of the players named 'NAMINAMI' had two stars in front of their name."

# game/dialogs.rpy:51
translate japanese scene1_962ec706:

    # "Two stars meant that they had won hundreds of games online. Since three stars are for the game developers, I guess that means I'm up against a pro of this game."
    "Two stars meant that they had won hundreds of games online. Since three stars are for the game developers, I guess that means I'm up against a pro of this game."

# game/dialogs.rpy:52
translate japanese scene1_a71313b4:

    # "Judging by the low ping, I guess they live around the same prefecture I live in. Small world..."
    "Judging by the low ping, I guess they live around the same prefecture I live in. Small world..."

# game/dialogs.rpy:53
translate japanese scene1_e0301d9f:

    # "In the dozens of games we played, NAMINAMI continued to beat everyone in the game room. But I was always the last one to fall."
    "In the dozens of games we played, NAMINAMI continued to beat everyone in the game room. But I was always the last one to fall."

# game/dialogs.rpy:55
translate japanese scene1_a40571de:

    # write "NAMINAMI> GG dude, u pretty much rock at this game!{fast}"
    write "NAMINAMI> GG dude, u pretty much rock at this game!{fast}"

# game/dialogs.rpy:57
translate japanese scene1_7c6f0607:

    # write "%(stringhero)s>{fast} Nah i'm not that good"
    write "%(stringhero)s>{fast} Nah i'm not that good"

# game/dialogs.rpy:59
translate japanese scene1_0d6dddc9:

    # write "NAMINAMI> yes u r. i had trouble winning against u.{fast}"
    write "NAMINAMI> yes u r. i had trouble winning against u.{fast}"

# game/dialogs.rpy:61
translate japanese scene1_87ab3c60:

    # write "NAMINAMI> care 4 a rematch? just u and me?{fast}"
    write "NAMINAMI> care 4 a rematch? just u and me?{fast}"

# game/dialogs.rpy:63
translate japanese scene1_828e7297:

    # write "%(stringhero)s>{fast} Maybe next time. It's getting late, i have school tomorrow."
    write "%(stringhero)s>{fast} Maybe next time. It's getting late, i have school tomorrow."

# game/dialogs.rpy:65
translate japanese scene1_0d8de120:

    # write "NAMINAMI> ur rite, me 2 anyway. hope 2 c u again soon!{fast}"
    write "NAMINAMI> ur rite, me 2 anyway. hope 2 c u again soon!{fast}"

# game/dialogs.rpy:66
translate japanese scene1_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:73
translate japanese scene1_ee02f0bc:

    # "I decide to watch some old episodes of the anime based on my favorite manga, {i}High School Samurai{/i}."
    "I decide to watch some old episodes of the anime based on my favorite manga, {i}High School Samurai{/i}."

# game/dialogs.rpy:74
translate japanese scene1_d91b4fcd:

    # "It's the story about a guy who lived in the mountains, learning the ways of Bushido. Then, all of a sudden, he must go to a new town to learn about life in the city."
    "It's the story about a guy who lived in the mountains, learning the ways of Bushido. Then, all of a sudden, he must go to a new town to learn about life in the city."

# game/dialogs.rpy:75
translate japanese scene1_e72d0922:

    # "He resolves a lot of his problems with his samurai skills and his heroic determination."
    "He resolves a lot of his problems with his samurai skills and his heroic determination."

# game/dialogs.rpy:80
translate japanese scene1_a19cfb71:

    # hero "「I decide to study. Keep up the great work, %(stringhero)s!"
    hero "「I decide to study. Keep up the great work, %(stringhero)s!」"

# game/dialogs.rpy:82
translate japanese scene1_ad5fbe82:

    # "I studied until very late at night."
    "I studied until very late at night."

# game/dialogs.rpy:84
translate japanese scene1_2f455d53:

    # "I don't know how long I stayed up, but I was so tired, I ended up falling asleep..."
    "I don't know how long I stayed up, but I was so tired, I ended up falling asleep..."

# game/dialogs.rpy:89
translate japanese scene1_cb77d3d7:

    # "*{i}Riiiiiiiiiiiing{/i}*"
    "*{i}ピピピピ……ピピピピ……{/i}*"

# game/dialogs.rpy:91
translate japanese scene1_4037429a:

    # "I switched off the alarm clock and went back to sleep again."
    "僕はアラームを切ってもう一度目を閉じた。"

# game/dialogs.rpy:92
translate japanese scene1_548721a3:

    # hero "「I'm so tired..."
    hero "「疲れた…」"

# game/dialogs.rpy:93
translate japanese scene1_2a13d6dc:

    # "Mom" "%(stringhero)s!! Wake up!!! You'll be late for school!!!"
    "母" "「%(stringhero)s！ 起きなさい！学校に遅れるわよ！！」"

# game/dialogs.rpy:94
translate japanese scene1_c83937b9:

    # hero "「Huh... What time is it..."
    hero "「…うーん…今何時…」"

# game/dialogs.rpy:95
translate japanese scene1_fc1e319f:

    # "Wait... 8:02AM?!{p} School starts at 8:30AM!!!"
    "…8時2分…{p}学校が始まるのが8時30分…"

# game/dialogs.rpy:97
translate japanese scene1_4217b105:

    # hero "「Dangit!"
    hero "「やばい！」"

# game/dialogs.rpy:99
translate japanese scene1_62c6f195:

    # hero "「I stayed up and played too much! I'm late!"
    hero "「I stayed up and played too much! I'm late!」"

# game/dialogs.rpy:101
translate japanese scene1_b8605e00:

    # hero "「I watched too much anime! I'm late!"
    hero "「I watched too much anime! I'm late!」"

# game/dialogs.rpy:103
translate japanese scene1_a476aee9:

    # hero "「I studied too much! I'm late!"
    hero "「I studied too much! I'm late!」"

# game/dialogs.rpy:106
translate japanese scene1_0ff90bcf:

    # "I jumped out of bed, put on my brand new school uniform, and ran down the stairs."
    "ベッドから飛び上がり、新しい制服を着て、階段を駆け下りた。"

# game/dialogs.rpy:108
translate japanese scene1_5c1508de:

    # "I ate my breakfast faster than usual."
    "いつもより早く朝食を食べ終え…"

# game/dialogs.rpy:109
translate japanese scene1_6a44f1a3:

    # "I took a map with a route to school and left after saying goodbye to my parents."
    "両親にサヨナラを伝え学校への地図を持って出発した。"

# game/dialogs.rpy:112
translate japanese scene1_ad63def5:

    # "I followed the map and dashed through the streets."
    "地図の通りに街を走る。"

# game/dialogs.rpy:113
translate japanese scene1_00e59ce7:

    # "This town looks pretty unique."
    "しかし…この街は中々ユニークだ。"

# game/dialogs.rpy:114
translate japanese scene1_689e5094:

    # "It's a strange town, like a mix between a suburb and a rural village."
    "郊外の様でもあり、田舎の村の様でもある。"

# game/dialogs.rpy:115
translate japanese scene1_a64e7e8b:

    # "Summer had just arrived. The cicadas were crying. And it sure was hot out here..."
    "季節はすっかり夏になり、蝉も日本でよく鳴いている様に鳴いている。そして何よりも…暑い。"

# game/dialogs.rpy:116
translate japanese scene1_71061539:

    # "I noticed only a few people on the streets."
    "道にはチラホラと人が見える。"

# game/dialogs.rpy:117
translate japanese scene1_1d275b47:

    # "Some of them were kind enough to help me find my way to the school..."
    "その中には親切に学校に行く道を探すのを手伝ってくれた人も居た。"

# game/dialogs.rpy:118
translate japanese scene1_e123d412:

    # "Then, as I was nearing an intersection..."
    "そして僕が交差点に差し掛かった時に…"

# game/dialogs.rpy:119
translate japanese scene1_da3e8686:

    # "I see a girl in a school uniform."
    "制服姿の少女を見掛けたのだ。"

# game/dialogs.rpy:121
translate japanese scene1_72d48eb1:

    # hero "「Well, since village only has one school...that must be where she's heading!"
    hero "「学校が一つしか無いのなら、この娘もそこに向かう筈だ。」"

# game/dialogs.rpy:122
translate japanese scene1_018ee7a3:

    # hero "「And if that is where she's going, maybe she could help me out!"
    hero "「道を教えてくれるかもだ！」"

# game/dialogs.rpy:123
translate japanese scene1_a95f40e0:

    # "I approached her carefully, as I didn't want to scare her."
    "彼女を怯えさせない様、慎重に接近し……"

# game/dialogs.rpy:124
translate japanese scene1_b0e8ce5c:

    # "She looked beautiful; long black hair, thin body, and beautiful legs..."
    "…それにしても綺麗な子だ…長い黒髪、細身の体、滑らかな肢体…"

# game/dialogs.rpy:125
translate japanese scene1_d501d47f:

    # "I hesitated for a moment before saying anything..."
    "声を掛ける事を一瞬躊躇してしまう程の……"

# game/dialogs.rpy:126
translate japanese scene1_9195ee62:

    # hero "「Ahem... Excuse me, miss?"
    hero "「えっと…すいません…」"

# game/dialogs.rpy:131
translate japanese scene1_951b2409:

    # "She turned around to face me."
    "少女は振り返ってこちらを向いた。"

# game/dialogs.rpy:132
translate japanese scene1_b7723325:

    # "Oh wow! She was insanely pretty!"
    "それが！彼女は僕の気が触れてしまう程に可愛かったのだ！！"

# game/dialogs.rpy:133
translate japanese scene1_64e26830:

    # "Her eyes were blue.{p}A wonderful, deep, ocean blue. They were like the clear waters you'd find in Hawaii!"
    "Her eyes were blue.{p}A wonderful, deep, ocean blue. They were like the clear waters you'd find in Hawaii!"

# game/dialogs.rpy:134
translate japanese scene1_a28c7063:

    # "She looked at me, confused, and a bit timid."
    "少々がおどおどしながら僕を見ている…"

# game/dialogs.rpy:136
translate japanese scene1_40d86de1:

    # s "「Yes?"
    s "「は、はい…？」"

# game/dialogs.rpy:137
translate japanese scene1_013ed851:

    # "Wow! Her voice sounds so adorable!"
    "声も可愛い…"

# game/dialogs.rpy:138
translate japanese scene1_ed63afd9:

    # "She is so cute, that I was at a loss for words."
    "僕は彼女の可憐さに一瞬言葉を失った。"

# game/dialogs.rpy:139
translate japanese scene1_d015b36e:

    # hero "「Uhhh... I'm new in the village, and I'm trying to find my school."
    hero "「あ…新しく村に来たばかりで、学校を探しているんですけど…」"

# game/dialogs.rpy:140
translate japanese scene1_759f29b6:

    # hero "「And since you're wearing a uniform... I thought you could..."
    hero "「…丁度あなたが制服を着ているのを見かけたから…えっと…」"

# game/dialogs.rpy:141
translate japanese scene1_9c182eb6:

    # "The girl stared at me with her big, majestic, blue eyes..."
    "少女はその青くて大きい、美麗な瞳で僕をじっと見つめている…"

# game/dialogs.rpy:142
translate japanese scene1_e85ec319:

    # "Finally, she smiled."
    "そして暫くして、ニッコリと笑った。"

# game/dialogs.rpy:144
translate japanese scene1_208138e6:

    # s "「Of course I can!"
    s "「勿論、構いませんよ！」"

# game/dialogs.rpy:145
translate japanese scene1_d94b34fa:

    # s "「I'm going there right now. You can follow me!"
    s "「今から行きますから、一緒に行きましょう！」"

# game/dialogs.rpy:146
translate japanese scene1_453ef329:

    # hero "「That would be great! Thank you!"
    hero "「それは助かります…！ありがとう！」"

# game/dialogs.rpy:150
translate japanese scene1_ef8271d0:

    # "We ended up walking to school together."
    "そして、僕達は一緒に学校に向かう事になった。"

# game/dialogs.rpy:151
translate japanese scene1_e8a9e895:

    # "Darn, looks like it's my lucky day. {p}I'm walking to school with a beautiful girl on the first day!!!"
    "どうやら今日は僕のラッキーデーみたいだな…{p}登校初日から可愛い女の子と学校に行くなんて…"

# game/dialogs.rpy:152
translate japanese scene1_cb9f5aad:

    # s "「You said you're new in town, right?"
    s "「この街に越して来たばかり…でしたっけ？」"

# game/dialogs.rpy:153
translate japanese scene1_36317276:

    # hero "「Hmm?"
    hero "「え？」"

# game/dialogs.rpy:155
translate japanese scene1_ecbbbe32:

    # "I had been daydreaming, so I didn't notice that she was talking to me."
    "ぼんやりしてたから何言ってるか聞いてなかった。"

# game/dialogs.rpy:156
translate japanese scene1_d86baf24:

    # "She started to blush."
    "少女は照れくさそうに顔を赤らめている。"

# game/dialogs.rpy:158
translate japanese scene1_0408e4ff:

    # s "「Oh... I'm sorry! I didn't mean to surprise you!"
    s "「あ…ごめんなさい！驚かすつもりは無かったんですけど…」"

# game/dialogs.rpy:159
translate japanese scene1_004a1338:

    # "She's just so adorable! I smiled to myself."
    "可愛い。僕は心が笑顔になった。"

# game/dialogs.rpy:160
translate japanese scene1_02ac9e9f:

    # hero "「You're not!{p}Actually, I just arrived last Friday. My family just moved here from Tokyo."
    hero "「いえ。{p}厳密には先週の金曜日にここに付いたんです。僕達は家族で東京から越して来ました。」"

# game/dialogs.rpy:162
translate japanese scene1_b0c354bc:

    # s "「You're from Tokyo?"
    s "「東京から来たんですか？」"

# game/dialogs.rpy:163
translate japanese scene1_7273bcfc:

    # s "「It seems like a wonderful city! I wish I could visit it someday!"
    s "「東京はそれは素晴らしい所です！私もいつの日か行ってみたいと思ってるんです！」"

# game/dialogs.rpy:164
translate japanese scene1_e87c5166:

    # hero "「I bet... I noticed there are fewer distractions here than in Tokyo."
    hero "「ええ…そういえば、ここに来てからは東京に有った様な娯楽施設を一切見てません…」"

# game/dialogs.rpy:166
translate japanese scene1_6ab8e3af:

    # s "「Mmhmm, it's true. The village is kinda quiet; so there's usually nothing to do..."
    s "「んー、そりゃそうです。この村は普段何もする事の無い静かな場所ですし。」"

# game/dialogs.rpy:167
translate japanese scene1_f5cb34b6:

    # s "「When we want to enjoy ourselves, we usually go to the nearest city."
    s "「もし何かエンジョイしようというのなら、遥々隣町まで行かなければいかんのです。」"

# game/dialogs.rpy:169
translate japanese scene1_f471c6c0:

    # s "「It's only a short train ride away, so it's okay!"
    s "「でも大丈夫ですよ！何たって電車が通ってますからね！」"

# game/dialogs.rpy:170
translate japanese scene1_6136ebba:

    # hero "「There's a train that takes you to the city nearby!?"
    hero "「電車？隣町！？」"

# game/dialogs.rpy:171
translate japanese scene1_bbdac77d:

    # hero "「I hope there's a lot of stuff to do there."
    hero "「そこには色々揃ってると良いけど…」"

# game/dialogs.rpy:172
translate japanese scene1_aa2bb7ad:

    # s "「There is, don't worry! There are fast food restaurants, shops, arcades..."
    s "「安心してください！そこには何でもありますよ。ファーストフード、お店、ゲームセンター…」"

# game/dialogs.rpy:173
translate japanese scene1_cd44299c:

    # hero "「You like arcade games?"
    hero "「行くんですか？ゲームセンター。」"

# game/dialogs.rpy:175
translate japanese scene1_69578672:

    # s "「Very much so!"
    s "「ええ、大好きです！」"

# game/dialogs.rpy:176
translate japanese scene1_13bf5fa0:

    # s "「I'm especially a fan of music and rhythm games, but I enjoy shooting games too!"
    s "「音楽ゲームを良くやるんですけど、ガンシューティングも同じ位好きなんです。」"

# game/dialogs.rpy:177
translate japanese scene1_0fef1f71:

    # hero "「Shooting games?"
    hero "「ガンシューティング…？」"

# game/dialogs.rpy:178
translate japanese scene1_ad5c53dd:

    # "Wow... I couldn't imagine such a cute and timid girl shooting zombies or soldiers in an arcade game..."
    "ううむ…こんなに可愛い女の子がゾンビや兵隊を撃ち殺してる姿が想像できない…"

# game/dialogs.rpy:179
translate japanese scene1_45a2a82d:

    # hero "「By the way, what grade are you in?"
    hero "「…そういえば、あなたは何年生なんですか？」"

# game/dialogs.rpy:181
translate japanese scene1_68018eef:

    # s "「I'm in the second section."
    s "「えっと…ここでは第二部生って言うんですけど…」"

# game/dialogs.rpy:182
translate japanese scene1_4c52535b:

    # hero "「「Uh...Second section?"
    hero "「第二部生」？」"

# game/dialogs.rpy:183
translate japanese scene1_3b1fd997:

    # s "「You see, our school is the only school in the village."
    s "「この村には学校が一つしかありませんよね。」"

# game/dialogs.rpy:184
translate japanese scene1_7e47a0e7:

    # s "「There are students of many different ages and grades at our school, but there's an unbalanced amount of students for the usual grade system."
    s "「だから普通の学校制度だと生徒の数がアンバランスになってしまうんです。」"

# game/dialogs.rpy:185
translate japanese scene1_a82424ad:

    # s "「So they made different classes, called sections, consisting of 25 students each, which are made up of students of different ages and grades."
    s "「そこで、25人ずつのクラスに分けた新しい組分け制度が導入されたんです。」"

# game/dialogs.rpy:186
translate japanese scene1_b83c9923:

    # hero "「「That sounds fun... I wonder where I'll be assigned to..."
    hero "「何だか面白そうだ…どんなクラスに編入されるのか気になってきました。」"

# game/dialogs.rpy:187
translate japanese scene1_696b7e4d:

    # s "「Well, there are only five sections."
    s "「クラスは全部で五つあるんです。」"

# game/dialogs.rpy:188
translate japanese scene1_029cc297:

    # hero "「「So I'll have a 1-in-5 chance of being in the same section as you!"
    hero "「もしかしたら一緒のクラスになれるかも知れませんね。」"

# game/dialogs.rpy:189
translate japanese scene1_c959a464:

    # "I chuckled a bit and she giggled, a little embarrassed."
    "お互い、少し笑い合った。"

# game/dialogs.rpy:191
translate japanese scene1_c152d24c:

    # s "「My name is Sakura... And yours?"
    s "「咲良って言います…あなたは？」"

# game/dialogs.rpy:192
translate japanese scene1_180ea3e3:

    # hero "「Name's %(stringhero)s."
    hero "「%(stringhero)sです。」"

# game/dialogs.rpy:194
translate japanese scene1_abeffde2:

    # s "「%(stringhero)s..."
    s "「%(stringhero)s…」"

# game/dialogs.rpy:196
translate japanese scene1_77296229:

    # s "「Nice to meet you, %(stringhero)s-san!"
    s "「はじめまして、%(stringhero)sさん！」"

# game/dialogs.rpy:197
translate japanese scene1_6ec72dd8:

    # hero "「Pleased to meet you as well, Sakura-san!"
    hero "「こちらこそはじめまして！咲良さん。」"

# game/dialogs.rpy:198
translate japanese scene1_548f585d:

    # "She gave me a sweet smile."
    "彼女は再び甘く微笑んだ。"

# game/dialogs.rpy:199
translate japanese scene1_4466f621:

    # "Gosh, if a smile could kill, I think I'd be dead right now..."
    "オオオ、もし笑顔が人を殺せるのだとしたら、僕は直ちに死んでいた事だろう…"

# game/dialogs.rpy:201
translate japanese scene1_1a8c3634:

    # "Deep down, I really hoped to be in the same class as her."
    "実際、僕は彼女と同じクラスになる事を切に願っている。"

# game/dialogs.rpy:202
translate japanese scene1_2fcc0bdf:

    # "I wanted to see her again and learn more about her... {image=heart.png}"
    "もう一度彼女を見たい。そしてもっとよく知りたい… {image=heart.png}"

# game/dialogs.rpy:210
translate japanese scene1_df51f81c:

    # "I can't believe it!"
    "信じられない！"

# game/dialogs.rpy:211
translate japanese scene1_06cbe8a5:

    # "I've been assigned to the second section!{p}I'm so happy!"
    "なんと僕が彼女と同じ第二部教室に割り当てられていたのだ！{p}感無量だ…"

# game/dialogs.rpy:212
translate japanese scene1_a8b3f3cd:

    # "However, I wonder if it's a good section."
    "しかし一方で、これからこの教室で上手くやって行けるのかという不安も有る。"

# game/dialogs.rpy:213
translate japanese scene1_3c0ab1de:

    # "I wouldn't like to be in a classroom full of jerks or something..."
    "ジャークや何やらで溢れ返る教室はちょっと勘弁だが…"

# game/dialogs.rpy:214
translate japanese scene1_9a96bdeb:

    # "If that's the case, I just hope I'll be near Sakura-san..."
    "まあ何にしても、僕は咲良さんの隣が良い…"

# game/dialogs.rpy:216
translate japanese scene1_1111d955:

    # "*{i}Bump{/i}*"
    "*{i}ポヨン{/i}*"

# game/dialogs.rpy:217
translate japanese scene1_89c4aa2c:

    # "While I was daydreaming again, I bumped into someone."
    "そんな事をぼけっと考えていると、何か弾力のある物にぶつかってしまった。"

# game/dialogs.rpy:218
translate japanese scene1_65939e34:

    # "It was a girl with pigtails."
    "それは水色の髪をした、これまた可愛い女の子だった。"

# game/dialogs.rpy:222
translate japanese scene1_7c921ed5:

    # r "「Hey, you! Watch where you're going!"
    r "「おう、あんさんどこ見て歩いてんねん！」"

# game/dialogs.rpy:223
translate japanese scene1_5359da65:

    # hero "「Sorry... It was my fault..."
    hero "「すいません…悪かったです…」"

# game/dialogs.rpy:225
translate japanese scene1_b17d24d0:

    # r "「Wait... You're new here, aren't ya?"
    r "「ちょい待ち…あんさん新顔やろ？な？」"

# game/dialogs.rpy:227
translate japanese scene1_39364aa2:

    # r "「Hmph, well whatever, I'll see you later."
    r "「まあ後でまた会うさかいな、ほな…」"

# game/dialogs.rpy:229
translate japanese scene1_b67db50c:

    # "She ran down the hall, probably heading to her classroom."
    "そう言って廊下を走り去ってしまった…恐らく教室に向かっているのだろうが…"

# game/dialogs.rpy:230
translate japanese scene1_e2ba9dde:

    # "That girl was kinda rude... But rather pretty-looking..."
    "何とも不行儀な奴だ…でも中々可愛かったな…"

# game/dialogs.rpy:231
translate japanese scene1_cd4a6e19:

    # "She reminds me of the main female character of the anime Domoco-chan because of her hair... And from what I could tell, her personality matched as well..."
    "あの子の髪型は、どこかアニメ「ドモコちゃん」のドモコちゃんを髣髴とさせる…あと性格も。"

# game/dialogs.rpy:234
translate japanese scene1_41041ae3:

    # "Here it is... Second section's classroom."
    "そしてここが…第二部教室だ。"

# game/dialogs.rpy:235
translate japanese scene1_201315b4:

    # "Some of students were already here."
    "生徒の何人かは既に席に着いていた。"

# game/dialogs.rpy:236
translate japanese scene1_8928782e:

    # "Sakura-san was right, there are students of all ages here."
    "そして咲良さんの言う様に、彼らの年齢には幅があった。"

# game/dialogs.rpy:237
translate japanese scene1_563881d0:

    # "The youngest looked around 8 years old and the oldest, about 18."
    "一番下はは8歳位、一番上は18歳位に見える。"

# game/dialogs.rpy:239
translate japanese scene1_75dbcf3d:

    # "I saw Sakura-san sitting by the window."
    "ふと、窓際に咲良さんが座っているのを見つけた。"

# game/dialogs.rpy:240
translate japanese scene1_3f9d91b2:

    # "She smiled as soon as saw me."
    "咲良さんも僕に気付いた時、笑いかけてくれた。"

# game/dialogs.rpy:242
translate japanese scene1_05ec4d50:

    # s "「Ah, %(stringhero)s-san! Over here! There's a desk available here!"
    s "「あ、%(stringhero)sさん、こっちですよ！空いてる席があるんです。」"

# game/dialogs.rpy:243
translate japanese scene1_a4ac0809:

    # "She pointed the empty desk behind her."
    "隣の空いている席を指差している。"

# game/dialogs.rpy:244
translate japanese scene1_c00ae698:

    # "How lucky! A desk near her!"
    "何たる幸運！本当に彼女の隣の机に座れるなんて！"

# game/dialogs.rpy:245
translate japanese scene1_95960ba8:

    # "This really is my lucky day!"
    "どうやら今日は本当にラッキーデーらしい…"

# game/dialogs.rpy:247
translate japanese scene1_c25aa128:

    # hero "「I'm so excited we're in the same section!"
    hero "「同じ教室で本当に嬉しいよ！」"

# game/dialogs.rpy:248
translate japanese scene1_7882f4c9:

    # s "「So am I!"
    s "「私もです！」"

# game/dialogs.rpy:249
translate japanese scene1_ef153560:

    # hero "「Our classmates really are made up of different ages!"
    hero "「Our classmates really are made up of different ages!」"

# game/dialogs.rpy:250
translate japanese scene1_198a38e2:

    # s "「Just like I told you!"
    s "「Just like I told you!」"

# game/dialogs.rpy:251
translate japanese scene1_44c33509:

    # hero "「Actually,{w} this whole town seems pretty special..."
    hero "「Actually,{w} this whole town seems pretty special...」"

# game/dialogs.rpy:252
translate japanese scene1_7a669bce:

    # hero "「I mean, the school system, the way the streets are, the kind-hearted townfolk..."
    hero "「I mean, the school system, the way the streets are, the kind-hearted townfolk...」"

# game/dialogs.rpy:253
translate japanese scene1_3b8a00b5:

    # s "「Do you like it?"
    s "「Do you like it?」"

# game/dialogs.rpy:254
translate japanese scene1_874f89c2:

    # hero "「So far...Yeah, I think I do like it."
    hero "「So far...Yeah, I think I do like it.」"

# game/dialogs.rpy:255
translate japanese scene1_d703df5c:

    # s "「I hope you'll enjoy your time here, %(stringhero)s-san!"
    s "「I hope you'll enjoy your time here, %(stringhero)s-san!」"

# game/dialogs.rpy:256
translate japanese scene1_0afeb16b:

    # hero "「Thank you, Sakura-senpai."
    hero "「Thank you, Sakura-senpai.」"

# game/dialogs.rpy:257
translate japanese scene1_034d31d6:

    # hero "「So, what are we learning in class today?"
    hero "「それで、今日は何をする予定なんですか？」"

# game/dialogs.rpy:258
translate japanese scene1_21c1af0a:

    # s "「Math."
    s "「数学ですよ。」"

# game/dialogs.rpy:259
translate japanese scene1_a9d114f6:

    # hero "「Oh geez..."
    hero "「あー…」"

# game/dialogs.rpy:260
translate japanese scene1_0940f6fc:

    # "Looks like my luck couldn't last all day..."
    "どうやら僕の幸運も一日は持たないらしい。"

# game/dialogs.rpy:261
translate japanese scene1_07506d68:

    # s "「You don't like math?"
    s "「数学が嫌いなんですか？」"

# game/dialogs.rpy:262
translate japanese scene1_280571a1:

    # hero "「Well, I'm not very good at it."
    hero "「まあ、どうも得意じゃなくって…」"

# game/dialogs.rpy:263
translate japanese scene1_21fba3a7:

    # s "「I'm pretty good with math. I can help you, if you want!"
    s "「私は大得意です。必要なら、私が教えますよ！」"

# game/dialogs.rpy:264
translate japanese scene1_0181a8b6:

    # hero "「That sounds perfect!"
    hero "「それは頼もしい！」"

# game/dialogs.rpy:265
translate japanese scene1_8b4aa3e9:

    # hero "「Thanks a lot, Sakura-senpai!"
    hero "「僕は感謝しきれません、咲良先輩！」"

# game/dialogs.rpy:266
translate japanese scene1_1e4821cf:

    # s "「Teehee! You're welcome!"
    s "「エヘヘ、こちらこそ！」"

# game/dialogs.rpy:268
translate japanese scene1_a8275802:

    # hero "「Well, since I stayed up all night studying, it shouldn't be that hard."
    hero "「Well, since I stayed up all night studying, it shouldn't be that hard.」"

# game/dialogs.rpy:269
translate japanese scene1_0df50adc:

    # s "「I doubt we'll start with something too difficult anyway. But I'll be here if you need me!"
    s "「I doubt we'll start with something too difficult anyway. But I'll be here if you need me!」"

# game/dialogs.rpy:273
translate japanese scene1_6220dd69:

    # "Class started without anything noteworthy happening."
    "授業は何とも無く始まった。"

# game/dialogs.rpy:274
translate japanese scene1_5181e117:

    # "Math ended up being pretty easy. It was planned for every level, probably because of all the different ages in class."
    "正直、数学は果てしなく簡単だった。多分教室の全年齢を対象にしてるからなんだろうけど。"

# game/dialogs.rpy:276
translate japanese scene1_a1cb44f3:

    # "Or it could be that all that studying really paid off..."
    "Or it could be that all that studying really paid off..."

# game/dialogs.rpy:277
translate japanese scene1_3523ed26:

    # "The lessons felt like they were going on for awhile though..."
    "授業はひたすら長く感じた…"

# game/dialogs.rpy:287
translate japanese scene3_2db6f6fb:

    # "The school bell rang to announce the end of classes."
    "The school bell rang to announce the end of classes."

# game/dialogs.rpy:288
translate japanese scene3_9a7755fc:

    # "I was surprised by the noise. It looked more like a bell for firemen. This is really not a urban school!"
    "I was surprised by the noise. It looked more like a bell for firemen. This is really not a urban school!"

# game/dialogs.rpy:289
translate japanese scene3_99288ad6:

    # "Sakura and I decided to take and eat our lunches together in the classroom, as well as some other classmates."
    "Sakura and I decided to take and eat our lunches together in the classroom, as well as some other classmates."

# game/dialogs.rpy:290
translate japanese scene3_d58c404f:

    # "During lunch, I asked Sakura-san if there were clubs here like in normal schools."
    "During lunch, I asked Sakura-san if there were clubs here like in normal schools."

# game/dialogs.rpy:292
translate japanese scene3_0e1303f7:

    # s "「Yes, for example, I'm in the manga club."
    s "「ええ、ありますよ。私は漫画部に入ってるんです。」"

# game/dialogs.rpy:293
translate japanese scene3_5d47cb20:

    # s "「I like it a lot, plus the leader of the club is a cosplayer."
    s "「良い所ですよー。コスプレイヤーの人が創ったんです。」"

# game/dialogs.rpy:294
translate japanese scene3_aae30260:

    # hero "「A manga club!?"
    hero "「漫画部！？」"

# game/dialogs.rpy:295
translate japanese scene3_04e97040:

    # hero "「Awesome! I love manga!"
    hero "「チョベリグです！僕も漫画大好き！」"

# game/dialogs.rpy:297
translate japanese scene3_6ec6ef6f:

    # s "「You do?{p}That's great!"
    s "「ほんと？{p}それは結構です！」"

# game/dialogs.rpy:298
translate japanese scene3_f9c203d9:

    # s "「You're more than welcome if you want to join us, %(stringhero)s-san!"
    s "「来てくれるならいつでも歓迎ですよ、%(stringhero)sさん！」"

# game/dialogs.rpy:299
translate japanese scene3_c7e8dac1:

    # hero "「That would be awesome!"
    hero "「オーサム！」"

# game/dialogs.rpy:300
translate japanese scene3_c0f4f625:

    # hero "「I just hope the club leader will let me join..."
    hero "「その創始者が僕の参加を認めてくれるように祈ります。」"

# game/dialogs.rpy:302
translate japanese scene3_fc29c8d9:

    # s "「I'm sure she will! We're always looking for new members, especially right now!"
    s "「彼女なら大丈夫ですよ！私たちはいつでも今でも、新しいメンバーを募集中ですから。」"

# game/dialogs.rpy:303
translate japanese scene3_3d9d0b13:

    # hero "「'She'?"
    hero "「『彼女』？」"

# game/dialogs.rpy:304
translate japanese scene3_ea9b5b89:

    # s "「Yup!"
    s "「ええ。」"

# game/dialogs.rpy:305
translate japanese scene3_cca5acb7:

    # s "「You know, a lot of girls enjoy anime and manga."
    s "「アニメ、漫画好きの女の子は結構居ますからね。」"

# game/dialogs.rpy:306
translate japanese scene3_10b77e4e:

    # hero "「Yeah, I know..."
    hero "「ええ、まあ確かに…」"

# game/dialogs.rpy:307
translate japanese scene3_65b45dcd:

    # "Actually, I have an older sister who loved shoujo anime and manga when we were younger."
    "そういえば、僕の姉も少女マンガ、アニメが好きだった。"

# game/dialogs.rpy:308
translate japanese scene3_b56e0491:

    # "But that was a long time ago. She's married now and is living in Tokyo with her husband."
    "しかしそれは姉がまだ若かった頃の話だ。今では結婚して夫と東京で住んでいる。"

# game/dialogs.rpy:309
translate japanese scene3_b784ebc5:

    # "I wonder how she is doing... I should e-mail her when I get back home."
    "今頃一体どうしているだろうか…帰ったらメールでも送ろうかな。"

# game/dialogs.rpy:310
translate japanese scene3_2494a3a5:

    # hero "「How many members are in the club?"
    hero "「部員は何人居るんですか？」"

# game/dialogs.rpy:312
translate japanese scene3_cb2e3f44:

    # s "「Actually, we only have three members right now..."
    s "「Actually, we only have three members right now...」"

# game/dialogs.rpy:313
translate japanese scene3_76a2a0d6:

    # hero "「Three? That's all?"
    hero "「Three? That's all?」"

# game/dialogs.rpy:314
translate japanese scene3_88a7dbf3:

    # s "「Yeah... Three girls."
    s "「Yeah... Three girls.」"

# game/dialogs.rpy:315
translate japanese scene3_1f17e8f8:

    # s "「It's rather hard to find new members, considering the only manga shop around is in the city..."
    s "「周りに漫画屋が一軒しか無い事を考えると、仕方ないのかも知れません。」"

# game/dialogs.rpy:316
translate japanese scene3_9f89b658:

    # s "「Usually, the people that join aren't too invested to get into new manga to read so they end up leaving the club."
    s "「入部した部員も新しい漫画探しに退屈して、部活を辞めてしまうんです。」"

# game/dialogs.rpy:317
translate japanese scene3_84b8bd32:

    # hero "「I see."
    hero "「成程。」"

# game/dialogs.rpy:318
translate japanese scene3_dd98eff8:

    # "Holy crap! Joining a manga club with only three girls!?"
    "なんてこったい！女の子三人だけの漫画部に入部！？"

# game/dialogs.rpy:319
translate japanese scene3_d931c57f:

    # "Seems like my luck is back! I feel like the hero of a harem anime or a visual novel!"
    "ツキが戻ってきたって事なのか… I feel like the hero of a harem anime or a visual novel!"

# game/dialogs.rpy:321
translate japanese scene3_454939b4:

    # s "「So, shall we go?"
    s "「そろそろ行きましょうか？」"

# game/dialogs.rpy:322
translate japanese scene3_0dab1b57:

    # hero "「Sure! You lead the way, Sakura-senpai!"
    hero "「はい！連れて行ってください、咲良先輩！」"

# game/dialogs.rpy:324
translate japanese scene3_452bb69b:

    # s "「Let's go!"
    s "「はい！」"

# game/dialogs.rpy:329
translate japanese scene3_afe022e1:

    # "I followed her through the halls. We climbed up the stairs and then entered a small classroom."
    "廊下を渡り、階段を昇って小さな教室に着いた。"

# game/dialogs.rpy:331
translate japanese scene3_31abf4ae:

    # "The room only had a few desks and they were all placed together to make a big table."
    "幾つか机がまとめて置かれている。"

# game/dialogs.rpy:332
translate japanese scene3_1b03234c:

    # "There were some manga posters on the walls and a bookshelf full of manga and artbooks."
    "棚にはいっぱいの漫画本やアートブック、壁には漫画のポスターが丁寧に張られている。"

# game/dialogs.rpy:333
translate japanese scene3_bfcd2b6d:

    # "There was also a small TV with a NATO GEO gaming console and a VCR plugged in it."
    "There was also a small TV with a NATO GEO gaming console and a VCR plugged in it."

# game/dialogs.rpy:335
translate japanese scene3_f22a2c8a:

    # "Two girls were already sitting at the table. One of them was completely focused on her handheld console."
    "Two girls were already sitting at the table. One of them was completely focused on her handheld console."

# game/dialogs.rpy:336
translate japanese scene3_85e454f4:

    # "I instantly recognized the other one. She was the same girl that I had bumped into this morning!"
    "I instantly recognized the other one. She was the same girl that I had bumped into this morning!"

# game/dialogs.rpy:340
translate japanese scene3_074e44c0:

    # r "「Sakura-chan! How are you?"
    r "「咲良ちゃん！もうかりまっか？」"

# game/dialogs.rpy:342
translate japanese scene3_bedfc3ef:

    # s "「Rika-chan! Nanami-chan!"
    s "「梨花ちゃん！七海ちゃん！」"

# game/dialogs.rpy:343
translate japanese scene3_8ac194aa:

    # s "「I found a new member for the club!"
    s "「新部員を連れてきました！」"

# game/dialogs.rpy:344
translate japanese scene3_a4e3e823:

    # hero "「Huh... Hello there?"
    hero "「えっと…こんにちは～」"

# game/dialogs.rpy:346
translate japanese scene3_7fdb9008:

    # r "「You!"
    r "「アンタ！」"

# game/dialogs.rpy:348
translate japanese scene3_0ca8552f:

    # s "「You know him already?"
    s "「お二人はお知り合いでしたか？」"

# game/dialogs.rpy:350
translate japanese scene3_771d8927:

    # r "「Yeah, he bumped into me in the hallway this morning!"
    r "「おう、コイツは今朝廊下でウチにぶつかってきおった奴やで！」"

# game/dialogs.rpy:351
translate japanese scene3_56462fb4:

    # hero "「Hey, I already told you that I'm sorry! I was daydreaming and I didn't see you!"
    hero "「その事はもう謝ったでしょ！ボケッとしてて見てなかったんです！」"

# game/dialogs.rpy:352
translate japanese scene3_4b30e319:

    # r "「Heh... I bet you were daydreaming about girls, weren't you!?"
    r "「ハッ。おなごの事考えてボケッとしとったんちゃうか？」"

# game/dialogs.rpy:353
translate japanese scene3_d003c469:

    # hero "「Yikes!"
    hero "「ギギギ」"

# game/dialogs.rpy:354
translate japanese scene3_67cc7e93:

    # "Well... she was kind of right..."
    "正解に近い。"

# game/dialogs.rpy:355
translate japanese scene3_7ee0bf30:

    # "I felt a little embarrassed."
    "困ったな…"

# game/dialogs.rpy:356
translate japanese scene3_b9df3e31:

    # "I noticed Nanami stopped playing and sneaked over silently to join us."
    "I noticed Nanami stopped playing and sneaked over silently to join us."

# game/dialogs.rpy:359
translate japanese scene3_2385f465:

    # s "「Don't pay attention to her sourness. All boys are perverts to Rika-chan, but it's not in a mean way!"
    s "「Don't pay attention to her sourness. All boys are perverts to Rika-chan, but it's not in a mean way!」"

# game/dialogs.rpy:360
translate japanese scene3_bdadc851:

    # hero "「I see. I just hope she doesn't kill me in my sleep."
    hero "「そう…寝てる間に彼女に刺し殺されない事を願うね。」"

# game/dialogs.rpy:361
translate japanese scene3_85dc831b:

    # r "「Hmph!"
    r "「フン！」"

# game/dialogs.rpy:363
translate japanese scene3_0160ed38:

    # s "「He likes anime and manga, plus he seems quite motivated. Don't you want to give him a chance?"
    s "「彼はアニメも漫画も大好きですし、やる気マンマンなんです。チャンスを与えてあげては？」"

# game/dialogs.rpy:364
translate japanese scene3_bfbf7878:

    # s "「We need more boys in this club too. Some people already think we're just a girls only club."
    s "「We need more boys in this club too. Some people already think we're just a girls only club.」"

# game/dialogs.rpy:368
translate japanese scene3_49d8fa00:

    # r "「Alright, fine, I accept. Welcome to the club, Mr...?"
    r "「まあええ、認めちゃるわ。漫画部にようこそやで、えーと…」"

# game/dialogs.rpy:369
translate japanese scene3_d2163b95:

    # hero "「%(stringhero)s. Nice to meet you."
    hero "「%(stringhero)sです。よろしく。」"

# game/dialogs.rpy:370
translate japanese scene3_da243e9b:

    # r "「Yeah, likewise."
    r "「ええ、よろしく。」"

# game/dialogs.rpy:371
translate japanese scene3_48579522:

    # "Rika spoke with a Kansai dialect. Just like most of the village's townfolk."
    "Rika spoke with a Kansai dialect. Just like most of the village's townfolk."

# game/dialogs.rpy:376
translate japanese scene3_d37e267a:

    # "I saw Nanami just looking at me curiously, so Sakura spoke."
    "I saw Nanami just looking at me curiously, so Sakura spoke."

# game/dialogs.rpy:377
translate japanese scene3_1901ff6f:

    # s "「So you already met Rika-chan. Here we have our little Nanami-chan! She entered the club just two weeks ago!"
    s "「So you already met Rika-chan. Here we have our little Nanami-chan! She entered the club just two weeks ago!」"

# game/dialogs.rpy:379
translate japanese scene3_35deb10d:

    # n "「Hey, I'm not little! I'm 16!"
    n "「Hey, I'm not little! I'm 16!」"

# game/dialogs.rpy:380
translate japanese scene3_a2b33e03:

    # "It's true, she looks a lot younger than she is. But she was still pretty cute looking."
    "It's true, she looks a lot younger than she is. But she was still pretty cute looking."

# game/dialogs.rpy:381
translate japanese scene3_15aa8440:

    # r "「She's small but strong! She doesn't brag much about it but she's good at video games. And I mean... {b}very{/b} good!"
    r "「She's small but strong! She doesn't brag much about it but she's good at video games. And I mean... {b}very{/b} good!」"

# game/dialogs.rpy:383
translate japanese scene3_28d5cd6d:

    # s "「She's the prefecture's champion in a lot of games! {i}Lead of Fighter '96, Pika Rocket Online,{/i}... Even {i}Tetranet Online{/i}!"
    s "「She's the prefecture's champion in a lot of games! {i}Lead of Fighter '96, Pika Rocket Online,{/i}... Even {i}Tetranet Online{/i}!」"

# game/dialogs.rpy:384
translate japanese scene3_a50ea391:

    # hero "「Cool!"
    hero "「Cool!」"

# game/dialogs.rpy:386
translate japanese scene3_a580dff0:

    # hero "「Wait wait wait...{p}Isn't your username... NAMINAMI?"
    hero "「Wait wait wait...{p}Isn't your username... NAMINAMI?」"

# game/dialogs.rpy:388
translate japanese scene3_582ff77a:

    # n "「Huh?! How did you know?"
    n "「Huh?! How did you know?」"

# game/dialogs.rpy:389
translate japanese scene3_8aa6fe25:

    # hero "「I think we played {i}Tetranet Online{/i} last night!"
    hero "「I think we played {i}Tetranet Online{/i} last night!」"

# game/dialogs.rpy:391
translate japanese scene3_88e0bb32:

    # n "「Wait, you're that %(stringhero)s-dude I had a hard time beating?"
    n "「Wait, you're that %(stringhero)s-dude I had a hard time beating?」"

# game/dialogs.rpy:392
translate japanese scene3_3052c7b9:

    # "{i}Dude{/i}?"
    "{i}Dude{/i}?"

# game/dialogs.rpy:393
translate japanese scene3_0fd6ee48:

    # hero "「Yeah that was me!"
    hero "「Yeah that was me!」"

# game/dialogs.rpy:394
translate japanese scene3_e6f7ec96:

    # hero "「What a small world..."
    hero "「What a small world...」"

# game/dialogs.rpy:395
translate japanese scene3_a43e5a9b:

    # n "「Let's meet up here after club activities. I challenge you to {i}Lead of Fighters '96{/i}!"
    n "「Let's meet up here after club activities. I challenge you to {i}Lead of Fighters '96{/i}!」"

# game/dialogs.rpy:396
translate japanese scene3_050d4a5f:

    # "Her eyes were sparkling, full of energy, like she was ready for a real challenge. It was pretty adorable."
    "Her eyes were sparkling, full of energy, like she was ready for a real challenge. It was pretty adorable."

# game/dialogs.rpy:397
translate japanese scene3_6376ce6f:

    # "I'm sure I'll get my ass kicked again..."
    "I'm sure I'll get my ass kicked again..."

# game/dialogs.rpy:398
translate japanese scene3_958d04dd:

    # "But whatever...She was really cute, so..."
    "But whatever...She was really cute, so..."

# game/dialogs.rpy:399
translate japanese scene3_65819df0:

    # hero "「Challenge accepted!"
    hero "「Challenge accepted!」"

# game/dialogs.rpy:403
translate japanese scene3_5de027f9:

    # n "「So you're the dude Rika-nee was talking about... Nice to meet you!"
    n "「So you're the dude Rika-nee was talking about... Nice to meet you!」"

# game/dialogs.rpy:404
translate japanese scene3_3052c7b9_1:

    # "{i}Dude{/i}?"
    "{i}Dude{/i}?"

# game/dialogs.rpy:405
translate japanese scene3_396612f8:

    # hero "「Nice to meet you too."
    hero "「Nice to meet you too.」"

# game/dialogs.rpy:406
translate japanese scene3_dd9f61de:

    # hero "「Hehe, sorry to say this, but you really don't look like a video game champion."
    hero "「Hehe, sorry to say this, but you really don't look like a video game champion.」"

# game/dialogs.rpy:407
translate japanese scene3_9458441a:

    # n "「One of these days, I'll show you how skilled I am!"
    n "「One of these days, I'll show you how skilled I am!」"

# game/dialogs.rpy:408
translate japanese scene3_54ab12a6:

    # "Her eyes were sparkling, full of energy. It was pretty adorable."
    "Her eyes were sparkling, full of energy. It was pretty adorable."

# game/dialogs.rpy:409
translate japanese scene3_40cb84ff:

    # hero "「Sure, you're on anytime!"
    hero "「Sure, you're on anytime!」"

# game/dialogs.rpy:411
translate japanese scene3_bddf279b:

    # "Then I focused my attention on Rika."
    "Then I focused my attention on Rika."

# game/dialogs.rpy:412
translate japanese scene3_fff0f085:

    # "Now that I've seen her up close, I noticed that her eyes were unusual."
    "ふむ、近くで見てみると…彼女の瞳は咲良とはまた違う。"

# game/dialogs.rpy:413
translate japanese scene3_3429571b:

    # "One eye was blue and another was green."
    "青と緑のオッドアイだ。"

# game/dialogs.rpy:414
translate japanese scene3_14f143f3:

    # "The guy whose allowed to dive into those eyes will be one lucky guy."
    "彼女の承認を得て入部できる男というのは多分ラッキーなのだろう。"

# game/dialogs.rpy:415
translate japanese scene3_540f6b1e:

    # "But given her personality, is there any guy who will even get that chance?"
    "でも本当に男嫌いだとしたら、男にチャンスを与える物なのかな。"

# game/dialogs.rpy:416
translate japanese scene3_1eaf8eb5:

    # hero "「If you don't mind me saying, Rika-san,...you look like Domoco-chan."
    hero "「言おうか迷ってたんだけど、君はドモコちゃんに似てるね。」"

# game/dialogs.rpy:418
translate japanese scene3_24649718:

    # "Rika-san smiled. She looked really pretty with one."
    "それを聞いて彼女は笑顔になった。あ、可愛い。"

# game/dialogs.rpy:419
translate japanese scene3_63ae1e58:

    # r "「Yeah! I love cosplaying as her, she's one of my favorite anime characters!"
    r "「せや！ウチはドモコちゃんのコスをしとるんやで。めっちゃ好きやねん！」"

# game/dialogs.rpy:420
translate japanese scene3_e1b3e1b9:

    # r "「You've seen the show?"
    r "「アンタもアニメ見たんか？」"

# game/dialogs.rpy:421
translate japanese scene3_dc1e9f8c:

    # hero "「I love it. It's a really funny anime!"
    hero "「あれ面白いですよね。僕も大好きですよ。」"

# game/dialogs.rpy:422
translate japanese scene3_7f21dac0:

    # s "「Rika-chan can do a really good imitation of Domoco-chan!"
    s "「梨花ちゃんはドモコちゃんのモノマネが本当に上手いんです！」"

# game/dialogs.rpy:423
translate japanese scene3_7020cc49:

    # r "「Watch this!{p}{i}Pipirupiru pépérato!{/i}{p}How is it?"
    r "「見てみ！{p}{i}ピピルピルペペラ戸～！{/i}{p}どや？」"

# game/dialogs.rpy:424
translate japanese scene3_30d87b21:

    # hero "「That was pretty great! It looked just like the real thing!"
    hero "「おお！まるで本物じゃないですか！」"

# game/dialogs.rpy:425
translate japanese scene3_87588078:

    # n "「Very true!"
    n "「まったくです！」"

# game/dialogs.rpy:426
translate japanese scene3_e0982078:

    # r "「I'm glad you liked it!"
    r "「気に入ってもろたようで何よりや。」"

# game/dialogs.rpy:429
translate japanese scene3_ed04f436:

    # r "「Now it's time for the question every member has to answer before they join the club!"
    r "「Now it's time for the question every member has to answer before they join the club!」"

# game/dialogs.rpy:430
translate japanese scene3_322a4cd3:

    # r "「Go for it, Sakura!"
    r "「Go for it, Sakura!」"

# game/dialogs.rpy:431
translate japanese scene3_5a20a3eb:

    # hero "「What's with that face? It's creeping me out..."
    hero "「What's with that face? It's creeping me out...」"

# game/dialogs.rpy:432
translate japanese scene3_041b4444:

    # s "「Don't worry, %(stringhero)s-san. It's not a hard question."
    s "「Don't worry, %(stringhero)s-san. It's not a hard question.」"

# game/dialogs.rpy:433
translate japanese scene3_9ae95b50:

    # s "「Here it is:{p}What is your all-time favorite manga?"
    s "「Here it is:{p}好きな漫画は何ですか？」"

# game/dialogs.rpy:434
translate japanese scene3_29f0a12c:

    # hero "「My all-time favorite one? Hm..."
    hero "「僕の一番好きな漫画…？うーん…」"

# game/dialogs.rpy:435
translate japanese scene3_2df7fab7:

    # "I started to think."
    "僕は考え始めた。"

# game/dialogs.rpy:436
translate japanese scene3_27a6f658:

    # "There are a few that I really like. I enjoy a lot of ecchi manga, but if I say that, the girls would look at me funny... Especially Rika-san."
    "幾つか好きな漫画はある。僕は所謂H漫画と言うのが好きなのだが、この場で言うとドン引きされかねん。特に梨花さんとか…"

# game/dialogs.rpy:437
translate japanese scene3_6275c604:

    # "But it's always good to be honest with girls as well..."
    "でも女の子には正直にした方が良いのかも…"

# game/dialogs.rpy:441
translate japanese scene3_152e1c76:

    # "Let's tell the truth..."
    "本当の事を言おう。"

# game/dialogs.rpy:442
translate japanese scene3_f2942a1c:

    # "After all, maybe they don't know about this one..."
    "この子達はこの漫画の事自体知らないかも知れないし…"

# game/dialogs.rpy:443
translate japanese scene3_923bf6bc:

    # hero "「I love {i}High School Samurai{/i}! That's my favorite one!"
    hero "「『ハイスクール・サムライ』が好きです。」"

# game/dialogs.rpy:448
translate japanese scene3_459d8d4d:

    # "They started to look funny, as I expected."
    "二人の様子がおかしい。"

# game/dialogs.rpy:449
translate japanese scene3_ea8e9dca:

    # "Rika-san looked pissed off again and Sakura-chan was blushing red."
    "梨花さんはなんかキレ気味で、咲良ちゃんは赤くなっている。"

# game/dialogs.rpy:450
translate japanese scene3_10363d18:

    # "Nanami was about to burst into tears of laughter."
    "Nanami was about to burst into tears of laughter."

# game/dialogs.rpy:451
translate japanese scene3_cebb2b3d:

    # "Aw crap, maybe I made the wrong choice..."
    "ヤウチ、どうやら選択肢を間違えたみたいだぞ…"

# game/dialogs.rpy:452
translate japanese scene3_d83f0622:

    # r "「You damn pervert! You're just like every other guy!"
    r "「ハ！アンタも所詮は男、ほんまもんのヘンタイやで！」"

# game/dialogs.rpy:453
translate japanese scene3_244ff0c7:

    # n "「Hahaha! You sure have guts, dude! Confessing something like that in front of Rika-nee!"
    n "「Hahaha! You sure have guts, dude! Confessing something like that in front of Rika-nee!」"

# game/dialogs.rpy:454
translate japanese scene3_e1554201:

    # s "「..."
    s "「……。」"

# game/dialogs.rpy:456
translate japanese scene3_e08d3442:

    # "I was hanging my head in shame, but suddenly Sakura-san spoke."
    "僕は惨めさに頭を抱えた。"

# game/dialogs.rpy:457
translate japanese scene3_76909f2d:

    # s "「Actually, Rika-chan..."
    s "「でもね、梨花ちゃん…」"

# game/dialogs.rpy:458
translate japanese scene3_4570740d:

    # s "「{i}High School Samurai{/i} isn't that bad at all."
    s "「ハイスクールサムライもそこまで悪い物じゃないんですよ。」"

# game/dialogs.rpy:459
translate japanese scene3_6bb1ac2f:

    # s "「You see, the hero isn't a typical boy like most harem manga. He's actually very heroic!"
    s "「ほら、主人公だって、普通のハーレム物と違ってもっとヒーローらしいじゃないですか。」"

# game/dialogs.rpy:460
translate japanese scene3_b2661f33:

    # s "「He has a great sense of honor and all the monologues he gives are very righteous and noble."
    s "「彼はちゃんとした羞恥心だって持ってますし、言ってる事も正しくそしてヒロイックです。」"

# game/dialogs.rpy:461
translate japanese scene3_8effa9e6:

    # s "「I admire heroes like that."
    s "「理想の主人公像と言ってもいい位です。」"

# game/dialogs.rpy:462
translate japanese scene3_f902c5fb:

    # s "「And by the way, the hero himself isn't really a pervert, since every ecchi scene where he's involved happens accidentally."
    s "「それに、主人公は別にヘンタイな訳でもありません。全部のHシーンだって彼が偶然巻き込まれただけなんです。」"

# game/dialogs.rpy:464
translate japanese scene3_0ddbdce1:

    # r "「Really?"
    r "「ほんまかいな…？」"

# game/dialogs.rpy:465
translate japanese scene3_68f8b283:

    # n "「I've seen some episodes. That guy reminds me of {i}Nanda no Ryu{/i}, who's always resolving conflicts with great punchlines and fighting when necessary."
    n "「I've seen some episodes. That guy reminds me of {i}Nanda no Ryu{/i}, who's always resolving conflicts with great punchlines and fighting when necessary.」"

# game/dialogs.rpy:466
translate japanese scene3_ced41922:

    # n "「Sure, it's ecchi sometimes, but it's actually a nice show to watch."
    n "「Sure, it's ecchi sometimes, but it's actually a nice show to watch.」"

# game/dialogs.rpy:467
translate japanese scene3_9816aa54:

    # r "「Well... I guess it's okay, then..."
    r "「まあ、ええにしちゃるわ。ほな…」"

# game/dialogs.rpy:471
translate japanese scene3_eca282df:

    # "Let's not take any risks..."
    "全てのリスクを排除しよう。"

# game/dialogs.rpy:472
translate japanese scene3_6ba925d2:

    # hero "「I love {i}Rosario Maiden{/i}. It's a great manga!"
    hero "「『ロザリオメイデン』が好きです。」"

# game/dialogs.rpy:477
translate japanese scene3_21a5901b:

    # "The three girls stared at me."
    "三人は僕をじっと見つめた。"

# game/dialogs.rpy:478
translate japanese scene3_7f0ae732:

    # "Finally, they all smiled. Rika-san spoke."
    "そして梨花さんが口を開いた。"

# game/dialogs.rpy:483
translate japanese scene3_79487764:

    # r "「I love that one too!"
    r "「ウチもそれ、好きやで。」"

# game/dialogs.rpy:484
translate japanese scene3_ca38b9e6:

    # r "「The art style is superb and the story is great. It's a pretty great series."
    r "「絵がええよなあ、話もおもろい。ええもんや。」"

# game/dialogs.rpy:485
translate japanese scene3_c24a4394:

    # s "「I like it too."
    s "「私も好きです。」"

# game/dialogs.rpy:486
translate japanese scene3_3d29180a:

    # s "「It's not my favorite but I like it."
    s "「一番ではありませんけど、結構好きです。」"

# game/dialogs.rpy:487
translate japanese scene3_1324b4b2:

    # s "「I especially love the dolls. I wish I could have one like that to take home!"
    s "「ドールが良いですよね…私も一つ家に欲しい…」"

# game/dialogs.rpy:488
translate japanese scene3_29de2f7c:

    # hero "「Yeah! They're really cute! Especially the green one!"
    hero "「可愛いですよね、特に緑の子！」"

# game/dialogs.rpy:489
translate japanese scene3_42f3f6f5:

    # s "「The green one is kinda mean but it's still my favorite one!"
    s "「私も緑の子が一番好きです。性格はちょっとアレですけど…」"

# game/dialogs.rpy:490
translate japanese scene3_bf59c037:

    # n "「I'm not too into the dolls. I prefer the protagonist. He's so handsome!"
    n "「I'm not too into the dolls. I prefer the protagonist. He's so handsome!」"

# game/dialogs.rpy:491
translate japanese scene3_f7e6d2b6:

    # r "「I prefer the red doll. She's just so... dominant... hehe! {image=heart.png}"
    r "「ウチは赤の子が好きやで。ごっつええで… {image=heart.png}」"

# game/dialogs.rpy:492
translate japanese scene3_40c59aa9:

    # "Rika-san gave me an evil look. What the heck could she be thinking? I'm confused and a bit scared."
    "梨花さんは僕にウインクした。意味が分からない。僕は混乱した。"

# game/dialogs.rpy:493
translate japanese scene3_745f17e5:

    # "But it looks like she's starting to like me."
    "でも、どうやら好印象を持たれたような気がする。"

# game/dialogs.rpy:494
translate japanese scene3_d79ab191:

    # "Well, at least I hope so..."
    "そうだと良いけど…"

# game/dialogs.rpy:502
translate japanese scene3_66851b4f:

    # "It was almost time to head home."
    "もうすっかり夕方になって、家に帰る時間となった。"

# game/dialogs.rpy:504
translate japanese scene3_a7a81522:

    # "Rika-chan and Sakura-chan left to head home. I ended up staying with Nanami-chan, ready to challenge her."
    "Rika-chan and Sakura-chan left to head home. I ended up staying with Nanami-chan, ready to challenge her."

# game/dialogs.rpy:509
translate japanese scene3_e5863c5e:

    # n "「So, are you ready, dude?"
    n "「So, are you ready, dude?」"

# game/dialogs.rpy:510
translate japanese scene3_30e27e13:

    # hero "「Of course I am. I was born ready!"
    hero "「Of course I am. I was born ready!」"

# game/dialogs.rpy:511
translate japanese scene3_fa127982:

    # n "「I meant, ready to get your butt kicked?"
    n "「I meant, ready to get your butt kicked?」"

# game/dialogs.rpy:512
translate japanese scene3_b87b3a25:

    # hero "「Only if you're ready to get yours kicked first!"
    hero "「Only if you're ready to get yours kicked first!」"

# game/dialogs.rpy:513
translate japanese scene3_7cf5de36:

    # "I pretty much know she's going to win the game. But I'm not gonna let win without a challenge. Although, she'll still call me a {i}dude{/i} afterwards."
    "I pretty much know she's going to win the game. But I'm not gonna let win without a challenge. Although, she'll still call me a {i}dude{/i} afterwards."

# game/dialogs.rpy:514
translate japanese scene3_c3b34a29:

    # "I gotta defend my pride, eh?"
    "I gotta defend my pride, eh?"

# game/dialogs.rpy:518
translate japanese scene3_d0713ddb:

    # "She turned on the TV and the NATO GEO after inserting Lead of Fighters '96 cartridge."
    "She turned on the TV and the NATO GEO after inserting Lead of Fighters '96 cartridge."

# game/dialogs.rpy:519
translate japanese scene3_3716d683:

    # hero "「It's pretty amazing the club has this gaming console here! That's one of the most expensive systems available!"
    hero "「It's pretty amazing the club has this gaming console here! That's one of the most expensive systems available!」"

# game/dialogs.rpy:520
translate japanese scene3_8ed428db:

    # n "「Actually, it's mine. I lent to the club to use."
    n "「Actually, it's mine. I lent to the club to use.」"

# game/dialogs.rpy:521
translate japanese scene3_8a31c669:

    # n "「Are you familiar with the {i}Lead of Fighters{/i} series?"
    n "「Are you familiar with the {i}Lead of Fighters{/i} series?」"

# game/dialogs.rpy:522
translate japanese scene3_165263c8:

    # hero "「Yeah, actually this game is my favorite one so far."
    hero "「Yeah, actually this game is my favorite one so far.」"

# game/dialogs.rpy:523
translate japanese scene3_5ff36a65:

    # hero "「It's my favorite game too, but I could only play it in the arcades in Tokyo."
    hero "「It's my favorite game too, but I could only play it in the arcades in Tokyo.」"

# game/dialogs.rpy:524
translate japanese scene3_facc5c5d:

    # n "「I see."
    n "「I see.」"

# game/dialogs.rpy:525
translate japanese scene3_9be30a14:

    # n "「So, who's your favorite character?"
    n "「So, who's your favorite character?」"

# game/dialogs.rpy:526
translate japanese scene3_913605a7:

    # n "「I usually like to pick the cute girls."
    n "「I usually like to pick the cute girls.」"

# game/dialogs.rpy:527
translate japanese scene3_3cba88d7:

    # n "「In most fighting games they rarely look threating, but when you know how to play with them, they can be deadly!"
    n "「In most fighting games they rarely look threating, but when you know how to play with them, they can be deadly!」"

# game/dialogs.rpy:528
translate japanese scene3_978b1237:

    # hero "「I'm not sure what my favorite is... I usually pick randomly."
    hero "「I'm not sure what my favorite is... I usually pick randomly.」"

# game/dialogs.rpy:529
translate japanese scene3_7e18c9db:

    # n "「If you're a beginner, you should take this guy, here. {w}He's good for be-{nw}"
    n "「If you're a beginner, you should take this guy, here. {w}He's good for be-{nw}」"

# game/dialogs.rpy:530
translate japanese scene3_0d60b236:

    # hero "「Hey I'm not a beginner! I was one of the best at my previous school!"
    hero "「Hey I'm not a beginner! I was one of the best at my previous school!」"

# game/dialogs.rpy:531
translate japanese scene3_72095650:

    # n "「Oh yeah? Let's see about that!"
    n "「Oh yeah? Let's see about that!」"

# game/dialogs.rpy:532
translate japanese scene3_704fb29f:

    # "So we selected our fighters and started the virtual brawl."
    "So we selected our fighters and started the virtual brawl."

# game/dialogs.rpy:533
translate japanese scene3_c8bd5427:

    # n "「So you're from Tokyo?"
    n "「So you're from Tokyo?」"

# game/dialogs.rpy:534
translate japanese scene3_6e3125fc:

    # hero "「Yeah. I moved here because my parents wanted to open a store in a quiet, peaceful village."
    hero "「Yeah. I moved here because my parents wanted to open a store in a quiet, peaceful village.」"

# game/dialogs.rpy:535
translate japanese scene3_352bd956:

    # n "「A store? You mean...the grocery store at the G. street?"
    n "「A store? You mean...the grocery store at the G. street?」"

# game/dialogs.rpy:536
translate japanese scene3_cf6c4656:

    # hero "「How do you know?"
    hero "「How do you know?」"

# game/dialogs.rpy:537
translate japanese scene3_3b26e73f:

    # n "「I... heard it was looking for new owners."
    n "「I... heard it was looking for new owners.」"

# game/dialogs.rpy:538
translate japanese scene3_9bf86260:

    # n "「My brother worked there for a while, back then."
    n "「My brother worked there for a while, back then.」"

# game/dialogs.rpy:540
translate japanese scene3_28e4c374:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"

# game/dialogs.rpy:541
translate japanese scene3_cd3c8571:

    # "Of course, she beat the crap out of me."
    "Of course, she beat the crap out of me."

# game/dialogs.rpy:542
translate japanese scene3_7944ba15:

    # "I'm not sure if I lost because of her skills or because we're talking at the same time.{p}It might be both..."
    "I'm not sure if I lost because of her skills or because we're talking at the same time.{p}It might be both..."

# game/dialogs.rpy:543
translate japanese scene3_66644a5e:

    # "We chose different fighters and started a new fight."
    "We chose different fighters and started a new fight."

# game/dialogs.rpy:544
translate japanese scene3_b7629213:

    # hero "「So you have a brother?"
    hero "「So you have a brother?」"

# game/dialogs.rpy:545
translate japanese scene3_c1b9564a:

    # n "「Yep, an older brother.{p}He's 27."
    n "「Yep, an older brother.{p}He's 27.」"

# game/dialogs.rpy:546
translate japanese scene3_6b9e255a:

    # hero "「Is he any good at games?"
    hero "「Is he any good at games?」"

# game/dialogs.rpy:547
translate japanese scene3_25066c43:

    # n "「Not really."
    n "「Not really.」"

# game/dialogs.rpy:548
translate japanese scene3_456c3557:

    # n "「I don't think it's from his generation."
    n "「I don't think it's from his generation.」"

# game/dialogs.rpy:549
translate japanese scene3_5f66a4a4:

    # hero "「That's possible. My sister is almost as old as him and it's not her thing either."
    hero "「That's possible. My sister is almost as old as him and it's not her thing either.」"

# game/dialogs.rpy:550
translate japanese scene3_a2e58feb:

    # n "「Oh yeah, your sister... What's she doing?"
    n "「Oh yeah, your sister... What's she doing?」"

# game/dialogs.rpy:551
translate japanese scene3_e1b6d146:

    # hero "「She works as a banker."
    hero "「She works as a banker.」"

# game/dialogs.rpy:552
translate japanese scene3_30b55ef7:

    # n "「That sounds boring."
    n "「That sounds boring.」"

# game/dialogs.rpy:553
translate japanese scene3_a5630de6:

    # hero "「I know right! I don't understand how she can do something like that without getting bored."
    hero "「I know right! I don't understand how she can do something like that without getting bored.」"

# game/dialogs.rpy:555
translate japanese scene3_28e4c374_1:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"

# game/dialogs.rpy:556
translate japanese scene3_821cdbe8:

    # "And yet again, I lost."
    "And yet again, I lost."

# game/dialogs.rpy:557
translate japanese scene3_b0f56878:

    # hero "「Alright. No more Mister Nice Guy, I'm gonna fight for real!"
    hero "「Alright. No more Mister Nice Guy, I'm gonna fight for real!」"

# game/dialogs.rpy:558
translate japanese scene3_7a9ca5b7:

    # n "「It's about time..."
    n "「It's about time...」"

# game/dialogs.rpy:559
translate japanese scene3_9f960747:

    # "Nanami gave me a mischievous grin.{p}That looked cute but kinda creepy too."
    "Nanami gave me a mischievous grin.{p}That looked cute but kinda creepy too."

# game/dialogs.rpy:560
translate japanese scene3_e6598e64:

    # "She probably knew I was already giving it my all..."
    "She probably knew I was already giving it my all..."

# game/dialogs.rpy:561
translate japanese scene3_ac835df6:

    # "We played through the next round without a single word."
    "We played through the next round without a single word."

# game/dialogs.rpy:562
translate japanese scene3_a3279e7e:

    # "I was finally was gaining an advantage! She stayed less and less calm as I was slowly winning."
    "I was finally was gaining an advantage! She stayed less and less calm as I was slowly winning."

# game/dialogs.rpy:563
translate japanese scene3_289633fa:

    # "And then finally, using a secret combo I knew well..."
    "And then finally, using a secret combo I knew well..."

# game/dialogs.rpy:565
translate japanese scene3_28e4c374_2:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"

# game/dialogs.rpy:566
translate japanese scene3_ed61300c:

    # "Nanami stared at me with a sad face and began to cry like a small child."
    "Nanami stared at me with a sad face and began to cry like a small child."

# game/dialogs.rpy:567
translate japanese scene3_15a85014:

    # "It didn't look real at all though. There were no tears or anything, she was just moaning like a baby."
    "It didn't look real at all though. There were no tears or anything, she was just moaning like a baby."

# game/dialogs.rpy:568
translate japanese scene3_decd73c2:

    # hero "「Hey hey, calm down! I'm sorry... I'll let you win next time!"
    hero "「Hey hey, calm down! I'm sorry... I'll let you win next time!」"

# game/dialogs.rpy:569
translate japanese scene3_ab688811:

    # "She instantly stopped crying and made her cute/creepy grin."
    "She instantly stopped crying and made her cute/creepy grin."

# game/dialogs.rpy:570
translate japanese scene3_c6891b22:

    # n "「Works everytime! {image=heart.png}"
    n "「Works everytime! {image=heart.png}」"

# game/dialogs.rpy:571
translate japanese scene3_56b514db:

    # hero "「Oh man..."
    hero "「Oh man...」"

# game/dialogs.rpy:572
translate japanese scene3_2d2950d5:

    # "I started laughing."
    "I started laughing."

# game/dialogs.rpy:573
translate japanese scene3_f2d7328e:

    # "Nanami made a goofy pouting face which made me laugh even more."
    "Nanami made a goofy pouting face which made me laugh even more."

# game/dialogs.rpy:574
translate japanese scene3_05ab44b2:

    # n "「Let's fight again!"
    n "「Let's fight again!」"

# game/dialogs.rpy:575
translate japanese scene3_241aaaf7:

    # hero "「Alright, alright, little one!"
    hero "「Alright, alright, little one!」"

# game/dialogs.rpy:576
translate japanese scene3_52c6a554:

    # n "「I'm not little!"
    n "「I'm not little!」"

# game/dialogs.rpy:581
translate japanese scene3_176ddd63:

    # "We played for a while, but eventually we both needed to get home."
    "We played for a while, but eventually we both needed to get home."

# game/dialogs.rpy:582
translate japanese scene3_ce50b0ab:

    # n "「That was fun! I hope we can do this again soon!"
    n "「That was fun! I hope we can do this again soon!」"

# game/dialogs.rpy:583
translate japanese scene3_24edf3d9:

    # hero "「Anytime, Nanami-chan."
    hero "「Anytime, Nanami-chan.」"

# game/dialogs.rpy:585
translate japanese scene3_7074277a:

    # n "「Oh by the way, here's my e-mail."
    n "「Oh by the way, here's my e-mail.」"

# game/dialogs.rpy:586
translate japanese scene3_cb953bdb:

    # "She gave me a tiny piece of paper with an e-mail written on it. There was a small heart where the @ sign was supposed to be."
    "She gave me a tiny piece of paper with an e-mail written on it. There was a small heart where the @ sign was supposed to be."

# game/dialogs.rpy:587
translate japanese scene3_a99fbe92:

    # "I blushed a bit."
    "I blushed a bit."

# game/dialogs.rpy:588
translate japanese scene3_9f566895:

    # n "「It's my personal e-mail... Just in case..."
    n "「It's my personal e-mail... Just in case...」"

# game/dialogs.rpy:589
translate japanese scene3_712996f5:

    # hero "「Thanks, Nanami-chan."
    hero "「Thanks, Nanami-chan.」"

# game/dialogs.rpy:592
translate japanese scene3_05df8eb4:

    # n "「See you tomorrow, %(stringhero)s-senpai! {image=heart.png}"
    n "「See you tomorrow, %(stringhero)s-senpai! {image=heart.png}」"

# game/dialogs.rpy:595
translate japanese scene3_759e6781:

    # "She left the school, hopping happily like a kid on the street."
    "She left the school, hopping happily like a kid on the street."

# game/dialogs.rpy:596
translate japanese scene3_37a9b710:

    # "Dang, for a gaming champion...she's really something."
    "Dang, for a gaming champion...she's really something."

# game/dialogs.rpy:599
translate japanese scene3_68c44298:

    # "The sun was starting to set."
    "The sun was starting to set."

# game/dialogs.rpy:600
translate japanese scene3_6f9822aa:

    # "Rika-chan and Nanami-chan take different routes to get home, so only Sakura-chan and I ended up walking home together."
    "Rika-chan and Nanami-chan take different routes to get home, so only Sakura-chan and I ended up walking home together."

# game/dialogs.rpy:606
translate japanese scene3_563d938f:

    # s "「So you like seinen manga?"
    s "「青年 漫画がお好きなんですか？」"

# game/dialogs.rpy:607
translate japanese scene3_04f08300:

    # hero "「Well yes..."
    hero "「ええ、まあ…」"

# game/dialogs.rpy:608
translate japanese scene3_14e7e4b7:

    # hero "「On the same subject, you said {i}Rosario Maiden{/i} wasn't your favorite. Which one is your favorite, then?"
    hero "「ロザリオメイデンが一番じゃないと言ってましたけど、何か他に好きな漫画でもあるんですか？」"

# game/dialogs.rpy:609
translate japanese scene3_41af6c01:

    # s "「In the seinen type? It's {i}Desu Motto{/i}."
    s "「青年漫画で一番好きなのは、『デスモット』ですね…」"

# game/dialogs.rpy:610
translate japanese scene3_a0282b9a:

    # hero "「You like {i}Desu Motto{/i}?"
    hero "「『デスモット！？』」"

# game/dialogs.rpy:611
translate japanese scene3_157e9fd5:

    # "I was kinda surprised."
    "驚いた。"

# game/dialogs.rpy:612
translate japanese scene3_3593807d:

    # "{i}Desu Motto{/i} is a story about a living book that eats human guts. It's full of all kinds of blood and gore, more so than in {i}Nanda no Ryu{/i}."
    "{i}Desu Motto{/i} is a story about a living book that eats human guts. It's full of all kinds of blood and gore, more so than in {i}Nanda no Ryu{/i}."

# game/dialogs.rpy:613
translate japanese scene3_976cd68f:

    # "And there's a magical girl who pursues this book with a chainsaw in order to destroy it."
    "And there's a magical girl who pursues this book with a chainsaw in order to destroy it."

# game/dialogs.rpy:614
translate japanese scene3_e7ee8e3e:

    # "Anyway, it's not the kind of thing girls her age would watch and like."
    "何にせよ彼女みたいな乙女はあまり好かなそうなジャンルだ。"

# game/dialogs.rpy:615
translate japanese scene3_2ae24650:

    # "Appearances can be surprising, sometimes..."
    "人は見かけに拠らないと言うが…"

# game/dialogs.rpy:616
translate japanese scene3_28bb09a7:

    # hero "「I like it too, but there's too much gore for me."
    hero "「僕も好きですよ。ちょっとグロテスク過ぎますけど…」"

# game/dialogs.rpy:617
translate japanese scene3_ed0c1175:

    # hero "「I can't stand too much blood in an anime. I remember being traumatized by {i}Elfic Leaves{/i}..."
    hero "「僕はアニメでもあんまり血がブシャーってしてると駄目なんです。昔見た「エルフェックリーブ」でトラウマになっちゃって…」"

# game/dialogs.rpy:618
translate japanese scene3_58b557f4:

    # "{i}Elfic Leaves{/i} is known to be the most violent and brutal anime in all of japanimation history. It's so gory, it was never broadcast on TV and it only exists on VHS tapes."
    "{i}Elfic Leaves{/i} is known to be the most violent and brutal anime in all of japanimation history. It's so gory, it was never broadcast on TV and it only exists on VHS tapes."

# game/dialogs.rpy:619
translate japanese scene3_64b818b7:

    # "I remember watching it by accident, about four years ago. It was on a VHS tape in my sister's room..."
    "I remember watching it by accident, about four years ago. It was on a VHS tape in my sister's room..."

# game/dialogs.rpy:621
translate japanese scene3_74bc980e:

    # s "「I can understand that. That one is really hardcore."
    s "「分かりますよ。アレはかなり過激ですよね。」"

# game/dialogs.rpy:622
translate japanese scene3_fe827394:

    # s "「Though I think it's okay, since they're only drawings. I don't think I could stand it either if it was real."
    s "「でも、ただの絵だと思えば別に何ともないものです。もしあれが現実だったとしたら私も立っていられないと思いますよ。」"

# game/dialogs.rpy:623
translate japanese scene3_189e996b:

    # hero "「Same for me."
    hero "「全くです…」"

# game/dialogs.rpy:625
translate japanese scene3_1f00d77e:

    # "Sakura stared at me for a moment."
    "Sakura deeply stared at me for a moment."

# game/dialogs.rpy:626
translate japanese scene3_24848926:

    # "It's like she was wondering about something... Or that she came a conclusion about something..."
    "It's like she was wondering about something... Or that she came a conclusion about something..."

# game/dialogs.rpy:627
translate japanese scene3_47159e79:

    # s "「...{w}{i}Rosario Maiden{/i} isn't actually favorite manga ever, right?"
    s "「...{w}{i}Rosario Maiden{/i} isn't actually favorite manga ever, right?」"

# game/dialogs.rpy:628
translate japanese scene3_ef3bce8f:

    # "Gah!!"
    "Gah!!"

# game/dialogs.rpy:629
translate japanese scene3_bb685cb0:

    # "How'd she make an incredible guess like that?!"
    "How'd she make an incredible guess like that?!"

# game/dialogs.rpy:630
translate japanese scene3_e633313f:

    # "I guess I couldn't hide it from her anymore..."
    "I guess I couldn't hide it from her anymore..."

# game/dialogs.rpy:631
translate japanese scene3_ae8949a7:

    # hero "「You got me..."
    hero "「You got me...」"

# game/dialogs.rpy:632
translate japanese scene3_8137fe91:

    # hero "「My favorite is {i}High School Samurai{/i}..."
    hero "「My favorite is {i}High School Samurai{/i}...」"

# game/dialogs.rpy:634
translate japanese scene3_5c82e516:

    # s "「Really?"
    s "「Really?」"

# game/dialogs.rpy:637
translate japanese scene3_d2de1953:

    # s "「So you like {i}High School Samurai{/i}?"
    s "「ハイスクールサムライがお好きなんですか？…フフ」"

# game/dialogs.rpy:638
translate japanese scene3_3d802b0c:

    # "I turned my head away from her a bit."
    "僕は俯いた。"

# game/dialogs.rpy:639
translate japanese scene3_6449a5e4:

    # hero "「Thanks again for saving me back there... Looks like Rika-san doesn't like that kind of manga and anime."
    hero "「さっきは助けていただいて…梨花さんはどうもあの種の漫画は好かないみたいでしたから。」"

# game/dialogs.rpy:641
translate japanese scene3_bd58aae1:

    # s "「It's okay. Rika-chan probably already forgot about it."
    s "「いいんですよ。梨花ちゃんもきっともう忘れてると思います。」"

# game/dialogs.rpy:642
translate japanese scene3_5798ec89:

    # s "「And to be honest...{p}I really enjoy {i}High School Samurai{/i}!"
    s "「それで、実を言うと…{p}私もハイスクールサムライ、好きなんです！」"

# game/dialogs.rpy:643
translate japanese scene3_b3ba8ed1:

    # hero "「You do?!"
    hero "「ほんとに？」"

# game/dialogs.rpy:644
translate japanese scene3_d207e06f:

    # s "「Yes!"
    s "「ほんとです。」"

# game/dialogs.rpy:647
translate japanese scene3_2098e30f:

    # s "「It's a great manga... The hero is really handsome."
    s "「良いですよね…主人公も男らしいですし…」"

# game/dialogs.rpy:648
translate japanese scene3_2f77db13:

    # s "「And... the girls are very cute, too..."
    s "「女の子達も本当に可愛いです…」"

# game/dialogs.rpy:649
translate japanese scene3_1ae45319:

    # hero "「Ah?"
    hero "「ええ。」"

# game/dialogs.rpy:650
translate japanese scene3_22eeaf4a:

    # s "「Yes, especially the main one. I wish I could look like her someday..."
    s "「特にヒロインが可愛いですよね。私もいつの日かあんな風に可愛くなれたら…」"

# game/dialogs.rpy:651
translate japanese scene3_1171cfe9:

    # hero "「You're already pretty cute though, Sakura-san."
    hero "「今でも同じくらい、可愛いですよ。」"

# game/dialogs.rpy:652
translate japanese scene3_aa49989e:

    # "I said that without thinking."
    "思わず口に出してしまった。"

# game/dialogs.rpy:653
translate japanese scene3_f463b1f2:

    # "I was on the verge of taking my words back until she replied."
    "ヤバイ…回収したい…今の台詞回収したい…"

# game/dialogs.rpy:655
translate japanese scene3_1c22c637:

    # s "「...Do you really think so?"
    s "「ほ…ほんとに…？」"

# game/dialogs.rpy:656
translate japanese scene3_b836a32c:

    # "Her face turned red out of embarrassment."
    "彼女は真っ赤になって身悶えしている。"

# game/dialogs.rpy:657
translate japanese scene3_87546ecc:

    # "So cute!"
    "ん…可愛い反応だ。"

# game/dialogs.rpy:658
translate japanese scene3_4baf7e22:

    # "It was so cute that I felt a little embarrassed too."
    "可愛すぎてこっちまでぎこちなくなってしった。"

# game/dialogs.rpy:659
translate japanese scene3_1c0b1c3e:

    # hero "「Well y-yeah... You're a pretty girl, Sakura-san..."
    hero "「えっと…は、はい…あなたは本当に可愛い女の子です、咲良さん…」"

# game/dialogs.rpy:660
translate japanese scene3_557b9c7c:

    # s "「. . ."
    s "「……。」"

# game/dialogs.rpy:661
translate japanese scene3_5d8f36c3:

    # s "「Thank you... %(stringhero)s-kun..."
    s "「…{p}あ、あ、ありがと…%(stringhero)sくん…」"

# game/dialogs.rpy:662
translate japanese scene3_2758fdb5:

    # "She hesitated for awhile before thanking me."
    "それを言うまで、少し間があった。"

# game/dialogs.rpy:663
translate japanese scene3_d3083c53:

    # "Was that her being timid? Probably."
    "照れているのかな？まあそうだろう。"

# game/dialogs.rpy:668
translate japanese scene3_88d97835:

    # "We finally reached the crossroads of our respective houses."
    "そして僕達は家の前の交差点に着いた。"

# game/dialogs.rpy:669
translate japanese scene3_95f36d4e:

    # hero "「Sakura-chan, since I don't know the path to school very well, do you mind if we go to school together every day?"
    hero "「その…まだ学校への道が良く分かってなくて…明日以降も一緒に登校して貰ってもいいですか…？」"

# game/dialogs.rpy:670
translate japanese scene3_c1d7d0f9:

    # s "「Sure! I'll wait for you here tomorrow."
    s "「ええ！明日の朝もここで待ってますね。」"

# game/dialogs.rpy:671
translate japanese scene3_139aa776:

    # hero "「Alright, thank you, Sakura-chan!"
    hero "「ありがとう、咲良ちゃん！」"

# game/dialogs.rpy:672
translate japanese scene3_3257668c:

    # s "「You're welcome..."
    s "「どういたしまして。」"

# game/dialogs.rpy:674
translate japanese scene3_00bab8bf:

    # s "「See you tomorrow, %(stringhero)s-kun!"
    s "「また明日、%(stringhero)sくん！」"

# game/dialogs.rpy:675
translate japanese scene3_6215ba95:

    # hero "「See you!"
    hero "「また明日！」"

# game/dialogs.rpy:678
translate japanese scene3_a46fbd6c:

    # "I watched her walk down the street."
    "彼女の去り姿を暫く見ていた。"

# game/dialogs.rpy:679
translate japanese scene3_5e5c6211:

    # "Her hips were swaying a little with each step, her hair moving with the summer wind."
    "歩くたびに左右に振れる小さな尻、そして夏の風に揺れる髪…"

# game/dialogs.rpy:681
translate japanese scene3_862f5cfa:

    # "The cicadas were crying, giving an atmosphere of something dreamy and peaceful with the hot summer weather."
    "蝉の鳴き声がこの暑い、平和的非現実的な夏の雰囲気を醸し出していた。"

# game/dialogs.rpy:684
translate japanese scene3_82417432:

    # "Before going to bed, I decided to get a breath of fresh air in front of the house."
    "Before going to bed, I decided to breath the air of the night in front of the house."

# game/dialogs.rpy:685
translate japanese scene3_be4997dc:

    # "I must admit, this place is beautiful."
    "I must admit, this place is beautiful."

# game/dialogs.rpy:686
translate japanese scene3_ba75a6ad:

    # "For sure, finding everything you want isn't as convenient as in Tokyo..."
    "For sure, finding everything you want isn't as convenient as in Tokyo..."

# game/dialogs.rpy:687
translate japanese scene3_d9ac4ff7:

    # "But gee,... It's so good to breath real air instead of smog..."
    "But gee,... It's so good to breath real air instead of smog..."

# game/dialogs.rpy:688
translate japanese scene3_8534ca42:

    # ". . ."
    "……。"

# game/dialogs.rpy:691
translate japanese scene3_f6793b5b:

    # "What's that?..."
    "What's that?..."

# game/dialogs.rpy:692
translate japanese scene3_41539cb7:

    # "Sounds like someone is playing the violin."
    "Sounds like someone is playing the violin."

# game/dialogs.rpy:693
translate japanese scene3_f3b91659:

    # "It's kinda appropriate for a night like this..."
    "It's kinda appropriate for a night like this..."

# game/dialogs.rpy:694
translate japanese scene3_8534ca42_1:

    # ". . ."
    "……。"

# game/dialogs.rpy:695
translate japanese scene3_f07946fa:

    # "I think I'm starting enjoying my new life here."
    "…新たな人生の幕開けといった感じだ。"

# game/dialogs.rpy:719
translate japanese scene4_840498c7:

    # centered "{size=+35}CHAPTER 2\nA fine date{fast}{/size}"
    centered "{size=+35}第2章\n良い日{fast}{/size}"

# game/dialogs.rpy:730
translate japanese scene4_46a27380:

    # write "Dear sister."
    write "姉さんへ"

# game/dialogs.rpy:732
translate japanese scene4_d033b41a:

    # write "How are things?{w} Mom, dad, and I have arrived at our new home.{w} Things here are way different than in Tokyo. It's peaceful, but there's nothing to do in the village.{w} You need to take the train to find something to do in the nearest town."
    write "調子はどう？{w}新しい家に着いたよ。{w}東京とは全く勝手が違うな。村はのどか過ぎてする事が無いよ。{w}何をするにも、隣街まで電車で行かなければいけないらしい。"

# game/dialogs.rpy:734
translate japanese scene4_dcc0ac1c:

    # write "I just came back from my first day at my new school. I've already made three friends!"
    write "丁度一日目の学校が終わった所だ。I've already made three friends!"

# game/dialogs.rpy:736
translate japanese scene4_9b62289a:

    # write "Things have changed a lot, but I think I'll get used to it soon enough."
    write "色々変わったけど、まあすぐに慣れるさ。"

# game/dialogs.rpy:737
translate japanese scene4_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:739
translate japanese scene4_66476017:

    # write "We're all part of a manga club at school.{w} Since they're all girls, can you recommend any shoujo manga that I can read and talk with them about?"
    write "僕はその友達の居る漫画部に入ったんだ。二人とも女の子なんだけど、何か一緒に語り合えるような少女マンガは無いかな？"

# game/dialogs.rpy:741
translate japanese scene4_c54efb3e:

    # write "I hope you'll visit us someday soon."
    write "姉さんもいつか一度こっちに来てみなよ。"

# game/dialogs.rpy:743
translate japanese scene4_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "%(stringhero)sより。"

# game/dialogs.rpy:744
translate japanese scene4_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:751
translate japanese scene4_0b847f9b:

    # "The week passed by quickly and it's already Friday."
    "The week passed by quickly and it's already Friday."

# game/dialogs.rpy:752
translate japanese scene4_a3a675c2:

    # "Spending time with the manga club is great."
    "女の子達と一緒に過ごしたこの一週間は素晴らしかった。"

# game/dialogs.rpy:753
translate japanese scene4_58d076ca:

    # "Sakura is a little shy, but very sweet.{p}Rika still thinks I'm a pervert..."
    "咲良ちゃんはいつも可愛く、そしてシャイだ。{p}リカちゃんはいつも私が変態だと思っていた..."

# game/dialogs.rpy:755
translate japanese scene4_8367dc19:

    # "Nanami is playful and competitive. Playing video games with her is always fun."
    "Nanami is playful and competitive. Playing video games with her is always fun."

# game/dialogs.rpy:757
translate japanese scene4_0cbaa7bb:

    # "Nanami and I even started to play games online a little bit!"
    "Nanami and I even started to play games online a little bit!"

# game/dialogs.rpy:758
translate japanese scene4_4995ec74:

    # "I know it sounds weird that the only friends I've made are three girls. But I already feel like I know I can count on them."
    "新しい村で出来た友達が二人だけってのは若しかしたらちょっと変なのかも知れない。{p}でも、僕は二人を信頼してる。"

# game/dialogs.rpy:759
translate japanese scene4_99d1ceb5:

    # "I think it's better to have a few close friends you can count on than a large group of friends that don't know each other very well."
    "あんまり仲が良くない友達100人より、少なくても頼りがいのある友達の方が良いと思わないか？"

# game/dialogs.rpy:760
translate japanese scene4_6dc4476d:

    # "I've learned a lot about manga, anime and the village itself thanks to them."
    "僕は二人に漫画やアニメの事、そして村の事について沢山教えてもらった。"

# game/dialogs.rpy:764
translate japanese scene4_bd1aa541:

    # "On my way to school, I was walking towards the crossroads where I usually join Sakura."
    "そして金曜日、僕はいつも咲良ちゃんと待ち合わせをしている交差点に向かっていた。"

# game/dialogs.rpy:766
translate japanese scene4_e8c9ca4f:

    # "As I got closer to her, I noticed that she wasn't alone."
    "咲良ちゃんが遠くに見えていたが暫くして、一人だけじゃない事に気が付いた。"

# game/dialogs.rpy:767
translate japanese scene4_5108d11f:

    # "When I got closer, I could see three rough looking guys around her."
    "もう少し近づくと、3人の不良めいた連中が彼女を取り囲んで居るのが見えた。"

# game/dialogs.rpy:768
translate japanese scene4_6898b2d9:

    # "It was like they were the typical bad boys you see in some manga or anime, with the same exact goofy hairstyles and everything."
    "髪型やらなんやら、いかにもアニメや漫画に居そうな典型的な不良スタイルの連中だった。"

# game/dialogs.rpy:769
translate japanese scene4_f2f59080:

    # "I vaguely knew who they were. They were from our school, but I remember Rika-chan telling me these guys never show up to school."
    "僕も、梨花ちゃんから薄っすらとは聞いた事がある。彼らは頻繁に学校をサボってる連中らしい…"

# game/dialogs.rpy:771
translate japanese scene4_b5bd2c54:

    # "Bad guy 1" "Go on! Say it!"
    "悪い奴 1" "「とっとと白状しやがれ！」"

# game/dialogs.rpy:772
translate japanese scene4_2c3b1b19:

    # "One of the guys shouted at Sakura. I instinctively hid myself behind a wall."
    "男が咲良ちゃんに向かって怒鳴っている。僕は本能のままに身を隠した。"

# game/dialogs.rpy:773
translate japanese scene4_b57e6ca3:

    # "Another guy who looked like their leader forcefully grabbed Sakura by her collar."
    "そのリーダー格の少年が力いっぱい咲良ちゃんの襟を掴み上げた。"

# game/dialogs.rpy:774
translate japanese scene4_bf711db5:

    # "Bad guy 1" "You're guy, we all know it! So say it! Tell us what you really are!"
    "悪い奴 1" "「この野郎、俺らは知ってんだ！とっとと目の前で白状しやがれ！」"

# game/dialogs.rpy:775
translate japanese scene4_b765fd13:

    # "Bad guy 2" "Yeah, stop playing the cute little girl!"
    "悪い奴 2" "「ええ、いたいけな少女を騙るのはもう止めてください。」"

# game/dialogs.rpy:776
translate japanese scene4_dc755d36:

    # "Bad guy 3" "I'm sure you're just a sick pervert that disguises himself into a girl to peek at other girls in the toilets, right!?"
    "悪い奴 3" "「テメェが女の子のフリして女子トイレから女子を覗いてんの、知ってんだぞオラァ！！{p}違いますか？」"

# game/dialogs.rpy:777
translate japanese scene4_7849a2e2:

    # "Bad guy 2" "Yeah, you're disgusting!"
    "悪い奴 2" "「全く、不愉快極まりないです。」"

# game/dialogs.rpy:778
translate japanese scene4_8006ab79:

    # "What the hell are they talking about?!"
    "何を言ってるんだ…？"

# game/dialogs.rpy:779
translate japanese scene4_e45090ba:

    # "Anyway, Sakura is trouble! I have to do something!"
    "何にせよ、咲良ちゃんがトラブルに巻き込まれたらしい。何とかしないと…！"

# game/dialogs.rpy:780
translate japanese scene4_af06121a:

    # "I'm not very good at fighting... I don't have the strength of a super hero..."
    "僕は喧嘩は得意じゃないんだ…スーパーヒーローみたいに強い訳じゃない…"

# game/dialogs.rpy:781
translate japanese scene4_77b9ebe0:

    # "But I don't care! Sakura-chan must be saved!"
    "でもそれでも構わない、咲良ちゃんを守らないと！"

# game/dialogs.rpy:782
translate japanese scene4_d3847689:

    # "Be strong like the {i}High School Samurai{/i}, %(stringhero)s!"
    "Be strong like the {i}High School Samurai{/i}, %(stringhero)s!"

# game/dialogs.rpy:783
translate japanese scene4_38f6654f:

    # "{size=+10}Chaaaaarge!!!!!{/size}"
    "{size=+10}Chaaaaarge!!!!!{/size}"

# game/dialogs.rpy:784
translate japanese scene4_e9b169e1:

    # "I ran between the group and started to shout at the guys."
    "僕はその集団まで走り、言い放った。"

# game/dialogs.rpy:785
translate japanese scene4_03cc4ea3:

    # hero "「Hey! Leave her alone!"
    hero "「彼女から離れろ！！！」"

# game/dialogs.rpy:786
translate japanese scene4_21ec0b43:

    # "Bad guy 2" "Huh? Who's that little brat?"
    "悪い奴 2" "「へ？何ですこの小さな肛門は。」"

# game/dialogs.rpy:787
translate japanese scene4_5a5a90ca:

    # "Bad guy 3" "Who are you? His boyfriend? You a faggot!?"
    "悪い奴 3" "「誰だテメェ！？コイツの彼氏かぁ！？お前ホモか？」"

# game/dialogs.rpy:789
translate japanese scene4_62845cb0:

    # "Bad guy 1" "Pfft! Whatever, let's go, guys. I had enough fun anyway."
    "悪い奴 1" "「けっ！まっことつまらんじゃき…もう行くぜよ。」"

# game/dialogs.rpy:790
translate japanese scene4_02769f02:

    # "B.g. 2 & 3" "What?"
    "悪い奴 2-3" "「へ？」"

# game/dialogs.rpy:791
translate japanese scene4_9d93e425:

    # "Bad guy 1" "I said let's go!!!"
    "悪い奴 1" "「行こうと言ったんじゃき。」"

# game/dialogs.rpy:792
translate japanese scene4_e19b48ff:

    # "Bad guy 1" "There's way more fun shit to do than kicking the asses of these faggots!"
    "悪い奴 1" "「このホモ共のケツ蹴っ飛ばすより面白いことが沢山あるぜよ。」"

# game/dialogs.rpy:793
translate japanese scene4_aadc99c4:

    # "I was ready for a fight but fortunately, they ended up walking away, arguing amongst themselves."
    "僕は既に戦闘態勢に移っていたが、どうやら彼らと一戦交える事は無さそうだ。"

# game/dialogs.rpy:794
translate japanese scene4_65210d1a:

    # "I doubt I scared them. Maybe they really thought that with me in the way, harassing Sakura wasn't fun anymore."
    "彼らが僕にビビッたとは思わない。もしかしたら僕の登場で咲良ちゃんを虐める事に楽しさを感じられなくなったのかも知れない。"

# game/dialogs.rpy:795
translate japanese scene4_5b2dcd53:

    # "Sakura collapsed on her knees and sat on the ground. She stayed motionless, looking down."
    "Sakura collapsed on her knees and sat on the ground. She stayed motionless, looking down."

# game/dialogs.rpy:797
translate japanese scene4_14ee51d1:

    # "I watched them walk away. The cicadas were crying."
    "僕は彼らの去る姿を眺めていた。蝉が鳴いている。"

# game/dialogs.rpy:799
translate japanese scene4_5714df67:

    # "I got on my knees to reach out to Sakura. She looked dead inside."
    "僕は膝を付いて咲良ちゃんに手を差し伸べた。かなりショックを受けた様子だった。"

# game/dialogs.rpy:800
translate japanese scene4_dcd2ce5e:

    # hero "「Sakura-chan, are you okay? Are you hurt?"
    hero "「咲良ちゃん、大丈夫？」"

# game/dialogs.rpy:801
translate japanese scene4_82675361:

    # "She didn't say a word. I took my bottle of water and gave it to her."
    "返事はなかった。僕は持っていた水筒を手渡した。"

# game/dialogs.rpy:802
translate japanese scene4_44a2c36a:

    # hero "「Here, drink. Try to calm down."
    hero "「飲んでください。もう大丈夫ですから…」"

# game/dialogs.rpy:803
translate japanese scene4_ecea0509:

    # s "「T... thank you..."
    s "「あ…ありがと…う」"

# game/dialogs.rpy:804
translate japanese scene4_0196f5ab:

    # "She drank from the bottle slowly. Tears were running down her face. She was definitely traumatized by what just happened."
    "彼女はゆっくりと飲み始めた。僕は彼女の大きくて青い瞳から涙が流れるのを見た。恐らく、今回の件がトラウマになってしまったのだろう。"

# game/dialogs.rpy:805
translate japanese scene4_e638db20:

    # "Or maybe it was just the refreshing feeling of drinking water, bringing her back to reality. I couldn't tell..."
    "Or maybe it was just the refreshing feeling of drinking water, bringing her back to reality. I couldn't tell..."

# game/dialogs.rpy:806
translate japanese scene4_76b6a439:

    # hero "「It's okay, they're gone... They won't bother you anymore..."
    hero "「大丈夫ですよ。彼らは行きました。もうあなたを虐める事はないでしょう。」"

# game/dialogs.rpy:807
translate japanese scene4_46f4b04d:

    # "I stayed with Sakura in the area for a while, waiting for her to recover."
    "僕は咲良ちゃんが立ち直るのを待って、もう暫くその場に留まった。"

# game/dialogs.rpy:808
translate japanese scene4_2bd9d9ea:

    # "Why did those guys bullied her like that?"
    "どうして連中はあんな風に咲良ちゃんを虐めたのだろう？"

# game/dialogs.rpy:809
translate japanese scene4_50b0bbf2:

    # "There's no way an innocent girl like her could be a guy in disguise! No way!"
    "こんな可愛い娘が男の子な訳が無い！悪い冗談だ。"

# game/dialogs.rpy:810
translate japanese scene4_4c3197ed:

    # "Maybe they picked on her because she has some tomboyish tastes or something...?"
    "多分咲良ちゃんがおてんばちゃんだからとかそんな理由じゃないかな…"

# game/dialogs.rpy:811
translate japanese scene4_a20e210b:

    # "Whatever, that was disgusting of them!"
    "何にせよ、理解できない行動だ！"

# game/dialogs.rpy:812
translate japanese scene4_3664d290:

    # "In fact, nothing they said made any sense... I still don't get it..."
    "理解に苦しむ。"

# game/dialogs.rpy:813
translate japanese scene4_846b18ad:

    # "Finally, after Sakura calmed down a bit, we got up and went to school."
    "やがて彼女も少し立ち直って、学校に向かった。"

# game/dialogs.rpy:817
translate japanese scene4_c8300016:

    # "We silently made our way to school together."
    "学校に着くまで、僕達は一言も喋らなかった。"

# game/dialogs.rpy:818
translate japanese scene4_291de2b0:

    # "I was worried. I think I'll need the help of Rika-chan."
    "僕は不安になった。あーこれは梨花ちゃんの助けが必要だ…"

# game/dialogs.rpy:825
translate japanese scene4_2eae0bd6:

    # hero "「...and that's what happened this morning."
    hero "「…と、かくかくしかじかな事があった訳です。」"

# game/dialogs.rpy:826
translate japanese scene4_4ddab44b:

    # "I didn't bother telling Rika what the bullies actually said to Sakura. Just all the rest."
    "I didn't bother telling Rika what the bullies actually said to Sakura. Just all the rest."

# game/dialogs.rpy:827
translate japanese scene4_6d41b106:

    # "Anyway, I'm pretty sure what they were saying doesn't even matter. They just made up a reason to bully her."
    "Anyway, I'm pretty sure what they were saying doesn't even matter. They just made up a reason to bully her."

# game/dialogs.rpy:828
translate japanese scene4_60f0cbde:

    # n "「Those bastards!"
    n "「あんの、ドグサレ共が！」"

# game/dialogs.rpy:829
translate japanese scene4_cf610c40:

    # r "「They were lucky that I wasn't there! I'm pretty good at trashing boys!"
    r "「アイツらはウチがそこに居らんで助かったで！ウチは男子をポイするのはホンマ得意やさかいなあ！」"

# game/dialogs.rpy:830
translate japanese scene4_ab36d9dc:

    # s "「It's okay, Rika-chan. They ran away when %(stringhero)s-kun came to rescue me."
    s "「いいんですよ、梨花ちゃん。%(stringhero)sくんが来たら彼らも逃げて行きました。」"

# game/dialogs.rpy:832
translate japanese scene4_9f03aed3:

    # s "「In fact, without his help, I don't know what would have happened..."
    s "「実際、助けてくれなかったら今頃どうなっていた事か…」"

# game/dialogs.rpy:833
translate japanese scene4_188ed7b1:

    # s "「Thank you... Thank you, %(stringhero)s-kun!"
    s "「ありがとう…本当にありがとう、%(stringhero)sくん！」"

# game/dialogs.rpy:834
translate japanese scene4_c1b029c3:

    # "Sakura bowed to me. I wasn't sure what to say."
    "咲良ちゃんは僕におじぎをした。何だか照れるな。"

# game/dialogs.rpy:838
translate japanese scene4_03382661:

    # r "「Well, I guess I have to thank you for helping Sakura-chan %(stringhero)s."
    r "「まあ、%(stringhero)sもウチの親友を助けてくれてありがとうな。」"

# game/dialogs.rpy:839
translate japanese scene4_77f35da3:

    # hero "「It's okay, it's okay. After all, it's my duty to save damsels in distress!"
    hero "「いいんです、いいんです。囚われの姫様を助けるのは僕の義務ですから。」"

# game/dialogs.rpy:840
translate japanese scene4_aadcce64:

    # "Sakura laughed and finally smiled."
    "咲良は笑った。"

# game/dialogs.rpy:841
translate japanese scene4_f25fd7bb:

    # hero "「Sakura-chan..."
    hero "「咲良ちゃん…」"

# game/dialogs.rpy:842
translate japanese scene4_75a10854:

    # hero "「I promise you right now."
    hero "「…僕は今、ここに宣言するよ。」"

# game/dialogs.rpy:843
translate japanese scene4_e7c0608a:

    # hero "「I'll dedicate myself to escorting you to and from school everyday!"
    hero "「これから毎日、登下校時は僕の身を挺して咲良ちゃんをエスコートするよ。」"

# game/dialogs.rpy:845
translate japanese scene4_6d4d7801:

    # s "「Really? You promise?"
    s "「…本当に？約束してくれますか？」"

# game/dialogs.rpy:847
translate japanese scene4_c89aabf6:

    # s "「Oh, thank you, %(stringhero)s-kun! {image=heart.png}"
    s "「ありがとう御座います、%(stringhero)sくん！ {image=heart.png}」"

# game/dialogs.rpy:849
translate japanese scene4_6bd67288:

    # r "「Hmph! Fine, I'll accept this for now...only because I live the exact opposite direction from her house!"
    r "「ふんっ！ウチは家の向きがちゃうからな…しゃあなく認めたってもええで！」"

# game/dialogs.rpy:850
translate japanese scene4_12240494:

    # r "「If I could, I would already be escorting her everyday!"
    r "「ほんまはウチが毎日エスコートしたりたい所やけどな！」"

# game/dialogs.rpy:852
translate japanese scene4_87cde803:

    # n "「Same for me..."
    n "「Same for me...」"

# game/dialogs.rpy:853
translate japanese scene4_641491a3:

    # r "「I just know I can count on you, %(stringhero)s."
    r "「I just know I can count on you, %(stringhero)s.」"

# game/dialogs.rpy:854
translate japanese scene4_b799ab6b:

    # "I smiled."
    "僕は笑った。"

# game/dialogs.rpy:855
translate japanese scene4_ea6ddde8:

    # "Rika really does seem like a tsundere but I found it pretty lovable."
    "梨花ちゃんは所謂ツンデレというやつなのかな。"

# game/dialogs.rpy:856
translate japanese scene4_4401cc13:

    # "I felt even better when I saw Sakura's bright smile."
    "咲良ちゃんの笑顔も見れて、僕も安心した。"

# game/dialogs.rpy:861
translate japanese scene4_77f4ec72:

    # r "「Anyways..."
    r "「ところでな、諸君。」"

# game/dialogs.rpy:862
translate japanese scene4_72233eeb:

    # r "「I have an important announcement for the members of the club!"
    r "「漫画部にめっちゃ重要なお知らせがあるっちゅうねん！」"

# game/dialogs.rpy:863
translate japanese scene4_40d86de1:

    # s "「Yes?"
    s "「？」"

# game/dialogs.rpy:864
translate japanese scene4_5ebd74f9:

    # hero "「Yes, ma'am!"
    hero "「イエス、マダム！」"

# game/dialogs.rpy:865
translate japanese scene4_2dd18f3e:

    # n "「What is it?"
    n "「What is it?」"

# game/dialogs.rpy:866
translate japanese scene4_8afe26e4:

    # r "「Two weeks from now, the summer festival will be happening in the village. And the club must meet together on this day and enjoy the festival!"
    r "「二週間後、村で夏祭りがあるんやさかい、部員皆で一緒に行こうっちゅう話や！」"

# game/dialogs.rpy:867
translate japanese scene4_b181bb98:

    # hero "「A summer festival? Really?"
    hero "「夏祭りが？本当に？」"

# game/dialogs.rpy:868
translate japanese scene4_7ea87b78:

    # "I've heard about japanese festivals like the Matsuri."
    "僕は祭りの様な日本のフェスティバルについて聞いたことがある。"

# game/dialogs.rpy:869
translate japanese scene4_0a708135:

    # "They're very popular in older villages. People do things like put on yukatas and visit food stands. There's also a lot games, traditional dances and even a fireworks display at the very end."
    "祭りは日本の古い村では広く行われていて、人々は着物を身に纏い、屋台に訪れる。遊びや慣習的な踊りもあり、最後は花火で締める。"

# game/dialogs.rpy:870
translate japanese scene4_2339b27a:

    # "I've only see them in anime. I've always wanted to go to a real one!"
    "アニメで何度か見た事しか無かったが、いつしか本当に行ってみたいと思っていたのだ。"

# game/dialogs.rpy:871
translate japanese scene4_97a399b0:

    # r "「You never went to any summer festival? You city rat!"
    r "「夏祭りに行ったことがあらへん？街ネズミやなほんま！」"

# game/dialogs.rpy:872
translate japanese scene4_e1074e1b:

    # hero "「Hey, shut up!"
    hero "「口を慎めよ…」"

# game/dialogs.rpy:873
translate japanese scene4_b091f4a6:

    # s "「You'll come to realize, the summer festival of this village is absolutely amazing."
    s "「夏祭りは本当に良い物ですよ。%(stringhero)sくんもきっと分かります！」"

# game/dialogs.rpy:874
translate japanese scene4_15e427d2:

    # s "「Sounds like it'll be a lot of fun!"
    s "「本当に楽しいですよ！」"

# game/dialogs.rpy:875
translate japanese scene4_0fcfce33:

    # hero "「And no need to take the train this time, right?"
    hero "「しかも電車に乗る必要が無い、ですよね？」"

# game/dialogs.rpy:876
translate japanese scene4_970c7b2d:

    # s "「True! *{i}giggles{/i}*"
    s "「ええ！」"

# game/dialogs.rpy:877
translate japanese scene4_6e7d7bc8:

    # r "「I hope you have your yukata, city rat!"
    r "「街ネズミのアンタも浴衣もっとるとええんやがな…」"

# game/dialogs.rpy:878
translate japanese scene4_7c767f63:

    # hero "「Hey! I do!"
    hero "「いや持ってる！持ってるっつの！」"

# game/dialogs.rpy:879
translate japanese scene4_2322fa79:

    # "Actually, I didn't. I just wanted her to quit nagging me."
    "持ってない。しかし都会のネズミと言われるのが割と尺に触ったのだ。"

# game/dialogs.rpy:886
translate japanese scene4_5805cafa:

    # "As I promised with Sakura-chan, I escorted her on the way home, after school."
    "約束した通り、僕は咲良ちゃんを家までエスコートした。"

# game/dialogs.rpy:887
translate japanese scene4_6713403c:

    # "Ah geez...It'll be a problem if I don't have a yukata before the summer festival. Rika would never let it go."
    "さて、梨花ちゃんには浴衣を持ってるとつい言い放ってしまったが…どうしたものか。"

# game/dialogs.rpy:888
translate japanese scene4_df7e7b2d:

    # "I have to find one as quickly as possible."
    "出来るだけ早く用意しなくては…"

# game/dialogs.rpy:889
translate japanese scene4_20088daa:

    # "I know wearing a yukata isn't mandatory for summer festivals, but I just want to show that tsundere that I'm not the city rat she thinks I am."
    "夏祭りは浴衣じゃなきゃ駄目だという訳じゃない事は分かってる{p}…けど、どうしてもあのツンデレガールを見返してやりたいのだ。"

# game/dialogs.rpy:890
translate japanese scene4_2100c01f:

    # hero "「Sakura-chan?"
    hero "「咲良ちゃん。」"

# game/dialogs.rpy:891
translate japanese scene4_40d86de1_1:

    # s "「Yes?"
    s "「はい？」"

# game/dialogs.rpy:892
translate japanese scene4_bf7e25cb:

    # hero "「Are you free tomorrow?"
    hero "「明日は暇ですか？」"

# game/dialogs.rpy:893
translate japanese scene4_9b65d1b6:

    # s "「Yes, why?"
    s "「ええ、暇ですけど…どうして…？」"

# game/dialogs.rpy:894
translate japanese scene4_b543397f:

    # hero "「Well..."
    hero "「その…」"

# game/dialogs.rpy:895
translate japanese scene4_d5ca9051:

    # hero "「I'm not familiar with the big city yet..."
    hero "「明日隣街まで行きたいんですけど…」"

# game/dialogs.rpy:896
translate japanese scene4_fb7dbaed:

    # hero "「And I'd like to go there tomorrow..."
    hero "「まだ行った事が無いからよく分からなくって…」"

# game/dialogs.rpy:897
translate japanese scene4_55bf8a2d:

    # hero "「So I was wondering if you would..."
    hero "「それで、もしよければその…」"

# game/dialogs.rpy:898
translate japanese scene4_4cf796da:

    # hero "「... come... with me...?"
    hero "「…一緒に…行ってくれ…ます…？」"

# game/dialogs.rpy:899
translate japanese scene4_0dc65489:

    # "Those last few words were hard to say. It's like I'm asking her out on a date!"
    "最後は少々ぎこちなくなってしまった。言ってて何だかデートに誘ってるかの様に思えてきたのだ。"

# game/dialogs.rpy:900
translate japanese scene4_02bf7f98:

    # "No... Wait... I'm totally asking her on a date right now!"
    "No... Wait... I'm totally asking her on a date right now!"

# game/dialogs.rpy:902
translate japanese scene4_01b0fe5a:

    # s "「Of course! I'd love to come with you to the city! {image=heart.png}"
    s "「…勿論！明日を楽しみにしてます！ {image=heart.png}」"

# game/dialogs.rpy:903
translate japanese scene4_c2837dd9:

    # hero "「Really? Thanks!"
    hero "「本当に？ありがとう御座います！」"

# game/dialogs.rpy:904
translate japanese scene4_12d152e0:

    # hero "「I couldn't think of a better guide than you!"
    hero "「咲良ちゃんが一緒に居てくれると心強いです！"

# game/dialogs.rpy:905
translate japanese scene4_4b122407:

    # "We giggled. She blushed."
    "笑い合った。彼女は照れた。"

# game/dialogs.rpy:906
translate japanese scene4_2e510646:

    # hero "「I'll see you at the train station at 9am."
    hero "「じゃあ、明日9時に駅で待ってます。」"

# game/dialogs.rpy:907
translate japanese scene4_4131ec23:

    # "Perfect! Just according to keikaku... I mean, the plan."
    "完璧！ ちょうど計画に従って。"

# game/dialogs.rpy:908
translate japanese scene4_2f4016d4:

    # "Not only will this tour in the city inform me of where all the important shops are, but I also can find a yukata!"
    "Not only will this tour in the city inform me of where all the important shops are, but I also can find a yukata!"

# game/dialogs.rpy:910
translate japanese scene4_de73d541:

    # "I think she just saved me again."
    "また彼女に助けてもらった気がした。"

# game/dialogs.rpy:912
translate japanese scene4_586d150a:

    # "I think she just saved me."
    "I think she just saved me."

# game/dialogs.rpy:913
translate japanese scene4_8713b332:

    # "I can't wait to go to the city with Sakura tomorrow!"
    "ああ、咲良ちゃんと隣街に行くのが待ち遠しい…"

# game/dialogs.rpy:922
translate japanese scene5_83f03a3f:

    # "The next morning."
    "翌朝。"

# game/dialogs.rpy:923
translate japanese scene5_432ea515:

    # "I was at the train station in the village. I'm not even sure that it was a station. It's so small and old."
    "僕は街の駅で待っていた。それは駅とは思えない程に古ぼけていた。"

# game/dialogs.rpy:924
translate japanese scene5_55ba7152:

    # "And so small... I didn't know there were such small train stations in the country..."
    "そして小さかった…こんなに小さい駅は今までに都会では見た事が無い…"

# game/dialogs.rpy:925
translate japanese scene5_ee63449f:

    # s "「%(stringhero)s-kuuuuuuuuuuuuun!!!"
    s "「%(stringhero)sくううううううううううううううううん！！！」"

# game/dialogs.rpy:926
translate japanese scene5_09ca9e90:

    # "Sakura arrived."
    "咲良ちゃんがやって来た。"

# game/dialogs.rpy:927
translate japanese scene5_f274dee3:

    # "She was wearing a small blue summer dress, a striped shirt, and cute laced shoes that didn't hide her tiny feet."
    "青い夏服、ストライプのシャツ、そして足を隠し切らない靴。"

# game/dialogs.rpy:928
translate japanese scene5_535385a4:

    # "I usually find school uniforms attractive, but I have to admit that the casual outfit she was wearing suits her so much."
    "制服も良い、しかし私服もまた、良い物だ。"

# game/dialogs.rpy:931
translate japanese scene5_7eeab041:

    # s "「Good morning, %(stringhero)s-kun!{p}Sorry for the wait!"
    s "「おはよう、%(stringhero)s！{p}おまたせ。」"

# game/dialogs.rpy:932
translate japanese scene5_a5707cad:

    # hero "「Good morning, Sakura-chan!"
    hero "「おはよう、咲良ちゃん！」"

# game/dialogs.rpy:934
translate japanese scene5_8736489d:

    # s "「Ready to visit the city?"
    s "「準備は出来ましたか？」"

# game/dialogs.rpy:935
translate japanese scene5_eef83bf6:

    # s "「It's not as big as Tokyo for sure, but there's a lot more there than the village!"
    s "「東京程大きく無い事は分かってますけど、この村よりは遥かに大きいですよ！」"

# game/dialogs.rpy:936
translate japanese scene5_fc3b562c:

    # hero "「I guess you'll be my tour guide, senpai!"
    hero "「準備万全です、先輩！」"

# game/dialogs.rpy:937
translate japanese scene5_c2497b21:

    # "I laughed a bit but I kinda realized something..."
    "僕は少し笑ったが、内心困っていた。"

# game/dialogs.rpy:938
translate japanese scene5_2326053d:

    # "If I go to the city to buy a yukata, she'll obviously notice it."
    "もし一緒に浴衣を買いに行ったら、彼女は僕の嘘に気付いてしまう。"

# game/dialogs.rpy:939
translate japanese scene5_1b0c84a6:

    # "But if I don't, Rika will torment me at the festival."
    "しかし、買わなかったら梨花ちゃんに祭りで笑われてしまう…"

# game/dialogs.rpy:943
translate japanese scene5_2100c01f:

    # hero "「Sakura-chan?"
    hero "「咲良ちゃん。」"

# game/dialogs.rpy:944
translate japanese scene5_40d86de1:

    # s "「Yes?"
    s "「はい？」"

# game/dialogs.rpy:952
translate japanese scene5_20b30901:

    # hero "「Yesterday...I lied. I don't have any yukata."
    hero "「実は昨日、嘘吐いたんです…浴衣なんて持ってなくて…」"

# game/dialogs.rpy:953
translate japanese scene5_adb520a7:

    # hero "「In fact, I was planning to buy one in the city while visiting it with you..."
    hero "「それで、今日一緒に買いに行ってくれたら嬉しいんですけど…」"

# game/dialogs.rpy:955
translate japanese scene5_584030bd:

    # s "「Is that so?"
    s "「えーそうなんですか？」"

# game/dialogs.rpy:957
translate japanese scene5_ef055bc2:

    # s "「It's okay! I'll help you to find one!"
    s "「フフ、いいですよ。手伝いましょう。」"

# game/dialogs.rpy:958
translate japanese scene5_404c741c:

    # hero "「Really? That's a relief! Thanks!"
    hero "「ホントに？ありがとう御座います！」"

# game/dialogs.rpy:959
translate japanese scene5_12d1e584:

    # hero "「I said I had one to avoid Rika-chan nagging me."
    hero "「梨花ちゃんをどうにか黙らせたかったんです…」"

# game/dialogs.rpy:960
translate japanese scene5_a7f1d53d:

    # s "「Rika-chan likes to give you a hard time, but I'm sure she means well!"
    s "「分かりますよ。同じ状況なら私もそう言うかも知れません。」"

# game/dialogs.rpy:961
translate japanese scene5_fb14ae9d:

    # s "「Our Rika-chan is very unique!"
    s "「梨花ちゃんは非常にユニークですからね！」"

# game/dialogs.rpy:962
translate japanese scene5_fe794b00:

    # "I sighed with relief and laughed with Sakura."
    "僕はホッとして、咲良ちゃんと笑い合った。"

# game/dialogs.rpy:963
translate japanese scene5_a45c2161:

    # "I'm glad she's so understanding!"
    "また彼女に助けてもらった気がする。"

# game/dialogs.rpy:967
translate japanese scene5_e1b13f3b:

    # "Hmm... no... It's bad if I tell her..."
    "いや…いかん…言うのはいかん…"

# game/dialogs.rpy:968
translate japanese scene5_7857c1c2:

    # "I'll just go out to have fun with her today. Once I see a yukata shop, I'll take note and I'll will come back tomorrow alone to buy it."
    "今日は一緒に行って、浴衣ショップを下見しよう。そして明日独りでもう一度行って買うことにしよう。"

# game/dialogs.rpy:969
translate japanese scene5_8ba6ee94:

    # "Yeah, that should be a good idea..."
    "ああ、いいアイデアだ…"

# game/dialogs.rpy:971
translate japanese scene5_1e69c51f:

    # s "「%(stringhero)s-kun, are you okay?"
    s "「%(stringhero)sくん、大丈夫？」"

# game/dialogs.rpy:972
translate japanese scene5_fcde3cfe:

    # hero "「Oh, yes, nevermind... I was daydreaming again..."
    hero "「ああ、はい…大丈夫です…少し白昼夢を見ていただけです…」"

# game/dialogs.rpy:979
translate japanese scene5_c85106ec:

    # "The train arrived and we boarded it towards the city."
    "電車が到着し、街に向かって出発した。"

# game/dialogs.rpy:980
translate japanese scene5_97c5ca7d:

    # "The city was three stations ahead. About 30 minutes of travel time. Sakura and I made small talk on the train."
    "街は三駅先だった。おおよそ30分間僕達は電車に座っていた。"

# game/dialogs.rpy:982
translate japanese scene5_66ca24f3:

    # "We finally arrived at the city."
    "そして、着いた。"

# game/dialogs.rpy:983
translate japanese scene5_0a3e36c2:

    # "It really is pretty big. Just as Sakura said."
    "咲良ちゃんの言っていた通り、街は大きかった。"

# game/dialogs.rpy:985
translate japanese scene5_0c293bd8:

    # s "「Here we are!"
    s "「着きました！」"

# game/dialogs.rpy:986
translate japanese scene5_5e4b4e2f:

    # s "「The shopping area isn't too far. Only two streets away."
    s "「ここから商店街までは遠くありませんよ。道を二つ行った所です。」"

# game/dialogs.rpy:987
translate japanese scene5_75ad6856:

    # hero "「Great! Let's get going, senpai!"
    hero "「さあ、行きましょうか。」"

# game/dialogs.rpy:988
translate japanese scene5_9e4c1aaf:

    # "Sakura smiled and we started walking through the busy streets."
    "咲良ちゃんは笑った。僕達は道を進んだ。"

# game/dialogs.rpy:992
translate japanese scene5_1b70d467:

    # "After some time, we finally reached a street full of a variety of different shops."
    "暫くして、沢山の店がある通りに出た。"

# game/dialogs.rpy:993
translate japanese scene5_454785ef:

    # "Clothing, multimedia, restaurants, arcades,..."
    "服屋、マルチメディア、レストラン、ゲームセンター…"

# game/dialogs.rpy:994
translate japanese scene5_a2720e0f:

    # "They had everything here!"
    "何でも揃っているといった具合だ。"

# game/dialogs.rpy:995
translate japanese scene5_4f036fa9:

    # "I hasn't been shopping since I moved from Tokyo. I was pretty excited."
    "僕は興奮した。東京以来、全くショッピングをする機会が無かったのだ！"

# game/dialogs.rpy:997
translate japanese scene5_0b0fbfd9:

    # "But first things first, I have to find a yukata shop."
    "しかし、まずは浴衣を探さねば…"

# game/dialogs.rpy:998
translate japanese scene5_69e0143d:

    # "So we started with the clothes shops."
    "僕達は服屋を探し始めた。"

# game/dialogs.rpy:1001
translate japanese scene5_8534ca42:

    # ". . ."
    "……。"

# game/dialogs.rpy:1002
translate japanese scene5_aceff1a2:

    # "Argh!!"
    "アア！"

# game/dialogs.rpy:1003
translate japanese scene5_249d4d18:

    # "There's absolutely no yukata under the price of ¥200,000!"
    "20万円以下の浴衣がどこにも無い！！"

# game/dialogs.rpy:1004
translate japanese scene5_6a82deb5:

    # "It's definitely too expensive for the budget I had."
    "僕の財布に負えない代物ばかりだぞ…"

# game/dialogs.rpy:1006
translate japanese scene5_3101ced6:

    # "As I was checking the prices, I noticed Sakura seemed to be thinking about something."
    "僕が値札を眺めている間、咲良ちゃんは何か考え込んでいた。"

# game/dialogs.rpy:1007
translate japanese scene5_01c5b3f7:

    # "She looked kinda sorry that I couldn't afford a yukata for myself."
    "僕が浴衣を買えない事に対して申し訳無さそうにしている様にも見えた。"

# game/dialogs.rpy:1008
translate japanese scene5_0ddddc7d:

    # hero "「Nah, forget it.{p}After all, it's not like my life depends on getting one."
    hero "「いや…忘れて下さい。それにこれ、多分僕には似合いませんしね…」"

# game/dialogs.rpy:1009
translate japanese scene5_a727d569:

    # hero "「Although...I just hope Rika forgot about it already. I should brace myself for anything..."
    hero "「…梨花ちゃんも若しかしたらもう忘れてるかも知れませんし。」"

# game/dialogs.rpy:1010
translate japanese scene5_41a74897:

    # hero "「Let's go somewhere else for now..."
    hero "「気を取り直して、次に行きましょうか。」"

# game/dialogs.rpy:1011
translate japanese scene5_5c76b703:

    # s "「Hmm... Okay..."
    s "「…ふーむ…分かりました。」"

# game/dialogs.rpy:1016
translate japanese scene5_041a4807:

    # s "「So, where do you want to go? Is there anything you want to see?"
    s "「じゃあ、何処から行きます？何か見たい物があるんですか？」"

# game/dialogs.rpy:1017
translate japanese scene5_72bc6d39:

    # hero "「How about checking out manga in the bookstores?"
    hero "「本屋で漫画を見るというのはどうでしょう？」"

# game/dialogs.rpy:1018
translate japanese scene5_b6d74bf5:

    # hero "「That could be useful for the club!"
    hero "「漫画部としての活動にもなりますしね！」"

# game/dialogs.rpy:1020
translate japanese scene5_fa9a9f16:

    # s "「Nice idea! Let's go!"
    s "「良いですね！行きましょう。」"

# game/dialogs.rpy:1024
translate japanese scene5_2cd8d6de:

    # "The manga bookstore was gigantic!"
    "その漫画本屋は巨大だった。"

# game/dialogs.rpy:1025
translate japanese scene5_3f7f2473:

    # "It was three floors full of manga as far as the eye could see!"
    "3階まで全て漫画で埋まっていたのだ。"

# game/dialogs.rpy:1026
translate japanese scene5_17b06ea6:

    # "I start by browsing through the shonen harem manga, since it's my favorite genre."
    "僕は少年ハーレム漫画を探した。好きだからだ。"

# game/dialogs.rpy:1027
translate japanese scene5_54cf672e:

    # "I found all the volumes of {i}High School Samurai{/i}, but I had them already."
    "ハイスクールサムライが全巻揃っているのも見つけたが、それは既に揃ってる。"

# game/dialogs.rpy:1028
translate japanese scene5_e98d6302:

    # "Then I saw a series I didn't anything know, called {i}Uchuu Tenshi Moechan{/i}. I found the manga cover to be quite attractive. As I was reaching out to grab a volume..."
    "そして、「宇宙天使モエちゃん」という本を見つけた。初めて見たけど絵が魅力的で、僕は手に取って見てみようとした。"

# game/dialogs.rpy:1030
translate japanese scene5_2c810467:

    # "My hand touched another which went towards the same book. We both retracted our hands back quickly."
    "…その時、同じ本を取ろうとしていた誰かの手と触れ合った。思わずお互い手を引っ込める。"

# game/dialogs.rpy:1031
translate japanese scene5_931b518d:

    # hero "「Oh, I'm so sorry..."
    hero "「あっ、すいません…」"

# game/dialogs.rpy:1032
translate japanese scene5_56a93df9:

    # s "「Sorry, my bad..."
    s "「いえ、こちらこそ…」"

# game/dialogs.rpy:1033
translate japanese scene5_8534ca42_1:

    # ". . ."
    "……。"

# game/dialogs.rpy:1034
translate japanese scene5_cc9a5909:

    # "!!!"
    "！！！"

# game/dialogs.rpy:1036
translate japanese scene5_85b2e7cc:

    # "Sakura!?"
    "咲良ちゃん！？"

# game/dialogs.rpy:1037
translate japanese scene5_a3335b11:

    # "I was a little caught off guard."
    "少し驚いた。"

# game/dialogs.rpy:1039
translate japanese scene5_960abe9a:

    # "Well only a little, since I knew that she liked {i}High School Samurai{/i} .{w} But I never imagined that she would like shonen harem manga that were aimed towards guys!"
    "が…そこまで驚いた訳ではなかった。以前から彼女がハイスクールサムライを好きだと知っていたからだ。{w}しかし、彼女がこの手の少年ハーレム漫画も好きだとは思っていなかった。"

# game/dialogs.rpy:1040
translate japanese scene5_adf46165:

    # hero "「Heh so...you must really like that genre huh, Sakura-chan?"
    hero "「へぇ、こういうのがお好きなんですか？咲良ちゃん？」"

# game/dialogs.rpy:1041
translate japanese scene5_cd229797:

    # s "「Well... y-yes..."
    s "「え、ええ…まあ…」"

# game/dialogs.rpy:1042
translate japanese scene5_0735783a:

    # "She was blushing... Looks like it was embarrassing for her to talk about it."
    "彼女は何か恥ずかしい事を口にしたかの様に赤くなった。"

# game/dialogs.rpy:1043
translate japanese scene5_40d8c136:

    # s "「I... I like these kinds of manga... The girls in them are very attractive..."
    s "「私…そういう本、好きなんです…出てくる女の子も可愛いですし…」"

# game/dialogs.rpy:1044
translate japanese scene5_cde483c5:

    # s "「I wish I was as beautiful as the women in these manga..."
    s "「私も、あんな風になれたら…」"

# game/dialogs.rpy:1045
translate japanese scene5_6ce4f6f3:

    # "That's kinda adorable."
    "何と愛らしい。"

# game/dialogs.rpy:1046
translate japanese scene5_b7cf4dac:

    # "Honestly though, she's far prettier than any girl I've seen in any shonen manga!"
    "どの少年漫画の女の子よりも可愛いよ！！"

# game/dialogs.rpy:1047
translate japanese scene5_7658f518:

    # "If I wasn't such a nervous wreck half the time, I'd say it again to her face..."
    "もし僕にこれを言う勇気があったら…"

# game/dialogs.rpy:1048
translate japanese scene5_f50ba84f:

    # "But I just couldn't... I lacked the courage to say it out loud."
    "でも、結局言う事は出来なかった…"

# game/dialogs.rpy:1049
translate japanese scene5_e3f223bb:

    # "My mind started to wander off about touching her hand by accident."
    "僕の指はまだ、彼女の手の柔らかな感覚を覚えている。"

# game/dialogs.rpy:1050
translate japanese scene5_25f48c38:

    # "It was so soft... It seemed almost surreal..."
    "柔らかく、非現実的なまでに柔らかかった…"

# game/dialogs.rpy:1051
translate japanese scene5_875908a3:

    # "I realized that I was daydreaming, and quickly regained my composure."
    "僕は意識を取り戻した。"

# game/dialogs.rpy:1052
translate japanese scene5_aa64fa57:

    # hero "「Oh...Uhh...So do you know what the story is about?"
    hero "「ああ、その…どういう話なんですか？」"

# game/dialogs.rpy:1054
translate japanese scene5_dc5fd421:

    # s "「Oh, it's a nice one."
    s "「あら、いい質問ですね。」"

# game/dialogs.rpy:1055
translate japanese scene5_9b8c5139:

    # s "「It's the story about a boy from Earth who is kidnapped by female aliens because they think he is a god that will save their planet."
    s "「地球人の少年が女の子エイリアンに連れ去られ、星を守る神として扱われるお話です。」"

# game/dialogs.rpy:1056
translate japanese scene5_85fb97ae:

    # s "「They all want to go out with him but he's very shy; plus he has to fight against an horrible and powerful entity..."
    s "「恥ずかしがり屋の少年が崇め奉られ、皆に誘われるんです。」"

# game/dialogs.rpy:1057
translate japanese scene5_f86e719b:

    # s "「But fortunately, the girls have super powers and he fights alongside the girls..."
    s "「あと、何かヤバい相手と闘わなければいけなかったりするんです。それでも女の子達の力を借りて、一緒に闘い始めるというそういうお話なんです。」"

# game/dialogs.rpy:1058
translate japanese scene5_ea859548:

    # s "「It's kinda funny and heroic at same time!"
    s "「面白いですし、素晴らしい作品です！」"

# game/dialogs.rpy:1059
translate japanese scene5_b293cdd7:

    # hero "「That sounds pretty entertaining!"
    hero "「良さげですね！」"

# game/dialogs.rpy:1060
translate japanese scene5_fa4d34a0:

    # hero "「Maybe I should start reading it..."
    hero "「今度読んでみたいと思います。」"

# game/dialogs.rpy:1062
translate japanese scene5_40a4c6ba:

    # s "「I can lend you the first few volumes at school if you want!"
    s "「良かったら、一巻貸しますよ。今度学校に持っていきます！」"

# game/dialogs.rpy:1063
translate japanese scene5_7fe42341:

    # hero "「That would be great! Thanks, Sakura-chan!"
    hero "「ありがとう、咲良ちゃん！」"

# game/dialogs.rpy:1067
translate japanese scene5_86f22ee4:

    # "After looking all around the store, we finally ended up leaving the shop."
    "そうして僕達は店を出た。"

# game/dialogs.rpy:1068
translate japanese scene5_b7801019:

    # "She bought the new volume of {i}Moechan{/i} while I got the first volume of {i}OGT{/i}, a seinen that I heard of a long time ago."
    "彼女はモエちゃんの新刊を、僕は前から買おうと思っていた青年漫画のOGT一巻を購入した。"

# game/dialogs.rpy:1071
translate japanese scene5_5ebb4f12:

    # s "「So, what else do you want to do?"
    s "「それで、次はどこに行きましょうか？」"

# game/dialogs.rpy:1072
translate japanese scene5_f7ff6212:

    # hero "「How about the arcade?"
    hero "「ゲームセンターとかどうですか？」"

# game/dialogs.rpy:1073
translate japanese scene5_d8b99f39:

    # hero "「I'm curious to see how good you are at shooting games."
    hero "「一体どんな風にシューティングゲームをするか、見てみたかったんです。」"

# game/dialogs.rpy:1075
translate japanese scene5_5b8b0c8c:

    # s "「Teehee! Alright, let's go! But I won't hold back!"
    s "「ヘヘ。いいですよ、行きましょうか！」"

# game/dialogs.rpy:1079
translate japanese scene5_fb2b36bb:

    # "We played a game of {i}Silent Crisis{/i} together."
    "その後、二人で「サイレントマウンテン」をプレイした。"

# game/dialogs.rpy:1080
translate japanese scene5_e30e02a0:

    # "She was definitely a pro at it!"
    "彼女は、確かに上手かった。"

# game/dialogs.rpy:1081
translate japanese scene5_6f318a21:

    # "She knew exactly where the zombies spawned and shot them before they could even attack!"
    "まるでゾンビが出てくる場所を知っているかの様で、攻撃する暇も与えず撃ち殺していく。"

# game/dialogs.rpy:1082
translate japanese scene5_1c41f171:

    # "I ended up using all my credits since I died over and over, while she didn't lose a single life at all!"
    "彼女が一点のライフも失わない内に、僕は全てのクレジットを消費してしまった。"

# game/dialogs.rpy:1083
translate japanese scene5_ac2581f9:

    # hero "「Wow... Sakura-chan, you're definitely better than I am..."
    hero "「僕よりも全然…いや、比べ物にならない程に上手ですね…」"

# game/dialogs.rpy:1084
translate japanese scene5_509bc659:

    # hero "「You know the game by heart or something?"
    hero "「ゲームの内容全部覚えてるんですか？」"

# game/dialogs.rpy:1085
translate japanese scene5_7ab00054:

    # "She was concentrated on the game."
    "凄く集中している…"

# game/dialogs.rpy:1086
translate japanese scene5_924b98a0:

    # s "「Not really... In fact, it's only the third time I played this one."
    s "「いえ、このゲームをプレイするのもまだ3度目ですし…」"

# game/dialogs.rpy:1087
translate japanese scene5_8c2adcf7:

    # s "「I think it's my reflexes that make me good at these types of games."
    s "「反射神経が良いのかも…」"

# game/dialogs.rpy:1089
translate japanese scene5_7846d5b4:

    # "？？？" "Hey guys! You're here too?"
    "？？？" "「Hey guys! You're here too?」"

# game/dialogs.rpy:1095
translate japanese scene5_43c67a2f:

    # hero "「Oh! Nanami-chan!"
    hero "「Oh! Nanami-chan!」"

# game/dialogs.rpy:1096
translate japanese scene5_640b15c3:

    # s "「Hey Nana-chan!"
    s "「Hey Nana-chan!」"

# game/dialogs.rpy:1097
translate japanese scene5_bccc3e1d:

    # hero "「What are you doing here?"
    hero "「What are you doing here?」"

# game/dialogs.rpy:1099
translate japanese scene5_dbdeb2ab:

    # n "「Duh, playing arcade games, you dork!"
    n "「Duh, playing arcade games, you dork!」"

# game/dialogs.rpy:1100
translate japanese scene5_86073654:

    # hero "「Eh? I'm a dork now?"
    hero "「Eh? I'm a dork now?」"

# game/dialogs.rpy:1101
translate japanese scene5_a555e2b6:

    # n "「Yes, you're a dork dude! {image=heart.png}"
    n "「Yes, you're a dork dude! {image=heart.png}」"

# game/dialogs.rpy:1102
translate japanese scene5_040d24b0:

    # "Sakura laughed so much at the lame joke, she almost took a hit in the game."
    "Sakura laughed so much at the lame joke, she almost took a hit in the game."

# game/dialogs.rpy:1103
translate japanese scene5_013529c0:

    # "Still, it's incredible how she can do multiple things at the same time. I heard it's an ability that girls naturally have.{p}I kinda envy them."
    "Still, it's incredible how she can do multiple things at the same time. I heard it's an ability that girls naturally have.{p}I kinda envy them."

# game/dialogs.rpy:1105
translate japanese scene5_38444d54:

    # "Nanami put a token in the arcade machine and pressed the button labeled 'Player 2'."
    "Nanami put a token in the arcade machine and pressed the button labeled 'Player 2'."

# game/dialogs.rpy:1107
translate japanese scene5_a015009c:

    # n "「Looks like you could use some back up!"
    n "「Looks like you could use some back up!」"

# game/dialogs.rpy:1108
translate japanese scene5_4bf0d20d:

    # "Then Nanami started to shoot at the virtual mobs in the game with Sakura."
    "Then Nanami started to shoot at the virtual mobs in the game with Sakura."

# game/dialogs.rpy:1110
translate japanese scene5_2bae2874:

    # n "「Hey, %(stringhero)s-senpai, do you know that some video game experts are saying that the golden age of arcade games is coming to an end?"
    n "「Hey, %(stringhero)s-senpai, do you know that some video game experts are saying that the golden age of arcade games is coming to an end?」"

# game/dialogs.rpy:1111
translate japanese scene5_e7066814:

    # hero "「Really?"
    hero "「Really?」"

# game/dialogs.rpy:1112
translate japanese scene5_b3e58373:

    # hero "「But arcade games seem pretty recent, though..."
    hero "「But arcade games seem pretty recent, though...」"

# game/dialogs.rpy:1113
translate japanese scene5_2f385a6e:

    # n "「It's because of the Internet and online gaming. It's taking more and more room in the gaming domain."
    n "「It's because of the Internet and online gaming. It's taking more and more room in the gaming domain.」"

# game/dialogs.rpy:1114
translate japanese scene5_c0e7820a:

    # n "「Soon there won't be as many arcades as there are now. Some are even afraid that arcade games will just end up being boring and flat compared to what you can find online."
    n "「Soon there won't be as many arcades as there are now. Some are even afraid that arcade games will just end up being boring and flat compared to what you can find online.」"

# game/dialogs.rpy:1115
translate japanese scene5_34b00fac:

    # n "「At least rhythm games that need specific peripherals like {i}Para Para Revolution{/i} will be alright!"
    n "「At least rhythm games that need specific peripherals like {i}Para Para Revolution{/i} will be alright!」"

# game/dialogs.rpy:1116
translate japanese scene5_5b2c12b3:

    # s "「Oooh I love that game!"
    s "「Oooh I love that game!」"

# game/dialogs.rpy:1117
translate japanese scene5_b5fe983b:

    # hero "「Ah..."
    hero "「Ah...」"

# game/dialogs.rpy:1118
translate japanese scene5_7c35731c:

    # hero "「Well, what about making arcade games that can be played online?"
    hero "「Well, what about making arcade games that can be played online?」"

# game/dialogs.rpy:1119
translate japanese scene5_b075ad7f:

    # n "「Hmm..."
    n "「Hmm...」"

# game/dialogs.rpy:1120
translate japanese scene5_0817fef1:

    # n "「Hey, maybe you're getting at something, senpai!"
    n "「Hey, maybe you're getting at something, senpai!」"

# game/dialogs.rpy:1121
translate japanese scene5_88898146:

    # s "「I'm not sure that would work for rhythm games..."
    s "「I'm not sure that would work for rhythm games...」"

# game/dialogs.rpy:1122
translate japanese scene5_25648fa4:

    # "While Nanami was talking about the history of arcade games, she was still doing an amazing job shooting at monsters on the screen."
    "While Nanami was talking about the history of arcade games, she was still doing an amazing job shooting at monsters on the screen."

# game/dialogs.rpy:1127
translate japanese scene5_70915e9e:

    # "After a while, Sakura finally lost her last life. Even Nanami was struggling to stay alive."
    "After a while, Sakura finally lost her last life. Even Nanami was struggling to stay alive."

# game/dialogs.rpy:1128
translate japanese scene5_1c665d0e:

    # "Nanami was pretty good, but not as good as when she's playing fighting or puzzle games..."
    "Nanami was pretty good, but not as good as when she's playing fighting or puzzle games..."

# game/dialogs.rpy:1129
translate japanese scene5_c09efcd8:

    # hero "「Looks like we found a game she's not a master of!"
    hero "「Looks like we found a game she's not a master of!」"

# game/dialogs.rpy:1130
translate japanese scene5_b4b28eb9:

    # s "「Well, nobody's perfect, right?"
    s "「Well, nobody's perfect, right?」"

# game/dialogs.rpy:1133
translate japanese scene5_ca00aade:

    # s "「You know,..."
    s "「You know,...」"

# game/dialogs.rpy:1134
translate japanese scene5_29a8910b:

    # s "「Nana-chan...{w}she... She is more fragile than she looks..."
    s "「Nana-chan...{w}she... She is more fragile than she looks...」"

# game/dialogs.rpy:1135
translate japanese scene5_6d59e0e0:

    # hero "「What do you mean?"
    hero "「What do you mean?」"

# game/dialogs.rpy:1136
translate japanese scene5_af6cfb46:

    # s "「Well, something happened to her that makes her pretty introverted and nervous around people she doesn't know..."
    s "「Well, something happened to her that makes her pretty introverted and nervous around people she doesn't know...」"

# game/dialogs.rpy:1137
translate japanese scene5_eb73848e:

    # hero "「What happened?"
    hero "「What happened?」"

# game/dialogs.rpy:1139
translate japanese scene5_557b9c7c:

    # s "「. . ."
    s "「. . .」"

# game/dialogs.rpy:1140
translate japanese scene5_b475ba93:

    # s "「I shouldn't tell you more, I'm sorry for bringing it up. I prefer she tells you herself..."
    s "「I shouldn't tell you more, I'm sorry for bringing it up. I prefer she tells you herself...」"

# game/dialogs.rpy:1143
translate japanese scene5_8811f347:

    # s "「But on a more positive note,. {p}it's the first time I've seen her speak so much with someone she's only known for a few days."
    s "「But on a more positive note,. {p}it's the first time I've seen her speak so much with someone she's only known for a few days.」"

# game/dialogs.rpy:1144
translate japanese scene5_f97de379:

    # s "「That's a pretty good sign for you!"
    s "「That's a pretty good sign for you!」"

# game/dialogs.rpy:1145
translate japanese scene5_291c83e9:

    # s "「Maybe you'll be one of her best friends!"
    s "「Maybe you'll be one of her best friends!」"

# game/dialogs.rpy:1146
translate japanese scene5_723e1b93:

    # hero "「Does she has alot of best friends?"
    hero "「Does she has alot of best friends?」"

# game/dialogs.rpy:1148
translate japanese scene5_94da3bfe:

    # s "「So far there's only Rika and me."
    s "「So far there's only Rika and me.」"

# game/dialogs.rpy:1150
translate japanese scene5_9a43a0d7:

    # s "「I'm sure she'll tell you herself someday, if she trusts you enough."
    s "「I'm sure she'll tell you herself someday, if she trusts you enough.」"

# game/dialogs.rpy:1151
translate japanese scene5_6a223ad6:

    # s "「Anyways, don't mind her if she doesn't speak too much. Befriending her takes time."
    s "「Anyways, don't mind her if she doesn't speak too much. Befriending her takes time.」"

# game/dialogs.rpy:1152
translate japanese scene5_b6cbfe55:

    # hero "「I understand...{p}Does she trust a lot of people?"
    hero "「I understand...{p}Does she trust a lot of people?」"

# game/dialogs.rpy:1153
translate japanese scene5_84d423c2:

    # s "「So far, she only trusts her big brother, Rika and me."
    s "「So far, she only trusts her big brother, Rika and me.」"

# game/dialogs.rpy:1154
translate japanese scene5_ed700bc4:

    # s "「I'm sure you'll be next on the list sooner or later!"
    s "「I'm sure you'll be next on the list sooner or later!」"

# game/dialogs.rpy:1156
translate japanese scene5_e668a3b6:

    # hero "「I see..."
    hero "「I see...」"

# game/dialogs.rpy:1157
translate japanese scene5_0f2a7110:

    # hero "「I'll do my best to be a good friend to her."
    hero "「I'll do my best to be a good friend to her.」"

# game/dialogs.rpy:1159
translate japanese scene5_e866394b:

    # s "「That's great to hear, %(stringhero)s-kun!"
    s "「That's great to hear, %(stringhero)s-kun!」"

# game/dialogs.rpy:1160
translate japanese scene5_d01d2141:

    # s "「I'm sure someday you and her will be best friends!"
    s "「I'm sure someday you and her will be best friends!」"

# game/dialogs.rpy:1161
translate japanese scene5_e741d135:

    # "For sure, but I wonder what Nanami thinks of me right now..."
    "For sure, but I wonder what Nanami thinks of me right now...」"

# game/dialogs.rpy:1162
translate japanese scene5_4464bb7e:

    # "And I'm wondering what Sakura thinks of me as well..."
    "And I'm wondering what Sakura thinks of me as well...」"

# game/dialogs.rpy:1167
translate japanese scene5_2f43b611:

    # "After Nanami finished playing, we ate some yakitori at a fast food restaurant together."
    "その後、僕達はファーストフード店で焼き鳥を食べた。"

# game/dialogs.rpy:1168
translate japanese scene5_b84a1878:

    # "Then we boarded the train to return to the village."
    "そして電車で村に帰った。"

# game/dialogs.rpy:1169
translate japanese scene5_b6fe7fc0:

    # "The three of us had a pretty fun day together."
    "The three of us had a pretty fun day together."

# game/dialogs.rpy:1171
translate japanese scene5_841a64f2:

    # "Nanami waved goodbye to us as she left to get back home."
    "Nanami waved goodbye to us as she left to get back home."

# game/dialogs.rpy:1173
translate japanese scene5_be1b33c3:

    # "Sakura and I were alone once again."
    "Sakura and I were alone once again."

# game/dialogs.rpy:1175
translate japanese scene5_4fcb2681:

    # "Sakura seemed to be deep in thought about something again."
    "咲良ちゃんは何かを考えている様だった。"

# game/dialogs.rpy:1177
translate japanese scene5_5dcdb866:

    # "As we arrived at Sakura's house, Sakura spoke."
    "家に着いた時も、まだ何かを考えていた。"

# game/dialogs.rpy:1178
translate japanese scene5_007ceac2:

    # s "「%(stringhero)s-kun..."
    s "「%(stringhero)sくん…」"

# game/dialogs.rpy:1179
translate japanese scene5_a4db12cc:

    # hero "「What's up?"
    hero "「はい？」"

# game/dialogs.rpy:1181
translate japanese scene5_0f62d32d:

    # s "「I think I have an idea! Just wait here a minute!"
    s "「私に考えがあります。少し待ってて下さい！」"

# game/dialogs.rpy:1183
translate japanese scene5_91bb60f0:

    # "She ran into her house and came back with something. It looked like a set neatly wrapped of clothes."
    "彼女は走って家まで行き、何か青い物を持って戻ってきた。服のセットの様に見えるが…"

# game/dialogs.rpy:1185
translate japanese scene5_8941c000:

    # s "「Here... Please, take it..."
    s "「これ…どうぞ…」"

# game/dialogs.rpy:1186
translate japanese scene5_e8a32052:

    # s "「It's a bit small but you can make it bigger with some touch-ups."
    s "「少し小さいでしょうけど、手直しをすればサイズも合いますよ。」"

# game/dialogs.rpy:1187
translate japanese scene5_a7e82bdc:

    # "I unwrapped the set of clothes. {p}I couldn't believe it!"
    "僕はその青い物を広げた。{p}信じられない！"

# game/dialogs.rpy:1188
translate japanese scene5_b12f3dbd:

    # "It was a blue yukata!"
    "浴衣だ！"

# game/dialogs.rpy:1189
translate japanese scene5_10b50536:

    # "I chuckled for a second. For a second, I thought it was one of her own yukata. I was laughing imagining how I would look like in a yukata made for girls."
    "恐らく彼女の物なのだろう。…女物の浴衣を着る自分を想像して少し笑ってしまった。"

# game/dialogs.rpy:1190
translate japanese scene5_d616579f:

    # "But after a closer look, I notice that it was really a yukata for men, with the right cuttings and colors!"
    "しかし良く見るとこれは…男物じゃないか。"

# game/dialogs.rpy:1191
translate japanese scene5_ac50db4a:

    # hero "「Whoa... For real? I can borrow this?"
    hero "「…本当に？貸してくれるんですか…？」"

# game/dialogs.rpy:1193
translate japanese scene5_3e45378b:

    # s "「You can keep it if you want, %(stringhero)s-kun, it's okay!"
    s "「貰ってくれて構いませんよ、%(stringhero)sくん！」"

# game/dialogs.rpy:1194
translate japanese scene5_68f22fc9:

    # hero "「Gee...thank you... Thank you so much!"
    hero "「ひい…ありがとう…本当にありがとう！」"

# game/dialogs.rpy:1195
translate japanese scene5_882e22b6:

    # hero "「Well... I gotta go... See you at school, Sakura-chan!"
    hero "「じゃあ、そろそろ帰ります。また学校で！」"

# game/dialogs.rpy:1196
translate japanese scene5_1a4bf588:

    # s "「Have a nice weekend, %(stringhero)s-kun!"
    s "「良い週末を、%(stringhero)sくん！」"

# game/dialogs.rpy:1199
translate japanese scene5_bb555e9c:

    # "Sakura waved goodbye and went back into her house."
    "咲良ちゃんは帰って行った。"

# game/dialogs.rpy:1200
translate japanese scene5_020b0a9c:

    # "She really has a habit of helping me out in a jam!"
    "僕はまるで習慣の様に彼女に助けられている。"

# game/dialogs.rpy:1201
translate japanese scene5_66a17c47:

    # "But it felt kinda strange."
    "でもちょっと待てよ。"

# game/dialogs.rpy:1202
translate japanese scene5_51ab6c7b:

    # "Whose yukata is this?"
    "この浴衣は一体何処から…？"

# game/dialogs.rpy:1203
translate japanese scene5_4735f789:

    # "It looked like it's sized for her..."
    "サイズは大体咲良ちゃんと同じ位…"

# game/dialogs.rpy:1204
translate japanese scene5_960d182a:

    # "Maybe it was her father's from when he was young..."
    "父親が若かった頃のお古なのだろうか…"

# game/dialogs.rpy:1205
translate japanese scene5_cb071560:

    # "But...It looks brand new..."
    "いや、それにしては新しい…それに一度も使われた形跡が無い様な…？"

# game/dialogs.rpy:1206
translate japanese scene5_0074c533:

    # "They couldn't have bought one by mistake..."
    "間違えて買ったんだろうか？うーん…"

# game/dialogs.rpy:1212
translate japanese scene5_8c3a38cf:

    # hero "「So... I'll see you later at school!"
    hero "「じゃあ、学校で！」"

# game/dialogs.rpy:1213
translate japanese scene5_c2d0f8e4:

    # s "「See you around %(stringhero)s-kun!"
    s "「さようなら、%(stringhero)sくん！」"

# game/dialogs.rpy:1214
translate japanese scene5_c0d35692:

    # s "「I had a lot of fun with you today!"
    s "「今日は本当に楽しかったです！」"

# game/dialogs.rpy:1215
translate japanese scene5_da65aa5d:

    # hero "「Likewise!"
    hero "「こちらこそ！」"

# game/dialogs.rpy:1218
translate japanese scene5_bb555e9c_1:

    # "Sakura waved goodbye and went back into her house."
    "彼女は手を振って帰っていった。"

# game/dialogs.rpy:1219
translate japanese scene5_c8d8f45f:

    # "Thanks to her, I knew how to get to the city and where the shops were."
    "彼女には感謝してる。"

# game/dialogs.rpy:1220
translate japanese scene5_67c032e7:

    # "During my visit, I spotted some yukata shops."
    "During my visit, I spotted some yukata shops."

# game/dialogs.rpy:1221
translate japanese scene5_5854a13b:

    # "Tomorrow, I'll go there alone and I'll buy myself a yukata."
    "明日、自分で浴衣を買いに行かなければ…"

# game/dialogs.rpy:1222
translate japanese scene5_bb429b41:

    # "I'll catch that loudmouth Rika by surprise at the festival!"
    "梨花ちゃんめ、今に見てろよ！"

# game/dialogs.rpy:1224
translate japanese scene5_a20cefa7:

    # "..."
    "……。"

# game/dialogs.rpy:1225
translate japanese scene5_667ec4fd:

    # "Oh well... Let's go home, now..."
    "まあ、帰るか…"

# game/dialogs.rpy:1246
translate japanese scene7_237e24d1:

    # centered "{size=+35}CHAPTER 3\nSakura's secret{fast}{/size}"
    centered "{size=+35}第3章\n咲良の秘密{fast}{/size}"

# game/dialogs.rpy:1251
translate japanese scene7_58905c5b:

    # "Another week has passed."
    "そしてもう一週間が経った。"

# game/dialogs.rpy:1252
translate japanese scene7_19ce328a:

    # "School was great, with Rika, Nanami and Sakura around."
    "それにこの二週間で、僕も村の一員という認識を持ち始めた。"

# game/dialogs.rpy:1253
translate japanese scene7_2fc12cae:

    # "During these last two weeks, I really started to feel that I was a part of the village now."
    "それにこの二週間で、僕も村の一員という認識を持ち始めた。"


# game/dialogs.rpy:1254
translate japanese scene7_2c9e68a3:

    # "I'm happy, but I'm unsure if I have feelings for Sakura..."
    "嬉しいけど、この咲良ちゃんに抱いている感情が恋愛感情なのかは未だよく分かっていない…"

# game/dialogs.rpy:1255
translate japanese scene7_909fa517:

    # "Or maybe even for Nanami..."
    "Or maybe even for Nanami..."

# game/dialogs.rpy:1257
translate japanese scene7_4dd65759:

    # "One thing's for sure, Nanami and I talk and play games more and more.{p}I think she really likes me."
    "One thing's for sure, Nanami and I talk and play games more and more.{p}I think she really likes me."

# game/dialogs.rpy:1258
translate japanese scene7_ef8f393e:

    # "But I still don't know what that thing Sakura told me about her was about..."
    "But I still don't know what that thing Sakura told me about her was about..."

# game/dialogs.rpy:1259
translate japanese scene7_28d00c59:

    # "Well... At least I don't have any feelings for Rika...No way!"
    "まあ…何にせよ梨花ちゃんに恋愛感情は抱いてないな！うん。"

# game/dialogs.rpy:1261
translate japanese scene7_10129566:

    # r "「Hey, %(stringhero)s!"
    r "「やい、%(stringhero)s！」"

# game/dialogs.rpy:1266
translate japanese scene7_57e4be97:

    # "Yikes!!! Speak of the devil herself..."
    "ゲゲ…悪魔の話をすると何とやらだ…"

# game/dialogs.rpy:1267
translate japanese scene7_391047cd:

    # hero "「Oh, hey, Rika-chan..."
    hero "「おお、梨花ちゃん…」"

# game/dialogs.rpy:1268
translate japanese scene7_9c2ccd47:

    # r "「Do you have some free time next weekend?"
    r "「来週空きあるかいな？」"

# game/dialogs.rpy:1269
translate japanese scene7_158b0faf:

    # hero "「This weekend?"
    hero "「This weekend?」"

# game/dialogs.rpy:1270
translate japanese scene7_3a013f55:

    # r "「No, you idiot. The weekend after this one!"
    r "「No, you idiot. The weekend after this one!」"

# game/dialogs.rpy:1271
translate japanese scene7_9d4e7e0f:

    # hero "「Oh... Sure I guess. What do you need?"
    hero "「Oh... Sure I guess. What do you need?」"

# game/dialogs.rpy:1272
translate japanese scene7_acfd623e:

    # r "「Well, since you're a manly man that has muscles..."
    r "「いやな、アンタ男やさかい、筋肉あるやろ？」"

# game/dialogs.rpy:1273
translate japanese scene7_67864244:

    # r "「I thought, maybe you could make yourself useful and help me prepare for festival!"
    r "「祭りの手伝い出来るんやないか思うてな！」"

# game/dialogs.rpy:1274
translate japanese scene7_e98e7579:

    # "Argh! Why did I agree before knowing why?!"
    "アア！何故俺は先に返事をしてしまったんだ！？やってしまった！！"

# game/dialogs.rpy:1275
translate japanese scene7_df0ced9f:

    # "Looks like I don't have a choice now..."
    "逃げ道は無さそうだな…"

# game/dialogs.rpy:1283
translate japanese scene7_8b156e71:

    # hero "「Alright, fine, I'll help."
    hero "「分かった、やるよ！」"

# game/dialogs.rpy:1285
translate japanese scene7_10eea5f6:

    # r "「Great!"
    r "「上等や！」"

# game/dialogs.rpy:1287
translate japanese scene7_6f5d2035:

    # r "「I'll tell you where to meet later!"
    r "「ほな、後でミーティングの場所教えるさかいな！」"

# game/dialogs.rpy:1288
translate japanese scene7_9766f460:

    # hero "「Rika-chan, I didn't know you were from a temple."
    hero "「梨花ちゃん、君が神社の回し者だとは知らなかったよ。」"

# game/dialogs.rpy:1289
translate japanese scene7_82bf0a2d:

    # r "「My parents are shinto priests. Sometimes I help them for festivals and other temple related stuff."
    r "「ウチは親が神主やさかい、時々ウチもこうやって手伝ったりするんやで。」"

# game/dialogs.rpy:1290
translate japanese scene7_c90a7240:

    # hero "「Do you live in the temple with your parents?"
    hero "「へえ、両親と一緒に神社で暮らしてるの？」"

# game/dialogs.rpy:1292
translate japanese scene7_a6c17ff8:

    # r "「My..."
    r "「ウチは…」"

# game/dialogs.rpy:1293
translate japanese scene7_95b777d9:

    # "Her face fell dark."
    "表情が曇り、笑顔が消えた。"

# game/dialogs.rpy:1294
translate japanese scene7_51eeeb24:

    # r "「My parents divorced."
    r "「ウチの両親は離婚したんや。」"

# game/dialogs.rpy:1295
translate japanese scene7_95db867f:

    # r "「I live with my mother and my father guards the temple."
    r "「ウチはおかんと暮らして、おとんは社を守っとる。」"

# game/dialogs.rpy:1297
translate japanese scene7_5fbbef96:

    # r "「But they're still on good terms and they still have their faith in the shinto order, so it's okay."
    r "「でも、お互いまだ交際関係は保っとって、神道も変わらず信じとるさかい、ええんや。」"

# game/dialogs.rpy:1298
translate japanese scene7_868b412a:

    # hero "「Ah, I'm sorry, I didn't know. It's good they're on positive terms though."
    hero "「あっ…うん…」"

# game/dialogs.rpy:1299
translate japanese scene7_0d2419ec:

    # r "「Yeah...I guess so. Anyway, I'll meet you at the club, see ya!"
    r "「ええで、ほな部活でな！」"

# game/dialogs.rpy:1301
translate japanese scene7_36df1f8b:

    # "I guess I learned something new about Rika. Still...I hope she won't be a total jerk towards me this weekend..."
    "まあ、あんまり酷い事される訳じゃないなら手伝ってやってもいいかな…"

# game/dialogs.rpy:1308
translate japanese scene7_219621f1:

    # s "「%(stringhero)s-kun!!"
    s "「%(stringhero)sくん！」"

# game/dialogs.rpy:1309
translate japanese scene7_51a60250:

    # hero "「Sakura-chan! How goes?"
    hero "「咲良ちゃん。調子はどう？」"

# game/dialogs.rpy:1310
translate japanese scene7_f4d7051e:

    # "Sakura bowed at me. As she rose back up, I noticed her face was flushed."
    "咲良ちゃんはお辞儀をした。顔は赤く染まっている。"

# game/dialogs.rpy:1312
translate japanese scene7_85835f50:

    # s "「%(stringhero)s-kun,...are you doing anything this weekend?"
    s "「%(stringhero)sくん…今週末、何か予定あります？」"

# game/dialogs.rpy:1314
translate japanese scene7_31d438a9:

    # hero "「The next weekend?"
    hero "「The next weekend?」"

# game/dialogs.rpy:1315
translate japanese scene7_9d85daa8:

    # hero "「I'm sorry, I'm helping Rika-chan set up stuff for the festival."
    hero "「I'm sorry, I'm helping Rika-chan set up stuff for the festival.」"

# game/dialogs.rpy:1316
translate japanese scene7_77ee7366:

    # s "「Huh?"
    s "「Huh?」"

# game/dialogs.rpy:1317
translate japanese scene7_8480da2c:

    # s "「Oh... No, I mean, the weekend of this week"
    s "「Oh... No, I mean, the weekend of this week」"

# game/dialogs.rpy:1318
translate japanese scene7_cb5a59ac:

    # hero "「Oh whoops. Guess my mind was wandering to far into the future... Hm..."
    hero "「Oh whoops. Guess my mind was wandering to far into the future... Hm...」"

# game/dialogs.rpy:1319
translate japanese scene7_d0aa7f64:

    # hero "「I'm not doing anything at all."
    hero "「勿論あります…」"

# game/dialogs.rpy:1320
translate japanese scene7_2217ef70:

    # hero "「Do you want to go out to the city again?"
    hero "「一緒にデートに行くっていう予定が、ね。」"

# game/dialogs.rpy:1322
translate japanese scene7_1d124fd8:

    # "Sakura's face lit up and she started to giggled."
    "咲良は笑った。"

# game/dialogs.rpy:1323
translate japanese scene7_0f5eeead:

    # s "「I...I was about to ask you the same question! {image=heart.png}"
    s "「ええ、誘おうと思ってたんです！ {image=heart.png}」"

# game/dialogs.rpy:1324
translate japanese scene7_9e2e5ae5:

    # hero "「Of course, I'd love to go out with you to the city!"
    hero "「僕はいつでも準備は出来てます。」"

# game/dialogs.rpy:1325
translate japanese scene7_22e33626:

    # hero "「Yeah, I'd like to have even more fun than last time!"
    hero "「Yeah, I'd like to have even more fun than last time!」"

# game/dialogs.rpy:1327
translate japanese scene7_c31b4dec:

    # s "「Same here! I'll wait for you at the train station at 9AM!"
    s "「よかった！じゃあ、明日の9時に駅で待ってますね！」"

# game/dialogs.rpy:1328
translate japanese scene7_5409a3f5:

    # hero "「Alright! I'll see you then!"
    hero "「了解です。」"

# game/dialogs.rpy:1330
translate japanese scene7_a7db0154:

    # "Ah, it's almost time for class..."
    "ああ、授業の時間だ…"

# game/dialogs.rpy:1334
translate japanese scene7_13719c4f:

    # "The school day dragged on and on... Maybe I just couldn't wait to hang out with Sakura."
    "The school day dragged on and on... Maybe I just couldn't wait to hang out with Sakura."

# game/dialogs.rpy:1335
translate japanese scene7_85ae4e7c:

    # "The next day came, and I arrived at the train station in the morning to go to the city with Sakura."
    "翌朝、咲良ちゃんとのデートに旅立った…"

# game/dialogs.rpy:1341
translate japanese SH1_e0b76a41:

    # "We took the train to the city. We started our day out eating at an American fast food place because we were already hungry."
    "僕達は腹ぺこだったので、まずはアメリカンのファーストフードを食べる事にした。"

# game/dialogs.rpy:1342
translate japanese SH1_d68b3fb1:

    # "After that, we decided to try karaoke."
    "その後、カラオケに向かった。"

# game/dialogs.rpy:1344
translate japanese SH1_0c6feee4:

    # "Sakura's voice was absolutely mesmerizing."
    "咲良ちゃんの声は信じられない程透き通っていた。"

# game/dialogs.rpy:1345
translate japanese SH1_a925770d:

    # hero "「You could be a famous pop idol, Sakura-chan."
    hero "「You could be a famous pop idol, Sakura-chan.」"

# game/dialogs.rpy:1346
translate japanese SH1_ee5a9ea8:

    # s "「Thank you... But I don't want to be one, though..."
    s "「Thank you... But I don't want to be one, though...」"

# game/dialogs.rpy:1347
translate japanese SH1_b809126d:

    # hero "「Eh? Why is that?"
    hero "「えーどうして？」"

# game/dialogs.rpy:1348
translate japanese SH1_93f8ec4b:

    # s "「Well..."
    s "「えっと…」"

# game/dialogs.rpy:1349
translate japanese SH1_3098f4a6:

    # "She hesitated a bit before replying."
    "彼女は長い事返事を躊躇っていた。"

# game/dialogs.rpy:1350
translate japanese SH1_25b5d3d5:

    # s "「I don't think I'd like the lifestyle of a pop idol..."
    s "「アイドルみたいな生活は私にあんまり合わないと思います…」"

# game/dialogs.rpy:1351
translate japanese SH1_9c5f10fe:

    # s "「It's so much work and you don't have a lot of time for yourself."
    s "「仕事ばかりで忙しいでしょうし、自分の為の時間が無さそうですから。」"

# game/dialogs.rpy:1352
translate japanese SH1_67650f2f:

    # hero "「Yeah, it must be rough..."
    hero "「うーむ…」"

# game/dialogs.rpy:1353
translate japanese SH1_759de575:

    # hero "「By the way, what would you would like to do in the future?"
    hero "「将来の夢とか、あるんですか？」"

# game/dialogs.rpy:1354
translate japanese SH1_9028f52b:

    # s "「I don't really know yet..."
    s "「まだよくは分かっていませんけど…」"

# game/dialogs.rpy:1355
translate japanese SH1_36411a29:

    # s "「Maybe I'll be a part of a symphony orchestra if I can."
    s "「出来る事なら…シンフォニーオーケストラの一員になりたいと思っています。」"

# game/dialogs.rpy:1356
translate japanese SH1_ba98acc9:

    # hero "「Do you play any instruments?"
    hero "「何か楽器を弾くんですか？」"

# game/dialogs.rpy:1357
translate japanese SH1_cc1fe723:

    # s "「Yes, the violin."
    s "「まあ、ヴァイオリンを少々…」"

# game/dialogs.rpy:1358
translate japanese SH1_5632a1a6:

    # "I suddenly remembered..."
    "思い出した。"

# game/dialogs.rpy:1363
translate japanese SH1_1075e8d3:

    # "So the violin I hear sometimes at night... That must be her..."
    "夕方に時々ヴァイオリンの音色が聞こえていた。あのヴァイオリンは、彼女のだったのか…"

# game/dialogs.rpy:1368
translate japanese SH1_6ae339be:

    # hero "「I think I hear you playing the violin during the evening from time to time."
    hero "「夕方に弾いていたのはあなただったんですか？」"

# game/dialogs.rpy:1369
translate japanese SH1_ccbcdf1d:

    # "She looked down, kinda embarrassed."
    "彼女は俯いて赤くなった。"

# game/dialogs.rpy:1370
translate japanese SH1_457a5f49:

    # s "「Oh... You heard me playing?"
    s "「聴いてたんですか？」"

# game/dialogs.rpy:1371
translate japanese SH1_0027dd08:

    # hero "「Yeah... You play so perfectly... It's amazing!"
    hero "「ええ…それなら、完璧な音色でしたよ…」"

# game/dialogs.rpy:1372
translate japanese SH1_55a77664:

    # s "「W-why T-t-thank you..."
    s "「あ…あ、ありがとう…」"

# game/dialogs.rpy:1373
translate japanese SH1_7b7e5c26:

    # hero "「I hope I can watch you play someday."
    hero "「いつか生で弾いてる所を聴きたいです。」"

# game/dialogs.rpy:1374
translate japanese SH1_32eac59f:

    # s "「I'd be happy to play for you, %(stringhero)s-kun!"
    s "「ええ、喜んで演奏しますよ、%(stringhero)sくん！」"

# game/dialogs.rpy:1378
translate japanese SH1_3e846a41:

    # "I tried to sing the next song, but I was horribly awful at singing. At least it made Sakura laugh."
    "その後数曲歌った。僕の歌声はかなり酷かったが（咲良ちゃんが思わず笑った程に）。"

# game/dialogs.rpy:1379
translate japanese SH1_7f6f8e16:

    # "When we were done, we headed back to to the city's train station."
    "僕達は帰りの駅に向かっていた。"

# game/dialogs.rpy:1383
translate japanese SH1_884ab08c:

    # "As we were about to cross the road..."
    "道路を横断する、その時だった。"

# game/dialogs.rpy:1384
translate japanese SH1_5b2451aa:

    # "A car, ignoring the red light, kept driving and almost hit Sakura!"
    "信号を無視して、物凄いスピードで車が突っ込んできたのだ。"

# game/dialogs.rpy:1385
translate japanese SH1_d2261bcd:

    # "Sakura quickly stepped back but lost her balance and was about to fall on her back."
    "咲良ちゃんは瞬時に後ろに跳ねて避け…"

# game/dialogs.rpy:1387
translate japanese SH1_eb7a286f:

    # "I managed to catch her just in time and she fell into arms."
    "僕は即座に彼女を腕でキャッチした。"

# game/dialogs.rpy:1388
translate japanese SH1_e5780a08:

    # "She looked into my eyes and I got lost in hers."
    "彼女が、僕を見ている。青く大きな瞳で…"

# game/dialogs.rpy:1389
translate japanese SH1_ac769ce2:

    # "Time stopped."
    "時が止まった。"

# game/dialogs.rpy:1390
translate japanese SH1_e75587c1:

    # "As we stared at each other, we forgot all about the car incident."
    "見詰め合っていた僕達は、既に車の事など忘れていた。"

# game/dialogs.rpy:1391
translate japanese SH1_0657ef81:

    # s "「%(stringhero)s....-kun...."
    s "「%(stringhero)s…くん…」"

# game/dialogs.rpy:1392
translate japanese SH1_c6dc2ed4:

    # hero "「....Sakura-chan..."
    hero "「咲良…ちゃん…」"

# game/dialogs.rpy:1394
translate japanese SH1_ae0ed22a:

    # "Her eyes slowly shut, her face closed in on mine, her small mouth beckoned for something... It was her lips that seemed to be waiting for me."
    "彼女は瞳を閉じ、唇はほんのりと開けてこちらを向いた。これってもしかして…"

# game/dialogs.rpy:1395
translate japanese SH1_07a9d021:

    # "Oh my gosh... I think she wants me to kiss her! I can't believe it! What do I do!?"
    "なんてこった…僕を…待ってるのか…！？"

# game/dialogs.rpy:1397
translate japanese SH1_4b8ffc21:

    # "I could hear my heart beating loudly. It felt so loud, I was almost sure that the people around us could hear it too."
    "心臓が音を立てて高鳴りだした。周りの人にも聞こえてしまう程の大きな音で。"

# game/dialogs.rpy:1398
translate japanese SH1_fe20f7a5:

    # "This was it. It's now or never. My face slowly approached hers..."
    "僕は少しずつ…"

# game/dialogs.rpy:1399
translate japanese SH1_8538723d:

    # "Very slowly..."
    "少しずつ…顔を…"

# game/dialogs.rpy:1400
translate japanese SH1_f1ab825c:

    # "I could feel her breath on my face."
    "…彼女の息遣いを感じつつ…"

# game/dialogs.rpy:1405
translate japanese SH1_16f58986:

    # "And the second my nose touched hers...{p}She turned her face away, completely embarrassed, and stood up almost immediately."
    "そして彼女と僕の鼻が触れ…{p}その瞬間、彼女はハッとして立ち上がった。"

# game/dialogs.rpy:1406
translate japanese SH1_56dcd521:

    # s "「I'm... I'm okay..."
    s "「いやその…大丈夫…大丈夫だから…」"

# game/dialogs.rpy:1410
translate japanese SH1_557b9c7c:

    # s "「. . ."
    s "「……。」"

# game/dialogs.rpy:1411
translate japanese SH1_b64fd408:

    # s "「Let's go home..."
    s "「えっと…帰りましょう…か…」"

# game/dialogs.rpy:1416
translate japanese SH1_32abdb6f:

    # "On the train ride home, I was in a complete daze..."
    "僕は帰るまでずっと、ぼうっとしていた。"

# game/dialogs.rpy:1417
translate japanese SH1_06b56af7:

    # "I thought she wanted me to kiss her... But in the end, she refused..."
    "彼女がキスを求めていたのは多分確かだ…それなのに結局断られてしまった。"

# game/dialogs.rpy:1418
translate japanese SH1_a20cefa7:

    # "..."
    "……。"

# game/dialogs.rpy:1419
translate japanese SH1_23dda12a:

    # "Dammit, stop being paranoid, %(stringhero)s!...{p}Maybe it's just because it was her first time. She might not have felt ready..."
    "Dammit, stop being paranoid, %(stringhero)s!...{p}多分彼女は初キスだったから、心の準備が出来ていなかったんだろう…"

# game/dialogs.rpy:1420
translate japanese SH1_e1f82eca:

    # "I felt kinda happy inside though. At least it means that she's attracted to me... I think..."
    "それでも、僕は内心嬉しかった。彼女が僕に興味がある事が確かめられたんだ。"

# game/dialogs.rpy:1421
translate japanese SH1_a3dbd16d:

    # "But I also felt just as confused..."
    "その分混乱もしているけど…"

# game/dialogs.rpy:1422
translate japanese SH1_246f930a:

    # "As we sat together in silence, I occasionally looked over to Sakura."
    "As we sat together in silence, I occasionally looked over to Sakura."

# game/dialogs.rpy:1423
translate japanese SH1_acd10f88:

    # "She didn't look angry. But she seemed really embarrassed still."
    "She didn't look angry. But she seemed really embarrassed still."

# game/dialogs.rpy:1424
translate japanese SH1_045668b1:

    # "But, most of all, she looked upset about something. She just sat there lost in her thoughts like she was profoundly thinking about something."
    "But, most of all, she looked upset about something. She just sat there lost in her thoughts like she was profoundly thinking about something."

# game/dialogs.rpy:1427
translate japanese SH1_3c386e89:

    # "Sakura was completely quiet along the way."
    "咲良ちゃんはずっと黙ったままだ。"

# game/dialogs.rpy:1428
translate japanese SH1_849b7889:

    # "I didn't know what to say...or what to do."
    "気まずい…"

# game/dialogs.rpy:1429
translate japanese SH1_c312d8ea:

    # "The atmosphere was uncomfortable."
    "さっきの事故以来、気まずい雰囲気だ。"

# game/dialogs.rpy:1430
translate japanese SH1_59826635:

    # "Suddenly, she broke the silence."
    "唐突に、彼女が話し始めた。"

# game/dialogs.rpy:1431
translate japanese SH1_1eeb4b66:

    # s "「...%(stringhero)s-kun?"
    s "「…%(stringhero)sくん？」"

# game/dialogs.rpy:1432
translate japanese SH1_4edce7c8:

    # hero "「Yes?"
    hero "「はい？」"

# game/dialogs.rpy:1434
translate japanese SH1_782acf27:

    # s "「I...{p}........................{p}I have something I want to tell you..."
    s "「私には…{p}…………………………{p}言わなければらならない事があるんです…」"

# game/dialogs.rpy:1435
translate japanese SH1_d2dcc110:

    # hero "「What is it?"
    hero "「えっ…？」"

# game/dialogs.rpy:1436
translate japanese SH1_719ce3f8:

    # "I followed her as she entered a rice field."
    "彼女は麦畑に入っていった。僕はその後を付いていく…"

# game/dialogs.rpy:1437
translate japanese SH1_d7b94be6:

    # "The path inside was small, surrounded by big rice plants."
    "道は狭く、周りは麦に囲まれていた。"

# game/dialogs.rpy:1438
translate japanese SH1_481e4154:

    # "It was completely desolate. There was nobody around us. Only the chants of the cicadas could be heard."
    "人の気配もしない、何も無い道だった。ただ蝉の合唱だけが聴こえてくる。"

# game/dialogs.rpy:1440
translate japanese SH1_8329e10f:

    # s "「This is something very difficult to tell you...{p}Painfully difficult..."
    s "「その、本当に…{p}本当に言い難い事なんです…」"

# game/dialogs.rpy:1441
translate japanese SH1_c02f0d8f:

    # s "「This isn't something you might not even comprehend, but..."
    s "「理解出来ないかも知れません…」"

# game/dialogs.rpy:1442
translate japanese SH1_7cf20b6f:

    # hero "「This sounds pretty serious."
    hero "「何やら深刻そうだ…」"

# game/dialogs.rpy:1443
translate japanese SH1_a4c7d906:

    # "I could feel a sort of anxiety building up from inside me."
    "心臓の高鳴りを感じる。"

# game/dialogs.rpy:1444
translate japanese SH1_ec713d94:

    # "I've never seen Sakura behave this seriously before."
    "こんなに畏まった咲良ちゃんを見た事が無かった。"

# game/dialogs.rpy:1445
translate japanese SH1_49f22c99:

    # s "「I'm about to tell you this because I trust you."
    s "「それでも、私は%(stringhero)sくんを信じて、話します。」"

# game/dialogs.rpy:1446
translate japanese SH1_afd8f97b:

    # "I smiled, but I was nervous."
    "それを聞いて、僕は微笑んだ。"

# game/dialogs.rpy:1447
translate japanese SH1_5066f9b8:

    # hero "「Y-you can trust me, Sakura-chan. You're my friend. You, Rika and Nanami are my best friends and I'll never betray any of you. Never."
    hero "「大丈夫ですよ。友達じゃないですか。あなたと、そして梨花ちゃんは僕の最高の友達です。裏切る様な事は絶対にしませんよ。」"

# game/dialogs.rpy:1448
translate japanese SH1_9b73dceb:

    # "I mean that, from the bottom of my heart."
    "僕の心からの言葉だ。"

# game/dialogs.rpy:1449
translate japanese SH1_64b40e00:

    # s "「Do you promise? Never?"
    s "「本当？絶対に？」"

# game/dialogs.rpy:1450
translate japanese SH1_b7fb0fa9:

    # hero "「Forever and ever."
    hero "「絶対です。」"

# game/dialogs.rpy:1451
translate japanese SH1_ac397e42:

    # "This time I smiled with a little more confidence."
    "もう一度笑った。"

# game/dialogs.rpy:1452
translate japanese SH1_43dc30fb:

    # "She gave a faint smile but reverted back to her serious face after."
    "彼女は少し笑ったが、それでもすぐ真面目な表情に戻った。"

# game/dialogs.rpy:1453
translate japanese SH1_ee01b059:

    # s "「It's... So hard for me to tell you...{p}Besides my parents, only Rika-chan knows about it.{p}Not even Nanami-chan knows..."
    s "「本当に言い難い…{p}この事を知ってるのは両親と、梨花ちゃんだけです。{p}Not even Nanami-chan knows...」"

# game/dialogs.rpy:1454
translate japanese SH1_a56d7a94:

    # s "「Everyone else seems to have just heard rumors about it... or aren't sure at all..."
    s "「他の人も若しかしたら噂で聞いた事があるかも知れませんけど…」"

# game/dialogs.rpy:1455
translate japanese SH1_d0d64bc6:

    # hero "「Oh..."
    hero "「ああ…」"

# game/dialogs.rpy:1456
translate japanese SH1_2fcf5cc9:

    # "I was confused... What was she trying to say?"
    "分からない、一体何の事だ？"

# game/dialogs.rpy:1457
translate japanese SH1_0ebf712b:

    # "Obviously, she and Rika-chan knew a secret.{p}And Sakura-chan was about to tell me this secret."
    "ハッキリしてるのは、彼女と梨花ちゃんだけがその秘密を知っている事、{p}そしてそれを僕に伝えてくれる事。"

# game/dialogs.rpy:1458
translate japanese SH1_4df61edb:

    # "I feel happy. That means she must feel I'm a close enough friend and that she really trusts me."
    "嬉しかった。彼女は僕を梨花ちゃんと同じ位の親友と認めてくれたという事に対して。"

# game/dialogs.rpy:1460
translate japanese SH1_32e60ae0:

    # "She stopped after walking a small distance from me, facing away from me."
    "彼女は僕から3メートル離れ、顔を背けた。"

# game/dialogs.rpy:1461
translate japanese SH1_a0dd6cdb:

    # "Her body covered the sun and I saw only her shadow in the distance."
    "逆光で、僕は影しか見る事が出来なかった。"

# game/dialogs.rpy:1463
translate japanese SH1_e1554201:

    # s "「..."
    s "「……。」"

# game/dialogs.rpy:1464
translate japanese SH1_007ceac2:

    # s "「%(stringhero)s-kun..."
    s "「%(stringhero)sくん…」"

# game/dialogs.rpy:1465
translate japanese SH1_d5eced1a:

    # s "「Do you remember the day when those thugs bullied me?"
    s "「私が不良に虐められてた日の事、覚えてますか？」"

# game/dialogs.rpy:1466
translate japanese SH1_d19c5e51:

    # hero "「Yes, I remember."
    hero "「ええ、覚えてますよ」"

# game/dialogs.rpy:1467
translate japanese SH1_3b33aa23:

    # s "「Do you remember what they said to me?"
    s "「彼らが何て言ったか知ってます？」"

# game/dialogs.rpy:1468
translate japanese SH1_ec6b7626:

    # hero "「Yeah, I remember."
    hero "「ええ、ハッキリと覚えてます。」"

# game/dialogs.rpy:1469
translate japanese SH1_b28fcf52:

    # hero "「They were trying to make you admit that you were a boy, or something like that."
    hero "「あなたが男だとかそれを認めろだとか…」"

# game/dialogs.rpy:1470
translate japanese SH1_bf9bdd8b:

    # s "「Yes."
    s "「ええ。」"

# game/dialogs.rpy:1471
translate japanese SH1_b5a2fb72:

    # s "「It's because they..."
    s "「彼らは…」"

# game/dialogs.rpy:1472
translate japanese SH1_9fce5758:

    # s "「They guessed it... For some reason they knew it..."
    s "「何らかの方法で知ったんです…」"

# game/dialogs.rpy:1473
translate japanese SH1_bbc36fab:

    # hero "「I... What are you trying to say?..."
    hero "「一体何を…何を言おうとしているんです…？」"

# game/dialogs.rpy:1474
translate japanese SH1_007ceac2_1:

    # s "「%(stringhero)s-kun..."
    s "「%(stringhero)sくん…」"

# game/dialogs.rpy:1483
translate japanese SH1_e4a4445c:

    # s "「I {b}really{/b} am a boy."
    s "「僕わ{b}男の子{/b}だ。」"

# game/dialogs.rpy:1484
translate japanese SH1_16bb48d0:

    # hero "「...W-what?"
    hero "「何？」"

# game/dialogs.rpy:1505
translate japanese SH1_47f1e757:

    # centered "{size=+35}CHAPTER 4\nBig decisions{fast}{/size}"
    centered "{size=+35}第4章\n偉大な決定{fast}{/size}"

# game/dialogs.rpy:1515
translate japanese SH1_bb09a9d7:

    # hero "「You... You're... a boy!?"
    hero "「お…お… 男だ？！」"

# game/dialogs.rpy:1516
translate japanese SH1_a82ece96:

    # "My mind went completely numb for a second."
    "真っ白だ。"

# game/dialogs.rpy:1517
translate japanese SH1_7bdf1789:

    # "I couldn't believe it."
    "どう…信じろって…"

# game/dialogs.rpy:1518
translate japanese SH1_cd989119:

    # "No, she couldn't be a boy!{p} That's impossible!!!"
    "いや…いやいや、彼女が男な訳が無い！{p}有り得ない！"

# game/dialogs.rpy:1519
translate japanese SH1_9f02c3f2:

    # "I started to nervously laugh to myself. There's no way. It was just a joke..."
    "僕は神経質に笑い始めた。信じる事が出来なかった。いや冗談に違いない。"

# game/dialogs.rpy:1520
translate japanese SH1_1faf5341:

    # hero "「Haha it's a joke...No way...No, you can't be a boy, Sakura-chan!"
    hero "「君は男じゃ無いんだ！咲良ちゃん！！」"

# game/dialogs.rpy:1521
translate japanese SH1_b293358c:

    # "Sakura-chan didn't say a thing."
    "返事は無かった。"

# game/dialogs.rpy:1522
translate japanese SH1_b2a3712a:

    # "She suddenly turned around, and approached me."
    "彼女は唐突に振り向いて、向かってきた。"

# game/dialogs.rpy:1523
translate japanese SH1_fcff274b:

    # s "「...If you don't believe me, take a look for yourself!"
    s "「信じないと言うのなら、これを…！」"

# game/dialogs.rpy:1528
translate japanese SH1_93439998:

    # "She suddenly lifted her skirt up, showing me her panties!"
    "彼女はスカートをめくった。"

# game/dialogs.rpy:1529
translate japanese SH1_6a7c30f8:

    # "I gasped out of embarrassment and shock. I took a look around, but fortunately we were still alone in the rice fields."
    "僕はびっくりして叫び、辺りを見回したが、幸い誰も居なかった。"

# game/dialogs.rpy:1530
translate japanese SH1_6db43b58:

    # "I tried my hardest to not look at Sakura and covered my eyes."
    "それでも僕はそれを見ることが出来ず、目を塞いだ。"

# game/dialogs.rpy:1531
translate japanese SH1_7987ae0e:

    # hero "「No, Sakura-chan, I can't look, this is just inappropriate!"
    hero "「何してるんだ、咲良ちゃん！」"

# game/dialogs.rpy:1532
translate japanese SH1_ea014391:

    # s "「Please, just look at me!"
    s "「見て下さい！」"

# game/dialogs.rpy:1533
translate japanese SH1_14a15247:

    # "I peeked a little and could see her eyes intensely glaring at me. She was dead serious."
    "彼女は青い瞳で真摯に見つめてくる。彼女は本気だ。"

# game/dialogs.rpy:1534
translate japanese SH1_453421f4:

    # "My eyes slowly scrolled down to look below her waistline."
    "僕は、ゆっくりと視線を下ろした。"

# game/dialogs.rpy:1535
translate japanese SH1_952d1725:

    # "She was wearing blue-colored panties..."
    "青い、女の子用の…ぱんつ…"

# game/dialogs.rpy:1536
translate japanese SH1_ede13101:

    # "But then, I noticed that...in her panties...there was...something.{p}Something that shouldn't have been there."
    "でもその中には、あった。{p}あるべきでない何かが…確かにあったのだ。"

# game/dialogs.rpy:1537
translate japanese SH1_dbbe62c0:

    # "...But the evidence was right there... "
    "これが…"

# game/dialogs.rpy:1538
translate japanese SH1_a97eb7ed:

    # "I couldn't believe my eyes but the truth was there..."
    "自分の目が信じられないが、事実がそこにはあった…"

# game/dialogs.rpy:1540
translate japanese SH1_6ae69507:

    # "She was really a HE!!!"
    "彼女は、いや彼は！！男の子だった！！！！！"

# game/dialogs.rpy:1541
translate japanese SH1_20403ffb:

    # "I started to shake."
    "体が震え始めた。"

# game/dialogs.rpy:1542
translate japanese SH1_99484c94:

    # "So the cute girl I was following for school.{p}The wonderful girl who was talking about manga and anime with Rika and Nanami..."
    "それじゃあ、僕と一緒に学校に行ってた女の子も、{p}梨花ちゃんと漫画について語ってたあの女の子も、"

# game/dialogs.rpy:1543
translate japanese SH1_0e81ba72:

    # "The girl I was infatuated with for almost two weeks...{p}The girl I was about to kiss today..."
    "二回も一緒にデートに行ったあの女の子も、{p}今日、キスをしようとしていたあの女の子も…"

# game/dialogs.rpy:1544
translate japanese SH1_a2b0302d:

    # "That girl... Was a boy! A guy! A dude! A male!!!!!"
    "全部、全部男で！男の子だったって言うのか！！！"

# game/dialogs.rpy:1545
translate japanese SH1_51a16ccf:

    # "I stood there, motionless."
    "僕は床に崩れ落ちた。"

# game/dialogs.rpy:1546
translate japanese SH1_0cbdd6b2:

    # hero "「You... yo-yo-yo-yo-you're a..."
    hero "「き、ききききききみは…」"

# game/dialogs.rpy:1547
translate japanese SH1_29fb07f0:

    # hero "「{size=+10}You're a boy?!{/size}"
    hero "「{size=+10}男！？{/size}」"

# game/dialogs.rpy:1548
translate japanese SH1_37a5e581:

    # "I ended up shouted at her. I couldn't believe it."
    "僕は叫んだ。信じられなかった。"

# game/dialogs.rpy:1549
translate japanese SH1_024d5fe2:

    # "My outburst made her...I mean him a bit scared. He stepped back a little."
    "僕の叫びが彼女を、いや…彼を、少し怖がらせてしまったようだ。彼は後ずさりした。"

# game/dialogs.rpy:1550
translate japanese SH1_12900b8c:

    # hero "「Sorry... Sorry, Sakura-chan, I didn't mean to scare you..."
    hero "「ごめん…ごめん、咲良ちゃん、怖がらせる気は無かったんだ…」"

# game/dialogs.rpy:1551
translate japanese SH1_a613c480:

    # hero "「Huh...no, I... I mean, Sakura-kun...? Huh... Aaaaargh!!"
    hero "「ああ、いや…咲良くん…ハ…ハハ…アア…アアアアアアアア！！」"

# game/dialogs.rpy:1552
translate japanese SH1_32d25ac1:

    # "My mind felt like it split into a million pieces."
    "混乱して目が回ってきた。"

# game/dialogs.rpy:1553
translate japanese SH1_e0277eeb:

    # hero "「It's... incredible... You..."
    hero "「そんなの…」"

# game/dialogs.rpy:1554
translate japanese SH1_00287bf2:

    # hero "「But... You know, I..."
    hero "「でも…やっぱり…」"

# game/dialogs.rpy:1559
translate japanese SH1_a783e872:

    # "As if reading my mind, Sakura turned around, facing the sun again, and he... or she... started to calmly speak."
    "As if reading my mind, Sakura turned around, facing the sun again, and he... or she... started to calmly speak."

# game/dialogs.rpy:1561
translate japanese SH1_b0e013d4:

    # s "「I was born with a very rare... {w}Well, I don't know if it's a genital disorder or something..."
    s "「私は、特殊な子として生まれました。私自身あまり良く分かっていませんが…」"

# game/dialogs.rpy:1562
translate japanese SH1_41913518:

    # s "「Technically, I inherited all the chromosomes of my mother, but I got a Y chromosome anyway."
    s "「お医者さんは染色体がどうとか…よく分かりません。」"

# game/dialogs.rpy:1563
translate japanese SH1_f7622dac:

    # s "「It made me a very girlish boy. So girlish that even my brain is one of a girl."
    s "「私は女の子みたいな男の子に育ちました。そして、私の心も…」"

# game/dialogs.rpy:1564
translate japanese SH1_20638c61:

    # s "「I was a normal boy when I was young, but my mind and body transformed me as I grew older."
    s "「小さい時は男の子として暮らしていました。でも成長するにつれ、心も身体も変化していきました。」"

# game/dialogs.rpy:1565
translate japanese SH1_1cb3b079:

    # s "「I was more attracted by dolls than little cars, and I preferred talking with girls than playing soccer with the boys..."
    s "「おもちゃの車よりお人形が好きで、男の子とサッカーをするより女の子とお喋りする方が好きだったんです。」"

# game/dialogs.rpy:1566
translate japanese SH1_425c4b72:

    # s "「When I was 11-years-old, my dad started to get exhausted of this... \"joke\", that change inside of me... So he started getting me back into \"male stuff\"..."
    s "「11歳の時、父はそれに耐えられなくなりました。そして私を男に戻そうとしたんです。」"

# game/dialogs.rpy:1567
translate japanese SH1_c69a2dcb:

    # s "「He bought me outfits and books usually made for boys..."
    s "「父は私に、男向けの服や本を買ってくれました…」"

# game/dialogs.rpy:1568
translate japanese SH1_31185f69:

    # s "「But as time passed by...he started to drink more... and his alcohol addiction grew. Ge started to become more violent."
    s "「それでも父はアルコール中毒が悪化するに連れ暴力的になっていき…」"

# game/dialogs.rpy:1569
translate japanese SH1_c8d0b471:

    # s "「He even tried to cut my hair once."
    s "「終いには私の髪を無理やり切ろうとしたり…」"

# game/dialogs.rpy:1570
translate japanese SH1_f480d83d:

    # s "「But my mother protected me. She has always accepted me for who I am, no matter what."
    s "「でも、そんな時は母が守ってくれました。母はいつも私の選択を尊重してくれました。」"

# game/dialogs.rpy:1571
translate japanese SH1_64529bc1:

    # s "「I can't help it. {p}I feel like a girl inside, that's all."
    s "「私は…私の中の女の子を抑えられないんです。」"

# game/dialogs.rpy:1572
translate japanese SH1_f674ff4c:

    # s "「Actually, my father is really pissed off at me. My mother tries to protect me but my father becomes crazy when I'm around..."
    s "「父は私の事を良く思っていません。私はいつも母に守って貰っています。」"

# game/dialogs.rpy:1573
translate japanese SH1_b935cc96:

    # "I nodded, taking in her...his monologue slowly, digesting his story..."
    "僕は彼のモノローグをゆっくりと聞きながら頷いていた…"

# game/dialogs.rpy:1575
translate japanese SH1_0317f7d7:

    # "I didn't know what to do..."
    "どうすれば良いんだ…"

# game/dialogs.rpy:1576
translate japanese SH1_03c57d51:

    # "His...her life must have been pretty tough growing up...but..."
    "何にせよ、あまり良い人生を送ってこなかったらしい…"

# game/dialogs.rpy:1577
translate japanese SH1_20a5c853:

    # hero "「So you want to be a girl. No matter what everybody thinks, and whatever your own body thinks, you want to be a girl and that's all."
    hero "「誰が何と言おうと、身体がどう思おうと、女の子に成りたい。そういう事…？」"

# game/dialogs.rpy:1578
translate japanese SH1_10c7cae4:

    # s "「Yes...more than anything."
    s "「そ。」"

# game/dialogs.rpy:1579
translate japanese SH1_71387f7a:

    # hero "「Hmm..."
    hero "「うーん…」"

# game/dialogs.rpy:1580
translate japanese SH1_60e224a2:

    # "I thought for a moment...and I finally smiled and accepted it."
    "僕は暫し考えたが、やっぱり…笑って受け入れる事にした。"

# game/dialogs.rpy:1581
translate japanese SH1_c35613b6:

    # "After all... Maybe she's a boy, but Sakura is still Sakura."
    "…それに、たとえ男だったとしても、咲良は咲良じゃないか。そう、そうだろう？"

# game/dialogs.rpy:1582
translate japanese SH1_de287c57:

    # hero "「Hmm okay... It's okay for me..."
    hero "「ふう…ええ、僕は別に…構いませんよ。」"

# game/dialogs.rpy:1583
translate japanese SH1_5c82e516:

    # s "「Really?"
    s "「…本当？」"

# game/dialogs.rpy:1584
translate japanese SH1_456228e3:

    # hero "「Yeah! If you're happy like this, then I'm happy for you as well!"
    hero "「…ええ！どうであれ、あなたがこうやって幸せそうにしていると僕も幸せです。」"

# game/dialogs.rpy:1585
translate japanese SH1_923bc3f8:

    # "Sakura was still facing away from me, she still seemed a little unsatisfied."
    "咲良は未だに向こうを向いたままだ。"

# game/dialogs.rpy:1586
translate japanese SH1_8a085455:

    # s "「You must have some questions at least, don't you?"
    s "「幾つか聞きたい事があるんじゃないんですか？」"

# game/dialogs.rpy:1587
translate japanese SH1_bc81f33c:

    # "Sheesh...she's right, I do have a lot of questions. It's scary how he...uh...she can read my mind."
    "その通りだ。ん、どうして分かったんだ？"

# game/dialogs.rpy:1588
translate japanese SH1_9614bee5:

    # "She spoke softly."
    "She spoke softly."

# game/dialogs.rpy:1589
translate japanese SH1_3caf349f:

    # s "「Go ahead, please. You're my friend, it's normal to have questions about this. And you have the right to know more."
    s "「どうぞ聞いて下さい。疑問を持つのは当然です。そしてあなたには知る権利があります。」"

# game/dialogs.rpy:1590
translate japanese SH1_64036f15:

    # "I calmed myself down a bit and gave Sakura a smile."
    "僕は笑った。"

# game/dialogs.rpy:1591
translate japanese SH1_9877ea0f:

    # hero "「Okay...{p}So you prefer that everyone say 'she' to you instead of 'he'?"
    hero "「それじゃ…{p}まず、これからも今までみたいに女の子として接してもいいかな？」"

# game/dialogs.rpy:1592
translate japanese SH1_bf9bdd8b_1:

    # s "「Yes."
    s "「ええ。」"

# game/dialogs.rpy:1593
translate japanese SH1_caa3249e:

    # hero "「I see... Alright..."
    hero "「分かった…えっと…」"

# game/dialogs.rpy:1594
translate japanese SH1_20b19b0b:

    # "The problem of she and he is all solved now. It feels as confusion is already going away."
    "The problem of she and he is all solved now. It feels as confusion is already going away."

# game/dialogs.rpy:1595
translate japanese SH1_9ef84186:

    # hero "「And, well... What about your... uhh... b... breasts?"
    hero "「それと…その…む、胸は…どうなって…」"

# game/dialogs.rpy:1596
translate japanese SH1_28e89323:

    # hero "「I see that y-your chest is... Well..."
    hero "「その…君のは結構…ある様に見えるんだけど…」"

# game/dialogs.rpy:1597
translate japanese SH1_7622ed66:

    # s "「Mmm? Oh... {p}Just a bra with some cotton inside..."
    s "「Mmm? Oh... {p}Just a bra with some cotton inside...」"

# game/dialogs.rpy:1598
translate japanese SH1_38942254:

    # hero "「Oh...why do that?"
    hero "「ええ何で…？」"

# game/dialogs.rpy:1599
translate japanese SH1_18c07f8f:

    # s "「I've always wanted real breasts and would like to wear a bra..."
    s "「私はずっと本物のが欲しかったんです。」"

# game/dialogs.rpy:1600
translate japanese SH1_f56274ef:

    # s "「They don't look big as Rika-chan's but it's enough to make me feel like I have breasts."
    s "「They don't look big as Rika-chan's but enough to make me feel I have breasts.」"

# game/dialogs.rpy:1601
translate japanese SH1_972441ce:

    # hero "「Oh, on the same subject...Have you ever thought about doing... surgery?"
    hero "「ああ、そうだ…その、手術とかは考えてるの…？」"

# game/dialogs.rpy:1602
translate japanese SH1_a711c810:

    # s "「Yes, I've thought about it..."
    s "「考えたことはあります…」"

# game/dialogs.rpy:1603
translate japanese SH1_a18ea8c8:

    # s "「But I don't want to... I'm too afraid of surgery!"
    s "「それでも、やっぱり嫌です…その、やっぱり怖いですから…」"

# game/dialogs.rpy:1604
translate japanese SH1_27bb92e4:

    # "The way she said that sounded pretty cute but she seemed pretty frustrated."
    "うん、可愛い。"

# game/dialogs.rpy:1605
translate japanese SH1_b5f35068:

    # "...huh? What the heck am I saying? It's a guy!"
    "…は？俺は一体何を言ってるんだ？相手は男だぞ！！"

# game/dialogs.rpy:1606
translate japanese SH1_d25154cc:

    # "I can't fall in love with a dude! Even if he looks like a girl!"
    "いくら女の子の様に見えるからといって恋に落ちるなんて事は出来ないんだ！"

# game/dialogs.rpy:1607
translate japanese SH1_86c14a59:

    # "Even if he looks like...{w}such a wonderful girl...{w}who seems so fragile in the shadow..."
    "いくらこんなに…{w}こんなに素晴らしくそして今にも壊れてしまいそうな程可愛い女の子に見えるからと言って…"

# game/dialogs.rpy:1608
translate japanese SH1_31273a30:

    # "Ugh dammit! What should I do?! I'm still confused!!!"
    "んんん、どうすればいいんだ！？"

# game/dialogs.rpy:1612
translate japanese SH1_7072f3b4:

    # "No..."
    "駄目だ…"

# game/dialogs.rpy:1613
translate japanese SH1_7eb4948e:

    # "No way... I can't! I am a pure and straight guy..."
    "駄目だよ…僕は男を愛せない。僕は純粋なノンケなんだ…"

# game/dialogs.rpy:1614
translate japanese SH1_5b52ab19:

    # "But whatever...{p}What's the big deal?"
    "でも…{p}これで何が変わると言うんだ？"

# game/dialogs.rpy:1615
translate japanese SH1_81380db8:

    # "It's a boy? Okay. So what? I'm just fixed on her gender."
    "男だって？構わないさ。それがどうした！僕はただ彼女の…彼の性別を訂正するだけだ。"

# game/dialogs.rpy:1616
translate japanese SH1_27c4c879:

    # "But that doesn't change much. I can't love her, but we're still friends!"
    "でもそれは大した問題ではない。愛す事が出来なくても今まで通り友達で居る事は出来る。"

# game/dialogs.rpy:1617
translate japanese SH1_d4af9d2c:

    # "With such a big secret between us, I feel like we're even closer friends now!"
    "With such a big secret between us, I feel like we're even closer friends now!"

# game/dialogs.rpy:1618
translate japanese SH1_2b941147:

    # hero "「Oh...Now I understand why you refused the kiss earlier..."
    hero "「さっきどうしてキスを拒んだか、分かったよ…」"

# game/dialogs.rpy:1619
translate japanese SH1_508719ae:

    # hero "「It's because you didn't want to break my heart, huh?"
    hero "「僕を傷付けるんじゃないかって心配したんだね？」"

# game/dialogs.rpy:1620
translate japanese SH1_e17592e6:

    # s "「Yes, exactly..."
    s "「ええ、そうです…」"

# game/dialogs.rpy:1621
translate japanese SH1_1f18b742:

    # hero "「You know, I feel happy, now."
    hero "「今、僕は幸せな気分だよ。」"

# game/dialogs.rpy:1623
translate japanese SH1_5ab99daa:

    # "Sakura turns around to face me."
    "咲良はこっちを振り向いた。"

# game/dialogs.rpy:1624
translate japanese SH1_51437fd0:

    # s "「Eh? Why is that?"
    s "「えっ。どうして…」"

# game/dialogs.rpy:1625
translate japanese SH1_d89b0eb5:

    # hero "「Because you shared your secret with me. And that you didn't want to break my heart."
    hero "「秘密を教えてくれた事。それに心配してくれた事も。」"

# game/dialogs.rpy:1626
translate japanese SH1_1a3b0477:

    # hero "「I feel like you must trust me a lot. And that means that you consider me a close friend..."
    hero "「僕を信頼してくれているのはよく分かったよ。つまり、それって親友って事だよね。」"

# game/dialogs.rpy:1627
translate japanese SH1_d9d841a7:

    # hero "「I'm happy to be your friend, Sakura-chan."
    hero "「あなたが親友で嬉しいよ、咲良ちゃん。」"

# game/dialogs.rpy:1629
translate japanese SH1_6caf3c5b:

    # "Sakura didn't say anything for a moment..."
    "咲良は暫く喋らなかった。"

# game/dialogs.rpy:1630
translate japanese SH1_bdc6ac8a:

    # "Finally she gave me a big smile. She looked like she was going to cry from happiness."
    "そして、微笑んだ。"

# game/dialogs.rpy:1633
translate japanese SH1_3f0e1dcd:

    # s "「Thank you, %(stringhero)s-kun!"
    s "「ありがとう、%(stringhero)s！」"

# game/dialogs.rpy:1634
translate japanese SH1_9db5fc3e:

    # s "「I'm so glad that you took the time to understand me! {p}And I'm happy to be your friend, too!"
    s "「理解してくれて嬉しいです！私も、友達で嬉しいですよ！」"

# game/dialogs.rpy:1635
translate japanese SH1_cc14d6a4:

    # hero "「I promise, I'll keep your secret safe."
    hero "「秘密は絶対守ると、約束します。」"

# game/dialogs.rpy:1636
translate japanese SH1_639e5f3c:

    # "We held our pinky fingers together to seal the promise. We both gave each other a big smile."
    "指きりした。そして笑い合った。"

# game/dialogs.rpy:1637
translate japanese SH1_3c805b45:

    # "Maybe she's a he... but she's a friend first and foremost."
    "「彼女」は「彼」かも知れない…それでも最初の、それに一番の親友である事に変わりはない。"

# game/dialogs.rpy:1643
translate japanese SH1_c8994519:

    # "We reached her house talking happily along the way about a variety of things."
    "僕達は彼女の家に向かった。"

# game/dialogs.rpy:1644
translate japanese SH1_fa80129a:

    # "We were enjoying our time, as the best of friends."
    "友達として、楽しい時間を過ごした。"

# game/dialogs.rpy:1645
translate japanese SH1_d62f35ea:

    # hero "「I wonder how Rika-chan will react when she realizes I know your secret too."
    hero "「僕も秘密を知った事を梨花ちゃんに伝えたらどうなるのかと考えた。」"

# game/dialogs.rpy:1647
translate japanese SH1_589baf2e:

    # s "「Teehee! She will probably try to hit on you, then! {image=heart.png}"
    s "「殴梨花かりますよ、多分。ふふっ。 {image=heart.png}」"

# game/dialogs.rpy:1648
translate japanese SH1_d5bffef3:

    # hero "「Gah! That's a bit cruel, Sakura-chan!"
    hero "「そんな!」"

# game/dialogs.rpy:1650
translate japanese SH1_b112bb86:

    # hero "「...Although you're probably right..."
    hero "「...Although you're probably right...」"

# game/dialogs.rpy:1651
translate japanese SH1_2a493962:

    # "We both laughed."
    "We both laughed."

# game/dialogs.rpy:1653
translate japanese SH1_7a9a62fd:

    # "Something crossed my mind though..."
    "Something crossed my mind though..."

# game/dialogs.rpy:1655
translate japanese SH1_2100c01f:

    # hero "「Sakura-chan?"
    hero "「Sakura-chan?」"

# game/dialogs.rpy:1656
translate japanese SH1_282f01ee:

    # hero "「You know..."
    hero "「You know...」"

# game/dialogs.rpy:1657
translate japanese SH1_e9e23b04:

    # hero "「Nanami-chan...{p}She really seems to care about you. More than you think."
    hero "「Nanami-chan...{p}She really seems to care about you. More than you think.」"

# game/dialogs.rpy:1658
translate japanese SH1_82f05805:

    # hero "「So... Maybe it's time for her to know the truth as well."
    hero "「So... Maybe it's time for her to know the truth as well.」"

# game/dialogs.rpy:1660
translate japanese SH1_37bc0b4d:

    # "Sakura started to think."
    "Sakura started to think."

# game/dialogs.rpy:1661
translate japanese SH1_eef310db:

    # s "「I know...{p}I really do want to tell her..."
    s "「I know...{p}I really do want to tell her...」"

# game/dialogs.rpy:1662
translate japanese SH1_08cb90bc:

    # s "「The fact is... I'm so scared of her reaction..."
    s "「The fact is... I'm so scared of her reaction...」"

# game/dialogs.rpy:1663
translate japanese SH1_337166d2:

    # hero "「Hmmm...I can understand."
    hero "「Hmmm...I can understand.」"

# game/dialogs.rpy:1664
translate japanese SH1_93f8ec4b_1:

    # s "「Well..."
    s "「Well...」"

# game/dialogs.rpy:1665
translate japanese SH1_d9c81ff9:

    # s "「I don't know..."
    s "「I don't know...」"

# game/dialogs.rpy:1666
translate japanese SH1_aac62100:

    # hero "「There's no reason to be scared, Sakura-chan."
    hero "「There's no reason to be scared, Sakura-chan.」"

# game/dialogs.rpy:1667
translate japanese SH1_45627ed5:

    # hero "「Listen..."
    hero "「Listen...」"

# game/dialogs.rpy:1669
translate japanese SH1_ad02dfe3:

    # hero "「I'll talk to Rika-chan. Next Monday, let's tell Nanami-chan when we're all together at the club."
    hero "「I'll talk to Rika-chan. Next Monday, let's tell Nanami-chan when we're all together at the club.」"

# game/dialogs.rpy:1670
translate japanese SH1_82ea3c85:

    # hero "「Rika and I will be there to support you."
    hero "「Rika and I will be there to support you.」"

# game/dialogs.rpy:1671
translate japanese SH1_f63d0a16:

    # hero "「Nanami-chan is your close friend, right? She has the right to know too. I'm sure she'll understand."
    hero "「Nanami-chan is your close friend, right? She has the right to know too. I'm sure she'll understand.」"

# game/dialogs.rpy:1672
translate japanese SH1_c91ce444:

    # hero "「You must take your life in your own hands, Sakura-chan!!!"
    hero "「You must take your life in your own hands, Sakura-chan!!!」"

# game/dialogs.rpy:1673
translate japanese SH1_938668c5:

    # "Sakura laughed at the lame line I said and finally smiled."
    "Sakura laughed at the lame line I said and finally smiled."

# game/dialogs.rpy:1675
translate japanese SH1_757d342d:

    # s "「Alright. Let's do this!"
    s "「Alright. Let's do this!」"

# game/dialogs.rpy:1676
translate japanese SH1_6a86d9b7:

    # hero "「On Monday then."
    hero "「On Monday then.」"

# game/dialogs.rpy:1677
translate japanese SH1_47e672b9:

    # hero "「Don't forget: Rika and I will be there for you. You won't be alone!"
    hero "「Don't forget: Rika and I will be there for you. You won't be alone!」"

# game/dialogs.rpy:1678
translate japanese SH1_69496358:

    # s "「Thank you, %(stringhero)s-kun!{p}I'll be as brave as I can!"
    s "「Thank you, %(stringhero)s-kun!{p}I'll be as brave as I can!」"

# game/dialogs.rpy:1681
translate japanese SH1_e029963b:

    # "She giggled and I waved goodbye to her as she went back to her house."
    "彼女は笑って、家に戻っていった。"

# game/dialogs.rpy:1682
translate japanese SH1_5feddf4e:

    # "She is really unique. I'm glad to have a friend like her."
    "本当にユニークな子だ。彼女と友達で良かったと思った。"

# game/dialogs.rpy:1683
translate japanese SH1_8534ca42:

    # ". . ."
    "……。"

# game/dialogs.rpy:1684
translate japanese SH1_aed2ad78:

    # "Although, I was a kinda depressed... I was so in love with her..."
    "Although, I was a kinda depressed... I was so in love with her..."

# game/dialogs.rpy:1685
translate japanese SH1_051a7c18:

    # "My heart was a bit broken..."
    "My heart was a bit broken..."

# game/dialogs.rpy:1686
translate japanese SH1_7af2582c:

    # "But strangely, I wasn't completely devastated."
    "But strangely, I wasn't completely devastated."

# game/dialogs.rpy:1687
translate japanese SH1_12bd407a:

    # "Despite the fact we can't be lovers, I feel a lot closer to her."
    "Despite the fact we can't be lovers, I feel a lot closer to her."

# game/dialogs.rpy:1688
translate japanese SH1_46bb790b:

    # "Plus, it looked like she felt the same... she didn't look that sad either."
    "Plus, it looked like she felt the same... she didn't look that sad either."

# game/dialogs.rpy:1689
translate japanese SH1_a20cefa7_1:

    # "..."
    "..."

# game/dialogs.rpy:1690
translate japanese SH1_8d8f367d:

    # "Nah... Everything is just fine.{p}Fine and even better."
    "Nah... Everything is just fine.{p}Fine and even better."

# game/dialogs.rpy:1696
translate japanese SH1_e3ea38e8:

    # "Yeah, okay... It's a guy..."
    "ああ…そうだ、男だ。"

# game/dialogs.rpy:1697
translate japanese SH1_40db10a1:

    # "So what？？？"
    "でもそれがどうした？？"

# game/dialogs.rpy:1698
translate japanese SH1_d6afd40a:

    # "Who said that love can only work with the opposite gender!?"
    "異性にしか恋をしてはいけないなんて、一体誰が決めたんだ！？"

# game/dialogs.rpy:1699
translate japanese SH1_4158b541:

    # "I loved Sakura since the start. I desired her since the beginning."
    "僕は始めから好きだった。最初から彼女を求めていたんだ。"

# game/dialogs.rpy:1700
translate japanese SH1_12080c5e:

    # "She told me that she's a boy, but I still love and want her! So be it!"
    "男だって伝えてくれた。でも、それでも僕は恋焦がれているんだ！一体何がいけないんだ！？"

# game/dialogs.rpy:1701
translate japanese SH1_a337f545:

    # "I love Sakura-chan, gender doesn't matter!"
    "僕は咲良ちゃんが好きだ。それが「彼」であろうと、「彼女」であろうと！"

# game/dialogs.rpy:1702
translate japanese SH1_b5134f34:

    # hero "「I understand, now, why you rejected me when I tried to kiss you..."
    hero "「さっきどうしてキスを拒んだか、分かったよ。」"

# game/dialogs.rpy:1703
translate japanese SH1_c7f3c846:

    # s "「Yes..."
    s "「そ。」"

# game/dialogs.rpy:1704
translate japanese SH1_690e9f0a:

    # "I smiled and started to come closer to her."
    "僕は静かに近づいた。"

# game/dialogs.rpy:1705
translate japanese SH1_a66f34d1:

    # s "「As a girl in soul, I prefer boys to girls,"
    s "「私、普通の女の子みたいに、男の子が好きなんです…」"

# game/dialogs.rpy:1706
translate japanese SH1_a5e73d0f:

    # s "「But I was afraid to break our hearts if I do this..."
    s "「それでも、あなたを傷付けるのが怖くて…」"

# game/dialogs.rpy:1707
translate japanese SH1_d4993a91:

    # "She felt that I was behind her so she turned around."
    "近づく僕に、彼女は振り向きながら続ける。"

# game/dialogs.rpy:1708
translate japanese SH1_ef87d060:

    # s "「I didn't want to lose my friend, and-"
    s "「友達を失うのが怖かったんです！それに――」"

# game/dialogs.rpy:1711
translate japanese SH1_5cc40c7f:

    # "I don't even let her finish her sentence."
    "最後まで言わせなかった。"

# game/dialogs.rpy:1712
translate japanese SH1_747a95a0:

    # "I took her in my arms and kissed her."
    "抱き締め、キスをした。"

# game/dialogs.rpy:1713
translate japanese SH1_de5e4ab3:

    # "She tried to step back for a second, {p}but finally she gave in."
    "彼女は一瞬驚いて、固くなった{p}が、やがて力を抜いて僕を受け入れた。"

# game/dialogs.rpy:1714
translate japanese SH1_ddcc906a:

    # "Our lips were lovingly united."
    "唇同士が優しく結び合う。"

# game/dialogs.rpy:1715
translate japanese SH1_96042711:

    # "Yes... I love her."
    "そうだ…僕は好きなんだ。"

# game/dialogs.rpy:1716
translate japanese SH1_5b84a0a9:

    # "I don't care about what people say.{p}She's a girl and I love her. It's as simple as that."
    "周りが何と言おうと構わない…彼女は確かに女の子だ。{p}僕は好きなんだ…"

# game/dialogs.rpy:1718
translate japanese SH1_21f57c0f:

    # "We stopped kissing and Sakura-chan looked me in the eyes."
    "キスを止め、咲良ちゃんが潤んだ瞳で僕を見つめてくる。"

# game/dialogs.rpy:1719
translate japanese SH1_7673b5e7:

    # "I found myself swimming into the endless ocean of beauty her gaze offered me..."
    "瞳の奥の大海に吸い込まれて行くかの如く…"

# game/dialogs.rpy:1720
translate japanese SH1_04263bd9:

    # s "「... %(stringhero)s... -kun..."
    s "「……%(stringhero)s…くん…」"

# game/dialogs.rpy:1721
translate japanese SH1_752a2eec:

    # hero "「... Sa... Sakura-san..."
    hero "「…さ…咲良ちゃん…」"

# game/dialogs.rpy:1730
translate japanese SH1_aab57b73:

    # "We kissed again, but this time, even more intensely."
    "その後、僕らは更に焼ける様なキスを交わした。"

# game/dialogs.rpy:1731
translate japanese SH1_d01f1362:

    # "I found my hands were running down her body. Her body was so womanly, so perfect."
    "手を彼女の身体に沿って滑らせていく…何て女の子らしくて、何て素晴らしい…"

# game/dialogs.rpy:1732
translate japanese SH1_b11b14d6:

    # "Her fingers were ruffling my hair as she was held me tighter."
    "彼女が僕の髪を触り、そして僕をギュッと抱きしめた。"

# game/dialogs.rpy:1733
translate japanese SH1_fd13e895:

    # "We stayed in the rice fields for a long while. We just couldn't stop..."
    "僕達は長いこと麦畑で静かに抱きしめ合って、時々キスをして…"

# game/dialogs.rpy:1734
translate japanese SH1_4237d00f:

    # "Even though my head was spinning a bit from the situation, I still couldn't stop giving her my love..."
    "Even though my head was spinning a bit from the situation, I still couldn't stop giving her my love..."

# game/dialogs.rpy:1735
translate japanese SH1_a7d8f7db:

    # "The sun began to set and filled the sky with a beautiful shade of orange as the evening arrived."
    "…気付けばすっかり太陽は橙色になり、夕方になっていた。"

# game/dialogs.rpy:1739
translate japanese SH1_5b186bfb:

    # "It was getting late. I had to let Sakura return home. We held hands as we walked to her house."
    "一緒に手を繋いで、家に帰ることにした。"

# game/dialogs.rpy:1740
translate japanese SH1_0163bcac:

    # "We were silent. There was no need for words. We were just enjoying every moment together."
    "静かな帰り道だった。二人の間に言葉は必要無かった。ただ恋人と二人で一緒に居るだけで、幸せだった。"

# game/dialogs.rpy:1741
translate japanese SH1_94a25b9b:

    # "My heart was still racing. I was overwhelmed with joy."
    "胸が痛くなる程までにドキドキしていた。それでも、構わない。"

# game/dialogs.rpy:1742
translate japanese SH1_32219c59:

    # "Suddenly, Sakura held my arm tighter."
    "握った咲良ちゃんの手の温もりを感じる…"

# game/dialogs.rpy:1743
translate japanese SH1_15b1a829:

    # s "「I wonder what Rika-chan will say when she finds out about us!"
    s "「梨花ちゃんが僕達を見つけたら何て言うかな…」"

# game/dialogs.rpy:1745
translate japanese SH1_164ae856:

    # s "「Knowing her, she might try to kill you! Haha!"
    s "「殺しにかかるかもしれません。」"

# game/dialogs.rpy:1746
translate japanese SH1_d0bb62e3:

    # hero "「...That's kinda scary to think about..."
    hero "「ああ…」"

# game/dialogs.rpy:1747
translate japanese SH1_d7c1b42b:

    # s "「In fact, I've always wondered if she was in love with me or something..."
    s "「それに、梨花ちゃんも秘密を知る仲ですし…」"

# game/dialogs.rpy:1748
translate japanese SH1_555aded3:

    # s "「She is the Rika-chan you know since she knows my secret."
    s "「実は私に気がある素振りを見せる事があるんです。」"

# game/dialogs.rpy:1749
translate japanese SH1_e7066814:

    # hero "「Really?"
    hero "「ええ？」"

# game/dialogs.rpy:1750
translate japanese SH1_2e63f4b7:

    # hero "「Heh... To be honest... She kinda scares me more, now!"
    hero "「ひゃー…それはまたこれから大変になりそうな…」"

# game/dialogs.rpy:1751
translate japanese SH1_edeec3ff:

    # "Sakura-chan giggles."
    "咲良ちゃんは笑った。"

# game/dialogs.rpy:1752
translate japanese SH1_c9ae305b:

    # s "「Don't worry, I'll protect you! {image=heart.png}"
    s "「心配しないで下さい。何とかなりますよ！ {image=heart.png}」"

# game/dialogs.rpy:1753
translate japanese SH1_9554a6c1:

    # "As we approached the front of her house, she rubbed her head against my shoulder."
    "僕の肩に頭をこすりながら言った。そして僕達は家に着いた。"

# game/dialogs.rpy:1755
translate japanese SH1_ee198ceb:

    # "I couldn't resist her touch. I took her in my arms and kissed her again."
    "手で催促され、もう一度抱きしめてからキスをした。"

# game/dialogs.rpy:1756
translate japanese SH1_ab476564:

    # "She softly kissed me back."
    "彼女は優しくキスを返した。"

# game/dialogs.rpy:1758
translate japanese SH1_8944b322:

    # "When she stopped, she smiled, squeezed my butt and whispered in my ear."
    "僕の尻を手で撫でて、耳元で囁いた。"

# game/dialogs.rpy:1759
translate japanese SH1_cc8b4089:

    # s "「See you tomorrow... sweetheart! {image=heart.png}"
    s "「また明日… あなた！ {image=heart.png}」"

# game/dialogs.rpy:1760
translate japanese SH1_30f6ec48:

    # hero "「Heh, you're acting more like a guy when you do stuff like that!"
    hero "「こういう時は結構男っぽいんですね…」"

# game/dialogs.rpy:1761
translate japanese SH1_f2fe27d5:

    # s "「Meanie! {image=heart.png}"
    s "「もう！ {image=heart.png}」"

# game/dialogs.rpy:1763
translate japanese SH1_31fe93a4:

    # "She sticked her tongue out at me before disappearing into her house..."
    "彼女は舌を出して家に入っていった…"

# game/dialogs.rpy:1765
translate japanese SH1_f5ae9218:

    # "I... don't know what happened within me..."
    "うーん放心状態だ。"

# game/dialogs.rpy:1766
translate japanese SH1_506f97de:

    # "I was flooding with energy and happiness!"
    "とにかく、その時の僕はエネルギーと多幸感でいっぱいだった。"

# game/dialogs.rpy:1767
translate japanese SH1_9e8998ef:

    # "I ran along the streets."
    "家に帰るまで、僕は走った。"

# game/dialogs.rpy:1768
translate japanese SH1_3f79e021:

    # "I ran, I ran, I ran. As fast as I could. Overwhelmed with joy."
    "出来る限り速く…走って、走って、走った。"

# game/dialogs.rpy:1769
translate japanese SH1_91e3f3ea:

    # "I wanted to sing. I wanted to laugh. I wanted to dance."
    "歌いたい。笑いたい。踊りたい。"

# game/dialogs.rpy:1770
translate japanese SH1_eb6be300:

    # "I felt completely crazy!!!"
    "僕は完璧にイってしまった。"

# game/dialogs.rpy:1771
translate japanese SH1_375cf97b:

    # "I had never felt that kind of euphoria before..."
    "こんな幸福感は感じたことが無い。"

# game/dialogs.rpy:1773
translate japanese SH1_e1e2560b:

    # "As I was running home, I passed by Nanami who was coming back from groceries."
    "As I was running home, I passed by Nanami who was coming back from groceries."

# game/dialogs.rpy:1774
translate japanese SH1_6bbff298:

    # n "「Hey, %(stringhero)s-senpai! What's cookin'?"
    n "「Hey, %(stringhero)s-senpai! What's cookin'?"

# game/dialogs.rpy:1775
translate japanese SH1_bfe48993:

    # "I lifted her off the ground like a small kitten and spun with her around happily!"
    "I lifted her off the ground like a small kitten and spun with her around happily!"

# game/dialogs.rpy:1776
translate japanese SH1_0da09179:

    # hero "「I'm so happy, Nanami-chan! So so so so so so {b}SO{/b} happy!"
    hero "「I'm so happy, Nanami-chan! So so so so so so {b}SO{/b} happy!」"

# game/dialogs.rpy:1778
translate japanese SH1_7e7bd642:

    # n "「Eeeeek! W-what the heck!? W-w-why you're so happy? You're acting crazy!"
    n "「Eeeeek! W-what the heck!? W-w-why you're so happy? You're acting crazy!」"

# game/dialogs.rpy:1779
translate japanese SH1_c2832101:

    # hero "「It's because I..."
    hero "「It's because I...」"

# game/dialogs.rpy:1780
translate japanese SH1_192f06a3:

    # "I put back Nanami on the ground."
    "I put back Nanami on the ground."

# game/dialogs.rpy:1781
translate japanese SH1_00ab9ecc:

    # hero "「No, wait- I don't want to spoil the surprise."
    hero "「No, wait- I don't want to spoil the surprise.」"

# game/dialogs.rpy:1782
translate japanese SH1_99c8156c:

    # hero "「I'll tell you at school on Monday alongside Rika-chan!"
    hero "「I'll tell you at school on Monday alongside Rika-chan!」"

# game/dialogs.rpy:1784
translate japanese SH1_a79e612d:

    # n "「Monday?! But that's too looooong!"
    n "「Monday?! But that's too looooong!」"

# game/dialogs.rpy:1785
translate japanese SH1_380da8df:

    # n "「Now I really want to know!"
    n "「Now I really want to know!」"

# game/dialogs.rpy:1786
translate japanese SH1_5f2414ee:

    # hero "「Patience, little one, patience!"
    hero "「Patience, little one, patience!」"

# game/dialogs.rpy:1788
translate japanese SH1_f5bde8dc:

    # "I continued running back home, screaming \"WOOOOOOOOOOOOO!\" out loud. I heard a scream from Nanami-"
    "I continued running back home, screaming \"WOOOOOOOOOOOOO!\" out loud. I heard a scream from Nanami-"

# game/dialogs.rpy:1790
translate japanese SH1_84fa99ca:

    # n "「{size=+15}I'M NOT LITTLE!{/size}"
    n "「{size=+15}I'M NOT LITTLE!{/size}」"

# game/dialogs.rpy:1792
translate japanese SH1_d69bfabe:

    # "My gosh, what a day...!"
    "ああ、何という日だ…"

# game/dialogs.rpy:1793
translate japanese SH1_0d4f6adf:

    # "Sakura called earlier. I'm going on another date with her tomorrow!"
    "そういえば明日のデートに誘われたのだが、この調子だとどうなってしまうか心配だ。"

# game/dialogs.rpy:1794
translate japanese SH1_dece2bab:

    # "I was about to ask her out on a date myself...looks like she beat me to it."
    "実は僕もさっき誘おうとしていたんだけどね。"

# game/dialogs.rpy:1795
translate japanese SH1_e91f1c64:

    # "Maybe we can read each other's minds now that we're lovers! Anyway...I better get some rest."
    "お互い心まで通じ合えるカップルなんて、そうそう居ないと思わないか？"

# game/dialogs.rpy:1805
translate japanese sceneS1_b5e51d3c:

    # "I was waiting for Sakura at the train station of the village."
    "僕は村の駅で咲良ちゃんを待っていた。"

# game/dialogs.rpy:1806
translate japanese sceneS1_6f70efee:

    # "I was getting used to these trips, now. But I'm a bit tired today."
    "この旅行には多少慣れた。でも今日は既にちょっと疲れている…"

# game/dialogs.rpy:1807
translate japanese sceneS1_6a4ff20d:

    # "I couldn't sleep at all last night...I was full of excitement thinking about our lovely date."
    "昨日はデートの事を考え過ぎるあまり一睡も出来なかったのだ…"

# game/dialogs.rpy:1808
translate japanese sceneS1_32c4a28b:

    # "Then a joyful Sakura appeared! She was listening to music on a CD player."
    "咲良ちゃんがCDプレーヤーで音楽を聴きながら楽しそうにやってきた。"

# game/dialogs.rpy:1809
translate japanese sceneS1_cfc9f30e:

    # "When she saw me, a big smile filled her face. She turned off the CD player and walked toward me."
    "そしてこっちを見て、ニッコリと笑った。CDを止め、歩いて向かってくる。"

# game/dialogs.rpy:1811
translate japanese sceneS1_641c11ba:

    # s "「%(stringhero)s-kun!! Sorry for the wait! {image=heart.png}"
    s "「%(stringhero)sくん！お〜ま〜 た〜 せ〜！ {image=heart.png}」"

# game/dialogs.rpy:1812
translate japanese sceneS1_a5707cad:

    # hero "「Good morning, Sakura-chan!"
    hero "「おはよう、咲良ちゃん！」"

# game/dialogs.rpy:1814
translate japanese sceneS1_bd4c0f66:

    # "We embraced each other with a kiss."
    "僕達はキスをした。"

# game/dialogs.rpy:1815
translate japanese sceneS1_a85140f3:

    # "Darn, I missed these sweet lips so much."
    "会いたかった…この甘い唇に会いたかったよ。"

# game/dialogs.rpy:1816
translate japanese sceneS1_0e294c8c:

    # "I had a feeling she had put on a little more makeup on her face for the occasion."
    "I had a feeling she had put on a little more makeup on her face for the occasion."

# game/dialogs.rpy:1818
translate japanese sceneS1_2c25f5b4:

    # hero "「You look wonderful today Sakura-chan."
    hero "「今日は何だか一段と素晴らしく見えるよ、咲良ちゃん。」"

# game/dialogs.rpy:1819
translate japanese sceneS1_280a32f7:

    # "She was overflowing with happiness."
    "本当に素晴らしい…まるで幸福が身体から溢れ出すかの様に輝いて見えた。"

# game/dialogs.rpy:1820
translate japanese sceneS1_f3a61c54:

    # "I have never seen her so happy before."
    "僕も今まででこんなに楽しそうな咲良ちゃんは見た事が無かった。"

# game/dialogs.rpy:1822
translate japanese sceneS1_1bc30b12:

    # s "「Oh, does that mean I'm not pretty everyday...?"
    s "「ふーん、それって他の日は可愛くないって事ですか?」"

# game/dialogs.rpy:1823
translate japanese sceneS1_103429d2:

    # hero "「Ah! N-no, I mean, you're as pretty as usual... I mean you are always..."
    hero "「アア！いや違う、違うんだ。僕が言いたいのはつまり毎日…えーとつまり君はその恒久的に…」"

# game/dialogs.rpy:1825
translate japanese sceneS1_ee5f4b9e:

    # "Sakura gave me a mischievous grin and gave me a quick kiss on the lips."
    "咲良ちゃんは悪戯っぽく笑って僕にキスをした。"

# game/dialogs.rpy:1826
translate japanese sceneS1_7641d5e7:

    # s "「Silly... I'm just messing with you! {image=heart.png}"
    s "「もうバカ…少し冗談を言ってみただけですよ！ {image=heart.png}」"

# game/dialogs.rpy:1827
translate japanese sceneS1_fe43f145:

    # hero "「Gah! That's too cruel! {image=heart.png}"
    hero "「あっちゃー…！ {image=heart.png}」"

# game/dialogs.rpy:1828
translate japanese sceneS1_73ac0c99:

    # "We laughed together as the train arrived in the station."
    "お互い笑った。"

# game/dialogs.rpy:1833
translate japanese sceneS1_c93ea523:

    # "We sat on the train, going towards the grand city."
    "街に向かう電車に乗った。"

# game/dialogs.rpy:1834
translate japanese sceneS1_2af5fbd9:

    # "I was wondering about her CD player, so I decided to ask her."
    "CDプレイヤーを見て以来気になっていたので、思い切って聞いてみる事にした。"

# game/dialogs.rpy:1835
translate japanese sceneS1_95d5b90a:

    # hero "「What were you listening to on your CD player?"
    hero "「さっきは何を聴いてたんですか？」"

# game/dialogs.rpy:1836
translate japanese sceneS1_d701c031:

    # s "「Oh, some classical music."
    s "「ああ、これですか…クラシックのCDなんです。」"

# game/dialogs.rpy:1837
translate japanese sceneS1_9071067a:

    # hero "「You like classical music?"
    hero "「クラシック…好きなんですか？」"

# game/dialogs.rpy:1838
translate japanese sceneS1_5ad3424b:

    # s "「I love it!"
    s "「ええ、大好き！」"

# game/dialogs.rpy:1839
translate japanese sceneS1_302806ba:

    # "I'm not especially a fan of that kind of music but I will admit that it's good to listen to sometimes. It can be very relaxing."
    "クラシック…個人的に特に凄い好きだと言う訳では無いが、リラックスしたい時など時々聞きたくなる事はある。"

# game/dialogs.rpy:1840
translate japanese sceneS1_865dd143:

    # hero "「What's your favorite song?"
    hero "「お気に入りの曲とかあるんですか？」"

# game/dialogs.rpy:1842
translate japanese sceneS1_ae336353:

    # "Sakura gave me one side of her headphones and started the CD player."
    "咲良ちゃんはニッコリと、片方のイヤホンを僕に着けて再生してくれた。"

# game/dialogs.rpy:1845
translate japanese sceneS1_8534ca42:

    # ". . ."
    "……。"

# game/dialogs.rpy:1846
translate japanese sceneS1_083198ae:

    # hero "「Oh, I know this song... but I don't remember its name..."
    hero "「ああ…名前は思い出せないけど…確かに聴いた事がある…」"

# game/dialogs.rpy:1847
translate japanese sceneS1_ef73b932:

    # s "「It's the {i}Air Orchestral suite #3{/i} of Bach."
    s "「これは Bach の {i}Air Orchestral suite #3{/i} ですよ。」"

# game/dialogs.rpy:1848
translate japanese sceneS1_00935374:

    # s "「I don't know why... But I love to listen to this when I'm in a good mood..."
    s "「何故か分かりませんが、嬉しい時にこれを聴きたくなるんです…」"

# game/dialogs.rpy:1849
translate japanese sceneS1_6bbb4eeb:

    # hero "「You must be in a pretty good mood then, huh?"
    hero "「そして、あなたは今、幸せか？」"

# game/dialogs.rpy:1850
translate japanese sceneS1_1833fef5:

    # "She doesn't reply. She just leans her head on my shoulder and closes her eyes."
    "返事は無かった。ただ静かに僕の肩に寄梨花かって…"

# game/dialogs.rpy:1851
translate japanese sceneS1_bb3922e5:

    # "I gently lean my head against her."
    "僕もそれをそのまま優しく受け止めた。"

# game/dialogs.rpy:1852
translate japanese sceneS1_54d8e14e:

    # "Her presence, her warmth, her strangely feminine smell, along with the music..."
    "温もり、香り、音楽…彼女の存在そのものを感じている。"

# game/dialogs.rpy:1853
translate japanese sceneS1_b2bf1fcc:

    # "I wish this train would never stop and that we could stay like this forever."
    "この電車が永遠に止まる事無く、このままこうして居られる事を願った。"

# game/dialogs.rpy:1858
translate japanese sceneS1_4bdd421c:

    # hero "「...Some things make more sense now that you've told me your secret."
    hero "「秘密を知ってから、納得した事があります。」"

# game/dialogs.rpy:1860
translate japanese sceneS1_49c95ed0:

    # s "「What do you mean?"
    s "「何ですか？」"

# game/dialogs.rpy:1861
translate japanese sceneS1_35d9c8b5:

    # hero "「Well, I finally understand why you like shooting games and harem ecchi manga."
    hero "「ガンシューティングやハーレムecchi漫画が好きだったり…」"

# game/dialogs.rpy:1862
translate japanese sceneS1_1dcf5a89:

    # hero "「You have some more boyish tastes in you, despite that the rest of you is pretty feminine."
    hero "「女の子の心の中に、確かに男の子っぽい部分も有る。」"

# game/dialogs.rpy:1863
translate japanese sceneS1_1ae549d3:

    # s "「Yes, it's true."
    s "「ええ、それは確かです。」"

# game/dialogs.rpy:1864
translate japanese sceneS1_64d14f00:

    # s "「I think I got that from my father."
    s "「その部分は、父に貰いました。」"

# game/dialogs.rpy:1865
translate japanese sceneS1_75219ee1:

    # s "「For my last few birthdays, my father bought me shooting games and ecchi manga."
    s "「シューティングゲームやH漫画は誕生日に貰ったものです。」"

# game/dialogs.rpy:1866
translate japanese sceneS1_0d2a822f:

    # s "「I guess it just stuck with me..."
    s "「その影響がまだ続いているんです…」"

# game/dialogs.rpy:1867
translate japanese sceneS1_e668a3b6:

    # hero "「I see..."
    hero "「うーん成程…」"

# game/dialogs.rpy:1868
translate japanese sceneS1_0676a71e:

    # hero "「How it was when you were younger? Was it difficult?"
    hero "「もっと小さい頃はどうだったんですか？」"

# game/dialogs.rpy:1870
translate japanese sceneS1_b6297f8f:

    # s "「My childhood was kinda chaotic, yes."
    s "「私の子供時代はカオティックでした。」"

# game/dialogs.rpy:1871
translate japanese sceneS1_125b816d:

    # s "「I was a boy like the others during my first years at the kindergarten."
    s "「幼稚園に入って最初の年までは、他の男の子と変わりありませんでした。」"

# game/dialogs.rpy:1872
translate japanese sceneS1_f7b3d45f:

    # s "「But the time passed..."
    s "「でも暫くして…」"

# game/dialogs.rpy:1873
translate japanese sceneS1_cc5f078f:

    # s "「It was clear that I had more girlish tendencies. Very strong ones in fact..."
    s "「ハッキリと女の子みたいな行動をとる様になったんです。」"

# game/dialogs.rpy:1876
translate japanese sceneS1_e1554201:

    # s "「..."
    s "「……。」"

# game/dialogs.rpy:1877
translate japanese sceneS1_146f1b24:

    # s "「When I was going through puberty, I clearly expressed my desire to be considered as a real girl."
    s "「思春期が来て、私は自分が女の子に成りたいと思っている事に気付きました。」"

# game/dialogs.rpy:1878
translate japanese sceneS1_384c09e4:

    # s "「I couldn't help it... My mind was locked in more feminine habits..."
    s "「私にはどうしようもありませんでした…それに丁度その頃、周りの皆と違って、私だけいつまで経っても変声期が来なかったんです。」"

# game/dialogs.rpy:1879
translate japanese sceneS1_eb52cb72:

    # s "「It looked like my own body was confused and didn't know how to evolve. For example, my voice didn't deepen like a male."
    s "「まるで私自身の身体がどう成長して良いのか戸惑っているかの様でした。」"

# game/dialogs.rpy:1880
translate japanese sceneS1_167b4ba2:

    # s "「So I let my hair grow, pierced my ears, started to wear skirts, used bras and even put stuff inside my bra to simulate real breasts..."
    s "「だから私は、髪を伸ばし、スカートを履き、ブラも着け、おしゃれをしてみました。」"

# game/dialogs.rpy:1881
translate japanese sceneS1_2476fd15:

    # s "「When I looked at myself in a mirror dressed like a girl, I knew definitely that I was one. I wanted to be one... And I will be one."
    s "「鏡の中に女の子を見たその時、私はやっと分かったんです。」"

# game/dialogs.rpy:1882
translate japanese sceneS1_e1554201_1:

    # s "「..."
    s "「……。」"

# game/dialogs.rpy:1883
translate japanese sceneS1_a678d317:

    # s "「My mother accepted it well. She loves me more than anything and will accept anything as long as it makes me happy."
    s "「母は私を何より愛してくれていて、何より理解してくれました。私の幸せを尊重してくれたんです。」"

# game/dialogs.rpy:1884
translate japanese sceneS1_6bc7ecb8:

    # s "「But other people didn't want to understand..."
    s "「それでも、他の周りの人には受け入れて貰えませんでした…」"

# game/dialogs.rpy:1885
translate japanese sceneS1_22ca470a:

    # s "「My father, my classmates,..."
    s "「同級生達…それに父…」"

# game/dialogs.rpy:1886
translate japanese sceneS1_f7a73d77:

    # s "「You get the picture...{p}Well to continue on, my first teenage years were horrible..."
    s "「私の10代前半はそれは相当なものでした…」"

# game/dialogs.rpy:1887
translate japanese sceneS1_e0339899:

    # hero "「You mean here? In the village?"
    hero "「ん…？それってここ？この村の話？」"

# game/dialogs.rpy:1888
translate japanese sceneS1_e40cc235:

    # s "「No.{p}We were living in Kyoto before, but we moved out here..."
    s "「いいえ。{p}私たちはここに来る前、京都に住んでました…」"

# game/dialogs.rpy:1889
translate japanese sceneS1_b830f777:

    # s "「When we started our new life, I decided that I would fight to be considered as a girl in the new town."
    s "「そして新しい生活を始める際に、私は女の子として暮らしていこうと決意しました。」"

# game/dialogs.rpy:1890
translate japanese sceneS1_e1a08602:

    # s "「Only my parents and Rika-chan know the secret...{p}And you, of course."
    s "「実際、両親と梨花ちゃんとしかこの秘密を知る人は居ません…{p}あと、もちろんあなたと…」"

# game/dialogs.rpy:1891
translate japanese sceneS1_fdbe45ca:

    # s "「And Nanami-chan, on Monday."
    s "「And Nanami-chan, on Monday.」"

# game/dialogs.rpy:1892
translate japanese sceneS1_e1554201_2:

    # s "「..."
    s "「……。」"

# game/dialogs.rpy:1893
translate japanese sceneS1_92a39690:

    # s "「You know, I..."
    s "「You know, I...」"

# game/dialogs.rpy:1894
translate japanese sceneS1_8e16ee4d:

    # s "「I wish I was a real girl..."
    s "「本当の女の子に成りたかった…」"

# game/dialogs.rpy:1895
translate japanese sceneS1_c10c0ef9:

    # s "「I've always wanted to be a girl. {p}I've always wanted to have long hair.{p}I've always wanted to wear dresses..."
    s "「ずっと女の子に成りたかった。{p}長い髪が欲しかった。{p}奇麗な服が着たかった。」"

# game/dialogs.rpy:1896
translate japanese sceneS1_d8ba4cdd:

    # s "「And I've always wanted... to have a boyfriend..."
    s "「そして…ボーイフレンドが…欲しかった…」"

# game/dialogs.rpy:1897
translate japanese sceneS1_dbdb5e9d:

    # "I listened to her in complete silence."
    "僕は静かに話を聞いていた。"

# game/dialogs.rpy:1898
translate japanese sceneS1_c2573e27:

    # "As she was remembering her childhood memories, I could see some tears were about to come out from her eyes."
    "子供時代を思い出したのか、彼女の目から涙が幾つか零れ落ちてきた。"

# game/dialogs.rpy:1899
translate japanese sceneS1_64770d3e:

    # "I put my hand on her shoulder and kiss her head."
    "彼女の肩に手を置き、頭にやさしくキスをした。"

# game/dialogs.rpy:1900
translate japanese sceneS1_f25fd7bb:

    # hero "「Sakura-chan..."
    hero "「咲良ちゃん…」"

# game/dialogs.rpy:1902
translate japanese sceneS1_1ad3847b:

    # hero "「I know your secret. But I promise you... {p}To me, you'll always be a girl. A real girl..."
    hero "「男だって事を知った今でも…それでも…{p}僕にとってはずっと、女の子のままですよ。」"

# game/dialogs.rpy:1904
translate japanese sceneS1_4ee0f788:

    # hero "「I don't care about what other people think.{p}I don't care what your body thinks."
    hero "「他の人がどう思うかなんて関係ない。{p}あなたの身体がどうであろうと構わない。」"

# game/dialogs.rpy:1905
translate japanese sceneS1_01d48485:

    # hero "「I don't even care if your father hates me someday because of us going out together."
    hero "「いつの日か、あなたのお父さんに嫌われようとも…」"

# game/dialogs.rpy:1906
translate japanese sceneS1_f0c87851:

    # hero "「Sakura-chan...You are a girl and I...{p}And I......{p}I........."
    hero "「咲良ちゃん…あなたは女の子で、僕は…{p}僕は……{p}…………」"

# game/dialogs.rpy:1907
translate japanese sceneS1_2573e7bb:

    # "I never thought it would be so hard to say it, even if you want to express it so much at the same time."
    "こんなに言い難いとは思わなかった。"

# game/dialogs.rpy:1908
translate japanese sceneS1_8dca2ab0:

    # "I forced myself to let it all out."
    "僕はありったけの勇気を振り絞った。"

# game/dialogs.rpy:1909
translate japanese sceneS1_5bec7fb2:

    # hero "「I... Sakura-chan, I lo... I lo......{p}{size=+15}I love you, Sakura-chan!!!{/size}"
    hero "「咲良ちゃん、僕は…僕は……{p}{size=+15}スキだ！！咲良ちゃん！！！！！！！{/size}」"

# game/dialogs.rpy:1910
translate japanese sceneS1_c352284f:

    # "I ended up loudly shouting that out."
    "振り絞り、力の限り叫んだ。"

# game/dialogs.rpy:1911
translate japanese sceneS1_68b19ed1:

    # "Some people in the street were so surprised, they stopped to stare at us."
    "道行く何人かは立ち止まって振り向いた。"

# game/dialogs.rpy:1912
translate japanese sceneS1_1496db50:

    # "I felt terribly embarrassed."
    "…これはかなり…恥ずかしい。"

# game/dialogs.rpy:1913
translate japanese sceneS1_7bb8b5bf:

    # "Even Sakura turned red with embarrassment."
    "咲良ちゃんもやっぱり赤くなっている…"

# game/dialogs.rpy:1914
translate japanese sceneS1_c3bbc64c:

    # "Suddenly I realized the deep sense of what I just said."
    "僕は自分の言った意味を今ハッキリと理解した。"

# game/dialogs.rpy:1915
translate japanese sceneS1_0c128ea7:

    # "Yes... Yes, I love her...{p}And I'm not afraid to say it!!!"
    "ああ…そうだ、僕は恋してるんだ…{p}そしてそれを宣言することを恐れる必要はない！！"

# game/dialogs.rpy:1916
translate japanese sceneS1_5d447c9e:

    # hero "「Yes everyone!! I love her!!! You hear me？？？ I love HER!!!"
    hero "「そうだよ！僕はこの娘が好きなんだ！！なあ皆も聞いただろ！？？{p}僕は、この娘が好きなんだ！！！」"

# game/dialogs.rpy:1917
translate japanese sceneS1_71bc4397:

    # "Strangers just stared at us awkwardly."
    "道を行く誰もがこの子が女の子である事に疑問を抱かないだろう。"

# game/dialogs.rpy:1918
translate japanese sceneS1_df63e596:

    # "Sakura was still blushing... But suddenly..."
    "咲良ちゃんは未だ赤くなったままだったが、唐突に…"

# game/dialogs.rpy:1919
translate japanese sceneS1_0f3f7530:

    # "She started to reply, in the same tone as me... Stuttering and trembling a bit but with the same conviction, and as loud as me!"
    "返事をした。僕と同じトーンで…少し口ごもって、それでも確信を持って、そして同じように大きな声で！"

# game/dialogs.rpy:1920
translate japanese sceneS1_c4c3f14c:

    # s "「Y... Ye...Yes!!!"
    s "「ハ…ハイ！！」"

# game/dialogs.rpy:1921
translate japanese sceneS1_d1b481d2:

    # s "「{size=+10}Yes, I am a girl!...{p}I am a girl and I love you too, %(stringhero)s-kun!!!!{/size}"
    s "「{size=+10}私は、私は女の子です！それに、私もスキです！！！！！%(stringhero)s！！！！！{/size}」"

# game/dialogs.rpy:1922
translate japanese sceneS1_6252a7c4:

    # "People continued to stare at us. But soon, they carried on with their lives, ignoring us."
    "周りの人々が僕達をじっと見ている。"

# game/dialogs.rpy:1923
translate japanese sceneS1_82c8f385:

    # "I rolled my eyes, appalled at the people."
    "僕は周りを見回し、唖然とした。"

# game/dialogs.rpy:1924
translate japanese sceneS1_7e3455d5:

    # hero "「Pfft... People..."
    hero "「は…皆…」"

# game/dialogs.rpy:1925
translate japanese sceneS1_e9431aa5:

    # "Sakura made a strange face... She was holding her mouth while blushing...{p}She was... laughing？？？"
    "咲良ちゃんは妙な感じだ。口を押さえて恥ずかしがっている…{p}…いや、笑っている？"

# game/dialogs.rpy:1927
translate japanese sceneS1_17b9939f:

    # "She starts to laugh loudly. It was a nice laugh. The sound filled me with comfort."
    "彼女は大声で笑い始めた。それは素晴らしいメロディを奏でるかの様な可愛らしい笑い声だった。"

# game/dialogs.rpy:1929
translate japanese sceneS1_cf3fcfe1:

    # "I was a little surprised, but her laugh made me want to laugh as well."
    "僕は驚いた。それでも、何だか僕も笑えてきた。"

# game/dialogs.rpy:1930
translate japanese sceneS1_5da66cff:

    # hero "「Hahaha, what's so funny, Sakura-chan?"
    hero "「一体どうしてそんなに笑ってるんですか？」"

# game/dialogs.rpy:1931
translate japanese sceneS1_8da8d865:

    # "She tried to reply, but couldn't stop laughing."
    "笑いながら返事をしようとしている。"

# game/dialogs.rpy:1932
translate japanese sceneS1_b3fc2700:

    # s "「It's... It's just I'm... I'm relieved now that I said it!!"
    s "「だって、だって…嬉しかったんです！」"

# game/dialogs.rpy:1933
translate japanese sceneS1_22593ea9:

    # s "「And.... the way you rolled your eyes... It was too funny, I couldn't hold it!"
    s "「それに…そんなに目をギョロギョロ見回してるんですもの、可笑くって我慢出来ません！」"

# game/dialogs.rpy:1934
translate japanese sceneS1_917bb3ec:

    # hero "「Hey, that's just how I naturally react!"
    hero "「ちょっと！それってどういう…」"

# game/dialogs.rpy:1935
translate japanese sceneS1_1809902d:

    # s "「Teeheehee! I'm so sorry, %(stringhero)s-kun..."
    s "「ヘヘっ、ごめんなさい、%(stringhero)sくん…」"

# game/dialogs.rpy:1936
translate japanese sceneS1_50395e0e:

    # "I rolled my eyes again without realizing and she started to laugh even louder..."
    "僕はもう一度辺りを見回して、彼女は再び大きく笑った。"

# game/dialogs.rpy:1937
translate japanese sceneS1_36e4217d:

    # "I began to laugh as well."
    "何故かは分からなかったけど、僕も笑いが抑えられなかった。"

# game/dialogs.rpy:1938
translate japanese sceneS1_3b7641fe:

    # "I was so happy."
    "凄く嬉しかった。僕は今、この幸せを笑いで表現しているのだ。"

# game/dialogs.rpy:1940
translate japanese sceneS1_8efda62d:

    # "She stopped after a while, wiping away her tears of laughter."
    "彼女は笑い過ぎて出てきた涙を拭った。"

# game/dialogs.rpy:1941
translate japanese sceneS1_e15c0017:

    # s "「I've never felt happiness like this."
    s "「私、今とても幸せです！」"

# game/dialogs.rpy:1942
translate japanese sceneS1_1baf5e06:

    # s "「Thanks to you, I feel that those sad memories have gone away now."
    s "「今までの悲しい思い出はどっかに行っちゃいました。」"

# game/dialogs.rpy:1943
translate japanese sceneS1_b27b381b:

    # s "「Thank you so much...%(stringhero)s-kun! Thank you!"
    s "「本当にありがとう…%(stringhero)sくん！ありがとう！」"

# game/dialogs.rpy:1944
translate japanese sceneS1_0cb95e5a:

    # "She embraced me tightly like a plushie, like a little girl to her father, with an adorable smile on her lips. I could feel her heart beating against my chest..."
    "そう言って僕を強く抱きしめた。子供が人形を抱く時の様に、もしくは小さな娘がパパにする時の様に。彼女の鼓動が胸越しに伝わる…"

# game/dialogs.rpy:1950
translate japanese sceneS1_00f63e60:

    # "We had a lot of fun today in the city."
    "今日は本当に楽しかった。"

# game/dialogs.rpy:1951
translate japanese sceneS1_936e8fa2:

    # "Holding hands, we were on our way back to the station."
    "手を繋いで帰りの駅まで向かった。"

# game/dialogs.rpy:1952
translate japanese sceneS1_eb6c4ac4:

    # "But suddenly, Sakura had stopped."
    "急に、咲良ちゃんが僕を止めた。"

# game/dialogs.rpy:1953
translate japanese sceneS1_e8cdcca5:

    # "I turned around to look at her, while still holding her hand."
    "僕は振り向いて、咲良ちゃんを見た。"

# game/dialogs.rpy:1955
translate japanese sceneS1_e9b16d76:

    # "Her expression was blank, then turned to embarrassment. She was beet-root red."
    "虚ろで、気まずそうで、そしてビーツの様に真っ赤だった。"

# game/dialogs.rpy:1956
translate japanese sceneS1_27c88c8d:

    # hero "「Something wrong?"
    hero "「何かあったの…？」"

# game/dialogs.rpy:1957
translate japanese sceneS1_0d15642b:

    # "She pointed toward a building near us..."
    "そして彼女は黙って隣の建物を指差した…"

# game/dialogs.rpy:1958
translate japanese sceneS1_1c82a33d:

    # "I looked to see, and it was...{w}{size=+5}love hotel!?!{/size}"
    "これは…{w}{size=+5}ホテル…！？{/size}"

# game/dialogs.rpy:1959
translate japanese sceneS1_df826d7c:

    # "Whoa wait what!?"
    "なんてこった！！"

# game/dialogs.rpy:1960
translate japanese sceneS1_58865132:

    # "Does she want to..."
    "それってつまり…"

# game/dialogs.rpy:1961
translate japanese sceneS1_57d99560:

    # hero "「D-do you...you want to...go there?"
    hero "「それって、つまり…行く…の…？」"

# game/dialogs.rpy:1962
translate japanese sceneS1_be50fa6b:

    # "She nodded and looked down at the ground to hide her shyness. Her hand gripped me tighter."
    "彼女は可愛く頷いた。"

# game/dialogs.rpy:1964
translate japanese sceneS1_c7711bb7:

    # hero "「S-sure, let's go."
    hero "「ええ、分かりました…行きましょう。"

# game/dialogs.rpy:1968
translate japanese sceneS1_8534ca42_1:

    # ". . ."
    "……。"

# game/dialogs.rpy:1969
translate japanese sceneS1_2c0af0e2:

    # "I woke up in the bed of the hotel."
    "僕はホテルのベッドで目を醒ました。"

# game/dialogs.rpy:1970
translate japanese sceneS1_1b6c4bc4:

    # "I think I doozed off for only 15 minutes or so. I was feeling pretty sleepy."
    "15分程眠ってた気がする…まだちょっと眠い…"

# game/dialogs.rpy:1971
translate japanese sceneS1_d36164f4:

    # "In my arms, Sakura was sleeping against me."
    "僕の腕の中で、咲良ちゃんは眠っている。"

# game/dialogs.rpy:1972
translate japanese sceneS1_9f8a91dc:

    # "She held me tight..."
    "僕をしっかりと抱きしめて…"

# game/dialogs.rpy:1973
translate japanese sceneS1_f78bfd73:

    # "I guess she must have been exhausted, after so much fun in that room..."
    "僕と同じくらい疲れているに違いない…"

# game/dialogs.rpy:1974
translate japanese sceneS1_2418a26e:

    # "She woke up still a little drowsy, and spoke with a weary voice,"
    "ふと、咲良ちゃんが目を醒まして、小さく呟いた。"

# game/dialogs.rpy:1975
translate japanese sceneS1_943f038e:

    # s "「%(stringhero)s-kun?.."
    s "「%(stringhero)s…くん…？」"

# game/dialogs.rpy:1976
translate japanese sceneS1_36317276:

    # hero "「Hmm?"
    hero "「何だ？」"

# game/dialogs.rpy:1977
translate japanese sceneS1_e490aa43:

    # s "「Was it... your first time too...?"
    s "「%(stringhero)sくんも…初めてでしたか…？」"

# game/dialogs.rpy:1978
translate japanese sceneS1_36fd2a62:

    # hero "「Y-yeah..."
    hero "「ええ。」"

# game/dialogs.rpy:1979
translate japanese sceneS1_f2210395:

    # s "「You... Do you regret it?"
    s "「後悔してません…？」"

# game/dialogs.rpy:1980
translate japanese sceneS1_13b2e8fe:

    # hero "「Me?... No... Why should I?"
    hero "「え？いや、全然ですけど…どうして…？」"

# game/dialogs.rpy:1981
translate japanese sceneS1_d723f1ce:

    # s "「Nevermind..."
    s "「いえ…気にしないで…」"

# game/dialogs.rpy:1982
translate japanese sceneS1_3191b879:

    # s "「I'm just... I still can't believe I finally found a boy who accepts me the way I am..."
    s "「私はまだ…私を受け入れてくれる男の人を見つけられた事が信じられないんです…」"

# game/dialogs.rpy:1983
translate japanese sceneS1_67fabef6:

    # s "「You even gave me my first time..."
    s "「You even gave me my first time...」"

# game/dialogs.rpy:1984
translate japanese sceneS1_f8751689:

    # hero "「I'm happy to be your first...{p}And I'm happy that you were my first too."
    hero "「I'm happy to be your first...{p}And I'm happy that you were my first too.」"

# game/dialogs.rpy:1985
translate japanese sceneS1_61308ad3:

    # s "「I'm so happy %(stringhero)s-kun... This is almost like a dream..."
    s "「I'm so happy %(stringhero)s-kun... This is almost like a dream...」"

# game/dialogs.rpy:1986
translate japanese sceneS1_66805949:

    # "We lay there in peaceful silence together..."
    "僕は静かに咲良ちゃんに寄り添った。"

# game/dialogs.rpy:1987
translate japanese sceneS1_0af1e620:

    # "I was completely crazy about her... "
    "ああ、僕はすっかり彼女の虜になってしまった…若しくは彼の…いや、どっちでもいいか…"

# game/dialogs.rpy:1988
translate japanese sceneS1_a71c4c3b:

    # "We got lost in each other's eyes."
    "お互いに瞳を見つめ合った。"

# game/dialogs.rpy:1989
translate japanese sceneS1_4a521333:

    # "Her eyes were filled with love and happiness. She smiled at me..."
    "彼女の目が愛と幸福に満たされているのを感じる…彼女は微笑んだ。"

# game/dialogs.rpy:1990
translate japanese sceneS1_dd054cb9:

    # "We ended up making love one more time... then some time later..."
    "その後、僕達は再び身体を重ね合い、数分が経った…"

# game/dialogs.rpy:1991
translate japanese sceneS1_0dcaa626:

    # "We took a shower, left the hotel, and took the train home. I walked her home."
    "We took a shower, left the hotel, and took the train home. I walked her home."

# game/dialogs.rpy:1995
translate japanese sceneS1_2724f4f7:

    # s "「See you at school, honey!"
    s "「また明日、学校で！」"

# game/dialogs.rpy:1996
translate japanese sceneS1_90456e76:

    # hero "「See you tomorrow, love..."
    hero "「ええ、また明日…」"

# game/dialogs.rpy:1998
translate japanese sceneS1_adc5375c:

    # "She waved goodbye and entered her house."
    "彼女は手を振って家に帰っていった。"

# game/dialogs.rpy:1999
translate japanese sceneS1_8c41d669:

    # "So much happened this weekend..."
    "今週末は凄まじい速さで過ぎていった。"

# game/dialogs.rpy:2000
translate japanese sceneS1_3eb2b85d:

    # "Sakura told me that she was a boy,{p}then we kissed and went on a date,{p}then we did \"it\"..."
    "咲良ちゃんは男であることを告白して、{p}その後僕達はキスをして、{p}その後僕達はそれをして…"

# game/dialogs.rpy:2001
translate japanese sceneS1_e36e1081:

    # "I'm so tired... So many things to wrap my head around..."
    "疲れた…考える事が多すぎる…"

# game/dialogs.rpy:2002
translate japanese sceneS1_45a34284:

    # "I'd like to take the day off tomorrow, but I'll get to see Sakura tomorrow too!"
    "もし咲良ちゃんが行かないんなら明日は休もう…"

# game/dialogs.rpy:2010
translate japanese sceneS1_443e2e2a:

    # r "「{size=+15}WHAAAAAAAAAT？？？？？？{/size}"
    r "「{size=+15}何！！？？？？？{/size}」"

# game/dialogs.rpy:2016
translate japanese sceneS1_d1f76e80:

    # r "「You... Y-Y-Y-Y-You are lo-lo-lo-lovers?!!!"
    r "「あ…あ、あ、あ、あんさん達が…こ、ここ恋人…？！！！」"

# game/dialogs.rpy:2017
translate japanese sceneS1_1182fed0:

    # s "「Teehee!..."
    s "「フフ…」"

# game/dialogs.rpy:2018
translate japanese sceneS1_70b77763:

    # hero "「Well, yeah..."
    hero "「ええ、まあ…」"

# game/dialogs.rpy:2019
translate japanese sceneS1_bf2888c1:

    # n "「Seriously, guys!!!!"
    n "「マジか！」"

# game/dialogs.rpy:2020
translate japanese sceneS1_87964ca3:

    # r "「B... B-B-B-Bu-Bu-Bu...."
    r "「せ、せせ…せやけど…」"

# game/dialogs.rpy:2021
translate japanese sceneS1_0e57396a:

    # r "「I'm... I-I-I....I'm...."
    r "「ウ…ウチ…ウチは…」"

# game/dialogs.rpy:2023
translate japanese sceneS1_322f4f6b:

    # r "「I'm so happy for both of you!!!"
    r "「…ほんまに嬉しいで！！」"

# game/dialogs.rpy:2025
translate japanese sceneS1_d35b9154:

    # r "「No, wait. I'm not. Not at all."
    r "「や、ちょい待ち、ちゃうわ。」"

# game/dialogs.rpy:2026
translate japanese sceneS1_b5c49fda:

    # r "「%(stringhero)s is a damn pervert, he... He will..."
    r "「%(stringhero)sはヘンタイやからな、コイツは…その…」"

# game/dialogs.rpy:2027
translate japanese sceneS1_5d72dbb5:

    # hero "「Hey!!!"
    hero "「おい！！」"

# game/dialogs.rpy:2028
translate japanese sceneS1_e5583358:

    # n "「{size=+10}Seriously, guys!!!!{/size}"
    n "「{size=+10}マジか！{/size}」"

# game/dialogs.rpy:2029
translate japanese sceneS1_35b8ccde:

    # "Sakura looked at me and smiles brightly as she turned red."
    "咲良ちゃんは赤くなって、恥ずかしそうにこっちを見て笑っている。"

# game/dialogs.rpy:2030
translate japanese sceneS1_124abe51:

    # "I looked right back at her and I smirked. She starts to laugh a little."
    "僕も彼女を見て笑った。あの日の事を思い出しながら…"

# game/dialogs.rpy:2032
translate japanese sceneS1_df3a5fe2:

    # "Rika sees this exchange and starts to stutter."
    "梨花ちゃんがそれを見て、吃り始めた。"

# game/dialogs.rpy:2033
translate japanese sceneS1_6151b676:

    # r "「No... N-N-No, d-d-d-don't tell me you... Don't tell me you already...."
    r "「い…いいいいや、いやや、言わんといて…言わんといてな…そんな…あんたらもう…」"

# game/dialogs.rpy:2034
translate japanese sceneS1_1d81a8c5:

    # hero "「Hmm, alright, we won't tell you, then..."
    hero "「ふむ、じゃあ言わないでおきますよ。その…」"

# game/dialogs.rpy:2040
translate japanese sceneS1_cc4c3814:

    # r "「{size=+15}EEEEEEEEHHHHHHHH!!!{/size}"
    r "「{size=+15}アアアアアアアアアアアアアアア！！！{/size}」"

# game/dialogs.rpy:2042
translate japanese sceneS1_e4cfb08c:

    # n "「{size=+15}SERIOUSLY, GUYS!!!!{/size}"
    n "「{size=+15}マジか！！！{/size}」"

# game/dialogs.rpy:2043
translate japanese sceneS1_0e6125af:

    # "Rika and Nanami were absolutely shocked."
    "Rika and Nanami were absolutely shocked."

# game/dialogs.rpy:2044
translate japanese sceneS1_1067a846:

    # "Even after a few days passed, Rika still wasn't over it!"
    "Even after a few days passed, Rika still wasn't over it!"

# game/dialogs.rpy:2045
translate japanese sceneS1_5c7feca9:

    # "And Nanami randomly shouted \"Seriously, guys!!\" at us for the same amount of time!"
    "And Nanami randomly shouted \"Seriously, guys!!\" at us for the same amount of time!"

# game/dialogs.rpy:2046
translate japanese sceneS1_9ed40c66:

    # "It was kinda fun to watch."
    "It was kinda fun to watch."

# game/dialogs.rpy:2068
translate japanese secretnanami_d274ba9e:

    # "The next day, Nanami invited me to play videogames at her house"
    "The next day, Nanami invited me to play videogames at her house"

# game/dialogs.rpy:2070
translate japanese secretnanami_d7735442:

    # "Nanami's room was a bit messy."
    "Nanami's room was a bit messy."

# game/dialogs.rpy:2071
translate japanese secretnanami_2bf0b11f:

    # "Her bed and computer setup took up most of the room. Her computer looked more powerful than the eMac I had in my bedroom."
    "Her bed and computer setup took up most of the room. Her computer looked more powerful than the eMac I had in my bedroom."

# game/dialogs.rpy:2072
translate japanese secretnanami_992fbe8e:

    # "There was also an old TV with several gaming consoles plugged in it."
    "There was also an old TV with several gaming consoles plugged in it."

# game/dialogs.rpy:2073
translate japanese secretnanami_2bd3e307:

    # "Random bags of snacks and clothes were scattered about. It was a mess, but kind of an impressive mess."
    "Random bags of snacks and clothes were scattered about. It was a mess, but kind of an impressive mess."

# game/dialogs.rpy:2074
translate japanese secretnanami_2f4f2a6e:

    # "Nanami seemed a bit embarrassed as I was looking around the room."
    "Nanami seemed a bit embarrassed as I was looking around the room."

# game/dialogs.rpy:2076
translate japanese secretnanami_4a017f1e:

    # n "「D-don't pay any attention to the mess, %(stringhero)s-senpai..."
    n "「D-don't pay any attention to the mess, %(stringhero)s-senpai...」"

# game/dialogs.rpy:2077
translate japanese secretnanami_7a1bf678:

    # hero "「It's okay, I don't mind."
    hero "「It's okay, I don't mind.」"

# game/dialogs.rpy:2078
translate japanese secretnanami_9b653222:

    # hero "「You have so much gaming stuff. It's impressive!"
    hero "「You have so much gaming stuff. It's impressive!」"

# game/dialogs.rpy:2079
translate japanese secretnanami_2b844856:

    # hero "「But there's barely any space. Doesn't it feel cramped for you at all?"
    hero "「But there's barely any space. Doesn't it feel cramped for you at all?」"

# game/dialogs.rpy:2081
translate japanese secretnanami_d4d4357b:

    # n "「Not at all!"
    n "「Not at all!」"

# game/dialogs.rpy:2082
translate japanese secretnanami_8d45096b:

    # n "「Everything I could ever need is here!"
    n "「Everything I could ever need is here!」"

# game/dialogs.rpy:2083
translate japanese secretnanami_3e97bcd0:

    # hero "「Heh, well after all, you're small. Seems perfect for you."
    hero "「Heh, well after all, you're small. Seems perfect for you.」"

# game/dialogs.rpy:2085
translate japanese secretnanami_db57c973:

    # n "「I'm not that small!"
    n "「I'm not that small!」"

# game/dialogs.rpy:2086
translate japanese secretnanami_2fc28d42:

    # "I laughed at her adorable reaction. I continued checking out the room."
    "I laughed at her adorable reaction. I continued checking out the room."

# game/dialogs.rpy:2087
translate japanese secretnanami_51b37813:

    # hero "「Your brother isn't here, today?"
    hero "「Your brother isn't here, today?」"

# game/dialogs.rpy:2089
translate japanese secretnanami_d2016bd8:

    # n "「Toshio-nii? No. He usually works on Sunday. He's only home on Mondays."
    n "「Toshio-nii? No. He usually works on Sunday. He's only home on Mondays.」"

# game/dialogs.rpy:2090
translate japanese secretnanami_b67b5fcc:

    # n "「We have the house to ourselves until 6PM!"
    n "「We have the house to ourselves until 6PM!」"

# game/dialogs.rpy:2091
translate japanese secretnanami_84b8bd32:

    # hero "「I see."
    hero "「I see.」"

# game/dialogs.rpy:2093
translate japanese secretnanami_fec633fd:

    # "I noticed a framed picture near her computer desk."
    "I noticed a framed picture near her computer desk."

# game/dialogs.rpy:2094
translate japanese secretnanami_03d84c1c:

    # "It looked like it was an old picture of Nanami as a child."
    "It looked like it was an old picture of Nanami as a child."

# game/dialogs.rpy:2095
translate japanese secretnanami_865a0544:

    # "Beside her, there was an older child. This must be her brother.{p}And behind the both of them, there was an adult couple. Probably her parents..."
    "Beside her, there was an older child. This must be her brother.{p}And behind the both of them, there was an adult couple. Probably her parents..."

# game/dialogs.rpy:2098
translate japanese secretnanami_d381eb8b:

    # "Nanami noticed I was looking at it and came closer. Her pleasant expression faded away."
    "Nanami noticed I was looking at it and came closer. Her pleasant expression faded away."

# game/dialogs.rpy:2099
translate japanese secretnanami_3d2aa8bc:

    # n "「That's...{w}the last picture I have of my parents."
    n "「That's...{w}the last picture I have of my parents.」"

# game/dialogs.rpy:2100
translate japanese secretnanami_2462192a:

    # hero "「You mean... They're gone? Are they divorced or something?"
    hero "「You mean... They're gone? Are they divorced or something?」"

# game/dialogs.rpy:2102
translate japanese secretnanami_3ac96ee3:

    # "She turned away from me. I faced her back, and she began to speak..."
    "She turned away from me. I faced her back, and she began to speak..."

# game/dialogs.rpy:2103
translate japanese secretnanami_03e340ee:

    # n "「No..."
    n "「No...」"

# game/dialogs.rpy:2104
translate japanese secretnanami_ad2cdc06:

    # n "「They are...{w}dead."
    n "「They are...{w}dead.」"

# game/dialogs.rpy:2106
translate japanese secretnanami_cc9f4dc4:

    # "I stayed motionless and was speechless. Shocked by the revelation."
    "I stayed motionless and was speechless. Shocked by the revelation."

# game/dialogs.rpy:2107
translate japanese secretnanami_2c0862cf:

    # hero "「I-... I'm so sorry to hear that, Nanami-chan... I didn't mean to bring up a sore subject..."
    hero "「I-... I'm so sorry to hear that, Nanami-chan... I didn't mean to bring up a sore subject...」"

# game/dialogs.rpy:2109
translate japanese secretnanami_80110a90:

    # "Nanami sat on the floor, holding her legs."
    "Nanami sat on the floor, holding her legs."

# game/dialogs.rpy:2111
translate japanese secretnanami_eb85d77c:

    # n "「You know...They were working at the grocery store your parents are managing today."
    n "「You know...They were working at the grocery store your parents are managing today.」"

# game/dialogs.rpy:2112
translate japanese secretnanami_de2b803f:

    # hero "「What...!?"
    hero "「What...!?」"

# game/dialogs.rpy:2113
translate japanese secretnanami_59529ad1:

    # "Just then, I remembered something. My parents got the shop because the previous owners hadn't been around for a few years."
    "Just then, I remembered something. My parents got the shop because the previous owners hadn't been around for a few years."

# game/dialogs.rpy:2114
translate japanese secretnanami_9fec9592:

    # "I didn't think they were actually gone..."
    "I didn't think they were actually gone..."

# game/dialogs.rpy:2115
translate japanese secretnanami_ee152b17:

    # "I sat next to Nanami."
    "I sat next to Nanami."

# game/dialogs.rpy:2116
translate japanese secretnanami_b4b7b899:

    # n "「It happened four years ago."
    n "「It happened four years ago.」"

# game/dialogs.rpy:2117
translate japanese secretnanami_f2bc7b70:

    # n "「It... It was on my brother's birthday."
    n "「It... It was on my brother's birthday.」"

# game/dialogs.rpy:2118
translate japanese secretnanami_90a4c3c8:

    # n "「They went to the city to get a surprise present for him..."
    n "「They went to the city to get a surprise present for him...」"

# game/dialogs.rpy:2119
translate japanese secretnanami_1a38e7d1:

    # n "「But they never came back..."
    n "「But they never came back...」"

# game/dialogs.rpy:2120
translate japanese secretnanami_728dd651:

    # n "「I heard they were hit by a truck when they were coming back..."
    n "「I heard they were hit by a truck when they were coming back...」"

# game/dialogs.rpy:2121
translate japanese secretnanami_28efb3c9:

    # n "「Toshio-nii told me. He was the one to get the phone call about my parents."
    n "「Toshio-nii told me. He was the one to get the phone call about my parents.」"

# game/dialogs.rpy:2123
translate japanese secretnanami_e75dc79b:

    # n "「You know... My brother, he...{p}he changed a lot after that."
    n "「You know... My brother, he...{p}he changed a lot after that.」"

# game/dialogs.rpy:2124
translate japanese secretnanami_d070d077:

    # n "「He blames himself for the death of our parents."
    n "「He blames himself for the death of our parents.」"

# game/dialogs.rpy:2125
translate japanese secretnanami_d8670523:

    # n "「I told him that wasn't true. That it's not his fault."
    n "「I told him that wasn't true. That it's not his fault.」"

# game/dialogs.rpy:2126
translate japanese secretnanami_65090fe7:

    # hero "「That's right. It's not his fault... It was the fate..."
    hero "「That's right. It's not his fault... It was the fate...」"

# game/dialogs.rpy:2127
translate japanese secretnanami_b6f21cea:

    # n "「But he doesn't listen."
    n "「But he doesn't listen.」"

# game/dialogs.rpy:2128
translate japanese secretnanami_76cf9acc:

    # n "「He's become a little unstable because of that. Sometimes, he scares me a bit."
    n "「He's become a little unstable because of that. Sometimes, he scares me a bit.」"

# game/dialogs.rpy:2129
translate japanese secretnanami_d1c2fbdd:

    # "I instantly thought about Sakura's relationship with her father. But Nanami corrected me immediately like she knew what I was thinking."
    "I instantly thought about Sakura's relationship with her father. But Nanami corrected me immediately like she knew what I was thinking."

# game/dialogs.rpy:2130
translate japanese secretnanami_d01d8042:

    # n "「He doesn't beat me or argue with me... It's more like the opposite..."
    n "「He doesn't beat me or argue with me... It's more like the opposite...」"

# game/dialogs.rpy:2131
translate japanese secretnanami_d40e7776:

    # n "「He's become over-protective of me."
    n "「He's become over-protective of me.」"

# game/dialogs.rpy:2132
translate japanese secretnanami_43d2f391:

    # n "「He says we're each other's only family now and that we have to take good care of each other."
    n "「He says we're each other's only family now and that we have to take good care of each other.」"

# game/dialogs.rpy:2133
translate japanese secretnanami_f8c997a1:

    # hero "「You don't have any grand-parents?"
    hero "「You don't have any grand-parents?」"

# game/dialogs.rpy:2134
translate japanese secretnanami_8fcd3561:

    # n "「My father's parents passed away before I was born."
    n "「My father's parents passed away before I was born.」"

# game/dialogs.rpy:2135
translate japanese secretnanami_eb73e5cd:

    # n "「My mother's parents are still alive... But they're all the way in Okinawa."
    n "「My mother's parents are still alive... But they're all the way in Okinawa.」"

# game/dialogs.rpy:2136
translate japanese secretnanami_4b6a67a0:

    # n "「We write to them, from time to time..."
    n "「We write to them, from time to time...」"

# game/dialogs.rpy:2137
translate japanese secretnanami_eb3d403f:

    # n "「Afterwards, Toshio-nii took a job to afford the house rent and our other needs."
    n "「Afterwards, Toshio-nii took a job to afford the house rent and our other needs.」"

# game/dialogs.rpy:2138
translate japanese secretnanami_e668a3b6:

    # hero "「I see..."
    hero "「I see...」"

# game/dialogs.rpy:2139
translate japanese secretnanami_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:2140
translate japanese secretnanami_0b1702b3:

    # "Darn, I don't know what to say."
    "Darn, I don't know what to say."

# game/dialogs.rpy:2141
translate japanese secretnanami_12251861:

    # "Losing your parents at this age must be traumatic. I can barely imagine it."
    "Losing your parents at this age must be traumatic. I can barely imagine it."

# game/dialogs.rpy:2142
translate japanese secretnanami_1d53eed8:

    # "I felt like Nanami was about to cry."
    "I felt like Nanami was about to cry."

# game/dialogs.rpy:2143
translate japanese secretnanami_966d23a4:

    # "I wrapped an arm around her shoulder and she started to sob silently in my arms."
    "I wrapped an arm around her shoulder and she started to sob silently in my arms."

# game/dialogs.rpy:2144
translate japanese secretnanami_ee5f81b5:

    # "She must have greatly loved her parents..."
    "She must have greatly loved her parents..."

# game/dialogs.rpy:2146
translate japanese secretnanami_8b459908:

    # "After a while, she stopped crying. She wiped her tears and smiled at me."
    "After a while, she stopped crying. She wiped her tears and smiled at me."

# game/dialogs.rpy:2150
translate japanese secretnanami_42d6bc4c:

    # n "「Only a few people know the truth about my parents. Sakura-nee, Rika-nee... And now, you too."
    n "「Only a few people know the truth about my parents. Sakura-nee, Rika-nee... And now, you too.」"

# game/dialogs.rpy:2151
translate japanese secretnanami_0fc2108d:

    # n "「People think our parents have gone back to Okinawa, since Toshio-nii is now an adult."
    n "「People think our parents have gone back to Okinawa, since Toshio-nii is now an adult.」"

# game/dialogs.rpy:2156
translate japanese secretnanami_511241cb:

    # "So that's what Sakura was talking about at the arcade..."
    "So that's what Sakura was talking about at the arcade..."

# game/dialogs.rpy:2161
translate japanese secretnanami_c3c4b0be:

    # hero "「How are you feeling now? Are you okay?"
    hero "「How are you feeling now? Are you okay?」"

# game/dialogs.rpy:2162
translate japanese secretnanami_8ed55284:

    # n "「Yeah... I'm okay, %(stringhero)s-nii."
    n "「Yeah... I'm okay, %(stringhero)s-nii.」"

# game/dialogs.rpy:2164
translate japanese secretnanami_c259ee4e:

    # "I smiled and gently pat her head."
    "I smiled and gently pat her head."

# game/dialogs.rpy:2165
translate japanese secretnanami_8f2bc02b:

    # hero "「So, are we playing some games? You did invite me for that."
    hero "「So, are we playing some games? You did invite me for that.」"

# game/dialogs.rpy:2167
translate japanese secretnanami_7b9494dd:

    # n "「Sure! Let me show you what I have."
    n "「Sure! Let me show you what I have.」"

# game/dialogs.rpy:2169
translate japanese secretnanami_b5ed2554:

    # "Some time later, we were playing a fighting game called {i}Mortal Battle{/i}."
    "Some time later, we were playing a fighting game called {i}Mortal Battle{/i}."

# game/dialogs.rpy:2170
translate japanese secretnanami_b69a371f:

    # "This was an American game known to have complicated and unconventional special attacks. It's hard to get used to it when you mostly play Japanese fighting games."
    "This was an American game known to have complicated and unconventional special attacks. It's hard to get used to it when you mostly play Japanese fighting games."

# game/dialogs.rpy:2171
translate japanese secretnanami_1145ba4f:

    # "But I ended up mastering one of the characters in a short amount of time."
    "But I ended up mastering one of the characters in a short amount of time."

# game/dialogs.rpy:2172
translate japanese secretnanami_c49f161e:

    # "Every time I selected this character, I ended up beating the crap out of Nanami's character."
    "Every time I selected this character, I ended up beating the crap out of Nanami's character."

# game/dialogs.rpy:2174
translate japanese secretnanami_073340cb:

    # n "「Eeeeeh! How did you do that special attack?!"
    n "「Eeeeeh! How did you do that special attack?!」"

# game/dialogs.rpy:2175
translate japanese secretnanami_0883b3a9:

    # hero "「It's not too hard. You just roll the stick like this, then like this and you press HK."
    hero "「It's not too hard. You just roll the stick like this, then like this and you press HK.」"

# game/dialogs.rpy:2177
translate japanese secretnanami_45d61fa6:

    # n "「...I still don't get it..."
    n "「...I still don't get it...」"

# game/dialogs.rpy:2178
translate japanese secretnanami_27514623:

    # hero "「Hold on, I'll show you."
    hero "「Hold on, I'll show you.」"

# game/dialogs.rpy:2183
translate japanese secretnanami_9d9c7a4c:

    # "I placed myself behind Nanami."
    "I placed myself behind Nanami."

# game/dialogs.rpy:2184
translate japanese secretnanami_d11638d3:

    # hero "「Don't move, I'll show you exactly how."
    hero "「Don't move, I'll show you exactly how.」"

# game/dialogs.rpy:2185
translate japanese secretnanami_98b0a82d:

    # n "「O-okay!"
    n "「O-okay!」"

# game/dialogs.rpy:2186
translate japanese secretnanami_9b84d6a3:

    # "I placed my hands on Nanami's, holding the controller with her."
    "I placed my hands on Nanami's, holding the controller with her."

# game/dialogs.rpy:2187
translate japanese secretnanami_e5d45e92:

    # "I realized how small her hands were...but they were so soft and warm as well. I started to get a little nervous and blush."
    "I realized how small her hands were...but they were so soft and warm as well. I started to get a little nervous and blush."

# game/dialogs.rpy:2188
translate japanese secretnanami_b142513a:

    # "I felt like she was starting get just as nervous as well."
    "I felt like she was starting get just as nervous as well."

# game/dialogs.rpy:2189
translate japanese secretnanami_72fc66bd:

    # "I went into the training mode in the game."
    "I went into the training mode in the game."

# game/dialogs.rpy:2190
translate japanese secretnanami_068e1468:

    # "Behind the game's sound effects, I could faintly hear Nanami's soft breathing."
    "Behind the game's sound effects, I could faintly hear Nanami's soft breathing."

# game/dialogs.rpy:2191
translate japanese secretnanami_dad13c6d:

    # "I started to feel the warmth of her back on my chest. I could feel myself losing focus..."
    "I started to feel the warmth of her back on my chest. I could feel myself losing focus..."

# game/dialogs.rpy:2192
translate japanese secretnanami_a3e2db4f:

    # hero "「O-okay... S-so you... You roll the left stick like that..."
    hero "「O-okay... S-so you... You roll the left stick like that...」"

# game/dialogs.rpy:2193
translate japanese secretnanami_b73c31d9:

    # "I tried the special attack but it failed."
    "I tried the special attack but it failed."

# game/dialogs.rpy:2194
translate japanese secretnanami_f8130d09:

    # hero "「W-wait, let me try again..."
    hero "「W-wait, let me try again...」"

# game/dialogs.rpy:2195
translate japanese secretnanami_8fe679da:

    # "I couldn't concentrate with Nanami in my arms like this."
    "I couldn't concentrate with Nanami in my arms like this."

# game/dialogs.rpy:2197
translate japanese secretnanami_9f50bc6f:

    # "I could feel my heart beating against her back."
    "I could feel my heart beating against her back."

# game/dialogs.rpy:2198
translate japanese secretnanami_516207af:

    # "I tried to redo the special attack but I just couldn't do it correctly. My hands were shaking."
    "I tried to redo the special attack but I just couldn't do it correctly. My hands were shaking."

# game/dialogs.rpy:2199
translate japanese secretnanami_9a928511:

    # "All my thoughts were floating towards Nanami."
    "All my thoughts were floating towards Nanami."

# game/dialogs.rpy:2200
translate japanese secretnanami_11daa19b:

    # "Her soft smell, her warmth invading my hands and my chest against her back..."
    "Her soft smell, her warmth invading my hands and my chest against her back..."

# game/dialogs.rpy:2201
translate japanese secretnanami_702709c6:

    # "I... I think I'm falling in love with Nanami..."
    "I... I think I'm falling in love with Nanami..."

# game/dialogs.rpy:2202
translate japanese secretnanami_0290a533:

    # "Nanami turned to face to me, her face was glowing red, just like mine."
    "Nanami turned to face to me, her face was glowing red, just like mine."

# game/dialogs.rpy:2203
translate japanese secretnanami_1126b44f:

    # "Her eyes were locked to mine. I got lost in her big emerald green eyes."
    "Her eyes were locked to mine. I got lost in her big emerald green eyes."

# game/dialogs.rpy:2204
translate japanese secretnanami_1b126339:

    # "It was the coup de grace"
    "It was the coup de grace"

# game/dialogs.rpy:2205
translate japanese secretnanami_f5a66b6d:

    # n "「.....%(stringhero)s-nii...."
    n "「.....%(stringhero)s-nii....」"

# game/dialogs.rpy:2206
translate japanese secretnanami_c7ff9e11:

    # hero "「...Na....Nanami-chan..."
    hero "「...Na....Nanami-chan...」"

# game/dialogs.rpy:2210
translate japanese secretnanami_07a482ff:

    # "I felt myself moving toward her lips."
    "I felt myself moving toward her lips."

# game/dialogs.rpy:2218
translate japanese secretnanami_6546765d:

    # "She came towards me as well, and our lips met."
    "She came towards me as well, and our lips met."

# game/dialogs.rpy:2219
translate japanese secretnanami_ad565af5:

    # "We both dropped the controller and embraced each other tightly, still kissing. Neither of us could stop at all."
    "We both dropped the controller and embraced each other tightly, still kissing. Neither of us could stop at all."

# game/dialogs.rpy:2220
translate japanese secretnanami_52b5a252:

    # "We fell to the floor in our embrace, trying to avoid some of the mess around."
    "We fell to the floor in our embrace, trying to avoid some of the mess around."

# game/dialogs.rpy:2221
translate japanese secretnanami_ca782044:

    # "Her kiss, her softness, her moaning of desire and her sweet smell were driving me crazy."
    "Her kiss, her softness, her moaning of desire and her sweet smell were driving me crazy."

# game/dialogs.rpy:2222
translate japanese secretnanami_474523d0:

    # "We kept going, sometimes stopping to pronounce each other's name..."
    "We kept going, sometimes stopping to pronounce each other's name..."

# game/dialogs.rpy:2223
translate japanese secretnanami_5ca23c3c:

    # "After a while, she took my hand and gently dragged me to her bed."
    "After a while, she took my hand and gently dragged me to her bed."

# game/dialogs.rpy:2224
translate japanese secretnanami_6d6ab9c6:

    # "We continued kissing there, more torridly..."
    "We continued kissing there, more torridly..."

# game/dialogs.rpy:2228
translate japanese secretnanami_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:2229
translate japanese secretnanami_0e5d8dad:

    # "The idle video game was still making some noise in the room."
    "The idle video game was still making some noise in the room."

# game/dialogs.rpy:2230
translate japanese secretnanami_95a4b2f0:

    # "I was on the bed, holding Nanami by my side."
    "I was on the bed, holding Nanami by my side."

# game/dialogs.rpy:2231
translate japanese secretnanami_d89bb97f:

    # "She was hugging me like a plushie."
    "She was hugging me like a plushie."

# game/dialogs.rpy:2232
translate japanese secretnanami_45685a47:

    # hero "「W-was it okay?"
    hero "「W-was it okay?」"

# game/dialogs.rpy:2233
translate japanese secretnanami_d1a94b7f:

    # n "「Y-yeah... It hurt a lil' bit at first,... but it felt good at the end..."
    n "「Y-yeah... It hurt a lil' bit at first,... but it felt good at the end...」"

# game/dialogs.rpy:2234
translate japanese secretnanami_5abb8e5f:

    # n "「I'm so happy that you were my first, %(stringhero)s-nii..."
    n "「I'm so happy that you were my first, %(stringhero)s-nii...」"

# game/dialogs.rpy:2235
translate japanese secretnanami_fa1ef15c:

    # hero "「Me too..."
    hero "「Me too...」"

# game/dialogs.rpy:2236
translate japanese secretnanami_1d6ad191:

    # n "「I feel like a woman now thanks to you..."
    n "「I feel like a woman now thanks to you...」"

# game/dialogs.rpy:2237
translate japanese secretnanami_4ce41c79:

    # "I kissed her forehead with a smile."
    "I kissed her forehead with a smile."

# game/dialogs.rpy:2238
translate japanese secretnanami_e9590123:

    # "Nanami suddenly giggled."
    "Nanami suddenly giggled."

# game/dialogs.rpy:2239
translate japanese secretnanami_08ea1428:

    # n "「I can't wait to see Sakura-nee and Rika-nee's faces when they find out about us!"
    n "「I can't wait to see Sakura-nee and Rika-nee's faces when they find out about us!」"

# game/dialogs.rpy:2240
translate japanese secretnanami_d3d236dd:

    # hero "「Hehe I'm curious to see that too..."
    hero "「Hehe I'm curious to see that too...」"

# game/dialogs.rpy:2241
translate japanese secretnanami_523e30b8:

    # "Then I had a mischievous thought..."
    "Then I had a mischievous thought..."

# game/dialogs.rpy:2242
translate japanese secretnanami_ceb62c81:

    # hero "「I have a fun idea:{p}Why not just let them figure it out themselves?"
    hero "「I have a fun idea:{p}Why not just let them figure it out themselves?」"

# game/dialogs.rpy:2243
translate japanese secretnanami_46f913e1:

    # hero "「They might end up even more surprised!"
    hero "「They might end up even more surprised!」"

# game/dialogs.rpy:2244
translate japanese secretnanami_ae388490:

    # "Nanami giggled louder."
    "Nanami giggled louder."

# game/dialogs.rpy:2245
translate japanese secretnanami_6ab9da22:

    # n "「Good idea! Let's do this!"
    n "「Good idea! Let's do this!」"

# game/dialogs.rpy:2246
translate japanese secretnanami_b3625191:

    # hero "「Right!"
    hero "「Right!」"

# game/dialogs.rpy:2247
translate japanese secretnanami_ec3a4b43:

    # "I giggled and kissed her lips again. She embraced me tighter, kissing back..."
    "I giggled and kissed her lips again. She embraced me tighter, kissing back..."

# game/dialogs.rpy:2248
translate japanese secretnanami_82623620:

    # "Before I could even notice, we were on for a second round..."
    "Before I could even notice, we were on for a second round..."

# game/dialogs.rpy:2249
translate japanese secretnanami_8116efac:

    # "Hehe... \"Second round\"...{p}I bet she finds this 'new game' better than any other game she's played!"
    "Hehe... \"Second round\"...{p}I bet she finds this 'new game' better than any other game she's played!"

# game/dialogs.rpy:2253
translate japanese secretnanami_3402d7bf:

    # "After that, it was late so it was time for me to go. "
    "After that, it was late so it was time for me to go. "

# game/dialogs.rpy:2254
translate japanese secretnanami_924e253f:

    # hero "「Don't forget: Don't tell anyone in the club!"
    hero "「Don't forget: Don't tell anyone in the club!」"

# game/dialogs.rpy:2256
translate japanese secretnanami_91f1d653:

    # n "「Yes sir!"
    n "「Yes sir!」"

# game/dialogs.rpy:2258
translate japanese secretnanami_743da19c:

    # "We kissed again and then I left for home."
    "We kissed again and then I left for home."

# game/dialogs.rpy:2262
translate japanese secretnanami_ffc14834:

    # "I was so happy. I was hopping around and dancing a bit."
    "I was so happy. I was hopping around and dancing a bit."

# game/dialogs.rpy:2263
translate japanese secretnanami_bb28eb57:

    # "People probably thought I was crazy but I didn't care."
    "People probably thought I was crazy but I didn't care."

# game/dialogs.rpy:2267
translate japanese secretnanami_50788d9c:

    # "Monday arrived..."
    "Monday arrived..."

# game/dialogs.rpy:2268
translate japanese secretnanami_e94279e1:

    # "I was thinking about all the new things I learned this weekend..."
    "I was thinking about all the new things I learned this weekend..."

# game/dialogs.rpy:2269
translate japanese secretnanami_ff29a530:

    # "I still can't believe Sakura was born as a boy."
    "I still can't believe Sakura was born as a boy."

# game/dialogs.rpy:2270
translate japanese secretnanami_d6d86bf5:

    # "But I can't wait to see her again anyway. She's my best friend."
    "But I can't wait to see her again anyway. She's my best friend."

# game/dialogs.rpy:2272
translate japanese secretnanami_12a5358b:

    # "I also thought about Nanami... My dear love..."
    "I also thought about Nanami... My dear love..."

# game/dialogs.rpy:2273
translate japanese secretnanami_55bde2ad:

    # "I can't wait to see her again too... And to make that joke to our friends!"
    "I can't wait to see her again too... And to make that joke to our friends!"

# game/dialogs.rpy:2274
translate japanese secretnanami_e6c4d8b3:

    # "Thinking of Sakura and Nanami made me remember my plan for Sakura."
    "Thinking of Sakura and Nanami made me remember my plan for Sakura."

# game/dialogs.rpy:2275
translate japanese secretnanami_da787f5b:

    # "I found Rika and told her I knew about Sakura, and that she needs help to tell the truth to Nanami as well."
    "I found Rika and told her I knew about Sakura, and that she needs help to tell the truth to Nanami as well."

# game/dialogs.rpy:2276
translate japanese secretnanami_f5d57b70:

    # "She gladly accepted."
    "She gladly accepted."

# game/dialogs.rpy:2277
translate japanese secretnanami_eb899bec:

    # "For the first time, I saw on her a sincere sweet smile."
    "For the first time, I saw on her a sincere sweet smile."

# game/dialogs.rpy:2278
translate japanese secretnanami_258e716e:

    # "She told me that I'm sure to be a special member of the club now."
    "She told me that I'm sure to be a special member of the club now."

# game/dialogs.rpy:2280
translate japanese secretnanami_4fe7e943:

    # "Of course, I didn't tell her about Nanami and I..."
    "Of course, I didn't tell her about Nanami and I..."

# game/dialogs.rpy:2281
translate japanese secretnanami_e02d74e7:

    # "Yet..."
    "Yet..."

# game/dialogs.rpy:2287
translate japanese secretnanami_3e63050d:

    # "Lunch time came."
    "Lunch time came."

# game/dialogs.rpy:2289
translate japanese secretnanami_7d31f878:

    # "I was eating lunch at my desk with Sakura, as we always do."
    "I was eating lunch at my desk with Sakura, as we always do."

# game/dialogs.rpy:2290
translate japanese secretnanami_96641885:

    # "Usually, Rika and Nanami join us a few minutes after we start."
    "Usually, Rika and Nanami join us a few minutes after we start."

# game/dialogs.rpy:2291
translate japanese secretnanami_fcd3f037:

    # hero "「Oh, guess what? I know about Nanami, now."
    hero "「Oh, guess what? I know about Nanami, now.」"

# game/dialogs.rpy:2292
translate japanese secretnanami_7f54d05d:

    # "Sakura nodded, knowing what I meant."
    "Sakura nodded, knowing what I meant."

# game/dialogs.rpy:2293
translate japanese secretnanami_26f2656b:

    # s "「Looks like she really likes and trusts you, now! That's great!"
    s "「Looks like she really likes and trusts you, now! That's great!」"

# game/dialogs.rpy:2294
translate japanese secretnanami_d5961b9f:

    # "I held a laugh. If only she knew how much she likes me now!"
    "I held a laugh. If only she knew how much she likes me now!"

# game/dialogs.rpy:2297
translate japanese secretnanami_ba640f4d:

    # "Nanami came first."
    "Nanami came first."

# game/dialogs.rpy:2298
translate japanese secretnanami_eb443bf4:

    # n "「Hey, Sakura-nee!"
    n "「Hey, Sakura-nee!」"

# game/dialogs.rpy:2300
translate japanese secretnanami_0f4878e5:

    # n "「Hey,...honey!"
    n "「Hey,...honey!」"

# game/dialogs.rpy:2302
translate japanese secretnanami_349c95fd:

    # "Nanami left a kiss on my cheek, took a free chair and table and joined us like if nothing happened."
    "Nanami left a kiss on my cheek, took a free chair and table and joined us like if nothing happened."

# game/dialogs.rpy:2303
translate japanese secretnanami_91c5f81c:

    # "Wow, she's good. It's hard to not blush in this situation."
    "Wow, she's good. It's hard to not blush in this situation."

# game/dialogs.rpy:2304
translate japanese secretnanami_0083c6b0:

    # "The face Sakura made was priceless."
    "The face Sakura made was priceless."

# game/dialogs.rpy:2305
translate japanese secretnanami_91e03536:

    # "Her face was flustered and expressed something along the lines of \"What the heck?\" and \"That's embarassing!\""
    "Her face was flustered and expressed something along the lines of \"What the heck?\" and \"That's embarassing!\""

# game/dialogs.rpy:2306
translate japanese secretnanami_d1b32a3c:

    # "I had a hard time trying to not laugh out loud.{p}I could tell, Nanami was holding it in too."
    "I had a hard time trying to not laugh out loud.{p}I could tell, Nanami was holding it in too."

# game/dialogs.rpy:2308
translate japanese secretnanami_5e2e8af1:

    # "Then Rika appeared and joined us."
    "Then Rika appeared and joined us."

# game/dialogs.rpy:2309
translate japanese secretnanami_2f6f7af4:

    # r "「Hey guys! Did you have a good Sunday?"
    r "「Hey guys! Did you have a good Sunday?」"

# game/dialogs.rpy:2310
translate japanese secretnanami_b408e187:

    # "Nanami and I almost choked on our meal at the question."
    "Nanami and I almost choked on our meal at the question."

# game/dialogs.rpy:2312
translate japanese secretnanami_e5fcea13:

    # r "「Sakura, you're okay?"
    r "「Sakura, you're okay?」"

# game/dialogs.rpy:2313
translate japanese secretnanami_5ab8d803:

    # s "「I'm... I'm not sure..."
    s "「I'm... I'm not sure...」"

# game/dialogs.rpy:2314
translate japanese secretnanami_5ddf9bba:

    # "All during lunch, Nanami and I were exchanging glances, and every time we had to hold a laugh."
    "All during lunch, Nanami and I were exchanging glances, and every time we had to hold a laugh."

# game/dialogs.rpy:2315
translate japanese secretnanami_3df5cd5f:

    # "All of this was making Sakura and Rika even more confused and uncomfortable."
    "All of this was making Sakura and Rika even more confused and uncomfortable."

# game/dialogs.rpy:2316
translate japanese secretnanami_0469525e:

    # "Finally, Rika decided to speak up."
    "Finally, Rika decided to speak up."

# game/dialogs.rpy:2318
translate japanese secretnanami_848c0a9a:

    # r "「Alright, guys. It looks like Sakura and I missed the memo. What's going on?"
    r "「Alright, guys. It looks like Sakura and I missed the memo. What's going on?」"

# game/dialogs.rpy:2319
translate japanese secretnanami_78a3d7bc:

    # r "「What happened this weekend?!"
    r "「What happened this weekend?!」"

# game/dialogs.rpy:2321
translate japanese secretnanami_a2210385:

    # "Nanami finally cracked. She burst into laughter. Some classmates turned their heads wondering what was going on."
    "Nanami finally cracked. She burst into laughter. Some classmates turned their heads wondering what was going on."

# game/dialogs.rpy:2322
translate japanese secretnanami_e7188aa7:

    # "Her laugh instantly unlocked mine and I finished laughing as well!"
    "Her laugh instantly unlocked mine and I finished laughing as well!"

# game/dialogs.rpy:2324
translate japanese secretnanami_1b6c8610:

    # r "「What the!?"
    r "「What the!?」"

# game/dialogs.rpy:2326
translate japanese secretnanami_5bc75703:

    # r "「Sakura, tell me! You must know something!"
    r "「Sakura, tell me! You must know something!」"

# game/dialogs.rpy:2327
translate japanese secretnanami_a3d554dc:

    # s "「I... They... I don't know!..."
    s "「I... They... I don't know!...」"

# game/dialogs.rpy:2328
translate japanese secretnanami_d6d264c2:

    # s "「Nana-chan kissed %(stringhero)s-kun on the cheek and they've been acting like this since!"
    s "「Nana-chan kissed %(stringhero)s-kun on the cheek and they've been acting like this since!」"

# game/dialogs.rpy:2330
translate japanese secretnanami_dba98263:

    # r "「No way! {w}You mean..."
    r "「No way! {w}You mean...」"

# game/dialogs.rpy:2331
translate japanese secretnanami_8fd173cb:

    # "I calmed down and spoke with my natural voice:"
    "I calmed down and spoke with my natural voice:"

# game/dialogs.rpy:2332
translate japanese secretnanami_588f721e:

    # hero "「Oh yeah, Nanami and I started going out together."
    hero "「Oh yeah, Nanami and I started going out together.」"

# game/dialogs.rpy:2333
translate japanese secretnanami_83c319f1:

    # hero "「We didn't tell you?"
    hero "「We didn't tell you?」"

# game/dialogs.rpy:2345
translate japanese secretnanami_e9fe3d46:

    # s "「{size=+25}EEEEEEEEHHHHHHHHH？？？!!!!!{/size}"
    s "「{size=+25}アアアアアアアアアアアアアアア！！！{/size}」"

# game/dialogs.rpy:2347
translate japanese secretnanami_e1326833:

    # r "「{size=+25}I KNEW IT!!!!!{/size}"
    r "「{size=+25}I KNEW IT!!!!!{/size}」"

# game/dialogs.rpy:2348
translate japanese secretnanami_e9aabc9d:

    # "Another burst of laughter occurred between Nanami and I."
    "Another burst of laughter occurred between Nanami and I."

# game/dialogs.rpy:2353
translate japanese secretnanami_597fe66a:

    # "After lunch, I found Sakura speaking with Rika alone."
    "After lunch, I found Sakura speaking with Rika alone."

# game/dialogs.rpy:2354
translate japanese secretnanami_d72a5ad0:

    # "When Rika saw me, she beckoned me to come over."
    "When Rika saw me, she beckoned me to come over."

# game/dialogs.rpy:2358
translate japanese secretnanami_eec796bb:

    # hero "「What's going on?"
    hero "「What's going on?」"

# game/dialogs.rpy:2359
translate japanese secretnanami_7f82104e:

    # r "「It's time. Nanami is waiting for us at the club room."
    r "「It's time. Nanami is waiting for us at the club room.」"

# game/dialogs.rpy:2360
translate japanese secretnanami_b6e3bb19:

    # r "「Are you both ready?"
    r "「Are you both ready?」"

# game/dialogs.rpy:2361
translate japanese secretnanami_326263c8:

    # hero "「I am. As long as Sakura is."
    hero "「I am. As long as Sakura is.」"

# game/dialogs.rpy:2363
translate japanese secretnanami_feb04fe2:

    # s "「I am... Let's go."
    s "「I am... Let's go.」"

# game/dialogs.rpy:2369
translate japanese secretnanami_9df2c3c5:

    # n "「Hey again, guys!"
    n "「Hey again, guys!」"

# game/dialogs.rpy:2373
translate japanese secretnanami_4ceec5a6:

    # r "「Hey, Nanami."
    r "「Hey, Nanami.」"

# game/dialogs.rpy:2374
translate japanese secretnanami_fe8acb83:

    # "I whispered \"Go on, don't worry!\" into Sakura's ear and she nodded."
    "I whispered \"Go on, don't worry!\" into Sakura's ear and she nodded."

# game/dialogs.rpy:2376
translate japanese secretnanami_10c08f00:

    # s "「Nanami-chan..."
    s "「Nanami-chan...」"

# game/dialogs.rpy:2378
translate japanese secretnanami_ee407c47:

    # s "「There's something I want to tell you."
    s "「There's something I want to tell you.」"

# game/dialogs.rpy:2380
translate japanese secretnanami_54f6b843:

    # n "「What is it?...{p}It looks serious... {w}Are you guys okay?"
    n "「What is it?...{p}It looks serious... {w}Are you guys okay?」"

# game/dialogs.rpy:2381
translate japanese secretnanami_1feba992:

    # hero "「Yeah, no worries. It's just...something that you need to know..."
    hero "「Yeah, no worries. It's just...something that you need to know...」"

# game/dialogs.rpy:2382
translate japanese secretnanami_609aaa6d:

    # s "「Since Rika-chan and %(stringhero)s-kun are aware of it, I thought it would be unfair for you to not know..."
    s "「Since Rika-chan and %(stringhero)s-kun are aware of it, I thought it would be unfair for you to not know...」"

# game/dialogs.rpy:2385
translate japanese secretnanami_827414d5:

    # n "「Hey hey, that's okay! Hiding things can be fun sometimes! Remember the lunch?"
    n "「Hey hey, that's okay! Hiding things can be fun sometimes! Remember the lunch?」"

# game/dialogs.rpy:2386
translate japanese secretnanami_1ac65a8e:

    # s "「It's not really the same thing, Nana-chan..."
    s "「It's not really the same thing, Nana-chan...」"

# game/dialogs.rpy:2388
translate japanese secretnanami_73096c17:

    # n "「Ah?..."
    n "「Ah?...」"

# game/dialogs.rpy:2389
translate japanese secretnanami_790191ec:

    # s "「It's always hard for me to find the right words so it doesn't shock you too much..."
    s "「It's always hard for me to find the right words so it doesn't shock you too much...」"

# game/dialogs.rpy:2390
translate japanese secretnanami_5667ce78:

    # n "「It's something that big?"
    n "「It's something that big?」"

# game/dialogs.rpy:2391
translate japanese secretnanami_22a96dc3:

    # "Sakura nodded."
    "Sakura nodded."

# game/dialogs.rpy:2392
translate japanese secretnanami_f8f4fa69:

    # s "「And before you ask..."
    s "「And before you ask...」"

# game/dialogs.rpy:2393
translate japanese secretnanami_6d099b03:

    # s "「I'm so sorry I didn't tell you earlier. I didn't know how to tell you."
    s "「I'm so sorry I didn't tell you earlier. I didn't know how to tell you.」"

# game/dialogs.rpy:2394
translate japanese secretnanami_2a2228a7:

    # s "「Before I tell you, can you forgive me for this?"
    s "「Before I tell you, can you forgive me for this?」"

# game/dialogs.rpy:2396
translate japanese secretnanami_564d9e25:

    # "Nanami smiled softly."
    "Nanami smiled softly."

# game/dialogs.rpy:2397
translate japanese secretnanami_67350be4:

    # n "「Of course I can. You're my Sakura-nee!"
    n "「Of course I can. You're my Sakura-nee!」"

# game/dialogs.rpy:2398
translate japanese secretnanami_c66e8457:

    # "Sakura nodded again with a faint smile. Then, after a moment, she started to reveal her secret."
    "Sakura nodded again with a faint smile. Then, after a moment, she started to reveal her secret."

# game/dialogs.rpy:2399
translate japanese secretnanami_727b9f43:

    # s "「Nana-chan..."
    s "「Nana-chan...」"

# game/dialogs.rpy:2400
translate japanese secretnanami_5b84e198:

    # s "「I am...{w}not...{w}technically a girl..."
    s "「I am...{w}not...{w}technically a girl...」"

# game/dialogs.rpy:2402
translate japanese secretnanami_33d36d5f:

    # n "「Huh?"
    n "「Huh?」"

# game/dialogs.rpy:2403
translate japanese secretnanami_e8a331c8:

    # s "「I..."
    s "「I...」"

# game/dialogs.rpy:2404
translate japanese secretnanami_ce4432b4:

    # "Rika came closer and held Sakura's hand to give her strength. Rika helped Sakura gather herself."
    "Rika came closer and held Sakura's hand to give her strength. Rika helped Sakura gather herself."

# game/dialogs.rpy:2405
translate japanese secretnanami_d8383a12:

    # r "「Sakura...{w}wasn't born as a girl, like you and me."
    r "「Sakura...{w}wasn't born as a girl, like you and me.」"

# game/dialogs.rpy:2406
translate japanese secretnanami_80777581:

    # "I placed my hand on Sakura's shoulder. I helped Rika speak on behalf of Sakura."
    "I placed my hand on Sakura's shoulder. I helped Rika speak on behalf of Sakura."

# game/dialogs.rpy:2407
translate japanese secretnanami_fd25aa6e:

    # hero "「Yeah...She's a girl on the inside, but originally, she was born as a boy. Like me."
    hero "「Yeah...She's a girl on the inside, but originally, she was born as a boy. Like me.」"

# game/dialogs.rpy:2409
translate japanese secretnanami_77609d5b:

    # "Nanami didn't say a word. She was trying to process the information. Her face displayed confusion."
    "Nanami didn't say a word. She was trying to process the information. Her face displayed confusion."

# game/dialogs.rpy:2410
translate japanese secretnanami_8961280a:

    # "Sakura gathered herself. She started to speak about her childhood, almost repeating word by word what she told me."
    "Sakura gathered herself. She started to speak about her childhood, almost repeating word by word what she told me."

# game/dialogs.rpy:2412
translate japanese secretnanami_d7384a8d:

    # "Nanami listened silently, nodding from time to time.{p}After Sakura finished, Nanami had similar questions to mine at time."
    "Nanami listened silently, nodding from time to time.{p}After Sakura finished, Nanami had similar questions to mine at time."

# game/dialogs.rpy:2413
translate japanese secretnanami_e145542f:

    # n "「I understand now..."
    n "「I understand now...」"

# game/dialogs.rpy:2415
translate japanese secretnanami_8d8cc9b7:

    # "Then Nanami grew a cute smile."
    "Then Nanami grew a cute smile."

# game/dialogs.rpy:2416
translate japanese secretnanami_5399f645:

    # n "「That's okay for me!"
    n "「That's okay for me!」"

# game/dialogs.rpy:2420
translate japanese secretnanami_e160708b:

    # n "「I'm happy that you told me your secret!"
    n "「I'm happy that you told me your secret!」"

# game/dialogs.rpy:2421
translate japanese secretnanami_93b2b022:

    # n "「I know I'm special to you, Sakura-nee, but now I feel it more than ever!"
    n "「I know I'm special to you, Sakura-nee, but now I feel it more than ever!」"

# game/dialogs.rpy:2422
translate japanese secretnanami_bfeb43f4:

    # "Sakura smiled, some tears coming from her eyes."
    "Sakura smiled, some tears coming from her eyes."

# game/dialogs.rpy:2425
translate japanese secretnanami_957c3f61:

    # "Nanami jumped on Sakura and hugged her tightly with a bright smile and some tears."
    "Nanami jumped on Sakura and hugged her tightly with a bright smile and some tears."

# game/dialogs.rpy:2426
translate japanese secretnanami_7c8449b8:

    # "That was touching."
    "That was touching."

# game/dialogs.rpy:2427
translate japanese secretnanami_106608f8:

    # n "「I understand why it was so hard to tell me that.{p}I'm sure it's not easy to tell a secret like that!"
    n "「I understand why it was so hard to tell me that.{p}I'm sure it's not easy to tell a secret like that!」"

# game/dialogs.rpy:2428
translate japanese secretnanami_df955372:

    # "Sakura chuckles."
    "Sakura chuckles."

# game/dialogs.rpy:2429
translate japanese secretnanami_3c734a09:

    # s "「It sure isn't!"
    s "「It sure isn't!」"

# game/dialogs.rpy:2430
translate japanese secretnanami_120f3ff5:

    # s "「But now I'm the happiest girl ever..."
    s "「But now I'm the happiest girl ever...」"

# game/dialogs.rpy:2431
translate japanese secretnanami_ba0e9538:

    # s "「Because, all the people I love and care about know my secret now."
    s "「Because, all the people I love and care about know my secret now.」"

# game/dialogs.rpy:2432
translate japanese secretnanami_1abc70b9:

    # s "「I don't need to hide anything to any of you now."
    s "「I don't need to hide anything to any of you now.」"

# game/dialogs.rpy:2433
translate japanese secretnanami_128578da:

    # n "「Hiding what?"
    n "「Hiding what?」"

# game/dialogs.rpy:2434
translate japanese secretnanami_77ee7366:

    # s "「Huh?"
    s "「Huh?」"

# game/dialogs.rpy:2435
translate japanese secretnanami_ce51de81:

    # n "「You're a girl, Sakura-nee."
    n "「You're a girl, Sakura-nee.」"

# game/dialogs.rpy:2436
translate japanese secretnanami_20f09b8b:

    # n "「You're born as a boy? That's a small detail."
    n "「You're born as a boy? That's a small detail.」"

# game/dialogs.rpy:2437
translate japanese secretnanami_1ca8316e:

    # n "「To me, you'll always be my Sakura-nee!"
    n "「To me, you'll always be my Sakura-nee!」"

# game/dialogs.rpy:2438
translate japanese secretnanami_e130ea9c:

    # "Sakura kissed Nanami's head and rubbed her hair like for a child."
    "Sakura kissed Nanami's head and rubbed her hair like for a child."

# game/dialogs.rpy:2439
translate japanese secretnanami_b45c3328:

    # s "「Thank you so much... My Nana-chan...{p}My little one..."
    s "「Thank you so much... My Nana-chan...{p}My little one...」"

# game/dialogs.rpy:2446
translate japanese secretnanami_9e1d86cb:

    # n "「I'm not little!..."
    n "「I'm not little!...」"

# game/dialogs.rpy:2471
translate japanese scene_R_13535866:

    # centered "{size=+35}CHAPTER 5\nThe festival{fast}{/size}"
    centered "{size=+35}第5章\n夏祭り{fast}{/size}"

# game/dialogs.rpy:2475
translate japanese scene_R_31591cda:

    # "The week went by quickly..."
    "一週間が経った。"

# game/dialogs.rpy:2478
translate japanese scene_R_57141cf2:

    # "Friday evening came. The day before the festival..."
    "そして、金曜の夕方が来た。祭りの前日だ。"

# game/dialogs.rpy:2481
translate japanese scene_R_1b27c60b:

    # "Preparations for the festival were awfully hard. But I promised Rika I'd help her out."
    "いや、大変だった。"

# game/dialogs.rpy:2482
translate japanese scene_R_2d950149:

    # "We had to prepare the stands and the decorations."
    "梨花ちゃんと約束した、祭りの屋台や飾りつけの手伝いをしたのだ。"

# game/dialogs.rpy:2483
translate japanese scene_R_9be7bc2a:

    # "Rika came to tell what to do and where to go."
    "梨花ちゃんはただそこに立って僕に物を置く場所などの指示をしている。"

# game/dialogs.rpy:2484
translate japanese scene_R_b0c123df:

    # hero "「Hey, maybe could you give us a hand!?"
    hero "「なあ、手伝ってはくれないのか？」"

# game/dialogs.rpy:2485
translate japanese scene_R_8a0b645d:

    # r "「I can't, I'm guiding you, idiot!"
    r "「出来るわけ無いやろ！ウチはアンタのガイドをしとるんやで、馬鹿！」"

# game/dialogs.rpy:2486
translate japanese scene_R_139a795e:

    # "We really don't have the same understanding of the word \"help.\" "
    "どうやらここには助け合いという概念は無いようだ…"

# game/dialogs.rpy:2488
translate japanese scene_R_08547004:

    # "I had to move some boxes inside the temple. I was alone."
    "僕は幾つか物を神社に運んだ。一人になった。"

# game/dialogs.rpy:2489
translate japanese scene_R_166ebec1:

    # "The boxes were inside a big dark room with a lot of old junk."
    "古いガラクタで溢れた部屋だ。"

# game/dialogs.rpy:2490
translate japanese scene_R_9899d4dc:

    # "It's so dark. I can't see a thing..."
    "暗い…暗くて物が見えない…"

# game/dialogs.rpy:2491
translate japanese scene_R_7f278c6a:

    # hero "「Rika-chan? Are you in there?"
    hero "「梨花ちゃん？そこに居るの？？」"

# game/dialogs.rpy:2492
translate japanese scene_R_a5dde9ae:

    # r "「Yes, come over here!"
    r "「ああ、こっちやで！」"

# game/dialogs.rpy:2493
translate japanese scene_R_cd339c0e:

    # "I follow the direction of her voice. But I suddenly stumbled onto something and I fell on the floor."
    "声がした方向に向かって進む。進む途中、急に何かに躓いて地面に倒れこんでしまった。"

# game/dialogs.rpy:2494
translate japanese scene_R_4aa7bb8a:

    # "Well, not exactly the floor. I fell on something warm and comfy."
    "いや、これは地面じゃないな…何か暖かくて心地よい…"

# game/dialogs.rpy:2495
translate japanese scene_R_e0fa74ca:

    # hero "「Ah, I fell on something..."
    hero "「ん、何だこれ…」"

# game/dialogs.rpy:2496
translate japanese scene_R_4642c748:

    # "My hand squeezed the mellow thing I was on."
    "何かこう凄い、凄い良い感じの物に触れた。"

# game/dialogs.rpy:2504
translate japanese scene_R_02b7fdcb:

    # hero "「What is this... I don't even-"
    hero "「一体これは…これは――」"

# game/dialogs.rpy:2505
translate japanese scene_R_d557eb58:

    # r "「Y-You {size=+20}IDIOT!!!!!!{/size}"
    r "「こんの、{size=+15}馬鹿！！！！！！！{/size}」"

# game/dialogs.rpy:2508
translate japanese scene_R_73674ca2:

    # "*{i}SMACK!{/i}*"
    "*{i}ドス！{/i}*"

# game/dialogs.rpy:2509
translate japanese scene_R_c81c00b8:

    # "Something hit my cheek and I instantly got up by instinct..."
    "頬を何かで殴られ、僕は本能的に立ち上がった。"

# game/dialogs.rpy:2510
translate japanese scene_R_ece959c9:

    # "I realized what I fell on. Or who I fell on..."
    "僕はやっと、それが何なのか理解した。"

# game/dialogs.rpy:2511
translate japanese scene_R_b70add4f:

    # r "「You PERVERT!!! Stop touching me!!!"
    r "「このヘンタイ！！ウチに触んなや！！！」"

# game/dialogs.rpy:2512
translate japanese scene_R_5462e3bc:

    # hero "「I stopped! I stopped!"
    hero "「もう触ってない、触ってないよ！」"

# game/dialogs.rpy:2513
translate japanese scene_R_5c272729:

    # hero "「I'm sorry, it was an accident!"
    hero "「や、ゴメン…これは事故なんだ！」"

# game/dialogs.rpy:2514
translate japanese scene_R_bd0bcebc:

    # r "「Turn the freakin light on and help me lift this box up there!"
    r "「ハア…そこの電気付けてそれ運ぶの手伝って欲しいんやけど…」"

# game/dialogs.rpy:2516
translate japanese scene_R_268b921a:

    # "Damn! This girl is nuts!!!"
    "何て無礼な奴なんだ！！アア！"

# game/dialogs.rpy:2517
translate japanese scene_R_a20cefa7:

    # "..."
    "……。"

# game/dialogs.rpy:2518
translate japanese scene_R_4d9f847b:

    # "Although her breast felt pretty nice...{p}It was warm... and mellow... and big..."
    "しかし胸の感触は中々素晴らしかった…{p}暖かく、豊潤で…芳醇で…"

# game/dialogs.rpy:2519
translate japanese scene_R_45a00c24:

    # "Eh!? What the hell am I thinking about!?"
    "おいおい、俺は一体何を考えているんだ！？"

# game/dialogs.rpy:2520
translate japanese scene_R_62d361bd:

    # "Rika is definitely not my type of girl. Not at all!"
    "こんな奴は僕のタイプじゃない！"

# game/dialogs.rpy:2522
translate japanese scene_R_9f1c3606:

    # "Plus I'm already with Sakura-chan!"
    "それに、僕には既に咲良ちゃんという人が居る。"

# game/dialogs.rpy:2524
translate japanese scene_R_076bdc09:

    # "Plus I'm already with Nanami-chan!"
    "それに、僕には既に七海ちゃんという人が居る。"

# game/dialogs.rpy:2526
translate japanese scene_R_a20cefa7_1:

    # "..."
    "…"

# game/dialogs.rpy:2532
translate japanese decision_31826e00:

    # "The day of the matsuri arrived."
    "今日は待ちに待ったお祭りの日だ。"

# game/dialogs.rpy:2533
translate japanese decision_400d0a26:

    # "As the club planned, I will go with Sakura-chan and Rika-chan. And maybe Nanami-chan if she's motivated."
    "As the club planned, I will go with Sakura-chan and Rika-chan. And maybe Nanami-chan if she's motivated."

# game/dialogs.rpy:2534
translate japanese decision_07ca0e28:

    # "I can't wait to see them!"
    "二人に会うのが待ちきれない！"

# game/dialogs.rpy:2545
translate japanese matsuri_S_2ef9ea22:

    # "I was waiting for Sakura in front of her house."
    "咲良ちゃんの家の前で待つ。"

# game/dialogs.rpy:2547
translate japanese matsuri_S_1dcaa12e:

    # "I was wearing my brand new yukata that she gave me."
    "今日は彼女に渡された例の新しい浴衣を着ている。"

# game/dialogs.rpy:2549
translate japanese matsuri_S_201b46ce:

    # "I was wearing the yukata I recently bought... One which was awfully expensive..."
    "今日僕は此間買ったあのクソ高かった浴衣を着ている…"

# game/dialogs.rpy:2550
translate japanese matsuri_S_9c64454a:

    # "She finally appeared, in a wonderful pink and red kimono."
    "咲良ちゃんが出てきた。美しい桃色と赤色の着物を着ている。"

# game/dialogs.rpy:2551
translate japanese matsuri_S_c633905a:

    # "She looked like a miko priestess. She was pretty..."
    "まるで巫女さんの様だ…可愛い…"

# game/dialogs.rpy:2553
translate japanese matsuri_S_678a9d48:

    # hero "「Wow... Sakura-chan... You look superb!"
    hero "「わあ…咲良ちゃん、今日はまた一段と素晴らしいね！」"

# game/dialogs.rpy:2554
translate japanese matsuri_S_4eeac269:

    # s "「%(stringhero)s-kun! The yukata suits you very nicely!"
    s "「%(stringhero)sくん！浴衣、とっても似合ってますよ！」"

# game/dialogs.rpy:2556
translate japanese matsuri_S_02c86c6c:

    # "She comes closer and whispers in my ear with a grin."
    "彼女は耳に近づいて悪戯っぽく囁いた。"

# game/dialogs.rpy:2557
translate japanese matsuri_S_9e55e999:

    # s "「I wonder what's hidden inside..."
    s "「浴衣の中に何が入ってるか…私、気になります！」"

# game/dialogs.rpy:2558
translate japanese matsuri_S_e44df551:

    # hero "「Hehe maybe you'll see later..."
    hero "「まあ、それはもう少し後で…」"

# game/dialogs.rpy:2560
translate japanese matsuri_S_96913011:

    # "I saw a woman come out of the house. That must be Sakura's mother."
    "彼女のお母さんが家から出てくるのが見えた。"

# game/dialogs.rpy:2561
translate japanese matsuri_S_af9ec1db:

    # "She looks incredibly young for a mother of an 18-year-old. She looks like she's in her early 30s at least!"
    "18の子供が居るにしては随分若い…35前後位に見える見た目だった。"

# game/dialogs.rpy:2565
translate japanese matsuri_S_6d0cbd15:

    # smom "Oh, you must be %(stringhero)s-san, I presume!"
    smom "「あら、あなたは%(stringhero)sさん、ですよね？」"

# game/dialogs.rpy:2566
translate japanese matsuri_S_fde87b49:

    # smom "My daughter never stops talking about you!"
    smom "「娘があなたの話ばかりしてくるんですよ。」"

# game/dialogs.rpy:2568
translate japanese matsuri_S_273b854b:

    # "Sakura looked embarrassed and blushed."
    "咲良ちゃんは恥ずかしそうにしている。"

# game/dialogs.rpy:2569
translate japanese matsuri_S_2bf9e723:

    # s "「Mom!!!"
    s "「母さん！！」"

# game/dialogs.rpy:2570
translate japanese matsuri_S_b8980021:

    # hero "「From the yukata you're wearing, I assume that you'll come to the matsuri too, ma'am?"
    hero "「その浴衣…祭りに行くんですか？」"

# game/dialogs.rpy:2572
translate japanese matsuri_S_ed2ca74b:

    # smom "Yes. Once my husband is ready, we'll go as well."
    smom "「ええ、主人も一緒に。」"

# game/dialogs.rpy:2573
translate japanese matsuri_S_5fc7645d:

    # s "「We'll be going on ahead, mom. See you later!"
    s "「私達は先に行ってるから、後でね！」"

# game/dialogs.rpy:2574
translate japanese matsuri_S_0c6a0f45:

    # smom "Have fun, you two!"
    smom "「楽しんできてね、二人とも！」"

# game/dialogs.rpy:2578
translate japanese matsuri_S_be3185f9:

    # "We were about to leave but then, Sakura's mother called me."
    "We were about to leave but then, Sakura's mother called me."

# game/dialogs.rpy:2580
translate japanese matsuri_S_6c045540:

    # "She spoke quietly."
    "She spoke quietly."

# game/dialogs.rpy:2581
translate japanese matsuri_S_8c173d23:

    # smom "I know that you and my daughter are going out."
    smom "「I know that you and my daughter are going out.」"

# game/dialogs.rpy:2582
translate japanese matsuri_S_c0582308:

    # "Yikes!"
    "Yikes!"

# game/dialogs.rpy:2583
translate japanese matsuri_S_a5ebdd09:

    # smom "Sakura told me how much you love each other."
    smom "「Sakura told me how much you love each other.」"

# game/dialogs.rpy:2584
translate japanese matsuri_S_043f4b2a:

    # smom "I'm so happy that my daughter found someone so kind and so understanding."
    smom "「I'm so happy that my daughter found someone so kind and so understanding.」"

# game/dialogs.rpy:2585
translate japanese matsuri_S_47abc600:

    # hero "「I-it's fine, ma'am... I...{p}I do love Sakura-chan. More than anything... No matter how she was born."
    hero "「I-it's fine, ma'am... I...{p}I do love Sakura-chan. More than anything... No matter how she was born.」"

# game/dialogs.rpy:2586
translate japanese matsuri_S_8a914f1f:

    # smom "That's sweet."
    smom "「That's sweet.」"

# game/dialogs.rpy:2587
translate japanese matsuri_S_9981c331:

    # smom "Can I count on you to make her the happiest woman she's always wished to be?"
    smom "「Can I count on you to make her the happiest woman she's always wished to be?」"

# game/dialogs.rpy:2588
translate japanese matsuri_S_707d7f10:

    # hero "「I promise I will, ma'am!"
    hero "「I promise I will, ma'am!」"

# game/dialogs.rpy:2590
translate japanese matsuri_S_c944f29d:

    # "She smiled and I went back to Sakura. We waved goodbye to Sakura's mom and made our way to the festival."
    "僕達は咲良ちゃんのお母さんに手を振って祭りに向かった。"

# game/dialogs.rpy:2591
translate japanese matsuri_S_463b86c5:

    # "Loud noises and music were becoming audible as we got closer to the festival."
    "既に祭りの喧騒や音楽が聞こえてくる。"

# game/dialogs.rpy:2596
translate japanese matsuri_S_9ab29675:

    # s "「What were you talking about with mom?"
    s "「What were you talking about with mom?」"

# game/dialogs.rpy:2597
translate japanese matsuri_S_b96431ab:

    # hero "「Umm... Nothing much..."
    hero "「Umm... Nothing much...」"

# game/dialogs.rpy:2598
translate japanese matsuri_S_3edbe663:

    # hero "「By the way, your mom looks incredibly young."
    hero "「若いお母さんだね。」"

# game/dialogs.rpy:2599
translate japanese matsuri_S_258d2b74:

    # s "「Yeah, she gave birth to me when she was only 16 years old."
    s "「ええ、16の時に私を産んだんです。」"

# game/dialogs.rpy:2600
translate japanese matsuri_S_bac1bc61:

    # s "「My father was her Japanese teacher."
    s "「父は彼女の国語の先生でした。」"

# game/dialogs.rpy:2601
translate japanese matsuri_S_d3f7f082:

    # hero "「Haha, sounds like a manga cliché!"
    hero "「ハハ、まるで漫画ですね。」"

# game/dialogs.rpy:2602
translate japanese matsuri_S_8dbb02af:

    # s "「Teehee! Yes it is!"
    s "「ええ、そうなんです！ふふっ。」"

# game/dialogs.rpy:2604
translate japanese matsuri_S_f72540d4:

    # "She smiled for a while, but then it grew thin."
    "彼女は笑ったが、暫くして表情が曇った。"

# game/dialogs.rpy:2605
translate japanese matsuri_S_e1554201:

    # s "「..."
    s "「……。」"

# game/dialogs.rpy:2606
translate japanese matsuri_S_c41191e9:

    # s "「I don't like my father very much..."
    s "「私は、父の事があまり好きではありません…」"

# game/dialogs.rpy:2607
translate japanese matsuri_S_b2052a3c:

    # hero "「Yeah, I remember why, I think. Because he doesn't understand you, right?"
    hero "「知ってます。あなたの事を理解してくれないから、ですよね？」"

# game/dialogs.rpy:2608
translate japanese matsuri_S_db0a0f02:

    # s "「Sometimes he scares me..."
    s "「時々父が怖いんです…」"

# game/dialogs.rpy:2609
translate japanese matsuri_S_9465293c:

    # s "「Recently he..."
    s "「此間なんか…」"

# game/dialogs.rpy:2610
translate japanese matsuri_S_557b9c7c:

    # s "「. . ."
    s "「……。」"

# game/dialogs.rpy:2611
translate japanese matsuri_S_2639bac0:

    # s "「He said he would never accept me as his daughter..."
    s "「父は私を絶対に娘だと認めないと…」"

# game/dialogs.rpy:2612
translate japanese matsuri_S_337c15c1:

    # "I didn't know what to say."
    "僕は頷いた。"

# game/dialogs.rpy:2613
translate japanese matsuri_S_e2c7c8ae:

    # "Her father seems to be confused with the case of his child."
    "彼女の父は時々狂乱する様だ。"

# game/dialogs.rpy:2614
translate japanese matsuri_S_0c7d97ed:

    # s "「In fact, he even admitted that he has always preferred having a real boy,"
    s "「父は、男の子を持つ事を夢見てたと言っていました。」"

# game/dialogs.rpy:2615
translate japanese matsuri_S_9f13d1a6:

    # s "「But I'm a boy with everything girly. And it makes him angry..."
    s "「それでも、私が女の子みたいで…それで父の怒りを買ってしまったんです…」"

# game/dialogs.rpy:2616
translate japanese matsuri_S_66341891:

    # "I nodded again."
    "僕はもう一度頷いた。"

# game/dialogs.rpy:2617
translate japanese matsuri_S_cd99f638:

    # hero "「And how does your mom feel, about everything?"
    hero "「それで、お母さんはどう思ってるんですか？」"

# game/dialogs.rpy:2619
translate japanese matsuri_S_257a9f41:

    # s "「She preferred a girl at the time, but she doesn't care at all, now."
    s "「彼女は女の子が良かったようですが、今では気にしていない様です。」"

# game/dialogs.rpy:2620
translate japanese matsuri_S_53c7acc4:

    # s "「No matter if I'm a boy or a girl, I'm her child first of all..."
    s "「私が男であろうと、女であろうと、子供には違いありません、と…」"

# game/dialogs.rpy:2621
translate japanese matsuri_S_08457630:

    # s "「She understands me and she takes me for who I am."
    s "「母は良き理解者として、私が誰かを教えてくれました。」"

# game/dialogs.rpy:2622
translate japanese matsuri_S_05bae488:

    # "I guessed that easily after the chat I had with her earlier."
    "I guessed that easily after the chat I had with her earlier."

# game/dialogs.rpy:2623
translate japanese matsuri_S_636c5687:

    # hero "「That's a good thing."
    hero "「ええ。」"

# game/dialogs.rpy:2624
translate japanese matsuri_S_9600023e:

    # hero "「If only your dad saw things like your mom does..."
    hero "「If only your dad saw things like your mom does...」"

# game/dialogs.rpy:2625
translate japanese matsuri_S_88285ade:

    # s "「Right."
    s "「Right.」"

# game/dialogs.rpy:2626
translate japanese matsuri_S_c43195f9:

    # hero "「You can count on your mother if something bad happens..."
    hero "「頼りになるお母さんですね。」"

# game/dialogs.rpy:2627
translate japanese matsuri_S_36e0944d:

    # hero "「And you know..."
    hero "「それに…」"

# game/dialogs.rpy:2628
translate japanese matsuri_S_5303d699:

    # hero "「You can count on Rika-chan, Nanami-chan, and me if you need."
    hero "「勿論、僕や梨花ちゃんと七海ちゃんにも頼ってくださいね。」"

# game/dialogs.rpy:2630
translate japanese matsuri_S_1b982e5f:

    # "Sakura smiled and held my arm tightly."
    "咲良ちゃんは笑って、僕の腕を強く抱いた。"

# game/dialogs.rpy:2631
translate japanese matsuri_S_2775e546:

    # s "「I know, %(stringhero)s-kun..."
    s "「分かってますよ、%(stringhero)sくん…」"

# game/dialogs.rpy:2636
translate japanese matsuri_S_8fa16578:

    # "We arrived at the festival and were joined by Rika-chan."
    "祭りに到着して、梨花ちゃんと合流した。"

# game/dialogs.rpy:2637
translate japanese matsuri_S_42b57c94:

    # "She was wearing a very pretty purple yukata that showed her shoulders outside."
    "何だかとってもセクシーな浴衣を着ている。"

# game/dialogs.rpy:2639
translate japanese matsuri_S_856a9a31:

    # r "「Hey hey hey, you lovers!!!"
    r "「ヘイヘイヘイヘイ、そこのお二人さん！」"

# game/dialogs.rpy:2641
translate japanese matsuri_S_aa24547d:

    # s "「Rika-chan!"
    s "「梨花ちゃん！」"

# game/dialogs.rpy:2642
translate japanese matsuri_S_70def7d1:

    # hero "「Rika-chan! Your yukata is great!!"
    hero "「梨花ちゃん！何やら素晴らしい浴衣だね！」"

# game/dialogs.rpy:2643
translate japanese matsuri_S_1c3fbf24:

    # r "「Pfft, I know what you really mean, you pervert! {image=heart.png}{p}How are you guys?"
    r "「エッチ。 {image=heart.png}{p}ごきげんよう。」"

# game/dialogs.rpy:2645
translate japanese matsuri_S_b140b475:

    # r "「My, my! Looks like our city rat has himself a nice yukata!"
    r "「お？お？この街ネズミがかっちょええ浴衣着とるやないかい！」"

# game/dialogs.rpy:2646
translate japanese matsuri_S_8051a326:

    # hero "「Thanks, Rika-chan- {p}Hey wait! I'm no city rat!!!"
    hero "「どうも、梨花ちゃん…{p}いやおい！誰が街ネズミだ！」"

# game/dialogs.rpy:2648
translate japanese matsuri_S_9cb75162:

    # hero "「By the way, where is Nanami-chan?"
    hero "「By the way, where is Nanami-chan?」"

# game/dialogs.rpy:2649
translate japanese matsuri_S_6683fd53:

    # r "「Knowing her, she probably went off to playing the shooting games."
    r "「Knowing her, she probably went off to playing the shooting games.」"

# game/dialogs.rpy:2650
translate japanese matsuri_S_0fef1f71:

    # hero "「Shooting games?"
    hero "「射的とか？」"

# game/dialogs.rpy:2651
translate japanese matsuri_S_8644460f:

    # hero "「Let's go join her and play too!"
    hero "「Let's go join her and play too!」"

# game/dialogs.rpy:2653
translate japanese matsuri_S_5a861c69:

    # s "「Oh I would love to!!!"
    s "「私、射的大好きです！」"

# game/dialogs.rpy:2654
translate japanese matsuri_S_e0fe552c:

    # r "「Beware, %(stringhero)s, Sakura is the best shooter in the whole village!"
    r "「気い付けや、%(stringhero)s、咲良ちゃんは村一番の射的の名手やで。」"

# game/dialogs.rpy:2655
translate japanese matsuri_S_64c71cc7:

    # hero "「We'll see about that!"
    hero "「それは実に楽しみです！」"

# game/dialogs.rpy:2656
translate japanese matsuri_S_acc4c1f3:

    # "So we walk to a shooting stand..."
    "そして射的の屋台に着いた。"

# game/dialogs.rpy:2660
translate japanese matsuri_S_1978a1c0:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2661
translate japanese matsuri_S_449ff6ae:

    # "Sakura made three perfect shots in a row on a big plushie of {i}ByeBye-Neko{/i} which fell on its back."
    "咲良ちゃんはバイバイ猫の巨大なぬいぐるみに三発ぶち込んで撃ち落とした。"

# game/dialogs.rpy:2662
translate japanese matsuri_S_5f5c92ba:

    # s "「Yay! I love plushies!!!"
    s "「ぬいぐるみ大好き！！」"

# game/dialogs.rpy:2663
translate japanese matsuri_S_d15d8e67:

    # n "「You're great at this, Sakura-nee!"
    n "「You're great at this, Sakura-nee!」"

# game/dialogs.rpy:2664
translate japanese matsuri_S_db10b573:

    # n "「It doesn't feel like it does in the arcade."
    n "「It doesn't feel like it does in the arcade.」"

# game/dialogs.rpy:2665
translate japanese matsuri_S_b799ab6b:

    # "I smiled."
    "フフフ。"

# game/dialogs.rpy:2666
translate japanese matsuri_S_dd8906ec:

    # "I knew what to do..."
    "さて…"

# game/dialogs.rpy:2667
translate japanese matsuri_S_0b28ef8e:

    # "I had to get another plushie for Sakura-chan!"
    "僕も咲良ちゃんにぬいぐるみを取ってあげようかな！"

# game/dialogs.rpy:2668
translate japanese matsuri_S_0d1e3f41:

    # hero "「My turn to try!"
    hero "「僕のターンだ。」"

# game/dialogs.rpy:2669
translate japanese matsuri_S_997b966e:

    # "I gave ¥100 to the guy at the stand, took the gun and targeted a cute crocodile plushie that seemed easy to get."
    "屋台のおじさんに100円を渡し、銃を構え、取りやすそうな可愛いワニのぬいぐるみに狙いを定めた。"

# game/dialogs.rpy:2670
translate japanese matsuri_S_1978a1c0_1:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2671
translate japanese matsuri_S_c8f69f16:

    # "Missed!"
    "当たらない…！"

# game/dialogs.rpy:2672
translate japanese matsuri_S_924fcb79:

    # hero "「Hold on, I'll try again!"
    hero "「ちょっと待って、もう一回！」"

# game/dialogs.rpy:2673
translate japanese matsuri_S_2ffa84ff:

    # "¥100 more on the table."
    "もう100円を置く。"

# game/dialogs.rpy:2674
translate japanese matsuri_S_1978a1c0_2:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2675
translate japanese matsuri_S_df4219e2:

    # "Darn!!! The plushie moved a bit but it wasn't enough for it to fall!"
    "ああっ！！ぬいぐるみは少し動いた。が、まだ落とせない！"

# game/dialogs.rpy:2676
translate japanese matsuri_S_6cc32b6b:

    # "¥100 more!"
    "もう100円！！"

# game/dialogs.rpy:2677
translate japanese matsuri_S_1978a1c0_3:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2678
translate japanese matsuri_S_bea4f24c:

    # "I got it!!! I got the plushie!!!"
    "取った！！取ったぞ！！"

# game/dialogs.rpy:2679
translate japanese matsuri_S_67dae2f0:

    # "I was about to give it to Sakura, but I remembered Rika and Nanami."
    "戦利品を咲良ちゃんに渡そうとしたとき、ふと思った。"

# game/dialogs.rpy:2680
translate japanese matsuri_S_ad68e441:

    # "Maybe I should get one for each of them too..."
    "もしかしたら梨花ちゃんと七海ちゃんにもあげた方がいいのかな…"

# game/dialogs.rpy:2681
translate japanese matsuri_S_52d0a5e1:

    # "As I was about to pay ¥100 more to the guy, Rika shoved herself through."
    "そう思って100円を置いた時、梨花ちゃんが来た。"

# game/dialogs.rpy:2682
translate japanese matsuri_S_602dd045:

    # r "「Hold it, city rat!!! It's my turn to play!!!"
    r "「待ち、ウチの番やで！」"

# game/dialogs.rpy:2683
translate japanese matsuri_S_8834e601:

    # r "「I'll get the same plushie with only three shots, you'll see!!!"
    r "「ウチが同じのを3発で取ったるさかい、見とき！」"

# game/dialogs.rpy:2684
translate japanese matsuri_S_2a606826:

    # "While Rika and Nanami were busy at the stand, I gave the plushie to Sakura."
    "梨花ちゃんがそうしている時に、僕は咲良ちゃんにぬいぐるみをあげた。"

# game/dialogs.rpy:2685
translate japanese matsuri_S_2c879bcb:

    # s "「For me?"
    s "「私にですか？」"

# game/dialogs.rpy:2686
translate japanese matsuri_S_c824434e:

    # s "「Oh, it's so cute!!!"
    s "「これ、かわいいですね！」"

# game/dialogs.rpy:2687
translate japanese matsuri_S_20fdfbfd:

    # s "「Thank you, %(stringhero)s-kun!!"
    s "「ありがとう、%(stringhero)sくん！」"

# game/dialogs.rpy:2688
translate japanese matsuri_S_f2aa2a51:

    # "She kissed me wildly in front of everybody, in a way that a man is more used to doing than a girl."
    "彼女は衆人環視の元ワイルドにキスをした。それは寧ろどこか男らしいキスだった。"

# game/dialogs.rpy:2689
translate japanese matsuri_S_fcea0f28:

    # "I felt embarrassed and shy, but happy..."
    "少し恥ずかしかったが、それでも嬉しかった。"

# game/dialogs.rpy:2690
translate japanese matsuri_S_15bd7f0a:

    # r "「Ah!!! Missed again, darn!"
    r "「ああ！また外してもうた…」"

# game/dialogs.rpy:2691
translate japanese matsuri_S_b3487258:

    # "Apparently, Rika-chan isn't very good at shooting games..."
    "どうやら梨花ちゃんはあんまり得意じゃ無いみたいだ…"

# game/dialogs.rpy:2692
translate japanese matsuri_S_4ca1680a:

    # "But she finally got a plushie too, after a while..."
    "それでも、何とか撃ち落とせた様だ。"

# game/dialogs.rpy:2693
translate japanese matsuri_S_016f8a4d:

    # "I think it's because the guy was seduced by Rika-chan's sexy yukata..."
    "屋台の人がセクシー浴衣に免じてオマケしてくれたのかも知れない。"

# game/dialogs.rpy:2694
translate japanese matsuri_S_20e94235:

    # "Gah, seriously..."
    "…どうやらそうだったみたいだ…"

# game/dialogs.rpy:2695
translate japanese matsuri_S_015dea9a:

    # hero "「How about we get some food now? I feel hungry."
    hero "「ちょっと何か食べない？お腹が空いたんだが…」"

# game/dialogs.rpy:2696
translate japanese matsuri_S_80443115:

    # r "「Excellent idea!!! Me too!!!"
    r "「そらええ！ウチもや！」"

# game/dialogs.rpy:2697
translate japanese matsuri_S_3e4fce92:

    # s "「Me too, a little."
    s "「私も少し…」"

# game/dialogs.rpy:2698
translate japanese matsuri_S_278f0f18:

    # r "「Let's go! I'll show you a good stand where they make excellent takoyaki!"
    r "「ほな、行こか！ごっつええ蛸焼きの店知っとるで！」"

# game/dialogs.rpy:2703
translate japanese takoyakis_b486162f:

    # "Our meal was just excellent, as Rika-chan promised!"
    "梨花ちゃんの言うとおり、ごっつええイカ揚げだった！"

# game/dialogs.rpy:2704
translate japanese takoyakis_b36de308:

    # hero "「I'm full!"
    hero "「お腹一杯だ！」"

# game/dialogs.rpy:2705
translate japanese takoyakis_c5430508:

    # r "「Told ya it was good! This is the best place to find them!"
    r "「Told ya it was good! This is the best place to find them!」"

# game/dialogs.rpy:2706
translate japanese takoyakis_f4e941cb:

    # r "「I want more! Hey, more of these please!!!"
    r "「ウチはまだ足りんで！じゃんじゃん持ってきや！」"

# game/dialogs.rpy:2707
translate japanese takoyakis_b42e6c2a:

    # n "「Can't... eat... more..."
    n "「私もう…無理」"

# game/dialogs.rpy:2708
translate japanese takoyakis_032debaa:

    # hero "「Rika-chan, how can you eat so much of this stuff？？？ Do you have a black hole instead of a stomach or what?"
    hero "「胃に穴でも開いてるのか？一体どこにこれだけ…」"

# game/dialogs.rpy:2709
translate japanese takoyakis_dc489790:

    # s "「Actually, Rika-chan can eat a mountain of food without adding on any pounds."
    s "「ええ、梨花ちゃんは山程食べても全く太らないんですよ。」"

# game/dialogs.rpy:2710
translate japanese takoyakis_11fca699:

    # hero "「Really? She's lucky."
    hero "「それは凄い。」"

# game/dialogs.rpy:2711
translate japanese takoyakis_70921009:

    # hero "「I have to control my own weight to make sure I don't end up too fat..."
    hero "「うーん僕もあまり太り過ぎない様にしなければ…」"

# game/dialogs.rpy:2712
translate japanese takoyakis_e6e2b7e9:

    # s "「Teehee! Same for me!"
    s "「私も！」"

# game/dialogs.rpy:2717
translate japanese takoyakis_b3e735ce:

    # "We finished the festival by watching the fireworks..."
    "最後は花火だった。"

# game/dialogs.rpy:2718
translate japanese takoyakis_d988d3e5:

    # "The show was fabulous."
    "綺麗だった…"

# game/dialogs.rpy:2719
translate japanese takoyakis_7e1eaf4f:

    # "At a moment, Sakura and I kissed discreetly in the dark under the fireworks..."
    "花火の下で、僕は咲良ちゃんと慎重にキスをした…"

# game/dialogs.rpy:2722
translate japanese takoyakis_f06e1942:

    # "Sakura-chan and I were on the way back home."
    "帰り道、二人で家に向かっていた。"

# game/dialogs.rpy:2723
translate japanese takoyakis_5cc5084b:

    # "I'm completely overwhelmed of happiness because of the festival and that night with Sakura-chan."
    "咲良ちゃんと一緒のお祭り、そしてこの夜。僕はあまりの幸せに眩暈がしてきた。"

# game/dialogs.rpy:2724
translate japanese takoyakis_6cf004ac:

    # "Without a second thought, I tried to act out a cliché from movies I've seen before."
    "いつか映画で聞いたようなセリフが、自然に口から出てきてしまった。"

# game/dialogs.rpy:2725
translate japanese takoyakis_60443369:

    # hero "「Want to come back to my place for a drink?"
    hero "「次は僕の部屋でチョコバナナとかどうかな？」"

# game/dialogs.rpy:2726
translate japanese takoyakis_52a57200:

    # s "「Huh...?"
    s "「むむ…？」"

# game/dialogs.rpy:2727
translate japanese takoyakis_676d485e:

    # "I suddenly noticed what I just said. I started to blush and spoke incoherently."
    "自分の言った事を改めて実感した。"

# game/dialogs.rpy:2728
translate japanese takoyakis_8d758b57:

    # hero "「I-I-I-I mean if... If you... Well I mean y-y--{nw}"
    hero "「え、え、えーっと今のは…もし良ければ…つまりその…{nw}」"

# game/dialogs.rpy:2729
translate japanese takoyakis_e043d770:

    # s "「Okay."
    s "「はい。」"

# game/dialogs.rpy:2730
translate japanese takoyakis_b16c33f4:

    # "I was surprised."
    "なんと。"

# game/dialogs.rpy:2732
translate japanese takoyakis_dc6dab16:

    # "Sakura watched me with a tinge of shyness."
    "咲良ちゃんは僕をほんのり恥ずかしそうな顔で見ている。"

# game/dialogs.rpy:2733
translate japanese takoyakis_74a6bdef:

    # "She looked like a cute begging kitten."
    "まるでミルクを欲しがる子犬の様な…"

# game/dialogs.rpy:2734
translate japanese takoyakis_da34f8b5:

    # s "「Y-Yes, take me to your home...%(stringhero)s-kun..."
    s "「じゃあ、わ、私を、家に連れてって下さい…%(stringhero)sくん…」"

# game/dialogs.rpy:2736
translate japanese takoyakis_ee16eceb:

    # "My heart skipped a beat."
    "鼓動が速くなる。"

# game/dialogs.rpy:2737
translate japanese takoyakis_ceb3d2b6:

    # "My own girlfriend... In a kimono...coming to my bedroom...for the night..."
    "彼女が…着物で…今夜…僕の寝室に…"

# game/dialogs.rpy:2738
translate japanese takoyakis_ddafe283:

    # "I'm pretty sure that we wouldn't just sleep that night..."
    "そのまま静かに就寝するだけじゃない事だけは確かだ…"

# game/dialogs.rpy:2742
translate japanese takoyakis_f412f669:

    # "Apparently my parents weren't back from the festival yet."
    "どうやら両親はまだ帰ってないみたいだ。"

# game/dialogs.rpy:2745
translate japanese takoyakis_4bb8cb02:

    # "We entered my house silently and went upstairs and into my bedroom."
    "僕たちは静かに家に入り、寝室に入った。"

# game/dialogs.rpy:2747
translate japanese takoyakis_86309b2e:

    # "When I turned on the lights, Sakura opened her eyes widely."
    "僕が電気を点けた時、咲良ちゃんは目を見開いた。"

# game/dialogs.rpy:2749
translate japanese takoyakis_abbe7785:

    # s "「Whoaaa!!!"
    s "「わああ！！」"

# game/dialogs.rpy:2750
translate japanese takoyakis_215547d3:

    # s "「It's incredible!!! All of these posters!"
    s "「凄い…！こんなにいっぱいアクションフィギュアが…！！」"

# game/dialogs.rpy:2751
translate japanese takoyakis_6430e2fa:

    # s "「Oh! It's {i}High School Samurai{/i}!"
    s "「これ、ハイスクールサムライですよね！！」"

# game/dialogs.rpy:2752
translate japanese takoyakis_52931ee2:

    # hero "「Haha, yeah. I took the habit of collecting posters..."
    hero "「ええ、今まで読んできた漫画の好きなキャラのフィギュアは全部集めてきたんです。」"

# game/dialogs.rpy:2753
translate japanese takoyakis_f00c4d4e:

    # s "「You bought them at Tokyo?"
    s "「東京で買ったんですか？」"

# game/dialogs.rpy:2754
translate japanese takoyakis_0551d793:

    # hero "「Yes. I was living near Gaymerz, the biggest manga shop of Tokyo."
    hero "「昔ゲィマーズという東京最大級の漫画ショップの隣に住んでましたからね。」"

# game/dialogs.rpy:2756
translate japanese takoyakis_b4f4239b:

    # s "「Oh, I've heard of it!"
    s "「そのお店、聞いた事あります！」"

# game/dialogs.rpy:2757
translate japanese takoyakis_4e9a74e5:

    # s "「I hope I can visit it someday."
    s "「私もいつか行ってみたいです…」"

# game/dialogs.rpy:2758
translate japanese takoyakis_05c9701b:

    # "As she was speaking, I hugged her back lovingly."
    "喋る彼女を後ろからそっと抱きかかえる。"

# game/dialogs.rpy:2763
translate japanese takoyakis_ef409314:

    # "We made sweet love to each other."
    "…その後、僕達は少しの間重なり合い、すぐに眠った。"

# game/dialogs.rpy:2764
translate japanese takoyakis_c6b9d465:

    # "We were so tired after this evening...We dozed off..."
    "今日は本当に疲れた…"

# game/dialogs.rpy:2767
translate japanese takoyakis_0cee0472:

    # "I woke up slowly."
    "僕はゆっくりと起きた。"

# game/dialogs.rpy:2768
translate japanese takoyakis_b2766fe2:

    # "Looking at the alarm clock, it's kinda late."
    "目覚まし時計を見る…結構経ったみたいだ。"

# game/dialogs.rpy:2769
translate japanese takoyakis_f87aab37:

    # "I felt something warm against me..."
    "何やら暖かい物が触れている…"

# game/dialogs.rpy:2770
translate japanese takoyakis_58e3a51c:

    # "It was Sakura."
    "咲良ちゃんだった。"

# game/dialogs.rpy:2771
translate japanese takoyakis_ec9d25fd:

    # "She was sleeping naked in my arms..."
    "彼女は腕の中で裸のまま眠っている…"

# game/dialogs.rpy:2772
translate japanese takoyakis_93a9f4ba:

    # "I smile and watched her sleeping..."
    "僕は笑って彼女が眠っているのを眺めていた。"

# game/dialogs.rpy:2773
translate japanese takoyakis_4774de34:

    # "Even after the obvious evidence I've seen, I still can't believe that she's actually a boy..."
    "事実を知ってしまった今でも、未だに彼女が男の子だという事が信じられない…"

# game/dialogs.rpy:2774
translate japanese takoyakis_c2358eed:

    # "She looks like a girl... She speaks with a voice of a girl... She smells like a girl... She has soft skin like a girl..."
    "どう見ても女の子だし…声も女の子で…笑顔も、それに柔らかな肌も、全部女の子だ…"

# game/dialogs.rpy:2775
translate japanese takoyakis_6f1e912a:

    # "I felt my heart beating sweetly as her warm sweet little hand was on my chest..."
    "彼女の手が僕の胸に触れる。"

# game/dialogs.rpy:2776
translate japanese takoyakis_eda104c0:

    # "She finally opened her eyes. As she saw me, she smiled and cuddled me tighter with a sweet sigh... and went back to sleep..."
    "咲良ちゃんの目が覚めた。僕を見て、笑って、抱きしめて、また寝た。"

# game/dialogs.rpy:2777
translate japanese takoyakis_0deabf9f:

    # "I sighed of happiness..."
    "僕は幸せに溜息を吐いた。"

# game/dialogs.rpy:2778
translate japanese takoyakis_a20cefa7:

    # "..."
    "……"

# game/dialogs.rpy:2779
translate japanese takoyakis_8b8ceb72:

    # "............"
    "…………………"

# game/dialogs.rpy:2780
translate japanese takoyakis_9ed134ce:

    # "Oh..."
    "あ…………"

# game/dialogs.rpy:2781
translate japanese takoyakis_c1399882:

    # "Oh no!!!{p}My parents!!!"
    "マズい！！{p}親が！！"

# game/dialogs.rpy:2782
translate japanese takoyakis_fb40c1b5:

    # "They're surely back from the festival!!!"
    "祭梨花ら帰って来たに違いない！"

# game/dialogs.rpy:2783
translate japanese takoyakis_3b788596:

    # "What are they gonna say if they see me with a girl in my bed？？？"
    "女の子とベッドに入ってるのを見て何て言うだろうか！？？"

# game/dialogs.rpy:2784
translate japanese takoyakis_2cffeee3:

    # "What should I do? Dammit, dammit, dammit!!!"
    "どうする…うわあああああああ！！"

# game/dialogs.rpy:2785
translate japanese takoyakis_c1fa5fd9:

    # hero "「Huh... Sakura-chan?..."
    hero "「ああ…咲良ちゃん？…」"

# game/dialogs.rpy:2786
translate japanese takoyakis_dfd2530f:

    # s "「{size=-5}%(stringhero)s-kun... I love you...{/size}"
    s "「{size=-5}%(stringhero)sくん…大好き…{/size}」"

# game/dialogs.rpy:2787
translate japanese takoyakis_817848c9:

    # "She fell asleep again!?"
    "参った…また寝てしまったぞ！"

# game/dialogs.rpy:2788
translate japanese takoyakis_0df5e11e:

    # "I stopped panicking to listen for a moment. I was waiting for an indication that would tell me if my parents were back."
    "僕は少しの間動かずに、耳を済ませて親が来ているかどうかを確かめた。"

# game/dialogs.rpy:2789
translate japanese takoyakis_9850f53c:

    # "Nothing."
    "それらしい音は聴こえない…"

# game/dialogs.rpy:2790
translate japanese takoyakis_d047fac0:

    # "I slowly moved out from my bed, letting Sakura sleep and looked outside the window."
    "咲良ちゃんを寝かしたまま、ベットから静かに出て、窓を覗いた。"

# game/dialogs.rpy:2791
translate japanese takoyakis_f6b451ec:

    # "My parent's car wasn't there... Maybe I can do something at last..."
    "車がない...どうにかなるやも知れないぞ..."

# game/dialogs.rpy:2792
translate japanese takoyakis_4d679905:

    # s "「%(stringhero)s-kun?...."
    s "「%(stringhero)sくん？...」"

# game/dialogs.rpy:2793
translate japanese takoyakis_08748d66:

    # "Sakura just woke up and sat on the bed."
    "咲良ちゃんが起きてベッドに座った。"

# game/dialogs.rpy:2794
translate japanese takoyakis_f25fd7bb:

    # hero "「Sakura-chan..."
    hero "「咲良ちゃん...」"

# game/dialogs.rpy:2795
translate japanese takoyakis_ddcb931f:

    # hero "「My parents aren't here yet. You should go home now before they come back."
    hero "「親がまだ来てない、来る前にどうにか抜け出そう。」"

# game/dialogs.rpy:2796
translate japanese takoyakis_7295fac4:

    # hero "「My parents are kinda strict when it comes to relationships."
    hero "「恋愛には結構厳格な親なんだ。」"

# game/dialogs.rpy:2797
translate japanese takoyakis_b3eeb634:

    # "Sakura nodded. She looked all sleepy."
    "咲良ちゃんは眠そうに頷いた。"

# game/dialogs.rpy:2798
translate japanese takoyakis_aee46875:

    # "That was cute. I smiled."
    "可愛い。"

# game/dialogs.rpy:2799
translate japanese takoyakis_8891ed58:

    # hero "「Aww. Had too much fun last night?"
    hero "「昨夜はお楽しみでしたね。」"

# game/dialogs.rpy:2800
translate japanese takoyakis_e15b24d1:

    # "Sakura giggled. She stood up and took me in her arms."
    "咲良ちゃんは笑った。僕は手を貸して起き上がらせた。"

# game/dialogs.rpy:2801
translate japanese takoyakis_67bc7996:

    # "Her warm naked body was against mine."
    "暖かい体が触れる。"

# game/dialogs.rpy:2802
translate japanese takoyakis_387596f6:

    # "I kissed her again."
    "僕らはもう一度キスをした。"

# game/dialogs.rpy:2804
translate japanese takoyakis_7dbc9795:

    # "After some time, I helped her put on her kimono and took her to her house..."
    "着物を着せるのを手伝い、家まで送った。"

# game/dialogs.rpy:2805
translate japanese takoyakis_5ffe88e0:

    # "We promised to have another date very soon..."
    "またすぐにデートをする約束をして..."

# game/dialogs.rpy:2806
translate japanese takoyakis_17899d6b:

    # "I went back home and turned on my computer."
    "僕は部屋に戻って、コンピューターを点けた。"

# game/dialogs.rpy:2810
translate japanese takoyakis_2b150c3e:

    # write "Dear sister,"
    write "姉さんへ。"

# game/dialogs.rpy:2812
translate japanese takoyakis_a50c844b:

    # write "How are things? It's been awhile since I wrote."
    write "最近どうだい？ It's been awhile since I wrote."

# game/dialogs.rpy:2814
translate japanese takoyakis_54b9d70d:

    # write "You'll never guess what happened!"
    write "僕があれからどうなったか分かるかい！"

# game/dialogs.rpy:2816
translate japanese takoyakis_67d2ac31:

    # write "I'm going out with someone!"
    write "今、付き合ってるんだ！"

# game/dialogs.rpy:2818
translate japanese takoyakis_df940a1c:

    # write "She's a wonderful person... A unique person...{w} It's kinda hard to tell you more about this...{w}You have to promise that you'll never tell our parents!"
    write "素晴らしい人だよ、そしてユニークだ。{w}これ以上の事は言いづらいんだけど…{w}少なくとも親には内緒にして欲しいね。"

# game/dialogs.rpy:2820
translate japanese takoyakis_bc7a4cdb:

    # write "At least, I'm happy... More happy than I have ever been... It's as if I just got the best Christmas present ever."
    write "何にせよ、僕は今幸せだよ。今までで最高のクリスマスプレゼントを貰った時みたいな気分だ…"

# game/dialogs.rpy:2821
translate japanese takoyakis_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2823
translate japanese takoyakis_bf522c92:

    # write "I'll send you a photo of us. I don't know how much longer our story will last, but I'm sure I'll never forget it for the rest of my life. What I'm living is really unique!"
    write "僕たちの写真を送るよ。どれだけ続くかは分からないけれど、この事を一生忘れない事だけは確かだ。"

# game/dialogs.rpy:2825
translate japanese takoyakis_f52f892c:

    # write "I hope everything is okay for you too and that you are as happy as I am..."
    write "それと、姉さん達二人も元気で居られる様に願ってるよ。僕たちと同じくらいの幸せも..."

# game/dialogs.rpy:2827
translate japanese takoyakis_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "%(stringhero)sより"

# game/dialogs.rpy:2829
translate japanese takoyakis_4387dacf:

    # write "PS: {w}The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write "PS：{w}夏祭りは素晴らしかったよ！東京でも似たようなイベントがあるといいね。"

# game/dialogs.rpy:2830
translate japanese takoyakis_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2852
translate japanese matsuri_N_46751f32:

    # "I came to the festival with Sakura."
    "I came to the festival with Sakura."

# game/dialogs.rpy:2853
translate japanese matsuri_N_17894765:

    # "She was wearing a cute yukata. It fit her well."
    "She was wearing a cute yukata. It fit her well."

# game/dialogs.rpy:2855
translate japanese matsuri_N_ec440c0a:

    # "I was wearing the brand new yukata that she gave me."
    "I was wearing the brand new yukata that she gave me."

# game/dialogs.rpy:2857
translate japanese matsuri_N_201b46ce:

    # "I was wearing the yukata I recently bought... One which was awfully expensive..."
    "I was wearing the yukata I recently bought... One which was awfully expensive..."

# game/dialogs.rpy:2858
translate japanese matsuri_N_38154d05:

    # "We quickly found Nanami and Rika despite the big crowd."
    "We quickly found Nanami and Rika despite the big crowd."

# game/dialogs.rpy:2863
translate japanese matsuri_N_866e240e:

    # "Rika was wearing a very pretty purple yukata that showed her shoulders outside."
    "Rika was wearing a very pretty purple yukata that showed her shoulders outside."

# game/dialogs.rpy:2864
translate japanese matsuri_N_e57fd5f6:

    # "Nanami was wearing a more simple green yukata but it was incredibly cute on her."
    "Nanami was wearing a more simple green yukata but it was incredibly cute on her."

# game/dialogs.rpy:2865
translate japanese matsuri_N_c1358676:

    # r "「So, what we do first? I can't wait!"
    r "「So, what we do first? I can't wait!」"

# game/dialogs.rpy:2867
translate japanese matsuri_N_e60b3319:

    # n "「I want to go to the shooting stands!"
    n "「I want to go to the shooting stands!」"

# game/dialogs.rpy:2868
translate japanese matsuri_N_a6a52bc4:

    # s "「That sounds nice!"
    s "「That sounds nice!」"

# game/dialogs.rpy:2869
translate japanese matsuri_N_e8f6b7d0:

    # hero "「I'd like that too!"
    hero "「I'd like that too!」"

# game/dialogs.rpy:2870
translate japanese matsuri_N_f8974ce9:

    # r "「Let's go!"
    r "「Let's go!」"

# game/dialogs.rpy:2872
translate japanese matsuri_N_228ae33a:

    # "We arrived just in time. The stand was pretty empty."
    "We arrived just in time. The stand was pretty empty."

# game/dialogs.rpy:2874
translate japanese matsuri_N_4cdd6550:

    # n "「Hey, %(stringhero)s-nii... I challenge you to make a plushie fall before I do!"
    n "「Hey, %(stringhero)s-nii... I challenge you to make a plushie fall before I do!」"

# game/dialogs.rpy:2875
translate japanese matsuri_N_2635f363:

    # hero "「I thought you weren't good at shooting games."
    hero "「I thought you weren't good at shooting games.」"

# game/dialogs.rpy:2876
translate japanese matsuri_N_88c489c7:

    # n "「I trained hard! You'll see!"
    n "「I trained hard! You'll see!」"

# game/dialogs.rpy:2881
translate japanese matsuri_N_cd54b02a:

    # "We both payed ¥100 and started to frantically shoot at the targets with our toy rifles."
    "We both payed ¥100 and started to frantically shoot at the targets with our toy rifles."

# game/dialogs.rpy:2882
translate japanese matsuri_N_1978a1c0:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2883
translate japanese matsuri_N_d4bb63e2:

    # "We both missed... We tried again."
    "We both missed... We tried again."

# game/dialogs.rpy:2884
translate japanese matsuri_N_1978a1c0_1:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2885
translate japanese matsuri_N_df8da603:

    # "We hit the same plush but it barely moved."
    "We hit the same plush but it barely moved."

# game/dialogs.rpy:2886
translate japanese matsuri_N_8d01865c:

    # n "「Eeeeh!"
    n "「Eeeeh!」"

# game/dialogs.rpy:2887
translate japanese matsuri_N_1feb4bc0:

    # hero "「No fair! It was a critical hit!"
    hero "「No fair! It was a critical hit!」"

# game/dialogs.rpy:2888
translate japanese matsuri_N_cf97790a:

    # n "「I'm not done yet!"
    n "「I'm not done yet!」"

# game/dialogs.rpy:2889
translate japanese matsuri_N_3093850e:

    # "We tried again once more..."
    "We tried again once more..."

# game/dialogs.rpy:2890
translate japanese matsuri_N_1978a1c0_2:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2891
translate japanese matsuri_N_ffac77fc:

    # "I got it! I got the plush!"
    "I got it! I got the plush!"

# game/dialogs.rpy:2895
translate japanese matsuri_N_2530a4ee:

    # hero "「Haha I won!"
    hero "「Haha I won!」"

# game/dialogs.rpy:2896
translate japanese matsuri_N_638da9e6:

    # "It was a cute plush of crocodile."
    "It was a cute plush of crocodile."

# game/dialogs.rpy:2897
translate japanese matsuri_N_b9dca595:

    # "Nanami started to pout."
    "Nanami started to pout."

# game/dialogs.rpy:2898
translate japanese matsuri_N_0d531ead:

    # n "「Eeeeh that's unfair, you got it because I hit it a couple times already!"
    n "「Eeeeh that's unfair, you got it because I hit it a couple times already!」"

# game/dialogs.rpy:2899
translate japanese matsuri_N_1bdd906d:

    # n "「I wanted that plush!"
    n "「I wanted that plush!」"

# game/dialogs.rpy:2900
translate japanese matsuri_N_adab4cae:

    # "I laughed and rubbed her hair."
    "I laughed and rubbed her hair."

# game/dialogs.rpy:2901
translate japanese matsuri_N_e0e8e5a5:

    # hero "「Don't be silly. {p}I got it for you."
    hero "「Don't be silly. {p}I got it for you.」"

# game/dialogs.rpy:2902
translate japanese matsuri_N_c55aa101:

    # "I gave Nanami the plushie."
    "I gave Nanami the plushie."

# game/dialogs.rpy:2903
translate japanese matsuri_N_3eb6b6ea:

    # "She looked at me with a cute expression, then she gave me a big hug."
    "She looked at me with a cute expression, then she gave me a big hug."

# game/dialogs.rpy:2905
translate japanese matsuri_N_5693f2ef:

    # n "「Yaaay! Thank you, %(stringhero)s-nii!"
    n "「Yaaay! Thank you, %(stringhero)s-nii!」"

# game/dialogs.rpy:2906
translate japanese matsuri_N_162f5687:

    # r "「Hey, get a room, you two!"
    r "「Hey, get a room, you two!」"

# game/dialogs.rpy:2907
translate japanese matsuri_N_9a893c8c:

    # "We all laughed."
    "We all laughed."

# game/dialogs.rpy:2912
translate japanese matsuri_N_9ced4dba:

    # hero "「I'm hungry... How about we get some food now?"
    hero "「I'm hungry... How about we get some food now?」"

# game/dialogs.rpy:2917
translate japanese matsuri_N_80443115:

    # r "「Excellent idea!!! Me too!!!"
    r "「Excellent idea!!! Me too!!!」"

# game/dialogs.rpy:2918
translate japanese matsuri_N_01dbb1d1:

    # s "「I'm a little hungry too."
    s "「I'm a little hungry too.」"

# game/dialogs.rpy:2919
translate japanese matsuri_N_5e163699:

    # r "「Let's go! I'll show you an amazing stand where they make the best takoyaki!"
    r "「Let's go! I'll show you an amazing stand where they make the best takoyaki!」"

# game/dialogs.rpy:2926
translate japanese matsuri_N2_b3e735ce:

    # "We finished the festival by watching the fireworks..."
    "We finished the festival by watching the fireworks..."

# game/dialogs.rpy:2927
translate japanese matsuri_N2_d988d3e5:

    # "The show was fabulous."
    "The show was fabulous."

# game/dialogs.rpy:2930
translate japanese matsuri_N2_576baed2:

    # "As we we walked around some more, Nanami took my hand and guided me into the forest behind the temple..."
    "As we we walked around some more, Nanami took my hand and guided me into the forest behind the temple..."

# game/dialogs.rpy:2931
translate japanese matsuri_N2_c50c979d:

    # "We made discreet, passionate love behind the bushes..."
    "We made discreet, passionate love behind the bushes..."

# game/dialogs.rpy:2932
translate japanese matsuri_N2_633fea5f:

    # "Now that's what I call a night!"
    "Now that's what I call a night!"

# game/dialogs.rpy:2935
translate japanese matsuri_N2_2ab4bd94:

    # "After escorting Sakura to her house, I went back to my own."
    "After escorting Sakura to her house, I went back to my own."

# game/dialogs.rpy:2937
translate japanese matsuri_N2_6f974e08:

    # "My parents weren't back yet so I just goofed around and read some manga.{p} I also decided to write to my sister."
    "My parents weren't back yet so I just goofed around and read some manga.{p} I also decided to write to my sister."

# game/dialogs.rpy:2940
translate japanese matsuri_N2_2b150c3e:

    # write "Dear sister,"
    write "Dear sister,"

# game/dialogs.rpy:2942
translate japanese matsuri_N2_92248cbb:

    # write "How are things?"
    write "How are things?"

# game/dialogs.rpy:2944
translate japanese matsuri_N2_bb83fcbc:

    # write "You'll never guess what happened to me actually!"
    write "You'll never guess what happened to me actually!"

# game/dialogs.rpy:2946
translate japanese matsuri_N2_4967d059:

    # write "I'm going out with a girl!"
    write "I'm going out with a girl!"

# game/dialogs.rpy:2948
translate japanese matsuri_N2_534e4421:

    # write "Her name is Nanami. She's a really cute and energetic girl from school.{w} We play a lot of video games together. We're really into each other!"
    write "Her name is Nanami. She's a really cute and energetic girl from school.{w} We play a lot of video games together. We're really into each other!"

# game/dialogs.rpy:2950
translate japanese matsuri_N2_4f13aea8:

    # write "I'm happy... Really happy... I can't wait to introduce her to you, someday! {w}I'll send you a photo of us soon!"
    write "I'm happy... Really happy... I can't wait to introduce her to you, someday! {w}I'll send you a photo of us soon!"

# game/dialogs.rpy:2951
translate japanese matsuri_N2_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2953
translate japanese matsuri_N2_ab4193c3:

    # write "I hope everything going well for you! I can't wait to see you again!"
    write "I hope everything going well for you! I can't wait to see you again!"

# game/dialogs.rpy:2955
translate japanese matsuri_N2_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "Your dear brother{p}%(stringhero)s"

# game/dialogs.rpy:2957
translate japanese matsuri_N2_91609d26:

    # write "PS:{w} The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write "PS:{w} The local summer festival was great! I hope you had a wonderful event like this in Tokyo."

# game/dialogs.rpy:2959
translate japanese matsuri_N2_deb10d39:

    # "I heard my cellphone ring."
    "I heard my cellphone ring."

# game/dialogs.rpy:2964
translate japanese matsuri_N2_dbd7e6ae:

    # hero "「Hello?"
    hero "「Hello?」"

# game/dialogs.rpy:2965
translate japanese matsuri_N2_77c066ad:

    # n "「{i}It's me, dude!{/i}"
    n "『It's me, dude!』"

# game/dialogs.rpy:2966
translate japanese matsuri_N2_f295726b:

    # "I chuckled as I recognized Nanami's voice."
    "I chuckled as I recognized Nanami's voice."

# game/dialogs.rpy:2967
translate japanese matsuri_N2_404c955e:

    # hero "「We're going out and you still call me dude?"
    hero "「We're going out and you still call me dude?」"

# game/dialogs.rpy:2968
translate japanese matsuri_N2_284b7d55:

    # n "「{i}Well, you're a guy after all, right?{/i}"
    n "『Well, you're a guy after all, right?』"

# game/dialogs.rpy:2969
translate japanese matsuri_N2_770ce25a:

    # n "「{i}I've seen you from enough close to see it with my own eyes!{/i} {image=heart.png}"
    n "『I've seen you from enough close to see it with my own eyes! {image=heart.png}』"

# game/dialogs.rpy:2970
translate japanese matsuri_N2_7139659c:

    # "I blushed, remembering what we did earlier in the forest."
    "I blushed, remembering what we did earlier in the forest."

# game/dialogs.rpy:2971
translate japanese matsuri_N2_e86fa184:

    # hero "「Hehe... That was good, yeah..."
    hero "「Hehe... That was good, yeah...」"

# game/dialogs.rpy:2972
translate japanese matsuri_N2_7bfb8ae4:

    # n "「{i}Teehee! To be honest... the more we do it, the more I want to do it again!{/i} {image=heart.png}"
    n "『Teehee! To be honest... the more we do it, the more I want to do it again! {image=heart.png}』"

# game/dialogs.rpy:2973
translate japanese matsuri_N2_69a9deb4:

    # n "「{i}Speaking which, are you busy tomorrow?{/i}"
    n "『Speaking which, are you busy tomorrow?』"

# game/dialogs.rpy:2974
translate japanese matsuri_N2_2224fcb7:

    # hero "「Except coming to see you? No, nothing at all."
    hero "「Except coming to see you? No, nothing at all.」"

# game/dialogs.rpy:2975
translate japanese matsuri_N2_8da73ec6:

    # "I laughed and heard Nanami laughing on the other end as well."
    "I laughed and heard Nanami laughing on the other end as well."

# game/dialogs.rpy:2976
translate japanese matsuri_N2_26411a89:

    # n "「{i}Alright, %(stringhero)s-nii. {w}I'll wait for you at el-{nw}{/i}"
    n "『Alright, %(stringhero)s-nii. {w}I'll wait for you at el-{nw}』"

# game/dialogs.rpy:2977
translate japanese matsuri_N2_587273a6:

    # n "「{i}.........{/i}"
    n "『.........』"

# game/dialogs.rpy:2978
translate japanese matsuri_N2_fd4b149d:

    # hero "「Hello? Nanami-chan?"
    hero "「Hello? Nanami-chan?」"

# game/dialogs.rpy:2979
translate japanese matsuri_N2_97743246:

    # n "「{i}{size=-12}It's my boyfriend.....{p}It's none of your business!!......{/size}{/i}"
    n "{size=-8}『It's my boyfriend.....{p}It's none of your business!!......』{/size}"

# game/dialogs.rpy:2980
translate japanese matsuri_N2_5027b83e:

    # n "「{i}{size=-12}Big brother!!!......{/size}{/i}"
    n "{size=-8}『Big brother!!!......』{/size}"

# game/dialogs.rpy:2981
translate japanese matsuri_N2_2f756018:

    # n "「{i}I'll call you back.{/i}"
    n "『I'll call you back.』"

# game/dialogs.rpy:2983
translate japanese matsuri_N2_6c3f05d2:

    # "*{i}click{/i}*"
    "*{i}click{/i}*"

# game/dialogs.rpy:2984
translate japanese matsuri_N2_2e0a800f:

    # "What was that about...? I shrugged and went back to my computer."
    "What was that about...? I shrugged and went back to my computer."

# game/dialogs.rpy:2985
translate japanese matsuri_N2_73d69e81:

    # "I have a bad feeling about this... {p}But, she said she'll call back."
    "I have a bad feeling about this... {p}But, she said she'll call back."

# game/dialogs.rpy:2988
translate japanese matsuri_N2_1569a42e:

    # "Some time passed as I goofed around. All of a sudden, I heard the doorbell."
    "Some time passed as I goofed around. All of a sudden, I heard the doorbell."

# game/dialogs.rpy:2990
translate japanese matsuri_N2_8ef37003:

    # "Suddenly, I got goosebumps. A sense of dread came over me. It pushed me to run downstairs and to open the door as fast I could."
    "Suddenly, I got goosebumps. A sense of dread came over me. It pushed me to run downstairs and to open the door as fast I could."

# game/dialogs.rpy:2992
translate japanese matsuri_N2_ee440385:

    # "Nanami was at the door. She was in tears."
    "Nanami was at the door. She was in tears."

# game/dialogs.rpy:2993
translate japanese matsuri_N2_e3c36773:

    # "When I opened up, she ran in the living room."
    "When I opened up, she ran in the living room."

# game/dialogs.rpy:2994
translate japanese matsuri_N2_fe828537:

    # hero "「Nanami-chan?? {p}Are you okay? What's going on?!"
    hero "「Nanami-chan?? {p}Are you okay? What's going on?!」"

# game/dialogs.rpy:2995
translate japanese matsuri_N2_0b51bf1c:

    # n "「He's coming! He's scaring me, %(stringhero)s-nii!"
    n "「He's coming! He's scaring me, %(stringhero)s-nii!」"

# game/dialogs.rpy:2996
translate japanese matsuri_N2_2952dd4d:

    # hero "「Who? Who's coming?"
    hero "「Who? Who's coming?」"

# game/dialogs.rpy:2997
translate japanese matsuri_N2_817e602f:

    # "I got my answer right after I asked."
    "I got my answer right after I asked."

# game/dialogs.rpy:2998
translate japanese matsuri_N2_d7a874f3:

    # "A guy entered the house with a demented look on his face."
    "A guy entered the house with a demented look on his face."

# game/dialogs.rpy:2999
translate japanese matsuri_N2_5b3076dd:

    # "I recognize him. He was on Nanami's picture frame in her bedroom."
    "I recognize him. He was on Nanami's picture frame in her bedroom."

# game/dialogs.rpy:3000
translate japanese matsuri_N2_203812e1:

    # hero "「Are you Toshio-san?"
    hero "「Are you Toshio-san?」"

# game/dialogs.rpy:3001
translate japanese matsuri_N2_23531e72:

    # "He ignored my question and barged in straight toward Nanami."
    "He ignored my question and barged in straight toward Nanami."

# game/dialogs.rpy:3003
translate japanese matsuri_N2_f15432c1:

    # "I instinctively interposed myself between them."
    "I instinctively interposed myself between them."

# game/dialogs.rpy:3004
translate japanese matsuri_N2_5d32f865:

    # hero "「Whoa! Wait wait wait... What are you going to do to her?!"
    hero "「Whoa! Wait wait wait... What are you going to do to her?!」"

# game/dialogs.rpy:3005
translate japanese matsuri_N2_91b34485:

    # toshio "「Nanami! Come back home right now!"
    toshio "「Nanami! Come back home right now!」"

# game/dialogs.rpy:3006
translate japanese matsuri_N2_54fe86a6:

    # n "「No! You're scaring me, big brother!"
    n "「No! You're scaring me, big brother!」"

# game/dialogs.rpy:3007
translate japanese matsuri_N2_9e884cfa:

    # "Then Toshio outrageously pointed at me."
    "Then Toshio outrageously pointed at me."

# game/dialogs.rpy:3008
translate japanese matsuri_N2_b495b159:

    # toshio "「And you...{p}How dare you soil my sister!"
    toshio "「And you...{p}How dare you soil my sister!」"

# game/dialogs.rpy:3009
translate japanese matsuri_N2_4d869fcc:

    # "What?!{p}Soil her?! He's crazy!"
    "What?!{p}Soil her?! He's crazy!"

# game/dialogs.rpy:3010
translate japanese matsuri_N2_47b87910:

    # hero "「Hey man, calm down!"
    hero "「Hey man, calm down!」"

# game/dialogs.rpy:3011
translate japanese matsuri_N2_5f2f5e83:

    # hero "「Calm down and listen, please!"
    hero "「Calm down and listen, please!」"

# game/dialogs.rpy:3012
translate japanese matsuri_N2_c025cc4c:

    # "Toshio ignored my words completely and approached me menacingly."
    "Toshio ignored my words completely and approached me menacingly."

# game/dialogs.rpy:3013
translate japanese matsuri_N2_d17a127d:

    # "Oh shit, he's going to kill me!"
    "Oh shit, he's going to kill me!"

# game/dialogs.rpy:3014
translate japanese matsuri_N2_8d99340f:

    # "In pure panic, I spoke what came to mind."
    "In pure panic, I spoke what came to mind."

# game/dialogs.rpy:3015
translate japanese matsuri_N2_d2e3bc0f:

    # hero "「Just calm down! I didn't soil her! I-"
    hero "「Just calm down! I didn't soil her! I-」"

# game/dialogs.rpy:3017
translate japanese matsuri_N2_499d2845:

    # hero "「{size=+15}I love her!!{/size}"
    hero "「{size=+15}I love her!!{/size}」"

# game/dialogs.rpy:3019
translate japanese matsuri_N2_e3f58031:

    # "There was an awkward silence in the room."
    "There was an awkward silence in the room."

# game/dialogs.rpy:3020
translate japanese matsuri_N2_0ca571d7:

    # "Toshio had stopped walking... He looked more upset than ever."
    "Toshio had stopped walking... He looked more upset than ever."

# game/dialogs.rpy:3021
translate japanese matsuri_N2_cef855da:

    # "Strangely, saying that out loud gave me more courage and strength."
    "Strangely, saying that out loud gave me more courage and strength."

# game/dialogs.rpy:3022
translate japanese matsuri_N2_e6bb1d42:

    # hero "「I love her... I love her with all my heart."
    hero "「I love her... I love her with all my heart.」"

# game/dialogs.rpy:3023
translate japanese matsuri_N2_d225817d:

    # hero "「I'm in love with her!... All my intentions towards her are from the bottom of my heart!"
    hero "「I'm in love with her!... All my intentions towards her are from the bottom of my heart!」"

# game/dialogs.rpy:3024
translate japanese matsuri_N2_f4b05b05:

    # toshio "「How... How dare you..."
    toshio "「How... How dare you...」"

# game/dialogs.rpy:3028
translate japanese matsuri_N2_bc61413a:

    # "Nanami came between us, protecting me this time. She was holding me tight."
    "Nanami came between us, protecting me this time. She was holding me tight."

# game/dialogs.rpy:3029
translate japanese matsuri_N2_ff4ee66e:

    # "Toshio stopped again."
    "Toshio stopped again."

# game/dialogs.rpy:3030
translate japanese matsuri_N2_95c7f55c:

    # toshio "「Nanami, get the fuck off him!!!"
    toshio "「Nanami, get the fuck off him!!!」"

# game/dialogs.rpy:3031
translate japanese matsuri_N2_5050139a:

    # n "「No! Stop it, Big brother!!"
    n "「No! Stop it, Big brother!!」"

# game/dialogs.rpy:3032
translate japanese matsuri_N2_746a8fc4:

    # n "「If you want to hit him, then hit me first!"
    n "「If you want to hit him, then hit me first!」"

# game/dialogs.rpy:3033
translate japanese matsuri_N2_1053aa7b:

    # n "「I love him too, big brother!!{p}{size=+15}I love him!!!{/size}"
    n "「I love him too, big brother!!{p}{size=+15}I love him!!!{/size}」"

# game/dialogs.rpy:3034
translate japanese matsuri_N2_bb6eef02:

    # n "「He did nothing wrong! I'm the only culprit!"
    n "「He did nothing wrong! I'm the only culprit!」"

# game/dialogs.rpy:3035
translate japanese matsuri_N2_7a8a2dfd:

    # "Toshio stood motionless."
    "Toshio stood motionless."

# game/dialogs.rpy:3036
translate japanese matsuri_N2_9b1a0a97:

    # "Nanami's confession filled me with joy... But I was too scared to savor it fully."
    "Nanami's confession filled me with joy... But I was too scared to savor it fully."

# game/dialogs.rpy:3037
translate japanese matsuri_N2_a79664fb:

    # "Tears were coming from Nanami's eyes. I knew she meant it. She was just as terrified."
    "Tears were coming from Nanami's eyes. I knew she meant it. She was just as terrified."

# game/dialogs.rpy:3038
translate japanese matsuri_N2_38b0386e:

    # toshio "「Nanami..."
    toshio "「Nanami...」"

# game/dialogs.rpy:3039
translate japanese matsuri_N2_e4239ff7:

    # toshio "「You can't...{p}You can't love him. You shouldn't."
    toshio "「You can't...{p}You can't love him. You shouldn't.」"

# game/dialogs.rpy:3041
translate japanese matsuri_N2_89e8a925:

    # toshio "「If you love him, someday he'll go away, just like our parents."
    toshio "「If you love him, someday he'll go away, just like our parents.」"

# game/dialogs.rpy:3042
translate japanese matsuri_N2_626d5481:

    # toshio "「We shouldn't love anyone again. We only have each other!"
    toshio "「We shouldn't love anyone again. We only have each other!」"

# game/dialogs.rpy:3043
translate japanese matsuri_N2_710fb61d:

    # toshio "「In this world, we can only count on ourselves, Nanami."
    toshio "「In this world, we can only count on ourselves, Nanami.」"

# game/dialogs.rpy:3044
translate japanese matsuri_N2_c60cf6df:

    # n "「No...{p}No you're wrong, big brother..."
    n "「No...{p}No you're wrong, big brother...」"

# game/dialogs.rpy:3045
translate japanese matsuri_N2_6112b197:

    # n "「When we lost our parents, I was stuck in the same mindset."
    n "「When we lost our parents, I was stuck in the same mindset.」"

# game/dialogs.rpy:3046
translate japanese matsuri_N2_3646ab26:

    # n "「I thought we shouldn't deeply love anyone anymore."
    n "「I thought we shouldn't deeply love anyone anymore.」"

# game/dialogs.rpy:3047
translate japanese matsuri_N2_7531d9d0:

    # n "「And as time passed, I thought I was going more and more crazy because of loneliness."
    n "「And as time passed, I thought I was going more and more crazy because of loneliness.」"

# game/dialogs.rpy:3048
translate japanese matsuri_N2_5366f0e4:

    # n "「Until I joined Rika-nee's manga club."
    n "「Until I joined Rika-nee's manga club.」"

# game/dialogs.rpy:3049
translate japanese matsuri_N2_2759bd16:

    # n "「Now I have two wonderful friends..."
    n "「Now I have two wonderful friends...」"

# game/dialogs.rpy:3050
translate japanese matsuri_N2_d47cc30c:

    # n "「And %(stringhero)s definitely changed my life..."
    n "「And %(stringhero)s definitely changed my life...」"

# game/dialogs.rpy:3051
translate japanese matsuri_N2_17bb8570:

    # n "「I've never been so happy in my life, thanks to them..."
    n "「I've never been so happy in my life, thanks to them...」"

# game/dialogs.rpy:3052
translate japanese matsuri_N2_1a096aae:

    # n "「They all made me realize that we shouldn't turn our back to love."
    n "「They all made me realize that we shouldn't turn our back to love.」"

# game/dialogs.rpy:3053
translate japanese matsuri_N2_9afef4f0:

    # n "「The only way to heal our hearts is to embrace love and friendship, big brother... Not being stuck in loneliness!"
    n "「The only way to heal our hearts is to embrace love and friendship, big brother... Not being stuck in loneliness!」"

# game/dialogs.rpy:3054
translate japanese matsuri_N2_ff64941d:

    # n "「If... If you kill %(stringhero)s, I... I will... I..."
    n "「If... If you kill %(stringhero)s, I... I will... I...」"

# game/dialogs.rpy:3055
translate japanese matsuri_N2_7f7dd2d3:

    # "Nanami broke into tears again... She probably couldn't bear to live without the manga club."
    "Nanami broke into tears again... She probably couldn't bear to live without the manga club."

# game/dialogs.rpy:3057
translate japanese matsuri_N2_7953243c:

    # "Toshio was still motionless. But his face was overflowing with tears now as he realized what he's done."
    "Toshio was still motionless. But his face was overflowing with tears now as he realized what he's done."

# game/dialogs.rpy:3058
translate japanese matsuri_N2_0b7e8bd9:

    # "He fell on his knees, staring down at the floor... Guilt seemed to overpower him."
    "He fell on his knees, staring down at the floor... Guilt seemed to overpower him."

# game/dialogs.rpy:3059
translate japanese matsuri_N2_5e48472d:

    # toshio "「You..."
    toshio "「You...」"

# game/dialogs.rpy:3060
translate japanese matsuri_N2_b9326ce8:

    # toshio "「You're right, Nanami..."
    toshio "「You're right, Nanami...」"

# game/dialogs.rpy:3061
translate japanese matsuri_N2_19220ebd:

    # toshio "「Oh dear, what have I done..."
    toshio "「Oh dear, what have I done...」"

# game/dialogs.rpy:3062
translate japanese matsuri_N2_1e2845d6:

    # toshio "「I... I thought you... I don't know..."
    toshio "「I... I thought you... I don't know...」"

# game/dialogs.rpy:3063
translate japanese matsuri_N2_1e3aa01c:

    # toshio "「I thought having a boyfriend would make you forget our parents..."
    toshio "「I thought having a boyfriend would make you forget our parents...」"

# game/dialogs.rpy:3064
translate japanese matsuri_N2_ad121cc5:

    # n "「How could I forget them?... I love them more than anything...!"
    n "「How could I forget them?... I love them more than anything...!」"

# game/dialogs.rpy:3065
translate japanese matsuri_N2_678f6c05:

    # n "「And I'm sure that they would be happy to see me with %(stringhero)s and with the others if they were still here..."
    n "「And I'm sure that they would be happy to see me with %(stringhero)s and with the others if they were still here...」"

# game/dialogs.rpy:3066
translate japanese matsuri_N2_d387126d:

    # toshio "「You're right..."
    toshio "「You're right...」"

# game/dialogs.rpy:3067
translate japanese matsuri_N2_c1e47d68:

    # toshio "「I'm an awful brother. A terrible brother..."
    toshio "「I'm an awful brother. A terrible brother...」"

# game/dialogs.rpy:3069
translate japanese matsuri_N2_721624ab:

    # "Nanami left my arms to hold her brother."
    "Nanami left my arms to hold her brother."

# game/dialogs.rpy:3070
translate japanese matsuri_N2_8e3dd312:

    # n "「No, you are not, big brother."
    n "「No, you are not, big brother.」"

# game/dialogs.rpy:3071
translate japanese matsuri_N2_98ee3ce3:

    # n "「You're just still grieving over what happened to us..."
    n "「You're just still grieving over what happened to us...」"

# game/dialogs.rpy:3072
translate japanese matsuri_N2_e056bbe7:

    # n "「Since they died, I've never seen you interact with others as much. You were always thinking of working to help us survive."
    n "「Since they died, I've never seen you interact with others as much. You were always thinking of working to help us survive.」"

# game/dialogs.rpy:3073
translate japanese matsuri_N2_2f0b2303:

    # n "「That's not healthy... {p}Loneliness isn't healthy..."
    n "「That's not healthy... {p}Loneliness isn't healthy...」"

# game/dialogs.rpy:3074
translate japanese matsuri_N2_0a9e2c99:

    # "I watched the scene without a word."
    "I watched the scene without a word."

# game/dialogs.rpy:3077
translate japanese matsuri_N2_f5d91d1c:

    # "When Toshio felt better, Nanami released her embrace."
    "When Toshio felt better, Nanami released her embrace."

# game/dialogs.rpy:3078
translate japanese matsuri_N2_2ef38f10:

    # hero "「You both need some fresh water. I'll get you some."
    hero "「You both need some fresh water. I'll get you some.」"

# game/dialogs.rpy:3079
translate japanese matsuri_N2_01068bf5:

    # toshio "「S-sure... Thank you..."
    toshio "「S-sure... Thank you...」"

# game/dialogs.rpy:3080
translate japanese matsuri_N2_56a7dd0c:

    # toshio "「I'm... I'm sorry for..."
    toshio "「I'm... I'm sorry for...」"

# game/dialogs.rpy:3081
translate japanese matsuri_N2_f3ee0ddb:

    # hero "「It's okay..."
    hero "「It's okay...」"

# game/dialogs.rpy:3082
translate japanese matsuri_N2_a44ad6f3:

    # hero "「I don't have a little sister. But I might be a bit worried if I knew she was seeing some random dude I didn't know about."
    hero "「I don't have a little sister. But I might be a bit worried if I knew she was seeing some random dude I didn't know about.」"

# game/dialogs.rpy:3083
translate japanese matsuri_N2_1e293463:

    # n "「Hey, I'm the one saying you're a dude, not you!"
    n "「Hey, I'm the one saying you're a dude, not you!」"

# game/dialogs.rpy:3084
translate japanese matsuri_N2_c02bf505:

    # "I giggled softly."
    "I giggled softly."

# game/dialogs.rpy:3085
translate japanese matsuri_N2_e1ca6ffd:

    # "I went to the kitchen and came back with three glasses of fresh water."
    "I went to the kitchen and came back with three glasses of fresh water."

# game/dialogs.rpy:3086
translate japanese matsuri_N2_9d66422e:

    # hero "「Toshio-san, I promise you that I want Nanami-chan to be happy and that I'll do my best to make her happy."
    hero "「Toshio-san, I promise you that I want Nanami-chan to be happy and that I'll do my best to make her happy.」"

# game/dialogs.rpy:3087
translate japanese matsuri_N2_b183c263:

    # toshio "「T-thank you..."
    toshio "「T-thank you...」"

# game/dialogs.rpy:3088
translate japanese matsuri_N2_407f6320:

    # "Nanami smiled at me..."
    "Nanami smiled at me..."

# game/dialogs.rpy:3090
translate japanese matsuri_N2_5b99a7c8:

    # "I escorted both Nanami and Toshio back to their house."
    "I escorted both Nanami and Toshio back to their house."

# game/dialogs.rpy:3091
translate japanese matsuri_N2_750a3cb5:

    # "I handshaked Toshio and we smiled to each other."
    "I handshaked Toshio and we smiled to each other."

# game/dialogs.rpy:3092
translate japanese matsuri_N2_471cd43e:

    # toshio "「You better not make her sad, %(stringhero)s-san!"
    toshio "「You better not make her sad, %(stringhero)s-san!」"

# game/dialogs.rpy:3093
translate japanese matsuri_N2_3fd749c7:

    # hero "「I won't. I promise."
    hero "「I won't. I promise.」"

# game/dialogs.rpy:3094
translate japanese matsuri_N2_611bd54f:

    # "He entered the house."
    "He entered the house."

# game/dialogs.rpy:3095
translate japanese matsuri_N2_0f0ea53d:

    # hero "「Nanami, wait..."
    hero "「Nanami, wait...」"

# game/dialogs.rpy:3096
translate japanese matsuri_N2_943066fb:

    # n "「{size=-5}I'm coming, big brother, hold on.{/size}"
    n "「{size=-5}I'm coming, big brother, hold on.{/size}」"

# game/dialogs.rpy:3098
translate japanese matsuri_N2_b0f85b49:

    # n "「Yes, %(stringhero)s-nii?"
    n "「Yes, %(stringhero)s-nii?」"

# game/dialogs.rpy:3099
translate japanese matsuri_N2_d6c71472:

    # hero "「What I said in that moment... {w}It wasn't very enjoyable... {p}So I want to try again, here, right now."
    hero "「What I said in that moment... {w}It wasn't very enjoyable... {p}So I want to try again, here, right now.」"

# game/dialogs.rpy:3100
translate japanese matsuri_N2_ce0929b4:

    # "I held her small hands."
    "I held her small hands."

# game/dialogs.rpy:3101
translate japanese matsuri_N2_6baf7cff:

    # hero "「I love you, Nanami-chan."
    hero "「I love you, Nanami-chan.」"

# game/dialogs.rpy:3103
translate japanese matsuri_N2_f18aa4de:

    # "Nanami smiled and lovingly kissed my lips."
    "Nanami smiled and lovingly kissed my lips."

# game/dialogs.rpy:3104
translate japanese matsuri_N2_30fea132:

    # n "「I love you too, %(stringhero)s-nii."
    n "「I love you too, %(stringhero)s-nii.」"

# game/dialogs.rpy:3106
translate japanese matsuri_N2_6f7d9b3e:

    # "Then she went into her house."
    "Then she went into her house."

# game/dialogs.rpy:3107
translate japanese matsuri_N2_75c7dfac:

    # "I sighed from relief... What a night..."
    "I sighed from relief... What a night..."

# game/dialogs.rpy:3108
translate japanese matsuri_N2_abd512ab:

    # "My emotions were put to the test tonight."
    "My emotions were put to the test tonight."

# game/dialogs.rpy:3109
translate japanese matsuri_N2_a7393e9c:

    # "But I get the strange feeling that I'm not done yet..."
    "But I get the strange feeling that I'm not done yet..."

# game/dialogs.rpy:3110
translate japanese matsuri_N2_230f323d:

    # "That sense of dread was still in the back of my mind. Something else is about to come soon..."
    "That sense of dread was still in the back of my mind. Something else is about to come soon..."

# game/dialogs.rpy:3130
translate japanese matsuri_R_040d4c37:

    # "When I arrived at the festival, I was joined by Rika-chan."
    "夏祭りに到着して、梨花ちゃんと合流した。"

# game/dialogs.rpy:3131
translate japanese matsuri_R_42b57c94:

    # "She was wearing a very pretty purple yukata that showed her shoulders outside."
    "彼女は何だか凄い可愛い肩がはだけた紫の浴衣を着ている。"

# game/dialogs.rpy:3132
translate japanese matsuri_R_a636fbfa:

    # "She was insanely sexy!"
    "かなりセクシーだ。"

# game/dialogs.rpy:3134
translate japanese matsuri_R_6db6001a:

    # hero "「Ah, Rika-chan! That yukata looks great on you!"
    hero "「梨花ちゃん、何だか凄いセクシーだね！」"

# game/dialogs.rpy:3135
translate japanese matsuri_R_4ec8e433:

    # r "「Hmph! Don't stare too much, pervert!"
    r "「フンッ！ヘンタイが！」"

# game/dialogs.rpy:3136
translate japanese matsuri_R_8686ea2b:

    # hero "「Where are the others?"
    hero "「Where are the others?」"

# game/dialogs.rpy:3138
translate japanese matsuri_R_e7b4be7c:

    # r "「Well... Sakura couldn't come."
    r "「ええと...咲良ちゃんは来れないんや...」"

# game/dialogs.rpy:3139
translate japanese matsuri_R_1457c98c:

    # r "「She felt sick so she stayed home."
    r "「何か調子が悪い言うて家で休んどんねん。」"

# game/dialogs.rpy:3140
translate japanese matsuri_R_f0ca7698:

    # r "「And Nanami wasn't around either. Festivals aren't really her thing. Too much of a crowd."
    r "「And Nanami wasn't around either. Festivals aren't really her thing. Too much of a crowd.」"

# game/dialogs.rpy:3141
translate japanese matsuri_R_4299e4c0:

    # hero "「Aw damn, that really sucks... Guess it's just us two..."
    hero "「あー、それは…」"

# game/dialogs.rpy:3142
translate japanese matsuri_R_42954815:

    # hero "「Do you think we should pay them a visit after the festival?"
    hero "「祭りの後でお見舞いに行くってのはどうです？」"

# game/dialogs.rpy:3143
translate japanese matsuri_R_ae397942:

    # r "「Hmm..."
    r "「うん...」"

# game/dialogs.rpy:3145
translate japanese matsuri_R_67d6aa19:

    # r "「That's a good idea."
    r "「ええで。」"

# game/dialogs.rpy:3146
translate japanese matsuri_R_4a7cd128:

    # r "「So, shall we grab a souvenir for them at the festival?"
    r "「ほな、祭りの土産もん持ってかんとな！」"

# game/dialogs.rpy:3147
translate japanese matsuri_R_2bd890ea:

    # "I nodded in agreement."
    "うむ。」"

# game/dialogs.rpy:3148
translate japanese matsuri_R_068f330c:

    # "We started to walk around the festival."
    "そしてパーティーが始まった。"

# game/dialogs.rpy:3152
translate japanese matsuri_R_19be20ef:

    # "We went to almost every stall, from the fishing game stands to every single food stand."
    "大体の事はやった。金魚すくいから屋台まで..."

# game/dialogs.rpy:3153
translate japanese matsuri_R_ff5174ca:

    # "Rika got pretty pissed off that I was better than her at some of the games."
    "梨花ちゃんはゲームで僕の方が上手いときはいつもふてくされていた。"

# game/dialogs.rpy:3154
translate japanese matsuri_R_8a3d2cd4:

    # "When she eventually gave up, we continued to roam around. Finally, an outburst."
    "そしてやり尽くしてその辺をぶらぶらと歩いていたとき、彼女は言った。"

# game/dialogs.rpy:3156
translate japanese matsuri_R_8693e2bc:

    # r "「Ugh, you know, you really don't know how to be a gentleman!"
    r "「アンタは紳士っぽくするっちゅう事を知らんのか！」"

# game/dialogs.rpy:3157
translate japanese matsuri_R_6b9f9eed:

    # r "「Didn't your mom ever teach you to respect women, city rat!?"
    r "「アンタのオカンはレディへの接し方も教えんかったんか？都会のネズミが！」"

# game/dialogs.rpy:3158
translate japanese matsuri_R_d78833cc:

    # "What the heck is she talking about?! I got irritated with her."
    "何だって！？"

# game/dialogs.rpy:3159
translate japanese matsuri_R_23fae77a:

    # hero "「Shut up! If you don't want to be with me then get lost. Nobody asked you to come!"
    hero "「口を慎めよ。お前が誘わなかったら誰もお前の事なんか誘わないぞ！」"

# game/dialogs.rpy:3160
translate japanese matsuri_R_848651e9:

    # r "「I can leave anytime! I just decided to stay because I felt bad that Sakura-chan and Nanami-chan ditched a loser like you!"
    r "「アンタもそうやろ！へ！」"

# game/dialogs.rpy:3161
translate japanese matsuri_R_c4e42251:

    # r "「You're the worst kind of guy! You're not even my type!"
    r "「何にせよアンタはウチのタイプの男の人ちゃうから！！」"

# game/dialogs.rpy:3162
translate japanese matsuri_R_1527ed79:

    # hero "「Fine! Whatever! You're not my type of woman either!!!"
    hero "「構わん！お前も僕のタイプなんかじゃないやい！！」"

# game/dialogs.rpy:3163
translate japanese matsuri_R_08fc819f:

    # r "「Fine!!!"
    r "「構へん！」"

# game/dialogs.rpy:3164
translate japanese matsuri_R_bb288886:

    # hero "「Fine!!!"
    hero "「構へん！」"

# game/dialogs.rpy:3165
translate japanese matsuri_R_1a15f35b:

    # "We turned back against each other, sulking..."
    "お互い、拗ねてそっぽを向いた…"

# game/dialogs.rpy:3166
translate japanese matsuri_R_a20cefa7:

    # "..."
    "……。"

# game/dialogs.rpy:3167
translate japanese matsuri_R_b9e5c171:

    # "And after a few seconds..."
    "そして5秒後…"

# game/dialogs.rpy:3168
translate japanese matsuri_R_ed6f129a:

    # "Something came over me. I don't know why...but..."
    "何故かは分からない、でも…"

# game/dialogs.rpy:3171
translate japanese matsuri_R_d9b9652e:

    # "We both turned around to face each other and we kissed wildly!"
    "僕たちはワイルドにキスをしていたのだ！！"

# game/dialogs.rpy:3172
translate japanese matsuri_R_0af6321e:

    # "It was a passionate, torrid kiss in front of a whole crowd of strangers!"
    "衆人環視の元で熱いキスを！"

# game/dialogs.rpy:3173
translate japanese matsuri_R_5fbe52ef:

    # "When we stopped, she was looking at me with her pretty bi-colored eyes. She was blushing but she still looked pretty pissed off."
    "キスの後、彼女はオッドアイの瞳で僕を見詰めてきた。未だ怒っているようで、それでいて恥ずかしがっているようで…"

# game/dialogs.rpy:3174
translate japanese matsuri_R_76e3f3b2:

    # hero "「Ri... Rika-chan... I..."
    hero "「り…梨花ちゃん、僕…」"

# game/dialogs.rpy:3175
translate japanese matsuri_R_cd9391ee:

    # r "「Shut up! Just shut up and kiss me again!"
    r "「しっ！黙ってキスせや！」"

# game/dialogs.rpy:3183
translate japanese matsuri_R_83960144:

    # "And we kissed again."
    "そして再びキスした。"

# game/dialogs.rpy:3184
translate japanese matsuri_R_0d89a6a1:

    # "...Rika took held my hand and guided me into the forest behind the temple."
    "その後、彼女は僕を神社の周りの森に連れていった。"

# game/dialogs.rpy:3188
translate japanese matsuri_R_02658004:

    # "Hidden by the bushes... we made intense love there."
    "僕達は森の中でこっそりとメイクラブした…"

# game/dialogs.rpy:3189
translate japanese matsuri_R_862f50c1:

    # "It was crazy..."
    "イカれてる…"

# game/dialogs.rpy:3190
translate japanese matsuri_R_91cf2fbf:

    # "I didn't think Rika was into me at all."
    "I didn't think Rika was into me at all."

# game/dialogs.rpy:3191
translate japanese matsuri_R_f1d355d1:

    # "And as strange as it looks, I thought I didn't like her at all either."
    "And as strange as it looks, I thought I didn't like her at all either."

# game/dialogs.rpy:3192
translate japanese matsuri_R_9540939d:

    # "Tsundere love is a really strange thing..."
    "ツンデレの愛情表現は実に奇妙だ。"

# game/dialogs.rpy:3193
translate japanese matsuri_R_9d8d28e3:

    # "I never thought that Rika, of all the people in the world, would be my first..."
    "梨花ちゃんとするとは思ってもみなかったな…"

# game/dialogs.rpy:3194
translate japanese matsuri_R_0256c5ee:

    # "...Actually, even though it was both our first time, she was pretty good at it..."
    "初めてしたけど、彼女は非常に上手だったと思う。"

# game/dialogs.rpy:3198
translate japanese matsuri_R_bff9fa0e:

    # "We were planning to go back to our respective homes."
    "家に向かっていた。"

# game/dialogs.rpy:3200
translate japanese matsuri_R_975037cd:

    # r "「Hey, I almost forgot!"
    r "「ああ、忘れとった！」"

# game/dialogs.rpy:3201
translate japanese matsuri_R_09ea128c:

    # r "「We should get a present and visit Sakura-chan and Nanami-chan."
    r "「咲良ちゃんにお土産を持ってかんと！」"

# game/dialogs.rpy:3202
translate japanese matsuri_R_23aa15f6:

    # hero "「Oh yeah, I almost forgot! Let's go!"
    hero "「ああ、そうだ！行こう！」"

# game/dialogs.rpy:3203
translate japanese matsuri_R_1f36ff66:

    # hero "「I wonder how she'll react when she finds out about us!"
    hero "「僕たちがしたって知ったら、どう思うんだろう…」"

# game/dialogs.rpy:3205
translate japanese matsuri_R_c90d887e:

    # r "「Haha, I bet she'll just think we're pulling a prank on her or-"
    r "「へへ。ウチも気になるで。」"

# game/dialogs.rpy:3209
translate japanese matsuri_R_22d23a85:

    # hero "「Huh? My cellphone? Who could that be?"
    hero "「Huh? My cellphone? Who could that be?」"

# game/dialogs.rpy:3213
translate japanese matsuri_R_e91d81d4:

    # hero "「Hello? Who is it?"
    hero "「Hello? Who is it?」"

# game/dialogs.rpy:3214
translate japanese matsuri_R_0c279eeb:

    # s "「{i}It's me, Sakura. How are you?{/i}"
    s "「{i}It's me, Sakura. How are you?{/i}」"

# game/dialogs.rpy:3215
translate japanese matsuri_R_baf56ae6:

    # hero "「Sakura-chan! Are you okay? I heard you were sick."
    hero "「Sakura-chan! Are you okay? I heard you were sick.」"

# game/dialogs.rpy:3216
translate japanese matsuri_R_16bb0b38:

    # s "「{i}I'm feeling better. Thank you, don't worry.{/i}"
    s "「{i}I'm feeling better. Thank you, don't worry.{/i}」"

# game/dialogs.rpy:3217
translate japanese matsuri_R_cafbc553:

    # s "「{i}I'm actually at Nana-chan's house. We were playing some video games together.{/i}"
    s "「{i}I'm actually at Nana-chan's house. We were playing some video games together.{/i}」"

# game/dialogs.rpy:3218
translate japanese matsuri_R_e322e7d4:

    # hero "「Oh alright! I'm glad you feel better!"
    hero "「Oh alright! I'm glad you feel better!」"

# game/dialogs.rpy:3219
translate japanese matsuri_R_311bdbc8:

    # hero "「Well hey, we were about to come visit you. So I guess we'll meet you at Nanami-chan's?"
    hero "「Well hey, we were about to come visit you. So I guess we'll meet you at Nanami-chan's?」"

# game/dialogs.rpy:3220
translate japanese matsuri_R_cb76d6a6:

    # s "「{i}Sure! We'll wait right here for you!{/i}"
    s "「{i}Sure! We'll wait right here for you!{/i}」"

# game/dialogs.rpy:3222
translate japanese matsuri_R_1713379a:

    # r "「Sakura's feeling better?"
    r "「Sakura's feeling better?」"

# game/dialogs.rpy:3223
translate japanese matsuri_R_ea44e64c:

    # hero "「Yeah. She's feeling better and she's at Nanami-chan's house."
    hero "「Yeah. She's feeling better and she's at Nanami-chan's house.」"

# game/dialogs.rpy:3224
translate japanese matsuri_R_17259851:

    # r "「Let's go visit them then!"
    r "「Let's go visit them then!」"

# game/dialogs.rpy:3229
translate japanese matsuri_R_2f5a201e:

    # "Nanami's room was a bit of a mess."
    "Nanami's room was a bit of a mess."

# game/dialogs.rpy:3230
translate japanese matsuri_R_53129eea:

    # "Her computer setup took up most of the space, besides her bed."
    "Her computer setup took up most of the space, besides her bed."

# game/dialogs.rpy:3231
translate japanese matsuri_R_992fbe8e:

    # "There was also an old TV with several gaming consoles plugged in it."
    "There was also an old TV with several gaming consoles plugged in it."

# game/dialogs.rpy:3232
translate japanese matsuri_R_3b146827:

    # "It was cramped but, it seemed homely for Nanami."
    "It was cramped but, it seemed homely for Nanami."

# game/dialogs.rpy:3233
translate japanese matsuri_R_61c51bff:

    # "I told the both of them about Rika and I. Sakura couldn't believe it when she heard the news!"
    "I told the both of them about Rika and I. Sakura couldn't believe it when she heard the news!"

# game/dialogs.rpy:3234
translate japanese matsuri_R_c281b0de:

    # "Nanami was in disbelief and was looking at us with a funny face."
    "Nanami was in disbelief and was looking at us with a funny face."

# game/dialogs.rpy:3238
translate japanese matsuri_R_586a170e:

    # s "「That's incredible!"
    s "「信じられません！」"

# game/dialogs.rpy:3239
translate japanese matsuri_R_72ae68c4:

    # n "「Seriously, guys!!"
    n "「マジか！！」"

# game/dialogs.rpy:3240
translate japanese matsuri_R_5bfe62d0:

    # s "「I'm so happy for both of you!"
    s "「二人とも、おめでとう御座います！」"

# game/dialogs.rpy:3241
translate japanese matsuri_R_391891ff:

    # s "「I'd never guess you two would end up together! Teehee!"
    s "「二人が一緒になるなんて思いませんでした！」"

# game/dialogs.rpy:3242
translate japanese matsuri_R_04b5e9a4:

    # hero "「Haha, I have to admit, me neither!"
    hero "「いやあ、僕もですよ。」"

# game/dialogs.rpy:3243
translate japanese matsuri_R_2a5f51c2:

    # r "「Hmph, it's true. I didn't think I'd fall for him either...He's still an idiot though..."
    r "「ウチもこんな阿呆とだなんて…」"

# game/dialogs.rpy:3244
translate japanese matsuri_R_73e2cbfb:

    # hero "「Whatever, you're the one whose blushing..."
    hero "「おい！」"

# game/dialogs.rpy:3245
translate japanese matsuri_R_34e95488:

    # "Sakura giggled as she watched us bicker as usual."
    "咲良ちゃんはいつものように言い争いをする僕たちを見て笑った。"

# game/dialogs.rpy:3246
translate japanese matsuri_R_885f1f06:

    # "Looks like our new relationship didn't change anything about the way we communicate."
    "いつもと変わらない日常だ。"

# game/dialogs.rpy:3247
translate japanese matsuri_R_7a965734:

    # "But that's okay with me..."
    "僕はこの日常に幸せを感じていた…"

# game/dialogs.rpy:3254
translate japanese matsuri_R_0cee0472:

    # "I woke up slowly."
    "I woke up slowly."

# game/dialogs.rpy:3255
translate japanese matsuri_R_b2766fe2:

    # "Looking at the alarm clock, it's kinda late."
    "Looking at the alarm clock, it's kinda late."

# game/dialogs.rpy:3256
translate japanese matsuri_R_f87aab37:

    # "I felt something warm against me..."
    "I felt something warm against me..."

# game/dialogs.rpy:3257
translate japanese matsuri_R_2d8ea27f:

    # "It was Rika."
    "It was Rika."

# game/dialogs.rpy:3258
translate japanese matsuri_R_8d3f93d7:

    # "She was fast asleep, naked in my arms."
    "She was fast asleep, naked in my arms."

# game/dialogs.rpy:3259
translate japanese matsuri_R_b91ebafe:

    # "I let out a smile as I held her tight."
    "I let out a smile as I held her tight."

# game/dialogs.rpy:3260
translate japanese matsuri_R_880d8986:

    # "After visiting Sakura and Nanami, I offered her \"to have a drink.\""
    "After visiting Sakura and Nanami, I offered her \"to have a drink.\""

# game/dialogs.rpy:3261
translate japanese matsuri_R_87283b9b:

    # "She responded in an incredibly seductive way, \"It's not that I want to sleep at your place...I'm just thirsty...\""
    "She responded in an incredibly seductive way, \"It's not that I want to sleep at your place...I'm just thirsty...\""

# game/dialogs.rpy:3262
translate japanese matsuri_R_d65a0334:

    # "We made intense and passionate love twice during the night."
    "We made intense and passionate love twice during the night."

# game/dialogs.rpy:3263
translate japanese matsuri_R_3e893f32:

    # "It's funny how much of a tsundere cliché she can be sometimes. But I like it a lot."
    "It's funny how much of a tsundere cliché she can be sometimes. But I like it a lot."

# game/dialogs.rpy:3264
translate japanese matsuri_R_0726d127:

    # "I pet Rika's hair tenderly. Her hair was longer than I thought with her pigtails removed."
    "I pet Rika's hair tenderly. Her hair was longer than I thought with her pigtails removed."

# game/dialogs.rpy:3265
translate japanese matsuri_R_c6b79cfc:

    # "She finally opened her eyes. As she saw me, she smiled and sat on the bed..."
    "She finally opened her eyes. As she saw me, she smiled and sat on the bed..."

# game/dialogs.rpy:3266
translate japanese matsuri_R_044a4f8a:

    # "She was still half asleep when she spoke."
    "She was still half asleep when she spoke."

# game/dialogs.rpy:3267
translate japanese matsuri_R_ddaea844:

    # r "「Hmmmm... Did you have a good nap, city rat?"
    r "「Hmmmm... Did you have a good nap, city rat?"

# game/dialogs.rpy:3268
translate japanese matsuri_R_ffcb2dd8:

    # hero "「Geez, even if we're in a relationship, even with you naked in my own bed, you'll still keep calling me that for the rest of your life huh?"
    hero "「Geez, even if we're in a relationship, even with you naked in my own bed, you'll still keep calling me that for the rest of your life huh?」"

# game/dialogs.rpy:3269
translate japanese matsuri_R_51e9effd:

    # r "「Mmmm stop complaining... You should be thankful to have a girl like me as your lover..."
    r "「Mmmm stop complaining... You should be thankful to have a girl like me as your lover...」"

# game/dialogs.rpy:3270
translate japanese matsuri_R_13bdddc3:

    # "I gave a small laugh and kissed her. She began to embrace me..."
    "I gave a small laugh and kissed her. She began to embrace me..."

# game/dialogs.rpy:3271
translate japanese matsuri_R_a20cefa7_1:

    # "..."
    "..."

# game/dialogs.rpy:3272
translate japanese matsuri_R_b072efdd:

    # "I suddenly realized something:{p}My parents!!!"
    "I suddenly realized something:{p}My parents!!!"

# game/dialogs.rpy:3273
translate japanese matsuri_R_361d12e9:

    # "Oh shit, they're surely back from the festival!!!"
    "Oh shit, they're surely back from the festival!!!"

# game/dialogs.rpy:3274
translate japanese matsuri_R_37e91653:

    # "They'll kill me if they see that I brought a girl home, let alone in my bed!!!"
    "They'll kill me if they see that I brought a girl home, let alone in my bed!!!"

# game/dialogs.rpy:3275
translate japanese matsuri_R_ead2c864:

    # "What should I do? Dammit, dammit!!!"
    "What should I do? Dammit, dammit!!!"

# game/dialogs.rpy:3276
translate japanese matsuri_R_a7bc91fd:

    # "I listened for a moment, waiting for an indication that would tell me if my parents were here."
    "I listened for a moment, waiting for an indication that would tell me if my parents were here."

# game/dialogs.rpy:3277
translate japanese matsuri_R_06130763:

    # "Nothing..."
    "Nothing..."

# game/dialogs.rpy:3278
translate japanese matsuri_R_bb28b60c:

    # "Rika looked a bit confused...that or she was still half asleep."
    "Rika looked a bit confused...that or she was still half asleep."

# game/dialogs.rpy:3279
translate japanese matsuri_R_c4b1d086:

    # hero "「Rika-chan..."
    hero "「Rika-chan...」"

# game/dialogs.rpy:3280
translate japanese matsuri_R_9e07d6af:

    # hero "「My parents aren't there. You should go home now before they come back."
    hero "「My parents aren't there. You should go home now before they come back.」"

# game/dialogs.rpy:3281
translate japanese matsuri_R_272a8b83:

    # hero "「My parents are kinda strict when it comes to love relationships."
    hero "「My parents are kinda strict when it comes to love relationships.」"

# game/dialogs.rpy:3282
translate japanese matsuri_R_7bf71025:

    # "Rika nodded."
    "Rika nodded."

# game/dialogs.rpy:3283
translate japanese matsuri_R_22236077:

    # r "「Alright... I understand. My parents would react the same in this kinda situation..."
    r "「Alright... I understand. My parents would react the same in this kinda situation...」"

# game/dialogs.rpy:3285
translate japanese matsuri_R_fb307b0d:

    # "I helped her put on her yukata and took her to her house..."
    "I helped her put on her yukata and took her to her house..."

# game/dialogs.rpy:3286
translate japanese matsuri_R_5ffe88e0:

    # "We promised to have another date very soon..."
    "We promised to have another date very soon..."

# game/dialogs.rpy:3287
translate japanese matsuri_R_17899d6b:

    # "I went back home and turned on my computer."
    "I went back home and turned on my computer."

# game/dialogs.rpy:3291
translate japanese matsuri_R_2b150c3e:

    # write "Dear sister,"
    write "姉さんへ。"

# game/dialogs.rpy:3293
translate japanese matsuri_R_92248cbb:

    # write "How are things?"
    write "調子はどうだい。"

# game/dialogs.rpy:3295
translate japanese matsuri_R_54b9d70d:

    # write "You'll never guess what happened!"
    write "想像できたかい？"

# game/dialogs.rpy:3297
translate japanese matsuri_R_4967d059:

    # write "I'm going out with a girl!"
    write "この僕が、とうとう女の子と付き合うことになったんだ！"

# game/dialogs.rpy:3299
translate japanese matsuri_R_c293cd3c:

    # write "Her name is Rika. She's a strange girl. It's a bit hard to understand she's really thinking.{w} She seems to mock me all the time, but we're really into each other."
    write "中々理解に苦しむ奇抜な子だ。いつも僕にちょっかいを出してくる。{w}でも、確かに愛し合ってる。それに、僕達はもう…分かるだろ？{w}僕は今、本当に嬉しいよ。{w}いつか紹介できる日を楽しみにしてる。{w}写真も送るよ。彼女は凄く可愛いんだ。"

# game/dialogs.rpy:3301
translate japanese matsuri_R_1773278b:

    # write "I'm happy... Really happy... I can't wait to introduce her to you someday! {w}I'll send you a photo of us. She's incredibly gorgeous."
    write "I'm happy... Really happy... I can't wait to introduce her to you someday! {w}I'll send you a photo of us. She's incredibly gorgeous."

# game/dialogs.rpy:3302
translate japanese matsuri_R_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3304
translate japanese matsuri_R_c42ad76e:

    # write "I hope everything is going great for you as well!"
    write "姉さん達も、僕達に負けない位に幸せになってくれ。"

# game/dialogs.rpy:3306
translate japanese matsuri_R_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "弟より。"

# game/dialogs.rpy:3308
translate japanese matsuri_R_91609d26:

    # write "PS:{w} The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write "PS : {w}村の夏祭りは凄かった！東京にもこんなイベントがあると良いね。"

# game/dialogs.rpy:3309
translate japanese matsuri_R_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3330
translate japanese scene8_66fd8474:

    # centered "{size=+35}FINAL CHAPTER\nI wish I was a real girl{fast}{/size}"
    centered "{size=+35}最終章\nもし本当に女の子だったら{fast}{/size}"

# game/dialogs.rpy:3336
translate japanese scene8_26da46e6:

    # centered "Three days after the festival..."
    centered "それから3日が経った。"

# game/dialogs.rpy:3339
translate japanese scene8_3193f804:

    # "It was late in the evening. I was in front of my computer, surfing on the web..."
    "僕はその夜、適当にネットをサーフィンしていた。"

# game/dialogs.rpy:3341
translate japanese scene8_216308f6:

    # "My cellphone rang."
    "突然、私の携帯電話が鳴った。"

# game/dialogs.rpy:3345
translate japanese scene8_dbd7e6ae:

    # hero "「Hello?"
    hero "「もしもし。」"

# game/dialogs.rpy:3346
translate japanese scene8_5354350a:

    # r "「{i}%(stringhero)s?{/i}"
    r "『%(stringhero)s?』"

# game/dialogs.rpy:3348
translate japanese scene8_30e755be:

    # hero "「What's cooking, love?"
    hero "「What's cooking, love?」"

# game/dialogs.rpy:3350
translate japanese scene8_6d9c1d81:

    # hero "「Yeah, Rika-chan?"
    hero "「居るけど？」"

# game/dialogs.rpy:3351
translate japanese scene8_237675bd:

    # r "「{i}Come to my house right now! It's very important!{/i}"
    r "『すぐきや！ほんまに大変やねん！』"

# game/dialogs.rpy:3353
translate japanese scene8_0bf1cc8f:

    # r "「{i}It's about Sakura!{/i}"
    r "『咲良ちゃんの事や！』"

# game/dialogs.rpy:3354
translate japanese scene8_eec796bb:

    # hero "「What's going on?"
    hero "「何かあったの？」"

# game/dialogs.rpy:3355
translate japanese scene8_87546516:

    # r "「{i}Just come!{/i}"
    r "『いいからきや！』"

# game/dialogs.rpy:3357
translate japanese scene8_495869e3:

    # "*{i}clic{/i}*"
    "*{i}clic{/i}*"

# game/dialogs.rpy:3359
translate japanese scene8_4f4c3c02:

    # "What could have happened to Sakura? Oh man, I hope she's okay."
    "What could have happened to Sakura? Oh man, I hope she's okay."

# game/dialogs.rpy:3361
translate japanese scene8_8a4ee6ed:

    # "I switched off my computer, took my bicycle and went to her house, worried to death."
    "僕はコンピューターを消し、自転車で駆けつけた。気が気でなかった。"

# game/dialogs.rpy:3362
translate japanese scene8_a60f3c18:

    # "It was late in the night and the little rural streets of the village were a bit scary in the dark."
    "夜の田舎道は暗くて薄気味が悪かった。"

# game/dialogs.rpy:3363
translate japanese scene8_85e64de3:

    # "Now isn't the time to be scared of the small things."
    "それでも、行かないと。"

# game/dialogs.rpy:3365
translate japanese scene8_b7c54990:

    # "My love needs help!"
    "恋人が助けを求めているんだ！"

# game/dialogs.rpy:3367
translate japanese scene8_ab18a7cd:

    # "My friend needs help!"
    "友達が助けを求めているんだ！"

# game/dialogs.rpy:3370
translate japanese scene8_993a8f06:

    # "*{i}Knock knock{/i}*"
    "梨花ちゃんの家に着いて、ドアベルを鳴らした。"

# game/dialogs.rpy:3372
translate japanese scene8_6d6e7ebc:

    # "The door of the house opened and Rika-chan appeared."
    "ドアが開き、梨花ちゃんが出てきた。"

# game/dialogs.rpy:3373
translate japanese scene8_68bac721:

    # "She looked very worried and upset, as I imagined."
    "非常に焦った様な、そして困った様な表情だった。"

# game/dialogs.rpy:3375
translate japanese scene8_47dbdd39:

    # r "「Ah, there you are. Come i-{nw}"
    r "「来たんか。はよ{nw}」"

# game/dialogs.rpy:3377
translate japanese scene8_27651eaf:

    # "I didn't give her any time to show me where Sakura-chan was."
    "咲良ちゃんがどこに居るかは言われなくても分かった。"

# game/dialogs.rpy:3378
translate japanese scene8_0a632bcf:

    # "I instinctively ran into the house and searched for her."
    "僕は直感的に家に入って、彼女の元に駆け寄った。"

# game/dialogs.rpy:3380
translate japanese scene8_c676b3c1:

    # "I found her in the living room."
    "I found her in the living room."

# game/dialogs.rpy:3382
translate japanese scene8_37ffbdd8:

    # r "「Ah, there you are. Come in, quickly."
    r "「来たんか。はよ入りや！」"

# game/dialogs.rpy:3384
translate japanese scene8_616170d4:

    # "I followed Rika-chan into her house, to the living room."
    "I followed Rika-chan into her house, to the living room."

# game/dialogs.rpy:3386
translate japanese scene8_b8ce5033:

    # "Near the kotatsu, there was Sakura sitting on her knees, wearing her pajamas."
    "パジャマ姿の彼女が、こたつに座っていた。"

# game/dialogs.rpy:3387
translate japanese scene8_c44364bc:

    # "Her long hair was hiding her face, but she looked shocked."
    "彼女の顔は長い髪で隠れていたが、ショックを受けていたのが見て取れた。"

# game/dialogs.rpy:3388
translate japanese scene8_d47b58c5:

    # "Thank heavens, she's alive."
    "僕は彼女が無事と分かり、一先ず安心した。"

# game/dialogs.rpy:3389
translate japanese scene8_c89376fe:

    # hero "「Sakura-chan! Are you okay?"
    hero "「咲良ちゃん！大丈夫？」"

# game/dialogs.rpy:3390
translate japanese scene8_1740f374:

    # "She turned her face to me, speechless."
    "静かにこちらを向く。"

# game/dialogs.rpy:3391
translate japanese scene8_15ef4c6c:

    # "She had a large bruise on her cheek and tears were flowing slowly from her sweet blue eyes."
    "頬には大きな痣があり、瞳からは涙が零れていた。"

# game/dialogs.rpy:3392
translate japanese scene8_d0d64bc6:

    # hero "「Oh..."
    hero "「ああ...」"

# game/dialogs.rpy:3393
translate japanese scene8_74d2762c:

    # hero "「Oh no..."
    hero "「畜生！！」"

# game/dialogs.rpy:3394
translate japanese scene8_6d9e86b7:

    # hero "「Who the heck did this to you?!"
    hero "「一体どこの畜生がやりやがったんだ！！？」"

# game/dialogs.rpy:3396
translate japanese scene8_67ac6839:

    # "Rika arrived at the living room."
    "梨花ちゃんが部屋に入ってきた。"

# game/dialogs.rpy:3397
translate japanese scene8_4fc2e4fc:

    # hero "「Rika-chan, what happened? Was it the gang from school again?"
    hero "「これは一体…！？またあの不良達なのか？」"

# game/dialogs.rpy:3398
translate japanese scene8_391ed42e:

    # "Rika shook her head."
    "梨花ちゃんは首を振った。"

# game/dialogs.rpy:3399
translate japanese scene8_93c707d1:

    # r "「Her father did this."
    r "「咲良ちゃんの、おとん。」"

# game/dialogs.rpy:3400
translate japanese scene8_03cc985d:

    # hero "「What!?{p}Her father!?"
    hero "「何だって？？？{p}お父さんが！！？？」"

# game/dialogs.rpy:3401
translate japanese scene8_7bf71025:

    # "Rika nodded."
    "梨花ちゃんは頷いた。"

# game/dialogs.rpy:3402
translate japanese scene8_6da77c2c:

    # r "「He was drunk, he hit her and even broke her violin..."
    r "「酔っぱらって、顔をぶって、ヴァイオリンを壊して...」"

# game/dialogs.rpy:3403
translate japanese scene8_75cef802:

    # r "「Her mother helped her to flee the house. So she came to me. When she told me what happened, I called you."
    r "「咲良ちゃんのおかんが何とかして家から出してな、ここに居るっちゅうことやねん。ウチが話を聞いてな、アンタを呼んだんや。」"

# game/dialogs.rpy:3404
translate japanese scene8_5820756e:

    # r "「She needs both of us, more than anything..."
    r "「今何より必要とされとんのは、ウチら二人な」"

# game/dialogs.rpy:3405
translate japanese scene8_f4f4126a:

    # hero "「Does Nanami-chan know?"
    hero "「Does Nanami-chan know?」"

# game/dialogs.rpy:3406
translate japanese scene8_65b003bb:

    # r "「Not yet. I prefer not to worry her."
    r "「Not yet. I prefer not to worry her.」"

# game/dialogs.rpy:3407
translate japanese scene8_8f451a85:

    # r "「She cares a lot for Sakura but she's very fragile."
    r "「She cares a lot for Sakura but she's very fragile.」"

# game/dialogs.rpy:3408
translate japanese scene8_35bf5a71:

    # "I nodded."
    "うん…"

# game/dialogs.rpy:3409
translate japanese scene8_5bd541e7:

    # "From what I saw, I could see that she really considers Sakura and Rika like her sisters."
    "From what I saw, I could see that she really considers Sakura and Rika like her sisters."

# game/dialogs.rpy:3410
translate japanese scene8_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:3411
translate japanese scene8_380894b8:

    # "Anyway, Sakura's father surely did this because he was still pissed that his boy is a girl inside."
    "兎に角、お父さんがこうなるのは、未だに彼女の中の女の子を認められないからだろう。"

# game/dialogs.rpy:3413
translate japanese scene8_63e75ac3:

    # "Or maybe he was pissed because Sakura gave away her yukata for men. I've heard that he bought it for her in the hopes of making her a man."
    "もしかしたら、咲良ちゃんが浴衣を手放したことに怒っているのかも知れない。今思えば、あの男用の浴衣は咲良ちゃんがまた男らしく戻ってくれることを願ってあげた物なのだったのだろう。"

# game/dialogs.rpy:3414
translate japanese scene8_0745cc53:

    # "I felt enraged."
    "僕は強い憤りを感じた。"

# game/dialogs.rpy:3415
translate japanese scene8_1a6c86d8:

    # "But actually, I had no idea of what to do."
    "しかし、実際どうすればいいのか分からなかった。"

# game/dialogs.rpy:3420
translate japanese scene8_91aef672:

    # "We stayed at Rika-chan's home for the night."
    "その夜は梨花ちゃん家に泊まることになった。"

# game/dialogs.rpy:3421
translate japanese scene8_7267642d:

    # "But I wanted to avenge my friend."
    "僕は親友の報復がしたかった。"

# game/dialogs.rpy:3422
translate japanese scene8_0057c44c:

    # "I had to go there and talk to her father!"
    "行って話を付けてこなければ...！"

# game/dialogs.rpy:3423
translate japanese scene8_8e59b631:

    # "While they were both asleep, I stood up and went outside with my bicycle, on my way to Sakura's house."
    "二人が寝ている間に、僕は自転車で咲良ちゃんの家に向かった。"

# game/dialogs.rpy:3426
translate japanese scene8_b1145ef5:

    # "In front of the house, there was her father."
    "家の前に、咲良ちゃんのお父さんが居た。"

# game/dialogs.rpy:3427
translate japanese scene8_8fb982f8:

    # "He was mumbling some gibberish and was holding a bottle of sake."
    "彼は何だかブツブツと小声で囁きながらボトルの酒を飲んでいる。"

# game/dialogs.rpy:3428
translate japanese scene8_d9426232:

    # "As I could guess, he was still drunk."
    "どうやら未だに酔っ払っているようだ。"

# game/dialogs.rpy:3429
translate japanese scene8_185ef70f:

    # hero "「Hey you!"
    hero "「やい、あんた！」"

# game/dialogs.rpy:3430
translate japanese scene8_cb6740c6:

    # sdad "「Wh--- Who are you?!"
    sdad "「あ…誰だお前？！」"

# game/dialogs.rpy:3432
translate japanese scene8_032d6f9c:

    # hero "「Name's %(stringhero)s... I'm the boyfriend of your daughter! Her boyfriend!!!"
    hero "「%(stringhero)s…あなたの娘の、ボーイフレンドです！」"

# game/dialogs.rpy:3434
translate japanese scene8_e3dcc113:

    # hero "「Name's %(stringhero)s... I'm a friend of your daughter!"
    hero "「%(stringhero)s…あなたの娘の、友達です！」"

# game/dialogs.rpy:3435
translate japanese scene8_18e3dade:

    # sdad "「*{i}hiccups{/i}* I don't have any d-d-daughter... I only have a son!..."
    sdad "「*{i}ヒック{/i}*{p}お、俺はむ、む、娘なんて持った覚えは無い！！俺には息子しか居ねえ！」"

# game/dialogs.rpy:3436
translate japanese scene8_9fd9385d:

    # sdad "「And my son is a freaking pervert who dresses like a girl, that's all!!!"
    sdad "「お前みたいなイカれたホモの息子しか居ない！！それだけだ！！！」"

# game/dialogs.rpy:3437
translate japanese scene8_4576c2b6:

    # hero "「SHUT UP!!!"
    hero "「黙れ！！」"

# game/dialogs.rpy:3438
translate japanese scene8_f199e2fa:

    # "I shouted so loud, some dogs in the neighborhood started to bark."
    "僕は叫んだ。近所の数匹の犬が吠え始めた。"

# game/dialogs.rpy:3439
translate japanese scene8_bb4f1d08:

    # hero "「Whether he is a girl or a boy,... he's your child!! Your own flesh and blood!!!"
    hero "「男でも女でも、あんたの子供じゃないか！！あんたの血と肉じゃないか！！」"

# game/dialogs.rpy:3440
translate japanese scene8_90a161cb:

    # hero "「You must accept him as he is!"
    hero "「彼自身を受け入れるべきだ！」"

# game/dialogs.rpy:3441
translate japanese scene8_042b0d72:

    # hero "「Sakura accepts her father like he is, so why don't you do the same!!?"
    hero "「咲良ちゃんはあんたを受け入れてる、何であんたは受け入れてやれないんだ！？」"

# game/dialogs.rpy:3442
translate japanese scene8_16e9224f:

    # sdad "「*{i}hiccups{/i}*"
    sdad "*{i}ヒック{/i}*"

# game/dialogs.rpy:3443
translate japanese scene8_a67faa19:

    # sdad "「I d-d-d-on't have any lessons to take from a kid!"
    sdad "「俺は、が、ガキに…ご、ご、御高説、垂らされる覚えはねえ！！」"

# game/dialogs.rpy:3444
translate japanese scene8_5f87fa70:

    # "He started to shout too. Some lights turned on in the neighborhood, awakened by the argument."
    "彼も叫んだ。近所の人もチラホラ起きたらしく、何軒かの家に電気が点いた。"

# game/dialogs.rpy:3445
translate japanese scene8_4d26840c:

    # "Sakura's mother came out from the house and stood by the porch. She had some bruises on her body and her eyes were red... she must have been crying a lot."
    "咲良ちゃんのお母さんが家から出てきた。身体には幾つかの痣があり、顔には涙の跡があった。"

# game/dialogs.rpy:3446
translate japanese scene8_3a555d15:

    # "She seemed scared and was watching the scene quietly."
    "彼女は怯えながら、静かにこの光景を眺めていた。"

# game/dialogs.rpy:3447
translate japanese scene8_380ead38:

    # "Looks like Sakura wasn't the only one who got hurt..."
    "どうやら咲良ちゃんだけが被害にあった訳じゃ無いみたいだ..."

# game/dialogs.rpy:3448
translate japanese scene8_4e5e9fd5:

    # "Her father broke his empty bottle, held it like a knife and grinned."
    "Her father broke his empty bottle, held it like a knife and grinned."

# game/dialogs.rpy:3449
translate japanese scene8_23076b47:

    # sdad "「Now get out or I'm gonna stab you... you gay brat!"
    sdad "「視界から消えろ、さもなくば刺すぞ...餓鬼！」"

# game/dialogs.rpy:3450
translate japanese scene8_415034f0:

    # "Shit, is this guy for real?!"
    "おいおいこいつ、僕を脅してくるぞ！！"

# game/dialogs.rpy:3451
translate japanese scene8_5ac5be9a:

    # "He began to charge at me brandishing the broken glass. I was paralyzed with fear."
    "そして僕の元に向かって走ってきた。{p}速い！酔っぱらいがこんなに速く動くなんて…"

# game/dialogs.rpy:3452
translate japanese scene8_22ee59f4:

    # sdad "「{size=+10}Go to hell!!{/size}"
    sdad "「{size=+10}死んで！！{/size}」"

# game/dialogs.rpy:3453
translate japanese scene8_f5b011b4:

    # "Just as he closed in, someone jumped in front of me."
    "その瞬間、横から人影が現れて…"

# game/dialogs.rpy:3456
translate japanese scene8_1d392ab1:

    # "It was a deafening sound.{p}I heard a painful moan.{p}It resonated in my head even after it was done.."
    "音はなかった。{p}隣で小さな呻き声が聞こえた。{p}遠くに叫び声が聞こえる。"

# game/dialogs.rpy:3457
translate japanese scene8_6ead108f:

    # "Time stopped. I just remember being pushed to the floor."
    "僕は何かに押されて床に伏した。"

# game/dialogs.rpy:3460
translate japanese scene8_1bd19034:

    # "When I came back to my senses, I saw Sakura's father flinched back, shaken and shocked. "
    "気がついたら、僕を刺そうとしていた男はたじろぎ、恐怖に震えていた。"

# game/dialogs.rpy:3461
translate japanese scene8_770343cb:

    # "The broken bottle he was holding fell on the ground and shattered."
    "The broken bottle he was holding fell on the ground and shattered."

# game/dialogs.rpy:3462
translate japanese scene8_2cf1e3ba:

    # "The pieces were stained with blood."
    "The pieces were stained with blood."

# game/dialogs.rpy:3464
translate japanese scene8_43934e89:

    # "On top of me, there was the body of Sakura."
    "僕の膝上に、咲良ちゃんが居た。"

# game/dialogs.rpy:3465
translate japanese scene8_9e470804:

    # "Her stomach was bleeding profusely."
    "お腹にはベットリと血が付いていた。"

# game/dialogs.rpy:3466
translate japanese scene8_fc381d0a:

    # hero "「Sakura!!!"
    hero "「咲良ちゃん！！！」"

# game/dialogs.rpy:3467
translate japanese scene8_c04fe468:

    # hero "「{size=+15}Sakura!!! Hang on!!! Sakura!!!{/size}"
    hero "「{size=+15}咲良ちゃん！！しっかりして！咲良ちゃん！！{/size}」"

# game/dialogs.rpy:3468
translate japanese scene8_281954cf:

    # "I started to feel tears in my eyes."
    "うう…そ、そんな…。"

# game/dialogs.rpy:3469
translate japanese scene8_b46a96ab:

    # "She saved me by sacrificing herself."
    "彼女は、命を呈して僕を守ってくれたのだ！"

# game/dialogs.rpy:3470
translate japanese scene8_2e1ee92a:

    # hero "「{size=+15}Please, don't die, Sakura!!!{/size}"
    hero "「{size=+15}死ぬな、咲良ちゃん！！{/size}」"

# game/dialogs.rpy:3471
translate japanese scene8_6f1c6352:

    # "Sakura weakly turned her head to me."
    "咲良ちゃんは弱々しくこちらを向いた。"

# game/dialogs.rpy:3473
translate japanese scene8_22286f8f:

    # "I caressed her cheek tenderly, removing a tear, and kissing her lips softly, my own tears preventing me to see how beautiful she looked, even in that state."
    "I caressed her cheek tenderly, removing a tear, and kissing her lips softly, my own tears preventing me to see how beautiful she looked, even in that state."

# game/dialogs.rpy:3474
translate japanese scene8_1204b721:

    # s "「..........{p}......%(stringhero)s....kun....."
    s "「………………{p}……………%(stringhero)s…………くん…………………」"

# game/dialogs.rpy:3476
translate japanese scene8_001b31dc:

    # s "「I'm... so happy to... be... your girlfriend......"
    s "「私………嬉しい…です………あなたと………に……なれて……………」"

# game/dialogs.rpy:3477
translate japanese scene8_d6ab4c63:

    # s "「I... love you... so much,...%(stringhero)s....kun...."
    s "「I... love you... so much,...%(stringhero)s....kun....」"

# game/dialogs.rpy:3478
translate japanese scene8_a2f900d8:

    # hero "「And I love you, Sakura, more than anything!"
    hero "「And I love you, Sakura, more than anything!」"

# game/dialogs.rpy:3479
translate japanese scene8_04745b3b:

    # hero "「That's why you must stay alive!! Stay with me!!"
    hero "「That's why you must stay alive!! Stay with me!!」"

# game/dialogs.rpy:3480
translate japanese scene8_e8a331c8:

    # s "「I..."
    s "「I...」"

# game/dialogs.rpy:3482
translate japanese scene8_379966c6:

    # s "「T... Too bad it...{w}didn't worked...between us..."
    s "「T... Too bad it...{w}didn't worked...between us...」"

# game/dialogs.rpy:3483
translate japanese scene8_362ff53c:

    # s "「But that's okay...{p}You... and the others...{w} took care of me so nicely..."
    s "「But that's okay...{p}You... and the others...{w} took care of me so nicely...」"

# game/dialogs.rpy:3484
translate japanese scene8_0868d776:

    # s "「I'm...so happy to... be... {w}your friend......"
    s "「私………嬉しい…です………あなたの………お友達に……なれて……………」"

# game/dialogs.rpy:3485
translate japanese scene8_f3331226:

    # s "「T... Thank...... you..."
    s "「…ありが…と………」"

# game/dialogs.rpy:3487
translate japanese scene8_a58e1fda:

    # s "「I regret I... didn't had the time... to tell Nanami-chan... the truth..."
    s "「I regret I... didn't had the time... to tell Nanami-chan... the truth...」"

# game/dialogs.rpy:3488
translate japanese scene8_f8ac1185:

    # s "「You know..."
    s "「……………………………ああ……………………………」"

# game/dialogs.rpy:3490
translate japanese scene8_cb894c8f:

    # s "「I wish......{p}I was...{p}a real girl..."
    s "「………もし本当に…………{p}………本当に女の子だったら…………………………」"

# game/dialogs.rpy:3492
translate japanese scene8_ccf33f5d:

    # "She smiled at me, then she fainted in my arms."
    "そう言って、瞳を閉じた。"

# game/dialogs.rpy:3496
translate japanese scene8_8e8bdb99:

    # hero "「Sakura!!!!!!{p}{size=+15}Sakura!!! Answer me!!!{/size}"
    hero "「咲良ちゃん！！！！！！！{p}{size=+12}しっかりして！！！！！咲良ちゃん！！！！！！！{/size}」"

# game/dialogs.rpy:3497
translate japanese scene8_43e0fc44:

    # hero "「{size=+20}SAKURAAAAAAAAAAAAAAA!!!!!!{/size}"
    hero "「{size=+20}ああああああああああああああああああああああああ！！！！！！！！！！！！！！！{/size}」"

# game/dialogs.rpy:3501
translate japanese scene8_7a909493:

    # "The police and the ambulance arrived shortly after, with Rika-chan."
    "少ししてパトカーと救急車がやって来た。そして梨花ちゃんも。"

# game/dialogs.rpy:3502
translate japanese scene8_dd0ebba0:

    # "There was still a pulse inside Sakura's body, but she was badly hurt."
    "咲良ちゃんには脈こそあったものの、相当の重症だ。"

# game/dialogs.rpy:3503
translate japanese scene8_913d018c:

    # "The police arrested her father for attempted homicide and domestic violence."
    "父親は殺人未遂と暴行罪で逮捕された。"

# game/dialogs.rpy:3504
translate japanese scene8_53b8960a:

    # "Sakura's mom went with her in the ambulance."
    "Sakura's mom went with her in the ambulance."

# game/dialogs.rpy:3506
translate japanese scene8_8f2b17c9:

    # "I stayed there with Rika-chan for a moment... when everyone was gone."
    "やがて誰も居なくなり、僕と梨花ちゃんだけがその場に取り残された。"

# game/dialogs.rpy:3508
translate japanese scene8_e09c696e:

    # "I cried in Rika-chan's arms, uncontrollably."
    "僕は抑えられず、梨花ちゃんの腕の中で泣いた。"

# game/dialogs.rpy:3509
translate japanese scene8_c44c2f72:

    # hero "「Sakura... No, damnit, not Sakura..."
    hero "「咲良ちゃん…ああ、咲良ちゃん…」"

# game/dialogs.rpy:3510
translate japanese scene8_68b58967:

    # "Rika was sobbing as well."
    "梨花ちゃんも泣いていた。"

# game/dialogs.rpy:3511
translate japanese scene8_c6f7844b:

    # "When we calmed down a little, she spoke."
    "一通り泣き終わったあと、梨花ちゃんが言った。"

# game/dialogs.rpy:3512
translate japanese scene8_d30a40a6:

    # r "「You know... {p}I don't know how to say it..."
    r "「なあ…{p}どう言えばええか分からんけどな…」"

# game/dialogs.rpy:3513
translate japanese scene8_906477c5:

    # r "「But I think it's the right time to tell you..."
    r "「でも、今言うのが良いと思うんや…」"

# game/dialogs.rpy:3514
translate japanese scene8_ea880f58:

    # r "「Before you came to our club... I was in love with Sakura..."
    r "「アンタが入部する前な…ウチ咲良の事、好きやったん…」"

# game/dialogs.rpy:3515
translate japanese scene8_6c7d6f4e:

    # hero "「You were? With Sakura-chan?"
    hero "「咲良ちゃんの事が…？」"

# game/dialogs.rpy:3516
translate japanese scene8_73d36031:

    # r "「Yes..."
    r "「せや…」"

# game/dialogs.rpy:3517
translate japanese scene8_1a4eb0ff:

    # r "「I was attracted by this boy who is so girly..."
    r "「ウチはその女の子みたいな男の子に惹かれたんや…」"

# game/dialogs.rpy:3518
translate japanese scene8_e94b0b53:

    # r "「After that, you came,... {size=-15}and it changed...{/size}"
    r "「アンタが来たのはその後や、{size=-10}そんで、変わった…{/size}」"

# game/dialogs.rpy:3519
translate japanese scene8_402f030d:

    # r "「By the way, you know why I consider Sakura as my best friend, now..."
    r "「それで、今でも咲良はウチの一番の親友ちゅうわけや…」"

# game/dialogs.rpy:3520
translate japanese scene8_e668a3b6:

    # hero "「I see..."
    hero "「つ…。」"

# game/dialogs.rpy:3521
translate japanese scene8_7030c013:

    # r "「But don't worry... I was shocked when I heard about you two, but..."
    r "「But don't worry... I was shocked when I heard about you two, but...」"

# game/dialogs.rpy:3522
translate japanese scene8_09ad0db3:

    # r "「I was genuinely happy for the both of you."
    r "「I was genuinely happy for the both of you.」"

# game/dialogs.rpy:3523
translate japanese scene8_39b70c91:

    # r "「I never seen Sakura so happy before. You changed her life so much..."
    r "「I never seen Sakura so happy before. You changed her life so much...」"

# game/dialogs.rpy:3524
translate japanese scene8_c0eddcd4:

    # r "「It's why I'm not mad at you... Sakura's happiness is all that matters..."
    r "「It's why I'm not mad at you... Sakura's happiness is all that matters...」"

# game/dialogs.rpy:3525
translate japanese scene8_190df487:

    # r "「And now..."
    r "「And now...」"

# game/dialogs.rpy:3526
translate japanese scene8_9a33713f:

    # r "「I just hope...{w}That she will be alright!"
    r "「今はただ、{w}無事を祈るばかりや…」"

# game/dialogs.rpy:3527
translate japanese scene8_0bd0684e:

    # "She started to weep too. I had never seen her like that before."
    "そしてまた泣き始めた。こんな彼女は見た事が無かった。"

# game/dialogs.rpy:3528
translate japanese scene8_0fb4909e:

    # "I held her in my arms gently."
    "僕は優しく彼女を抱いた。"

# game/dialogs.rpy:3532
translate japanese scene8_f4f6d6da:

    # r "「Are you okay, hun?"
    r "「大丈夫？"

# game/dialogs.rpy:3534
translate japanese scene8_422d11ba:

    # r "「Are you okay?"
    r "「大丈夫？"

# game/dialogs.rpy:3535
translate japanese scene8_e8331494:

    # hero "「I'm fine... Horribly anxious but fine..."
    hero "「I'm fine... Horribly anxious but fine...」"

# game/dialogs.rpy:3536
translate japanese scene8_f8b52a34:

    # "Rika stayed silent for a moment. But finally, she cried in my arms."
    "梨花ちゃんは暫く静かにしていたが、とうとう僕の腕の中で泣き出してしまった。"

# game/dialogs.rpy:3537
translate japanese scene8_a4d9e75c:

    # r "「I'm so scared... I don't want to loose Sakura...!"
    r "「ウチ…怖いんや…咲良を失うのが…！」"

# game/dialogs.rpy:3539
translate japanese scene8_103c3486:

    # hero "「I know, babe... Me neither..."
    hero "「僕もだよ…」"

# game/dialogs.rpy:3541
translate japanese scene8_bfb9febc:

    # hero "「I know... Me neither..."
    hero "「僕もだよ…」"

# game/dialogs.rpy:3542
translate japanese scene8_5546a02e:

    # "She cried for a long time in my arms."
    "長い事泣いていた。"

# game/dialogs.rpy:3543
translate japanese scene8_46acf1af:

    # "When she stopped, she spoke."
    "やがて泣き終わったとき、話し始めた。"

# game/dialogs.rpy:3544
translate japanese scene8_72e004e1:

    # r "「You know... She told me that..."
    r "「あのな…咲良が教えてくれたんや。」"

# game/dialogs.rpy:3545
translate japanese scene8_72694f8f:

    # r "「Sakura told me that she loved you..."
    r "「咲良がアンタの事好きだっちゅう事…」"

# game/dialogs.rpy:3546
translate japanese scene8_1ae45319:

    # hero "「Ah?"
    hero "「え…？」"

# game/dialogs.rpy:3547
translate japanese scene8_6ea028e4:

    # "I had guessed it since the car incident, some days ago."
    "数日前の車の事故の時に僕も思ったことがある。"

# game/dialogs.rpy:3548
translate japanese scene8_4e396aba:

    # "I had the feeling that she was in love with me, sort-of..."
    "彼女が僕に何か恋愛感情の様なものを感じているんじゃないかと。"

# game/dialogs.rpy:3549
translate japanese scene8_77b8faa3:

    # r "「She knew that it was an impossible love since you're straight...{p}But she had always loved you with all her heart..."
    r "「アンタがストレートやから、それが実現しない恋と知ってな…{p}それでもいつも心からアンタの事を思っとったねん…」"

# game/dialogs.rpy:3551
translate japanese scene8_5f050a71:

    # r "「And to be honest... It's thanks to her that we are going out together now."
    r "「それに実を言うと…ウチらがこうして付き合っとるのも咲良のお陰やねん。」"

# game/dialogs.rpy:3552
translate japanese scene8_9fcae4dd:

    # hero "「What?"
    hero "「何だって？」"

# game/dialogs.rpy:3553
translate japanese scene8_601c81c7:

    # hero "「But...{p}How? {w}Wh-{nw}"
    hero "「But...{p}How? {w}Wh-{nw}」"

# game/dialogs.rpy:3554
translate japanese scene8_b37d1c72:

    # r "「She was the one who organized everything in our first date, at the festival, with the help of Nanami."
    r "「She was the one who organized everything in our first date, at the festival, with the help of Nanami.」"

# game/dialogs.rpy:3555
translate japanese scene8_4a5bf62a:

    # r "「It didn't happen as she expected it, but the result was the same, in the end."
    r "「咲良の思った通りにはいかへんかったけどな、それでも結果的にウチらは上手く行ったんや。」"

# game/dialogs.rpy:3556
translate japanese scene8_1900c1b3:

    # hero "「That's why they weren't there at the festival?"
    hero "「That's why they weren't there at the festival?」"

# game/dialogs.rpy:3557
translate japanese scene8_bec3b4af:

    # r "「Yes."
    r "「せや…」"

# game/dialogs.rpy:3558
translate japanese scene8_6220eee7:

    # r "「I wasn't very okay about the idea first, but she knew that..."
    r "「ウチは最初は反対やったんけどな…それでも咲良がその…」"

# game/dialogs.rpy:3559
translate japanese scene8_a0391de0:

    # "She blushed."
    "少しはにかんで言った。"

# game/dialogs.rpy:3560
translate japanese scene8_4232035f:

    # r "「That I had... feelings for you too..."
    r "「ウチが…アンタを好きやっちゅう事…」"

# game/dialogs.rpy:3561
translate japanese scene8_65088067:

    # "I smiled. Tears started to come again from my eyes."
    "僕は笑い掛けた。Tears started to come again from my eyes."

# game/dialogs.rpy:3562
translate japanese scene8_33789624:

    # hero "「She wanted us to be all together."
    hero "「きっと僕たちに付き合って欲しかったんですよ。」"

# game/dialogs.rpy:3563
translate japanese scene8_b16b0eb8:

    # hero "「That she will be in our relationship like a best unique friend... Like a sister to each of us..."
    hero "「彼女は妹みたいにになりたかったんです…僕達と一緒に……」"

# game/dialogs.rpy:3564
translate japanese scene8_e66f523f:

    # hero "「The sister who is ready to abandon everything...just for the happiness of her best friends..."
    hero "「The sister who is ready to abandon everything...just for the happiness of her best friends...」"

# game/dialogs.rpy:3565
translate japanese scene8_f1b0dae1:

    # "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."
    "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."

# game/dialogs.rpy:3566
translate japanese scene8_cfdac421:

    # "And she even went to create happiness between me and Rika just because she loved us..."
    "And she even went to create happiness between me and Rika just because she loved us..."

# game/dialogs.rpy:3567
translate japanese scene8_0bd26214:

    # "It was my turn to cry, thinking about that adorable angel Sakura."
    "It was my turn to cry, thinking about that adorable angel Sakura."

# game/dialogs.rpy:3569
translate japanese scene8_f1b0dae1_1:

    # "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."
    "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."

# game/dialogs.rpy:3570
translate japanese scene8_86a7a846:

    # "It was my turn to cry, thinking about Sakura's kind heart."
    "It was my turn to cry, thinking about Sakura's kind heart."

# game/dialogs.rpy:3571
translate japanese scene8_01146bd8:

    # "At the same time, I felt like a jerk..."
    "At the same time, I felt like a jerk..."

# game/dialogs.rpy:3572
translate japanese scene8_23b3af63:

    # "Rika-chan held me tight in her arms, rubbing my hair."
    "梨花ちゃんが僕を抱きしめた。"

# game/dialogs.rpy:3573
translate japanese scene8_f0b3fd2d:

    # "I was confused. It's not something she would do normally..."
    "普段の彼女との違いに多少戸惑った。"

# game/dialogs.rpy:3574
translate japanese scene8_2e4a7bba:

    # "But I felt better in her arms..."
    "それでも…気分は悪くなかった。"

# game/dialogs.rpy:3575
translate japanese scene8_d1802fa3:

    # "The night was silent..."
    "静かな夜だった…"

# game/dialogs.rpy:3582
translate japanese scene9_e9b7540d:

    # "The next day at school was gloomy for me, Nanami and Rika."
    "次の日の学校は僕と梨花ちゃんと七海ちゃんにとって憂鬱だった。"

# game/dialogs.rpy:3584
translate japanese scene9_43a31284:

    # "At the club, we were just sitting... doing nothing, saying nothing."
    "部活の時間、僕たちはただ座っていた…何もせず、何も言わず。"

# game/dialogs.rpy:3585
translate japanese scene9_d9d4a59c:

    # "The only noise we could hear was Nanami's crying."
    "The only noise we could hear was Nanami's crying."

# game/dialogs.rpy:3586
translate japanese scene9_de531cea:

    # "She couldn't stop crying when she heard the news..."
    "She couldn't stop crying when she heard the news..."

# game/dialogs.rpy:3587
translate japanese scene9_748065c0:

    # "The club was silent without Sakura..."
    "咲良ちゃんが居ない部活は静かだった…"

# game/dialogs.rpy:3589
translate japanese scene9_be3ea4ed:

    # "Finally I spoke as I stood up, smashing my fist on the table."
    "僕は立ち上がり、手を机に叩きつけて言った。"

# game/dialogs.rpy:3591
translate japanese scene9_7a7f745d:

    # hero "「I can't stay here like this! I have to the hospital and get news about Sakura!"
    hero "「こんなところでこうして居られない！病院に行って、咲良ちゃんに会わないと！！」"

# game/dialogs.rpy:3592
translate japanese scene9_cf4c0510:

    # "Rika-chan smiled, determined, and stood up as well."
    "梨花ちゃんは決然として、それでいて穏やかな表情で立ち上がった。"

# game/dialogs.rpy:3593
translate japanese scene9_00b28479:

    # r "「You're right, kid! Let's go there right now!"
    r "「そのとおりや！ほな、さっさと行こうで！」"

# game/dialogs.rpy:3594
translate japanese scene9_611208b8:

    # r "「Nobody said that the manga club would leave one of its members alone!"
    r "「漫画部は絶対にメンバーを一人ぼっちになんかせんからな！」"

# game/dialogs.rpy:3595
translate japanese scene9_3966d3ed:

    # "I smiled. I at last found the Rika-chan I knew."
    "僕は笑った。いつもの梨花ちゃんだ。"

# game/dialogs.rpy:3597
translate japanese scene9_551012d0:

    # "Finally, Rika-chan stood up, determined."
    "そして、梨花ちゃんは決意して立ち上がった。"

# game/dialogs.rpy:3599
translate japanese scene9_3cf11638:

    # r "「I can't stay here like this! I must go to the hospital and get news about Sakura-chan!"
    r "「こんなことしてられへん！ウチは病院に行って咲良に会わな！！」"

# game/dialogs.rpy:3600
translate japanese scene9_567a77f3:

    # "I nodded and stood up as well."
    "僕もうなずいて、立ち上がった。"

# game/dialogs.rpy:3601
translate japanese scene9_00c27679:

    # hero "「You're right! Let's go right now!"
    hero "「ええ、行きましょう！」"

# game/dialogs.rpy:3602
translate japanese scene9_2fe99060:

    # "Nanami didn't say a word, but she stopped crying, dried her tears and followed us with a cute determined face."
    "Nanami didn't say a word, but she stopped crying, dried her tears and followed us with a cute determined face."

# game/dialogs.rpy:3604
translate japanese scene9_243a70b8:

    # hero "「So, Doctor, how is Sakura?"
    hero "「それで…先生。咲良ちゃんは…？」"

# game/dialogs.rpy:3606
translate japanese scene9_9eefb10b:

    # "Doctor" "Oh, don't worry, kids. Your friend is fine, now!"
    "医者" "「ああ、大丈夫ですよ。お友達は大丈夫です。」"

# game/dialogs.rpy:3607
translate japanese scene9_063c6c0a:

    # "Doctor" "We managed to save him right on time, thanks to his mother's call!"
    "医者" "「私たちが治療いたしました。早急に連絡を下さったお母さんに感謝します。」"

# game/dialogs.rpy:3608
translate japanese scene9_24b1777f:

    # "We couldn't believe it! We heaved a sigh of relief."
    "やった！僕達は安堵の溜め息を吐いた。"

# game/dialogs.rpy:3609
translate japanese scene9_f6dd7e91:

    # "Doctor" "He should be ready in a few days."
    "医者" "「彼は数日もすればよくなるでしょう。」"

# game/dialogs.rpy:3610
translate japanese scene9_e7741f1a:

    # "Doctor" "He can have visitors, if you want to see him."
    "医者" "「面会できますが、されますか？」"

# game/dialogs.rpy:3611
translate japanese scene9_61585f71:

    # "We nodded and followed the doctor to Sakura's hospital room."
    "面会をすることにした。咲良ちゃんの病室に入る。"

# game/dialogs.rpy:3613
translate japanese scene9_81c4b68b:

    # "Her face shone as she saw us."
    "咲良ちゃんが僕たちに気づいた。そして眩い笑顔をこちらに向けた。"

# game/dialogs.rpy:3614
translate japanese scene9_9b4114a6:

    # s "「%(stringhero)s-kun!! Rika-chan!! Nana-chan!!"
    s "「%(stringhero)sくん！！梨花ちゃん！！ななちゃん！！」"

# game/dialogs.rpy:3615
translate japanese scene9_417ded14:

    # n "「Sakura-nee!!!"
    n "「Sakura-nee!!!」"

# game/dialogs.rpy:3616
translate japanese scene9_c0cdba56:

    # "Nanami jumped on the bed to hug Sakura."
    "Nanami jumped on the bed to hug Sakura."

# game/dialogs.rpy:3617
translate japanese scene9_5fcae85e:

    # "She flinched a little bit by the pain but hugged Nanami back, rubbing her head like child."
    "She flinched a little bit by the pain but hugged Nanami back, rubbing her head like child."

# game/dialogs.rpy:3618
translate japanese scene9_06d60896:

    # s "「Easy, little one... I need time for the wound to heal."
    s "「Easy, little one... I need time for the wound to heal.」"

# game/dialogs.rpy:3619
translate japanese scene9_52c6a554:

    # n "「I'm not little!"
    n "「I'm not little!」"

# game/dialogs.rpy:3620
translate japanese scene9_00bc78d2:

    # "She said that while going back to crying. But of joy this time."
    "She said that while going back to crying. But of joy this time."

# game/dialogs.rpy:3621
translate japanese scene9_f46ea2ef:

    # hero "「Oh my gosh... Sakura-chan...{p}I'm so happy you survived!"
    hero "「ああ…咲良ちゃん…{p}無事で良かったよ！！咲良ちゃん！！」"

# game/dialogs.rpy:3622
translate japanese scene9_b9968b56:

    # hero "「I was so scared that you...!"
    hero "「怖かった！」"

# game/dialogs.rpy:3623
translate japanese scene9_1508b046:

    # "She seemed to be really okay, as if nothing ever happened."
    "どうやら見る限り本当に大丈夫そうだ。まるで何事も無かったかのように。"

# game/dialogs.rpy:3625
translate japanese scene9_b8aeb0be:

    # s "「I will not leave this planet where I can have the best boyfriend ever!"
    s "「I will not leave this planet where I can have the best boyfriend ever!」"

# game/dialogs.rpy:3626
translate japanese scene9_0d3e5a44:

    # s "「As well as the best friends!"
    s "「As well as the best friends!」"

# game/dialogs.rpy:3628
translate japanese scene9_0ac19270:

    # s "「I will not leave this planet where I can have my best friends!"
    s "「I will not leave this planet where I can have my best friends!」"

# game/dialogs.rpy:3629
translate japanese scene9_fadc1602:

    # r "「We all love you, Sakura! {image=heart.png}"
    r "「We all love you, Sakura! {image=heart.png}」"

# game/dialogs.rpy:3630
translate japanese scene9_ab6bc56f:

    # "Sakura smiled to us."
    "Sakura smiled to us."

# game/dialogs.rpy:3631
translate japanese scene9_c7659595:

    # s "「I love you all too... So much..."
    s "「I love you all too... So much...」"

# game/dialogs.rpy:3633
translate japanese scene9_dad43788:

    # hero "「You know, Sakura-chan..."
    hero "「You know, Sakura-chan...」"

# game/dialogs.rpy:3634
translate japanese scene9_1acb2356:

    # hero "「I feel I was a terrible friend to you..."
    hero "「I feel I was a terrible friend to you...」"

# game/dialogs.rpy:3635
translate japanese scene9_8d593ce9:

    # hero "「Please forgive me..."
    hero "「Please forgive me...」"

# game/dialogs.rpy:3636
translate japanese scene9_79003247:

    # "Sakura put a finger on my lips."
    "Sakura put a finger on my lips."

# game/dialogs.rpy:3637
translate japanese scene9_69c6fa27:

    # s "「Hush! Stop talking nonsense."
    s "「Hush! Stop talking nonsense.」"

# game/dialogs.rpy:3638
translate japanese scene9_e9594450:

    # s "「You faced my father to defend me and you were almost ready to die for that!"
    s "「You faced my father to defend me and you were almost ready to die for that!」"

# game/dialogs.rpy:3639
translate japanese scene9_7b24500a:

    # s "「How can you possibly think you're an awful friend?"
    s "「How can you possibly think you're an awful friend?」"

# game/dialogs.rpy:3640
translate japanese scene9_0cde7c21:

    # s "「You're my best friend!"
    s "「You're my best friend!」"

# game/dialogs.rpy:3641
translate japanese scene9_993b25b5:

    # "Tears rolled down my eyes."
    "Tears rolled down my eyes."

# game/dialogs.rpy:3642
translate japanese scene9_1cce67e1:

    # hero "「You're an angel, Sakura-chan."
    hero "「You're an angel, Sakura-chan.」"

# game/dialogs.rpy:3644
translate japanese scene9_57b35236:

    # r "「Did your mom visit you?"
    r "「Did your mom visit you?」"

# game/dialogs.rpy:3645
translate japanese scene9_85c4e509:

    # s "「Yes she did."
    s "「Yes she did.」"

# game/dialogs.rpy:3646
translate japanese scene9_a3b8f1b9:

    # s "「She's a bit sad that father was taken to jail,... {w}But I think she can't love him anymore after what he did."
    s "「She's a bit sad that father was taken to jail,... {w}But I think she can't love him anymore after what he did.」"

# game/dialogs.rpy:3647
translate japanese scene9_f1990ad2:

    # s "「I don't know what to think either. {w}It's a relief that he won't bother me and mom anymore...{w}but he's still my father..."
    s "「I don't know what to think either. {w}It's a relief that he won't bother me and mom anymore...{w}but he's still my father...」"

# game/dialogs.rpy:3648
translate japanese scene9_05ffe314:

    # hero "「Don't worry, Sakura-chan. We'll all be here for you!"
    hero "「Don't worry, Sakura-chan. We'll all be here for you!」"

# game/dialogs.rpy:3649
translate japanese scene9_0c126df5:

    # hero "「I don't know how yet, but we'll find out!"
    hero "「I don't know how yet, but we'll find out!」"

# game/dialogs.rpy:3650
translate japanese scene9_363e4438:

    # r "「Yes, we will!"
    r "「Yes, we will!」"

# game/dialogs.rpy:3651
translate japanese scene9_6a9ed22c:

    # n "「Yeah!"
    n "「Yeah!」"

# game/dialogs.rpy:3652
translate japanese scene9_eecde369:

    # "A tear of joy rolled down Sakura's cheek as she smiled."
    "A tear of joy rolled down Sakura's cheek as she smiled."

# game/dialogs.rpy:3653
translate japanese scene9_cb78f15e:

    # s "「I know you all do."
    s "「I know you all do.」"

# game/dialogs.rpy:3654
translate japanese scene9_e926c122:

    # s "「Thank you..."
    s "「Thank you...」"

# game/dialogs.rpy:3655
translate japanese scene9_81a1d957:

    # "Since Rika-chan brought some manga with us, we did our club activities with Sakura in her room."
    "梨花ちゃんが持ってきた漫画で、僕達は病室で部活動をした。"

# game/dialogs.rpy:3656
translate japanese scene9_87845d82:

    # "That was fun."
    "楽しかった。"

# game/dialogs.rpy:3660
translate japanese scene9_b859f96a:

    # "I was so happy..."
    "僕は幸せだった。"

# game/dialogs.rpy:3661
translate japanese scene9_4055f551:

    # "I think my luck helped me again..."
    "また僕の幸運に助けられた気がした。"

# game/dialogs.rpy:3662
translate japanese scene9_9095e36e:

    # "But this time, it helped Sakura too."
    "But this time, it helped Sakura too."

# game/dialogs.rpy:3665
translate japanese scene9_8dbb56a9:

    # n "「Why the doctor said \"he\" to Sakura?"
    n "「Why the doctor said \"he\" to Sakura?」"

# game/dialogs.rpy:3666
translate japanese scene9_ad0764fc:

    # "Uh-oh..."
    "Uh-oh..."

# game/dialogs.rpy:3667
translate japanese scene9_ff11d5e2:

    # "Well, better late than never."
    "Well, better late than never."

# game/dialogs.rpy:3668
translate japanese scene9_984c292d:

    # "Since there probably wouldn't be any other better moment for that, We told Nanami about Sakura's secret."
    "Since there probably wouldn't be any other better moment for that, We told Nanami about Sakura's secret."

# game/dialogs.rpy:3669
translate japanese scene9_fcc6280e:

    # "She took it well and understood. Her feelings for Sakura didn't changed at all. Just as I expected of her."
    "She took it well and understood. Her feelings for Sakura didn't changed at all. Just as I expected of her."

# game/dialogs.rpy:3670
translate japanese scene9_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:3671
translate japanese scene9_19a38d50:

    # "Though, for a whole week, she randomly said \"Seriously, guys!!\" multiple times!"
    "Though, for a whole week, she randomly said \"Seriously, guys!!\" multiple times!"

# game/dialogs.rpy:3672
translate japanese scene9_7c3cef17:

    # "I think she was a bit upset that we kept the secret to us until now."
    "I think she was a bit upset that we kept the secret to us until now."

# game/dialogs.rpy:3673
translate japanese scene9_0bfbc1f4:

    # "We promised it was the last time we kept a secret from Nanami."
    "We promised it was the last time we kept a secret from Nanami."

# game/dialogs.rpy:3677
translate japanese end_d9a3a0bf:

    # "Summer vacation came."
    "Summer vacation came."

# game/dialogs.rpy:3678
translate japanese end_d7bb0ba9:

    # "To all of us, school was over until next September."
    "To all of us, school was over until next September."

# game/dialogs.rpy:3683
translate japanese end_d95c02cb:

    # "Love can be really strange..."
    "恋と言うのは全く不思議だ。"

# game/dialogs.rpy:3684
translate japanese end_c409fabd:

    # "You think you know what kind of person you'll end up with but..."
    "自分が誰に一目惚れをするか分からない。"

# game/dialogs.rpy:3685
translate japanese end_5eba593d:

    # "Love at first sight can hit anybody..."
    "予想だにしなかった相手を好きになる事もあるのだ。"

# game/dialogs.rpy:3686
translate japanese end_617e0a41:

    # "I was pretty sure I would only love real girls..."
    "僕は女の子しか愛せないと思っていた…"

# game/dialogs.rpy:3687
translate japanese end_e3e4774b:

    # "And my first girlfriend is actually a very girlish boy!"
    "それなのに僕の初恋は女の子みたいな男の子だった。"

# game/dialogs.rpy:3688
translate japanese end_6292a02f:

    # "Hehe, who could have predicted this?{p}Not me, for sure..."
    "こんなこと、一体誰が予想出来ただろうか？{p}少なくとも僕は…"

# game/dialogs.rpy:3689
translate japanese end_e4e1f13a:

    # "I don't know how much time we will stay together, Sakura and I."
    "僕と咲良ちゃんが今後どれだけの間一緒にいるのかは分からない。"

# game/dialogs.rpy:3690
translate japanese end_32aa38be:

    # "Months? Years? Maybe for the rest of our lives...?"
    "一月？一年？それともずっと…？"

# game/dialogs.rpy:3691
translate japanese end_478778fd:

    # "I'm not sure of what the future holds for us..."
    "僕たちにどんな運命が待ち構えているのかも…"

# game/dialogs.rpy:3692
translate japanese end_d076867a:

    # "But if my story with Sakura must end someday,{w} I'll never forget the wonderful times I passed with her."
    "それでももし、今後この恋が終わったとしても、{w}彼女と居た素晴らしい日々を忘れることは無いだろう。"

# game/dialogs.rpy:3693
translate japanese end_7dbb760e:

    # "Or him..."
    "いや、彼か…？"

# game/dialogs.rpy:3694
translate japanese end_54a608c6:

    # "Who cares..."
    "まあ、いいか。"

# game/dialogs.rpy:3695
translate japanese end_85655125:

    # s "「Honey! What are you doing? Come see the view!"
    s "「何してるの？おいでよ！」"

# game/dialogs.rpy:3696
translate japanese end_7c43af43:

    # hero "「I'm coming, sweetie!"
    hero "「は〜〜〜〜い！」"

# game/dialogs.rpy:3703
translate japanese end_59a65d95:

    # "We're waiting at our plane at the airport."
    "We're waiting at our plane at the airport."

# game/dialogs.rpy:3704
translate japanese end_ae3dd256:

    # "Nanami and I decided to take some vacations at Nanami's homeland, Okinawa."
    "Nanami and I decided to take some vacations at Nanami's homeland, Okinawa."

# game/dialogs.rpy:3705
translate japanese end_d12ce3e4:

    # "My head was full of ondo and traditional music, like at the festival."
    "My head was full of ondo and traditional music, like at the festival."

# game/dialogs.rpy:3706
translate japanese end_4452fe89:

    # "Toshio escorted us to the airport.{p}Rika and Sakura were here too."
    "Toshio escorted us to the airport.{p}Rika and Sakura were here too."

# game/dialogs.rpy:3707
translate japanese end_8b58bec9:

    # "It's been a while that Rika and Sakura started to going out together, now."
    "It's been a while that Rika and Sakura started to going out together, now."

# game/dialogs.rpy:3708
translate japanese end_e30bea45:

    # "They look like a cute couple of girls."
    "They look like a cute couple of girls."

# game/dialogs.rpy:3709
translate japanese end_ca017532:

    # toshio "「Tell our grand-parents that I love them."
    toshio "「Tell our grand-parents that I love them.」"

# game/dialogs.rpy:3710
translate japanese end_a7442fed:

    # n "「I will, don't worry."
    n "「I will, don't worry.」"

# game/dialogs.rpy:3711
translate japanese end_d3330439:

    # toshio "「%(stringhero)s-san... Take good care of my sister."
    toshio "「%(stringhero)s-san... Take good care of my sister.」"

# game/dialogs.rpy:3712
translate japanese end_a831d3fe:

    # hero "「I will."
    hero "「I will.」"

# game/dialogs.rpy:3713
translate japanese end_17ebd900:

    # hero "「And you, good luck with your new job."
    hero "「And you, good luck with your new job.」"

# game/dialogs.rpy:3714
translate japanese end_5b348dc8:

    # "My parents hired him at the grocery shop."
    "My parents hired him at the grocery shop."

# game/dialogs.rpy:3715
translate japanese end_16f4a6f4:

    # "That way, he can renew with his past while keeping going straight forward to his new adult life."
    "That way, he can renew with his past while keeping going straight forward to his new adult life."

# game/dialogs.rpy:3716
translate japanese end_95d64dcd:

    # s "「Take a lot of photos, %(stringhero)s-kun! The whole place is beautiful, it deserve photographs!"
    s "「Take a lot of photos, %(stringhero)s-kun! The whole place is beautiful, it deserve photographs!」"

# game/dialogs.rpy:3717
translate japanese end_8f710e49:

    # r "「And bring back some souvenirs!{p}Like, an okinawa manga or something!"
    r "「And bring back some souvenirs!{p}Like, an okinawa manga or something!」"

# game/dialogs.rpy:3718
translate japanese end_9cf1c80f:

    # "I chuckled at Rika's request."
    "I chuckled at Rika's request."

# game/dialogs.rpy:3719
translate japanese end_a725dde6:

    # "Then, I heard the call for our plane."
    "Then, I heard the call for our plane."

# game/dialogs.rpy:3720
translate japanese end_bb0d139e:

    # "Nanami and I went for our departure."
    "Nanami and I went for our departure."

# game/dialogs.rpy:3721
translate japanese end_8ea8e17b:

    # "We waved at our friends and they waved back."
    "We waved at our friends and they waved back."

# game/dialogs.rpy:3722
translate japanese end_ff8fb8bb:

    # "One thing is certain,..."
    "One thing is certain,..."

# game/dialogs.rpy:3723
translate japanese end_06a038b7:

    # "It's in moments like this that having friends like that is a true luck."
    "It's in moments like this that having friends like that is a true luck."

# game/dialogs.rpy:3724
translate japanese end_099873e9:

    # "And with a girlfriend like Nanami,...{w}and friends like I have,..."
    "And with a girlfriend like Nanami,...{w}and friends like I have,..."

# game/dialogs.rpy:3725
translate japanese end_3f02d717:

    # "I can tell I'm the luckiest dude ever."
    "I can tell I'm the luckiest dude ever."

# game/dialogs.rpy:3726
translate japanese end_bc578e9b:

    # s "「Don't forget to come back!"
    s "「Don't forget to come back!」"

# game/dialogs.rpy:3727
translate japanese end_867c4c03:

    # n "「We will, Sakura-nee!"
    n "「We will, Sakura-nee!」"

# game/dialogs.rpy:3734
translate japanese end_2d2fc7dd:

    # "I finally asked Rika-chan to marry me."
    "僕はとうとう梨花ちゃんに結婚を申し出た。"

# game/dialogs.rpy:3735
translate japanese end_7d486c68:

    # "And I promised Sakura-chan that she will be the godparent of our children."
    "そして咲良ちゃんには子供の名付け親になって貰うことになった。"

# game/dialogs.rpy:3736
translate japanese end_16d52d33:

    # "I hope Sakura will find love someday. She really deserves it."
    "咲良ちゃんがいつか恋人を見つけられる日を願っているよ。"

# game/dialogs.rpy:3737
translate japanese end_8052eee9:

    # "I was actually ready for the ceremony."
    "僕は今から結婚式だ。"

# game/dialogs.rpy:3738
translate japanese end_d8e35e56:

    # "In the village, we usually do traditional weddings, but Rika wanted an occidental wedding."
    "村では普通伝統的な結婚式を行うが、梨花ちゃんは近代的な結婚式を望んだのだ。"

# game/dialogs.rpy:3739
translate japanese end_e5ffcd4a:

    # "I guess it's for the dress..."
    "I guess it's for the dress..."

# game/dialogs.rpy:3740
translate japanese end_c371d9bb:

    # "It's the best cosplay she ever did. Except, this time, it wasn't a cosplay. It was real."
    "It's the best cosplay she ever did. Except, this time, it wasn't a cosplay. It was real."

# game/dialogs.rpy:3741
translate japanese end_ecc6ece6:

    # "Priest" "Dear %(stringhero)s-san, do you take Rika-san to be your wedded wife, to live together in marriage?"
    "Priest" "「Dear %(stringhero)s-san, do you take Rika-san to be your wedded wife, to live together in marriage?」"

# game/dialogs.rpy:3742
translate japanese end_02471825:

    # hero "「You bet I do!"
    hero "「You bet I do!」"

# game/dialogs.rpy:3743
translate japanese end_34741d03:

    # r "「Can't you just say \"I do.\", you city rat?!"
    r "「Can't you just say \"I do.\", you city rat?!」"

# game/dialogs.rpy:3744
translate japanese end_96f20b66:

    # "*{i}laughs{/i}*"
    "*{i}laughs{/i}*"

# game/dialogs.rpy:3745
translate japanese end_c2e91305:

    # hero "「Hey do you plan to call me city rat even after being married!"
    hero "「Hey do you plan to call me city rat even after being married!」"

# game/dialogs.rpy:3746
translate japanese end_5848829c:

    # r "「Yes I will!"
    r "「Yes I will!」"

# game/dialogs.rpy:3747
translate japanese end_96f20b66_1:

    # "*{i}laughs{/i}*"
    "*{i}laughs{/i}*"

# game/dialogs.rpy:3748
translate japanese end_6ddd1312:

    # "Priest" "Hem hem..."
    "Priest" "「Hem hem...」"

# game/dialogs.rpy:3749
translate japanese end_aea646e4:

    # "Priest" "And you, dear Rika-san,...do you take %(stringhero)s-san to be your wedded husband, to live together in marriage?"
    "Priest" "「And you, dear Rika-san,...do you take %(stringhero)s-san to be your wedded husband, to live together in marriage?」"

# game/dialogs.rpy:3750
translate japanese end_742d691c:

    # "Then, Rika made a smile.{p}A smile I never seen on her before."
    "Then, Rika made a smile.{p}A smile I never seen on her before."

# game/dialogs.rpy:3751
translate japanese end_2665aacc:

    # "Not that naughty smile of when she prepares a joke or anything."
    "Not that naughty smile of when she prepares a joke or anything."

# game/dialogs.rpy:3752
translate japanese end_dfe9a447:

    # "A shining, bright smile. The cutest smile she ever made."
    "A shining, bright smile. The cutest smile she ever made."

# game/dialogs.rpy:3753
translate japanese end_b8af1a6a:

    # r "「... I do!"
    r "「... I do!」"

# game/dialogs.rpy:3806
translate japanese trueend_906ebb63:

    # centered "You finished the game for the first time!\nGo check the Bonus menu to see what you have unlocked!"
    centered "You finished the game for the first time!\nGo check the Bonus menu to see what you have unlocked!"

# game/dialogs.rpy:3811
translate japanese trueend_66eedbda:

    # centered "You finished the game with Sakura's route for the first time!"
    centered "You finished the game with Sakura's route for the first time!"

# game/dialogs.rpy:3816
translate japanese trueend_1561cc61:

    # centered "You finished the game with Rika's route for the first time!"
    centered "You finished the game with Rika's route for the first time!"

# game/dialogs.rpy:3821
translate japanese trueend_bb63381f:

    # centered "You finished the game with Nanami's route for the first time!"
    centered "You finished the game with Nanami's route for the first time!"

# game/dialogs.rpy:3838
translate japanese bonus1_25742137:

    # hero "「Hey girls, do you remember the first anime you ever watched?"
    hero "「ねえ、初めて見たアニメって覚えてる？」"

# game/dialogs.rpy:3840
translate japanese bonus1_6e60ac8f:

    # s "「Hmm... That's a hard one..."
    s "「うーん…」"

# game/dialogs.rpy:3841
translate japanese bonus1_25a3fcbc:

    # r "「Hmm, I think my first one was {i}Sanae-san{/i}."
    r "「ウチの初めては「サナエさん」やったと思うな。」"

# game/dialogs.rpy:3842
translate japanese bonus1_1ac8f05b:

    # hero "「{i}Sanae-san{/i}?{p}That anime that about politics and stuff?"
    hero "「「サナエさん」？{p}あの古い、政治とかを語るアニメの事？」"

# game/dialogs.rpy:3843
translate japanese bonus1_1e55e98f:

    # r "「Yeah! That one was nice!"
    r "「せや！好きやったで。」"

# game/dialogs.rpy:3844
translate japanese bonus1_89c89c91:

    # hero "「But it was aired when we were around four or five years old! You could understand what they were talking about?"
    hero "「でもアレが放映されていたのは僕たちが4,5歳の頃じゃないか！内容理解できたの？」"

# game/dialogs.rpy:3846
translate japanese bonus1_b9b8111c:

    # "Rika gave me a disgusted look."
    "梨花はちょっと怒った顔でこちらを見た。"

# game/dialogs.rpy:3847
translate japanese bonus1_17789b93:

    # r "「Whatever! Well what about you? What was your first anime?!"
    r "「じゃあアンタは何を見てたっちゅうねん！」"

# game/dialogs.rpy:3848
translate japanese bonus1_517679f1:

    # hero "「{i}Panpanman{/i}. You know, the one with the donut-headed guy..."
    hero "「「パンパンマン」。あの有名な、顔がドーナツで出来た男の話だよ。」"

# game/dialogs.rpy:3849
translate japanese bonus1_c5d8e4d4:

    # r "「And could you understand everything in that show?"
    r "「で、アンタは内容理解出来たんか？」"

# game/dialogs.rpy:3850
translate japanese bonus1_8b365fc3:

    # "Ah damn, she's got a point there."
    "む…しかし、彼女の言っている事も確かだ。"

# game/dialogs.rpy:3851
translate japanese bonus1_85a0d1bb:

    # "{i}Panpanman{/i} wasn't as complicated as {i}Sanae-san{/i}, but it's just as old. I was young so...I just followed along the animation."
    "パンパンマンはサナエさん程複雑な話では無い。が、当時の僕は幼く、絵しか理解する事が出来なかった。"

# game/dialogs.rpy:3852
translate japanese bonus1_61a5b6c2:

    # hero "「And you, Nanami-san?"
    hero "「And you, Nanami-san?」"

# game/dialogs.rpy:3856
translate japanese bonus1_b075ad7f:

    # n "「Hmm..."
    n "「Hmm...」"

# game/dialogs.rpy:3857
translate japanese bonus1_2d685261:

    # n "「I think my first one was {i}Galaxyboy{/i}..."
    n "「I think my first one was {i}Galaxyboy{/i}...」"

# game/dialogs.rpy:3858
translate japanese bonus1_88ba5484:

    # r "「The one with that robot kid?"
    r "「The one with that robot kid?」"

# game/dialogs.rpy:3859
translate japanese bonus1_31293345:

    # n "「Yeah! I remember it being really colorful and fun to watch."
    n "「Yeah! I remember it being really colorful and fun to watch.」"

# game/dialogs.rpy:3860
translate japanese bonus1_cd1e46e5:

    # n "「There was a game called {i}Punkman{/i} with a similar looking robot."
    n "「There was a game called {i}Punkman{/i} with a similar looking robot.」"

# game/dialogs.rpy:3861
translate japanese bonus1_f81a7f2c:

    # n "「Maybe that's why I remember {i}Galaxyboy{/i} a little."
    n "「Maybe that's why I remember {i}Galaxyboy{/i} a little.」"

# game/dialogs.rpy:3863
translate japanese bonus1_e11f5261:

    # n "「Although, I don't remember the story at all!"
    n "「Although, I don't remember the story at all!」"

# game/dialogs.rpy:3865
translate japanese bonus1_d7bf3979:

    # hero "「What about you, Sakura-san?{p}What was your first anime?"
    hero "「まあいいや…咲良ちゃんは…？{p}What was your first anime?」"

# game/dialogs.rpy:3867
translate japanese bonus1_85feadfd:

    # s "「Hmm... Well... I think my first one was {i}La fleur de Paris{/i}..."
    s "「うーん、多分…私が最初に見たのは「La Fleur de Paris」だと思います…」"

# game/dialogs.rpy:3868
translate japanese bonus1_7076984c:

    # hero "「The one based on a book with that girl from France?"
    hero "「The one based on a book with that girl from France?」"

# game/dialogs.rpy:3869
translate japanese bonus1_e929395f:

    # hero "「That one is more recent... It only aired around six years ago."
    hero "「結構最近のですね…確か放映されたのは6年前…」"

# game/dialogs.rpy:3870
translate japanese bonus1_4343cc28:

    # s "「Yeah. My love for anime started a little later. I was 12 when I started watching it."
    s "「ええ、私がアニメを好きになったのは遅めなんです。見始めた当時は12歳でした。」"

# game/dialogs.rpy:3871
translate japanese bonus1_5a7baf3a:

    # s "「I loved the story. It was about a girl who disguises herself as a man just to get closer to her king."
    s "「国王様とお近付きになる為に男装する主人公の女の子が好きなんです。」"

# game/dialogs.rpy:3872
translate japanese bonus1_64c82b58:

    # hero "「{i}La fleur de Paris{/i} sounds like a shoujo... I think my sister tried to read it once..."
    hero "「「La Fleur de Paris」は確か少女マンガでしたね…姉が読んでました。」"

# game/dialogs.rpy:3873
translate japanese bonus1_9c8d2196:

    # s "「I didn't know you had a sister!"
    s "「お姉さんが居るんですか？」"

# game/dialogs.rpy:3874
translate japanese bonus1_376fc77d:

    # hero "「Well, yeah."
    hero "「ええ、まあ…」"

# game/dialogs.rpy:3875
translate japanese bonus1_a4020f07:

    # hero "「But she's way older than me. She's already married."
    hero "「でも彼女とは結構年が離れてますし、何よりもう結婚してます。」"

# game/dialogs.rpy:3876
translate japanese bonus1_5d47aa0b:

    # hero "「She's in Tokyo with her husband."
    hero "「今は東京で夫と一緒に暮らしてるんです。」"

# game/dialogs.rpy:3877
translate japanese bonus1_3e2e6e7f:

    # r "「That's sounds nice!"
    r "「めっちゃええことやん！」"

# game/dialogs.rpy:3879
translate japanese bonus1_5a889d44:

    # s "「Don't you miss having your sister around?"
    s "「でも、離れ離れで暮らすのはちょっと寂しいですね…」"

# game/dialogs.rpy:3880
translate japanese bonus1_38aa09d8:

    # hero "「Oh well, she's always been independent. I got used to it after awhile, so it's okay! I still write to her from time to time."
    hero "「まあ、姉ももう大人ですし、それに僕ももう慣れました…」"

# game/dialogs.rpy:3881
translate japanese bonus1_2e2dc283:

    # hero "「By the way, do you guys have siblings?"
    hero "「そういえば、三人は兄弟とか居たりしますか？」"

# game/dialogs.rpy:3882
translate japanese bonus1_ceca0652:

    # n "「I have an older brother."
    n "「I have an older brother.」"

# game/dialogs.rpy:3883
translate japanese bonus1_1e3491ce:

    # s "「I don't."
    s "「いいえ。」"

# game/dialogs.rpy:3884
translate japanese bonus1_7c225574:

    # r "「Me neither."
    r "「Me neither.」"

# game/dialogs.rpy:3885
translate japanese bonus1_bc19b4bf:

    # hero "「Ever wish you had one?"
    hero "「実は欲しかったりとかしません？」"

# game/dialogs.rpy:3886
translate japanese bonus1_bdb5b07a:

    # r "「Nah, I never did. I'm happy with my current life."
    r "「せんせん、ウチは一人で十分満足や。」"

# game/dialogs.rpy:3887
translate japanese bonus1_58cc7ba4:

    # s "「Well for me... I sometimes dream of having a big brother."
    s "「Well for me... I sometimes dream of having a big brother.」"

# game/dialogs.rpy:3888
translate japanese bonus1_dc1a8fcd:

    # n "「I can understand that! My big brother is awesome!"
    n "「I can understand that! My big brother is awesome!」"

# game/dialogs.rpy:3889
translate japanese bonus1_f18686d4:

    # s "「But I'll never have any siblings. My parents don't want to..."
    s "「But I'll never have any siblings. My parents don't want to...」"

# game/dialogs.rpy:3890
translate japanese bonus1_7568d34b:

    # hero "「It's sad...{p}But maybe it's better like this."
    hero "「うーん…でも、こう思うのはどうですか？」"

# game/dialogs.rpy:3891
translate japanese bonus1_453d2365:

    # s "「Why is that?"
    s "「…？」"

# game/dialogs.rpy:3892
translate japanese bonus1_d027b066:

    # hero "「Usually big brothers are mean to their little sisters in the first years."
    hero "「兄というのは得てして妹を苛める物です。そうでしょう？」"

# game/dialogs.rpy:3893
translate japanese bonus1_3fae45d0:

    # n "「Hmm... My big brother was a jerk to me when I was younger... {p}But not anymore..."
    n "「Hmm... My big brother was a jerk to me when I was younger... {p}But not anymore...」"

# game/dialogs.rpy:3894
translate japanese bonus1_6d59e0e0:

    # hero "「What do you mean?"
    hero "「What do you mean?」"

# game/dialogs.rpy:3895
translate japanese bonus1_e148f620:

    # n "「Well... {w}Nevermind... {p}I guess it's normal for siblings after some time..."
    n "「Well... {w}Nevermind... {p}I guess it's normal for siblings after some time...」"

# game/dialogs.rpy:3896
translate japanese bonus1_207b93d2:

    # hero "「You must be right...{p}Anyway,..."
    hero "「You must be right...{p}Anyway,...」"

# game/dialogs.rpy:3897
translate japanese bonus1_0ee56e3f:

    # hero "「What I mean is, at least nobody can harm you, Sakura-san."
    hero "「What I mean is, at least nobody can harm you, Sakura-san.」"

# game/dialogs.rpy:3898
translate japanese bonus1_557b9c7c:

    # s "「. . ."
    s "「……。」"

# game/dialogs.rpy:3899
translate japanese bonus1_b3b9c0e8:

    # s "「Hmm..."
    s "「うーん…」"

# game/dialogs.rpy:3901
translate japanese bonus1_743a5cb3:

    # s "「Yes, you're probably right, %(stringhero)s-san."
    s "「ええ、まあ確かにそうですね…%(stringhero)sさん。」"

# game/dialogs.rpy:3902
translate japanese bonus1_d501d7aa:

    # "That hesitation again...{p}I don't find it very natural..."
    "またこの間だ…{p}時々妙に不自然に思える反応をする…"

# game/dialogs.rpy:3903
translate japanese bonus1_b9ad9038:

    # "Is she hiding something?..."
    "彼女は何かを隠している…？"

# game/dialogs.rpy:3904
translate japanese bonus1_7d45cddc:

    # "And Nanami's acting weird too. Maybe I brought up a sore subject..."
    "And Nanami's acting weird too. Maybe I brought up a sore subject..."

# game/dialogs.rpy:3905
translate japanese bonus1_8534ca42:

    # ". . ."
    "……。"

# game/dialogs.rpy:3906
translate japanese bonus1_a191a0bc:

    # "Nah, I'm just being paranoid..."
    "いや、考え過ぎか…"

# game/dialogs.rpy:3927
translate japanese bonus2_8d40556d:

    # "I was watching Sakura as she was looking for her yukata."
    "咲良ちゃんが浴衣を探しているのを見ていた。"

# game/dialogs.rpy:3928
translate japanese bonus2_2eb7bc22:

    # "She noticed that I was staring and she smiled and stopped."
    "彼女は僕が笑っているのに気付いて止まった。"

# game/dialogs.rpy:3929
translate japanese bonus2_1ab585f3:

    # s "「Why the smile, honey?"
    s "「どうして笑ってるんですか？」"

# game/dialogs.rpy:3930
translate japanese bonus2_f27ba686:

    # hero "「Well, now that I've seen you nude... I realized something..."
    hero "「だって、君がそんな格好をしてるのを見てて…思ったんだ…」"

# game/dialogs.rpy:3931
translate japanese bonus2_28c06110:

    # hero "「You look so girly with that hair and thin body..."
    hero "「髪の毛も、身体も、こんなに女の子らしいのに…」"

# game/dialogs.rpy:3932
translate japanese bonus2_1740af69:

    # hero "「But you have the chest of a thin guy... and a penis too..."
    hero "「胸は男だし、それにそこだって…」"

# game/dialogs.rpy:3933
translate japanese bonus2_cf68db7f:

    # hero "「I have the evidence that you're born as a boy... And I was pretty sure I would only like girls."
    hero "「君が男だってのは分かってる…それに僕は女の子しか愛せない…そう思ってた。」"

# game/dialogs.rpy:3934
translate japanese bonus2_a2c879ff:

    # hero "「But in my eyes, you're way prettier than any woman or girl I've ever seen. And I can't stop loving you anyway..."
    hero "「でも、今の君はその辺の女の子よりずっと可愛く見える… And I can't stop loving you anyway...」"

# game/dialogs.rpy:3935
translate japanese bonus2_bc3f3461:

    # "She smiled. Then she sat beside me on the bed."
    "彼女は笑って、ベッドに居た僕の隣に座った。"

# game/dialogs.rpy:3936
translate japanese bonus2_6e930048:

    # s "「Do you love me because I look like a girl?"
    s "「それって、私が女の子みたいだから好きって事ですか？」"

# game/dialogs.rpy:3937
translate japanese bonus2_32aff81a:

    # s "「You think it would be different if I was a real boy?"
    s "「もし私が男の子みたいだったら…違ってましたか？」"

# game/dialogs.rpy:3938
translate japanese bonus2_b543397f:

    # hero "「Well..."
    hero "「むーん…」"

# game/dialogs.rpy:3939
translate japanese bonus2_aa6128c8:

    # hero "「To be honest, I..."
    hero "「To be honest, I...」"

# game/dialogs.rpy:3940
translate japanese bonus2_2983804d:

    # "I felt a headache."
    "I felt a headache."

# game/dialogs.rpy:3941
translate japanese bonus2_f7a250f4:

    # hero "「Darn, actually, I have no idea. I don't know at all..."
    hero "「Darn, actually, I have no idea. I don't know at all...」"

# game/dialogs.rpy:3942
translate japanese bonus2_4c90784c:

    # hero "「The first thing that attracted me to you was your feminine features and personality...{w} I didn't realize you were male at the time."
    hero "「The first thing that attracted me to you was your feminine features and personality...{w} I didn't realize you were male at the time.」"

# game/dialogs.rpy:3943
translate japanese bonus2_7a20d415:

    # hero "「I fell in love with you at first sight...But surprisingly, your secret didn't stop me from loving you."
    hero "「I fell in love with you at first sight...But surprisingly, your secret didn't stop me from loving you.」"

# game/dialogs.rpy:3944
translate japanese bonus2_7fb40657:

    # hero "「I'm wondering about my own sexuality... Does this mean I'm \"biologically gay\" or something?"
    hero "「I'm wondering about my own sexuality... Does this mean I'm \"biologically gay\" or something?」"

# game/dialogs.rpy:3945
translate japanese bonus2_8b8a7483:

    # hero "「Maybe it's not gay since you are a girl inside and that I love you..."
    hero "「Maybe it's not gay since you are a girl inside and that I love you...」"

# game/dialogs.rpy:3946
translate japanese bonus2_7ae5be64:

    # hero "「I have so many questions about myself and I don't have any answers..."
    hero "「I have so many questions about myself and I don't have any answers...」"

# game/dialogs.rpy:3947
translate japanese bonus2_0a5fc0a9:

    # "Sakura gently leaned her head against mine and looked into my eyes."
    "咲良ちゃんは僕の頭を膝に乗せて、僕の目を深く見つめた。"

# game/dialogs.rpy:3948
translate japanese bonus2_4951c5b2:

    # s "「The important thing isn't what your mind wants."
    s "「重要なのは、身体が求めてる事じゃ無いんですよ。」"

# game/dialogs.rpy:3949
translate japanese bonus2_91e5ab91:

    # s "「But what your heart wants."
    s "「心が、求めてる事なんです。」"

# game/dialogs.rpy:3950
translate japanese bonus2_c269d052:

    # s "「You know... sometimes I ask that about myself too."
    s "「私もずっと自分の人生について考えてきました…」"

# game/dialogs.rpy:3951
translate japanese bonus2_77498e04:

    # s "「My father was always telling me that I should like girls. And I actually do, to be honest..."
    s "「父はずっと私に女の子を好きになるべきだと、それに…確かに私は好きでしたし…」"

# game/dialogs.rpy:3952
translate japanese bonus2_891bc3a6:

    # s "「But as I tried to become more comfortable with who I was..."
    s "「それでも、父は私に優しくしてはくれませんでした。それに、他に兄弟も居ませんでしたしね。」"

# game/dialogs.rpy:3953
translate japanese bonus2_dbb3d044:

    # s "「I started to be attracted to boys as well."
    s "「私は、次第に男の子を好きになるようになったんです…」"

# game/dialogs.rpy:3954
translate japanese bonus2_97ff28cd:

    # s "「I was completely undecided during this time. I was feeling the same exact way you are right now. But I finally figured out..."
    s "「ずっと悩んできました…でも、やっと分かったんです。」"

# game/dialogs.rpy:3955
translate japanese bonus2_f9789510:

    # s "「Girls? Boys? It doesn't matter."
    s "「女の子でも、男の子でも関係ありません。」"

# game/dialogs.rpy:3956
translate japanese bonus2_6041b77d:

    # s "「The most important is: {w}Does the person you love, return as much love as you do?"
    s "「一番重要なのは、自分と同じ位愛してくれるか、です！」"

# game/dialogs.rpy:3957
translate japanese bonus2_7d1800b4:

    # "Looking at her cute blue eyes made my heart beat louder."
    "咲良ちゃんの言う通りだ…僕も分かったよ…"

# game/dialogs.rpy:3958
translate japanese bonus2_49959dea:

    # "Yeah, she's right. Why does gender matter...?"
    "彼女の可愛らしい瞳を見ていたら、心臓が高鳴ってきた。"

# game/dialogs.rpy:3959
translate japanese bonus2_473681a8:

    # hero "「Sakura-chan... My love... My heart wants to stay with you..."
    hero "「咲良ちゃん…僕の心が、君と一緒に居たいって…」"

# game/dialogs.rpy:3960
translate japanese bonus2_8482b65d:

    # hero "「I don't care about about your body and what others may think."
    hero "「前も言ったけど、君の身体、他の人達が何を言おうが関係無いよ。」"

# game/dialogs.rpy:3961
translate japanese bonus2_e9d4d741:

    # hero "「No matter what happens, my feelings for you won't change. You're the one I want to be with."
    hero "「何が起こったとしても、君に対する思いが変わる事は無いよ。僕が自分で選んだ人だからね…」"

# game/dialogs.rpy:3962
translate japanese bonus2_b8d074ce:

    # hero "「You're right... What's gender, anyway? It's just a box to check on a paper."
    hero "「You're right... What's gender, anyway? It's just a box to check on a paper."

# game/dialogs.rpy:3963
translate japanese bonus2_9df6f08e:

    # s "「Exactly... Genders are just a label..."
    s "「Exactly... Genders are just a label...」"

# game/dialogs.rpy:3964
translate japanese bonus2_d9b204fb:

    # hero "「I love you, Sakura-chan... I love you..."
    hero "「愛してるよ、咲良ちゃん…愛してる…」"

# game/dialogs.rpy:3965
translate japanese bonus2_96516aa7:

    # hero "「And my only hope is that you love me too..."
    hero "「そして、僕のただ一つの望みは、君が僕を好きで居てくれる事…」"

# game/dialogs.rpy:3966
translate japanese bonus2_05a7a895:

    # "She cuddled me tighter."
    "彼女は僕をギュッと抱きしめた。"

# game/dialogs.rpy:3967
translate japanese bonus2_66ae317a:

    # s "「I do, %(stringhero)s-kun. I love you, more than anything..."
    s "「愛してますよ、%(stringhero)sくん！何よりも、愛してます…」"

# game/dialogs.rpy:3968
translate japanese bonus2_25c4ff40:

    # s "「I fell in love for you the first time we met, as well."
    s "「I fell in love for you the first time we met, as well.」"

# game/dialogs.rpy:3969
translate japanese bonus2_a7a9aeaa:

    # s "「You are the man of my life."
    s "「You are the man of my life.」"

# game/dialogs.rpy:3970
translate japanese bonus2_20830efe:

    # hero "「And you are the girl of mine, Sakura-chan. Forever."
    hero "「And you are the girl of mine, Sakura-chan. Forever.」"

# game/dialogs.rpy:3971
translate japanese bonus2_6b6e7e46:

    # hero "「And that secret of yours... Actually, it makes you pretty unique..."
    hero "「And that secret of yours... Actually, it makes you pretty unique...」"

# game/dialogs.rpy:3972
translate japanese bonus2_27fcf534:

    # hero "「And I feel so proud and happy to have a so unique girlfriend."
    hero "「And I feel so proud and happy to have a so unique girlfriend.」"

# game/dialogs.rpy:3973
translate japanese bonus2_57845a53:

    # "I kissed her tenderly."
    "僕は彼女に優しくキスをした。"

# game/dialogs.rpy:3974
translate japanese bonus2_3db00aad:

    # "I leaned on her naked body as I carried on."
    "僕は裸の彼女にもたれ掛かった。"

# game/dialogs.rpy:3975
translate japanese bonus2_0949b3d0:

    # "I stopped a moment to look at her beautiful face."
    "ふと、彼女の顔を見て止まった。"

# game/dialogs.rpy:3976
translate japanese bonus2_ad103b2f:

    # "Some tears were falling."
    "少し泣いていた。"

# game/dialogs.rpy:3977
translate japanese bonus2_2b9b486c:

    # hero "「Why are you crying, hun?"
    hero "「どうして泣いてるんですか？」"

# game/dialogs.rpy:3978
translate japanese bonus2_193b5b96:

    # s "「I'm just... so happy... %(stringhero)s-kun..."
    s "「嬉しいんです…%(stringhero)sくん…」"

# game/dialogs.rpy:3979
translate japanese bonus2_a9227760:

    # s "「I'm so happy that you love me, no matter what I am."
    s "「私が誰とか関係なく、私を愛してくれて、嬉しいんです…」"

# game/dialogs.rpy:3980
translate japanese bonus2_09611ce1:

    # s "「I love you so much, %(stringhero)s-kun... So much..."
    s "「心から愛してます、%(stringhero)sくん…」"

# game/dialogs.rpy:3981
translate japanese bonus2_c1ae5d6f:

    # "I cuddled her lovingly. Her body was warming mine."
    "僕は彼女を強く抱きしめた。暖かかった。"

# game/dialogs.rpy:3982
translate japanese bonus2_8d1c6006:

    # "Then I suddenly remembered... My parents! They must not find Sakura here and naked!"
    "Then I suddenly remembered... My parents! They must not find Sakura here and naked!"

# game/dialogs.rpy:3983
translate japanese bonus2_0f61a346:

    # "After a moment, we stood up and I helped Sakura putting her yukata again..."
    "少しして、僕達は立ち上がって、咲良ちゃんに浴衣を着せた。"

# game/dialogs.rpy:3987
translate japanese bonus2_38df4441:

    # "In front of her house, I felt my stomach twisting. I don't want to be without her..."
    "彼女の家の前で、僕はお腹の辺りが捩れるのを感じた。彼女と離れたく無い。"

# game/dialogs.rpy:3988
translate japanese bonus2_82ba49f2:

    # hero "「I don't want to leave you, I want to stay with you!"
    hero "「離れたく無いんだ。一緒に居たいよ。」"

# game/dialogs.rpy:3989
translate japanese bonus2_1a74f79c:

    # s "「Me too... I miss your arms already... {image=heart.png}"
    s "「Me too... I miss your arms already... {image=heart.png}」"

# game/dialogs.rpy:3990
translate japanese bonus2_4a161b65:

    # s "「It's okay, love, we'll meet again tomorrow at school, remember?"
    s "「大丈夫ですよ、明日学校でも会えるじゃないですか。」"

# game/dialogs.rpy:3991
translate japanese bonus2_2b99de4b:

    # s "「It will be the last week until the summer vacation."
    s "「それに、夏休みまであと一週間です。」"

# game/dialogs.rpy:3992
translate japanese bonus2_f405c4f8:

    # s "「And I will be free during this period."
    s "「私、ずっと暇ですから…」"

# game/dialogs.rpy:3993
translate japanese bonus2_c5fd93a1:

    # hero "「So will I..."
    hero "「それなら僕は…」"

# game/dialogs.rpy:3994
translate japanese bonus2_4ff85ef0:

    # hero "「And you'll be sure that I'll see you everyday during the vacations."
    hero "「絶対…！毎日会いに行くよ！！」"

# game/dialogs.rpy:3995
translate japanese bonus2_903c8c1d:

    # "She smiled and pecks my cheek."
    "彼女は笑って僕の頬をつんと押した。"

# game/dialogs.rpy:3996
translate japanese bonus2_f3fb3329:

    # s "「I can't wait for it, %(stringhero)s-honey-bun! {image=heart.png}"
    s "「待ちきれません、%(stringhero)sくん！ {image=heart.png}」"

# game/dialogs.rpy:4015
translate japanese bonus3_5f502601:

    # write "{b}Headline news : A father tried to murder his daughter.{/b}"
    write "{b}娘を斬りつけ44歳の父親を逮捕　N村{/b}"

# game/dialogs.rpy:4016
translate japanese bonus3_eae14b39:

    # write "Last night, in the little village of N. in the Osaka prefecture at 1am, a drunken father, 44 years old, attempted to murder the young %(stringhero)s, 19 years old.{w} The father's daughter protected him and got stabbed in the abdomen."
    write "Last night, in the little village of N. in the Osaka prefecture at 1am, a drunken father, 44 years old, attempted to murder the young %(stringhero)s, 19 years old.{w} The father's daughter protected him and got stabbed in the abdomen."

# game/dialogs.rpy:4017
translate japanese bonus3_1fc20115:

    # write "The father was arrested and the girl, 18 years old, has been taken to the hospital.{w} The reasons of the fight are unknown, but the Police suspect it was due to the effects of the alcohol and the offender's criminal record."
    write "The father was arrested and the girl, 18 years old, has been taken to the hospital.{w} The reasons of the fight are unknown, but the Police suspect it was due to the effects of the alcohol and the offender's criminal record."

# game/dialogs.rpy:4018
translate japanese bonus3_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:4053
translate japanese bonus4_244a1f9c:

    # "It was Sunday. Our first Sunday of summer vacation."
    "It was Sunday. Our first Sunday of summer vacation."

# game/dialogs.rpy:4054
translate japanese bonus4_d04b10e5:

    # "We decided to hold a picnic near the rice field."
    "We decided to hold a picnic near the rice field."

# game/dialogs.rpy:4055
translate japanese bonus4_41f4816b:

    # "I remembered it was here where Sakura revealed her secret."
    "I remembered it was here where Sakura revealed her secret."

# game/dialogs.rpy:4057
translate japanese bonus4_c69f2e23:

    # "It's also here that we kissed for the first time."
    "It's also here that we kissed for the first time."

# game/dialogs.rpy:4058
translate japanese bonus4_edd1ee56:

    # "Rika organized a little game for us and we started playing after lunch."
    "梨花ちゃんはちょっとしたゲームを企画していたらしく、昼食後に実行する事になっていた。"

# game/dialogs.rpy:4063
translate japanese bonus4_c4dbd1b7:

    # r "「Okay, this is a game based on manga, anime and videogames."
    r "「ほな、漫画部らしくマンガやアニメを使ったゲームをしようや。」"

# game/dialogs.rpy:4064
translate japanese bonus4_e067d3ab:

    # r "「It's a simple quiz where the losers will have to throw dice and the punishment is decided by the who rolls the highest."
    r "「It's a simple quiz where the losers will have to throw dice and the punishment is decided by the who rolls the highest.」"

# game/dialogs.rpy:4065
translate japanese bonus4_3d239b9b:

    # hero "「Uhhh... What kind of punishment...?"
    hero "「その、罰ってのはどういう…」"

# game/dialogs.rpy:4067
translate japanese bonus4_5b7ca773:

    # r "「Ohohoho. Scared to lose, city rat?"
    r "「なんや、怖いんか？街ネズミ。」"

# game/dialogs.rpy:4069
translate japanese bonus4_53fc8dd1:

    # hero "「I'm not a city rat! I'm your boyfriend!{p}Bring on the questions, I'm not afraid!"
    hero "「I'm not a city rat! I'm your boyfriend!{p}Bring on the questions, I'm not afraid!」"

# game/dialogs.rpy:4071
translate japanese bonus4_785461af:

    # hero "「I'm not a city rat! Bring on the questions, I'm not afraid!"
    hero "「誰が街ネズミだ！それに怖くなんて無いね、掛かって来いだ！」"

# game/dialogs.rpy:4072
translate japanese bonus4_0a90e627:

    # s "「Please be gentle with %(stringhero)s-kun, it's his first time."
    s "「優しくしてあげて下さいね、彼は初めてですし…」"

# game/dialogs.rpy:4073
translate japanese bonus4_5e33fd5b:

    # r "「Don't worry, Sakura-chan... I'll be just fine..."
    r "「安心せな、咲良ちゃん。きっちりやったるで…」"

# game/dialogs.rpy:4074
translate japanese bonus4_57287ff9:

    # "I don't like that smirk on Rika's face..."
    "なんて邪悪な笑顔なんだ。"

# game/dialogs.rpy:4075
translate japanese bonus4_1f4b7948:

    # "Nanami noticed the smirk as well."
    "Nanami noticed the smirk as well."

# game/dialogs.rpy:4077
translate japanese bonus4_bb701bb3:

    # n "「Ooooh, you will not like this, %(stringhero)s-nii!"
    n "「Ooooh, you will not like this, %(stringhero)s-nii!」"

# game/dialogs.rpy:4079
translate japanese bonus4_03d1d4c1:

    # n "「Ooooh, you will not like this, %(stringhero)s-senpai!"
    n "「Ooooh, you will not like this, %(stringhero)s-senpai!」"

# game/dialogs.rpy:4080
translate japanese bonus4_ae65064e:

    # "Rika took some cards in her bag."
    "彼女はカバンからカードの束を取り出した。"

# game/dialogs.rpy:4083
translate japanese bonus4_0158abfa:

    # r "「Okay guys, Here's the first question..."
    r "「ほな、最初の問題や。」"

# game/dialogs.rpy:4084
translate japanese bonus4_c9e531e1:

    # r "「\"What is the name of the tsundere girl in the game {i}Real Love '95{/i}?\""
    r "「『{i}REAL LOVE '95{/i} に出てくる女の子の内、ツンデレの娘の名前はなーんだ？』」"

# game/dialogs.rpy:4085
translate japanese bonus4_9f44cf6d:

    # "Ouch, this first question is hard..."
    "ゲ、最初からいきなり難しいぞ…でもちょっと思い出せそうな感じだ…"

# game/dialogs.rpy:4086
translate japanese bonus4_db066cbc:

    # "Knowing Rika, I'm surprised about this question, since {i}Real Love '95{/i} is actually an eroge for PC."
    "しかし驚いたな、{i}REAL LOVE '95{/i}ってエロゲーだぞ。僕は東京に居た頃にプレイしてたが…"

# game/dialogs.rpy:4087
translate japanese bonus4_0e0a402b:

    # "And I remember playing it a few months ago, when I still was in Tokyo."
    "And I remember playing it a few months ago, when I still was in Tokyo."

# game/dialogs.rpy:4088
translate japanese bonus4_d39d805f:

    # "So... The tsundere girl... Hmmmm...{p}Yes! I remember!"
    "ツンデレ…ツンデレ…{p}分かったぞ！"

# game/dialogs.rpy:4089
translate japanese bonus4_e4df0c97:

    # hero "「It was Mayumi!"
    hero "「まゆみだ！」"

# game/dialogs.rpy:4092
translate japanese bonus4_0a0fb39e:

    # r "「Good answer, honey!"
    r "「正解や！」"

# game/dialogs.rpy:4094
translate japanese bonus4_74267ea4:

    # r "「Good answer, city rat!"
    r "「正解や、街ネズミ！」"

# game/dialogs.rpy:4095
translate japanese bonus4_eab1e51f:

    # r "「Sakura, Nanami, roll the dice!"
    r "「Sakura, Nanami, roll the dice!」"

# game/dialogs.rpy:4096
translate japanese bonus4_b1e1b336:

    # "Sakura got a 2.{w} Nanami got a 3."
    "Sakura got a 2.{w} Nanami got a 3."

# game/dialogs.rpy:4097
translate japanese bonus4_50bfd4d4:

    # r "「Sakura-chan, you lost this round, so Nanami-chan must choose a punishment for you!"
    r "「Sakura-chan, you lost this round, so Nanami-chan must choose a punishment for you!」"

# game/dialogs.rpy:4099
translate japanese bonus4_86b16bed:

    # "Sakura started to feel shy."
    "咲良ちゃんは何だか恥ずかしそうにしている。"

# game/dialogs.rpy:4100
translate japanese bonus4_4a9a8b0c:

    # "Nanami made a funny grin."
    "Nanami made a funny grin."

# game/dialogs.rpy:4102
translate japanese bonus4_724f715d:

    # n "「Sakura-nee, you must call us by a cute anime-style name for the rest of the day!"
    n "「Sakura-nee, you must call us by a cute anime-style name for the rest of the day!」"

# game/dialogs.rpy:4103
translate japanese bonus4_a42514e8:

    # n "「I want to be called \"Senpai\",... Senpai! *{i}giggles{/i}*"
    n "「I want to be called 『先輩』,... Senpai! *{i}giggles{/i}*」"

# game/dialogs.rpy:4104
translate japanese bonus4_997b66a7:

    # r "「I want to be called \"Big sister\"!"
    r "「I want to be called 『お姉様』!」"

# game/dialogs.rpy:4106
translate japanese bonus4_f47c567a:

    # s "「Okay!"
    s "「Okay!」"

# game/dialogs.rpy:4107
translate japanese bonus4_4b1e12da:

    # s "「And you, %(stringhero)s-kun? What should I call you?" nointeract
    s "「And you, %(stringhero)s-kun? What should I call you?」" nointeract

# game/dialogs.rpy:4115
translate japanese bonus4_aab8ef6a:

    # hero "「Sakura-chan...{w}I want you to call me \"[hnick!t]\"!"
    hero "「咲良ちゃん…{w}今日一日…{w}「[hnick!t]」って呼んで欲しいんだ！」"

# game/dialogs.rpy:4117
translate japanese bonus4_7d57ea39:

    # s "「E... Eeeeeeeh!!!"
    s "「え、ええええ！！」"

# game/dialogs.rpy:4118
translate japanese bonus4_5541ddc2:

    # "She became embarrassed, hiding her little mouth behind her fist... So cute..."
    "彼女はとても恥ずかしがって、口を手で押さえている。愛らしい…"

# game/dialogs.rpy:4119
translate japanese bonus4_cebdf164:

    # "I blushed myself, waiting for her answer, knowing what she was about to say already..."
    "僕は恥ずかしがりながら、彼女の返事を待っていた。"

# game/dialogs.rpy:4121
translate japanese bonus4_c5c21450:

    # s "「A-Alright... I'll d-do it... [hnick!t]..."
    s "「わ、分かりました… [hnick!t]…」"

# game/dialogs.rpy:4123
translate japanese bonus4_70397aff:

    # hero "「Gah! The cuteness!"
    hero "「可愛い！」"

# game/dialogs.rpy:4124
translate japanese bonus4_b312e964:

    # "If this was an anime, my nose would be bleeding."
    "僕は鼻から出血しながら地面に勢い良く倒れ込んだ。"

# game/dialogs.rpy:4126
translate japanese bonus4_92f46d2c:

    # r "「Hey, wake up, city rat! Quit daydreaming and get ready for the next question. It's Sakura's turn to ask the question!"
    r "「おい、起きろや。次の問題いくで。ほな、ルールに従って負けた咲良ちゃんに出題して貰うで。」"

# game/dialogs.rpy:4127
translate japanese bonus4_24294182:

    # "Yikes! It means that it will be me against Rika and Nanami! I must not fail!"
    "ゲッ！そうなると梨花ちゃんと七海ちゃんとの一騎討ちって事か…！まけられない！"

# game/dialogs.rpy:4131
translate japanese bonus4_731520a2:

    # s "「O... Okay... So..."
    s "「そ、それじゃあ…」"

# game/dialogs.rpy:4132
translate japanese bonus4_1011202f:

    # s "「The question is:{p}\"How many episodes are in the anime {i}Dragon Sphere{/i}?\""
    s "「『アニメ{i}DRAGON SPHERE{/i}の話数は全部で何話？』」"

# game/dialogs.rpy:4133
translate japanese bonus4_37f34882:

    # "I knew this anime well. I was about to answer but Rika was faster."
    "良く知ってるアニメだ。しかし答えるのは梨花ちゃんの方が早かった。"

# game/dialogs.rpy:4135
translate japanese bonus4_60b1dd5c:

    # r "「26! {w}And 3 OVAs!"
    r "「26! {w}And 3 OVAs!」"

# game/dialogs.rpy:4137
translate japanese bonus4_ed2c49db:

    # s "「Correct, Big sister!"
    s "「正解です、お姉様！」"

# game/dialogs.rpy:4138
translate japanese bonus4_c9aee644:

    # s "「[hnick!t], Nanami-senpai, roll the dice."
    s "「[hnick!t], Nanami-senpai, roll the dice.」"

# game/dialogs.rpy:4139
translate japanese bonus4_906b5890:

    # "I got a five! Yes!"
    "I got a five! Yes!"

# game/dialogs.rpy:4140
translate japanese bonus4_1581741a:

    # "Nanami rolls...{w}and got a 6! Oh come on!"
    "Nanami rolls...{w}and got a 6! Oh come on!"

# game/dialogs.rpy:4141
translate japanese bonus4_db50cb13:

    # "Nanami directed an evil smile at me. Now I'm pretty scared..."
    "七海ちゃんは凶悪な笑顔をこっちに向けてきた。嫌な予感がする…"

# game/dialogs.rpy:4142
translate japanese bonus4_021acfb6:

    # "She took something in Rika's bag and gave it to me."
    "She took something in Rika's bag and gave it to me."

# game/dialogs.rpy:4143
translate japanese bonus4_d1658509:

    # "Cat-ears!"
    "猫耳だ！"

# game/dialogs.rpy:4144
translate japanese bonus4_3d592873:

    # n "「Here, put this on your head and say 'Nyan nyan~'!"
    n "「ほな、これを頭に乗せてこう言うんや、「にゃんにゃん～」」"

# game/dialogs.rpy:4145
translate japanese bonus4_e4a0fb20:

    # hero "「Heeeeey!{p}I'm pretty sure this was Rika-chan's idea, huh!"
    hero "「Heeeeey!{p}I'm pretty sure this was Rika-chan's idea, huh!」"

# game/dialogs.rpy:4146
translate japanese bonus4_eb7fa7f2:

    # r "「Who knows?... Get to work, city rat! Or I should say 'City cat'!"
    r "「知らんがな。さっさとやりや、街ネズミ！いや、都会ネコ？」"

# game/dialogs.rpy:4147
translate japanese bonus4_8bc9d837:

    # n "「Get to work, you slacker! *{i}with a fake strong male voice from some video game{/i}*"
    n "「Get to work, you slacker! *{i}with a fake strong male voice from some video game{/i}*」"

# game/dialogs.rpy:4148
translate japanese bonus4_ff3a6290:

    # "I sighed and I put the ears on my head."
    "僕は溜息を吐いてそれを装着した。"

# game/dialogs.rpy:4149
translate japanese bonus4_cf248989:

    # "We didn't have any mirrors around so I couldn't see how I looked. But I'm sure it was ridiculous."
    "周りに鏡が無いから自分の姿を確認できない。が、とても恥ずかしい姿であろう事は容易に想像できる…"

# game/dialogs.rpy:4150
translate japanese bonus4_7ae9ab60:

    # "I put my fists in front of me, kitty-like."
    "猫の様に手を正面に掲げる。"

# game/dialogs.rpy:4151
translate japanese bonus4_ad791bf6:

    # hero "「N... Ny... Nyan~ nyan~!"
    hero "「に……にゃ……にゃんにゃん～！」"

# game/dialogs.rpy:4157
translate japanese bonus4_3686e5b0:

    # "Sakura made a big charming smile that instantly made me feel better."
    "咲良ちゃんがすっごい満開の笑みなのが唯一の救いだ。"

# game/dialogs.rpy:4159
translate japanese bonus4_af9b813d:

    # "Sakura made a big charming smile."
    "Sakura made a big charming smile."

# game/dialogs.rpy:4160
translate japanese bonus4_17625773:

    # s "「Awwww it's so cuuuuute!!!"
    s "「あうううう！かっわいいいいいいいいいいいいい！！！！！」"

# game/dialogs.rpy:4161
translate japanese bonus4_6328f30b:

    # "Rika smiled too, but her smile was way more sinister."
    "梨花ちゃんも笑っていた。こっちは若干邪悪ではあったが。"

# game/dialogs.rpy:4162
translate japanese bonus4_bda42d96:

    # "Nanami was just laughing out loud. I definitely should look ridiculous."
    "Nanami was just laughing out loud. I definitely should look ridiculous."

# game/dialogs.rpy:4163
translate japanese bonus4_8d7688c3:

    # r "「Hehe, doesn't fit you at all!"
    r "「いやー全く似合わへんなあ。ハハハ。」"

# game/dialogs.rpy:4164
translate japanese bonus4_12c6774c:

    # r "「Since you lost, it's your turn to ask the question!"
    r "「ほな、次はアンタが問題出す番やで。」"

# game/dialogs.rpy:4169
translate japanese bonus4_ede3e123:

    # "Rika gave me another card and I read the question."
    "梨花ちゃんは僕に問題の書いてあるカードを手渡した。"

# game/dialogs.rpy:4170
translate japanese bonus4_0e18d25a:

    # "I was also thinking about the pledge they could imagine."
    "僕はその時、彼女達がどんな罰ゲームを与えるのかを考えていたりした。"

# game/dialogs.rpy:4171
translate japanese bonus4_9fe745b2:

    # hero "「Okay so...{p}\"What is the name of the seiyuu who plays Haruki in {i}Panty, Bra und Strumpfgürtel{/i}?\""
    hero "「えっとじゃあ…{p}『{i}Panty & Bra : sehr niedlich{/i}のハルキを務めた声優は誰？』」"

# game/dialogs.rpy:4172
translate japanese bonus4_9c8a3481:

    # "Nanami answered immediately, just before Rika could answer."
    "すぐに梨花ちゃんが答えようとしていたが、七海ちゃんの方が速かった。"

# game/dialogs.rpy:4174
translate japanese bonus4_5cc59595:

    # n "「Hidaka Megumi!"
    n "「日高めぐみ！」"

# game/dialogs.rpy:4175
translate japanese bonus4_a7215825:

    # hero "「You got it, Nanami-chan!"
    hero "「七海ちゃん、正解！」"

# game/dialogs.rpy:4177
translate japanese bonus4_fc07c2f9:

    # hero "「Rika-chan, Sakura-chan, roll the dice!"
    hero "「Rika-chan, Sakura-chan, roll the dice!」"

# game/dialogs.rpy:4178
translate japanese bonus4_1a6d10bf:

    # "Sakura rolled a 3."
    "Sakura rolled a 3."

# game/dialogs.rpy:4180
translate japanese bonus4_d821c96c:

    # "Rika made a naughty grin and rolled the dice..."
    "Rika made a naughty grin and rolled the dice..."

# game/dialogs.rpy:4182
translate japanese bonus4_619191be:

    # "She pouted when she got a one..."
    "She pouted when she got a one..."

# game/dialogs.rpy:4184
translate japanese bonus4_667bfdcf:

    # s "「Hmmm... Let's see..."
    s "「それじゃあ…」"

# game/dialogs.rpy:4187
translate japanese bonus4_ddb63045:

    # "She thought for a moment but finally made a smile... One worse than Rika-chan's!"
    "彼女は暫く考えて、そして笑った。梨花ちゃんをも凌ぐ凶悪な笑顔で。"

# game/dialogs.rpy:4188
translate japanese bonus4_b721b3d2:

    # "It's so unusual to see such a smile on Sakura's face."
    "こんな咲良ちゃん、見たことないぞ…！"

# game/dialogs.rpy:4190
translate japanese bonus4_226ccb5a:

    # s "「Big sister, I want you to kiss [hnick!t] on the mouth, in front of us!"
    s "「お姉様, I want you to kiss [hnick!t] on the mouth, in front of us!」"

# game/dialogs.rpy:4192
translate japanese bonus4_288e9d30:

    # s "「Big sister, I want you to kiss [hnick!t] on the cheek!"
    s "「お姉様、それじゃあ…[hnick!t]のほっぺたに、キスして下さい。」"

# game/dialogs.rpy:4194
translate japanese bonus4_54428ce0:

    # r "「Whaaaaaaat？？？"
    r "「へえええええええ？？？」"

# game/dialogs.rpy:4195
translate japanese bonus4_3efa7eb4:

    # hero "「Eeeeeeeeh!!!"
    hero "「えええええええええええええ！！！」"

# game/dialogs.rpy:4198
translate japanese bonus4_329e5a99:

    # n "「Eeeeh but it's my boyfriend!!"
    n "「Eeeeh but it's my boyfriend!!」"

# game/dialogs.rpy:4200
translate japanese bonus4_7529a0c6:

    # r "「In front of you all?!"
    r "「In front of you all?!」"

# game/dialogs.rpy:4202
translate japanese bonus4_2f9c2ac6:

    # r "「I don't want to, Sakura-chan!{p}It's mean!"
    r "「んなことしたかない！咲良ちゃん！カンニンや！！」"

# game/dialogs.rpy:4203
translate japanese bonus4_d7114010:

    # s "「I'm sorry but it's in the rules. You told us earlier, Rika-chan!{p}And it wouldn't be a punishment if it was too easy!"
    s "「ごめんなさい、でも…ルールは絶対。そうでしょう？お姉様。{p}それにあんまり簡単だと罰ゲームになりませんしね！」"

# game/dialogs.rpy:4206
translate japanese bonus4_eb063792:

    # n "「I feel like punished too, even if I won..."
    n "「I feel like punished too, even if I won...」"

# game/dialogs.rpy:4207
translate japanese bonus4_a2b2dbfe:

    # "Sakura can be scary sometimes..."
    "咲良ちゃんは時々怖い…"

# game/dialogs.rpy:4209
translate japanese bonus4_b9f6e14e:

    # r "「Hmpf!... {w}A... Alright... I'll do it!"
    r "「わ、わかったで…ほな、やったる！！」"

# game/dialogs.rpy:4210
translate japanese bonus4_6213e31d:

    # "My eyes wide open, I saw Rika-chan crawling slowly to me with her pissed expression."
    "僕は目を見開き、嫌そうに接近してくる梨花ちゃんを凝視した。"

# game/dialogs.rpy:4213
translate japanese bonus4_7062c20b:

    # "Then we closed our eyes and we kissed, after some hesitation."
    "Then we closed our eyes and we kissed, after some hesitation."

# game/dialogs.rpy:4214
translate japanese bonus4_23c6d1be:

    # "At the moment our lips touched, I think we both forgot the girls were here because the kiss was long and lovey."
    "At the moment our lips touched, I think we both forgot the girls were here because the kiss was long and lovey."

# game/dialogs.rpy:4216
translate japanese bonus4_6e084a46:

    # "My heart was beating loudly..."
    "My heart was beating loudly..."

# game/dialogs.rpy:4218
translate japanese bonus4_0defb206:

    # "Then she closed her eyes and neared her face to my cheek."
    "梨花ちゃんは目を閉じ、僕の頬に近づいた。"

# game/dialogs.rpy:4220
translate japanese bonus4_f50c2971:

    # "My heart was beating loudly as she draws near..."
    "心臓がドキドキする…"

# game/dialogs.rpy:4221
translate japanese bonus4_fcdd662b:

    # ".........{i}*chu~*{/i}......"
    "……………………………{i}*ちゅっ*{/i}………………………………"

# game/dialogs.rpy:4222
translate japanese bonus4_e916cd9d:

    # s "「Awww, so cute!!!"
    s "「二人共、凄く可愛いです！！！！！！」"

# game/dialogs.rpy:4224
translate japanese bonus4_4ef9b2c5:

    # n "「You better wash your cheek and kiss me at once after this!"
    n "「You better wash your cheek and kiss me at once after this!」"

# game/dialogs.rpy:4226
translate japanese bonus4_79a8814d:

    # n "「Hey, get a room, you two!"
    n "「Hey, get a room, you two!」"

# game/dialogs.rpy:4228
translate japanese bonus4_d218ebbe:

    # "Then Rika got back to where she was sitting."
    "梨花ちゃんは元座っていた場所に戻った。"

# game/dialogs.rpy:4230
translate japanese bonus4_e986db9f:

    # "She was embarrassed but her face was still looking pissed."
    "ふてくされたような顔のまま、赤くなっていた。"

# game/dialogs.rpy:4232
translate japanese bonus4_98a2e64c:

    # r "「T-t-that's private stuff! You're not supposed to watch that!..."
    r "「T-t-that's private stuff! You're not supposed to watch that!...」"

# game/dialogs.rpy:4233
translate japanese bonus4_20c841e6:

    # "I silently agreed, blushing red."
    "I silently agreed, blushing red."

# game/dialogs.rpy:4234
translate japanese bonus4_479b77b6:

    # "But well...as Sakura said, it was a punishment..."
    "But well...as Sakura said, it was a punishment..."

# game/dialogs.rpy:4236
translate japanese bonus4_8dbc83a8:

    # r "「D... Don't expect anything, you pervert, you city rat!... It's just a punishment!"
    r "「な……なに期待しとんねん！この、ヘンタイ！街ネズミ！…罰ゲームだからやっただけやからな！アホ！バーカ！」"

# game/dialogs.rpy:4238
translate japanese bonus4_e77cd0a8:

    # r "「It should be Sakura's job, anyway!"
    r "「It should be Sakura's job, anyway!」"

# game/dialogs.rpy:4240
translate japanese bonus4_b9726a19:

    # r "「It should be Nanami's job, anyway!"
    r "「It should be Nanami's job, anyway!」"

# game/dialogs.rpy:4241
translate japanese bonus4_929d41bb:

    # n "「Right!"
    n "「Right!」"

# game/dialogs.rpy:4242
translate japanese bonus4_d0abd9fc:

    # hero "「Hmph! Fine with me!"
    hero "「ふ…ふん！こっちだって同じだ！」"

# game/dialogs.rpy:4243
translate japanese bonus4_9b79ecac:

    # "I felt that I'm turning red too..."
    "I felt that I'm turning red too..."

# game/dialogs.rpy:4245
translate japanese bonus4_8fc82e11:

    # "Sakura woke me up in my confusion."
    "Sakura woke me up in my confusion."

# game/dialogs.rpy:4246
translate japanese bonus4_ad51f715:

    # s "「Hey, I'm your girlfriend, don't you forget!"
    s "「Hey, I'm your girlfriend, don't you forget!」"

# game/dialogs.rpy:4247
translate japanese bonus4_2aa2b9a6:

    # "I smiled and kissed Sakura fondly on the cheek."
    "I smiled and kissed Sakura fondly on the cheek."

# game/dialogs.rpy:4248
translate japanese bonus4_a2e4f8f1:

    # n "「Saku and %(stringhero)s, sitting in a tree, K. I. S. S. I. N. G.!..."
    n "「Saku and %(stringhero)s, sitting in a tree, K. I. S. S. I. N. G.!...」"

# game/dialogs.rpy:4250
translate japanese bonus4_4420fe81:

    # "Nanami woke me up in my confusion."
    "Nanami woke me up in my confusion."

# game/dialogs.rpy:4251
translate japanese bonus4_ad51f715_1:

    # s "「Hey, I'm your girlfriend, don't you forget!"
    s "「Hey, I'm your girlfriend, don't you forget!」"

# game/dialogs.rpy:4252
translate japanese bonus4_c6260d04:

    # "I smiled and kissed Nanami fondly on the cheek."
    "I smiled and kissed Nanami fondly on the cheek."

# game/dialogs.rpy:4253
translate japanese bonus4_a5c16330:

    # hero "「By the way... It's your turn for the next question, Rika-chan..."
    hero "「そうだ、梨花ちゃん…次は君の番じゃないか…」"

# game/dialogs.rpy:4259
translate japanese bonus4_fc3d4898:

    # "The game was long and the punishments was fun... Sometimes evil but fun..."
    "僕達は長いことゲームを続けた。罰ゲームは何だかんだ言って楽しかった…時々凶悪で…それでも楽しかった…"

# game/dialogs.rpy:4260
translate japanese bonus4_4c6939cf:

    # "When evening came, we started to go back to our homes."
    "日も暮れて、僕達は家に帰ることにした。"

# game/dialogs.rpy:4266
translate japanese bonus4_f83000c7:

    # "I was escorting Sakura-chan to her house as usual and we were chatting about the picnic."
    "咲良ちゃんを家に送りながら、僕達はピクニックの話をした。"

# game/dialogs.rpy:4267
translate japanese bonus4_ca69579e:

    # s "「That was fun... [hnick!t]!"
    s "「楽しかったですね、[hnick!t]！」"

# game/dialogs.rpy:4268
translate japanese bonus4_313e8d85:

    # hero "「You know, you don't need to keep calling me [hnick!t], now. The girls aren't with us anymore."
    hero "「もうそうやって呼ぶ必要はありませんよ。梨花ちゃんも居ませんし。」"

# game/dialogs.rpy:4269
translate japanese bonus4_c63b8929:

    # s "「It's okay, I like taking the punishment seriously...{p}Unless you don't like me calling you that?"
    s "「いいんです、こういうの好きですから。{p}それとも…こうやって呼ばれるの、イヤですか？」"

# game/dialogs.rpy:4270
translate japanese bonus4_e5dc6483:

    # hero "「No, it's okay, I enjoy it!"
    hero "「いやいやそんな…！楽しいですよ！」"

# game/dialogs.rpy:4273
translate japanese bonus4_90714309:

    # hero "「Even if it might sound weird between lovers..."
    hero "「Even if it might sound weird between lovers...」"

# game/dialogs.rpy:4274
translate japanese bonus4_b9b5a607:

    # "We smiled together."
    "僕達は笑いあった。"

# game/dialogs.rpy:4277
translate japanese bonus4_756dbf3c:

    # s "「[hnick!t], do you...{p}Do you think I would look pretty in a maid outfit?"
    s "「[hnick!t]…その…{p}…私にメイド服って、似合うと思います？」"

# game/dialogs.rpy:4279
translate japanese bonus4_6dc8daef:

    # "I was close to nose bleeding as I was imagining Sakura in a maid outfit."
    "脳内で想像したら鼻の奥から血が出そうになった。"

# game/dialogs.rpy:4280
translate japanese bonus4_6049d7bb:

    # hero "「I... I'm pretty sure of it, Sakura-chan!"
    hero "「絶対似合うよ、咲良ちゃん！」"

# game/dialogs.rpy:4281
translate japanese bonus4_e234cd17:

    # hero "「In fact... No matter what you'll wear, you always will be cute to me."
    hero "「と言うより…咲良ちゃんの場合は何を着ても可愛いというか…」"

# game/dialogs.rpy:4282
translate japanese bonus4_138db92c:

    # "We blushed together."
    "お互い少し照れた。"

# game/dialogs.rpy:4284
translate japanese bonus4_554402fd:

    # hero "「Well... I guess you would..."
    hero "「Well... I guess you would...」"

# game/dialogs.rpy:4285
translate japanese bonus4_03905b91:

    # hero "「You know how cute you look already."
    hero "「You know how cute you look already.」"

# game/dialogs.rpy:4286
translate japanese bonus4_a0391de0:

    # "She blushed."
    "She blushed."

# game/dialogs.rpy:4289
translate japanese bonus4_4722aed7:

    # s "「You know... I like to call you [hnick!t]..."
    s "「その…[hnick!t]って呼ぶの…好きなんです…」"

# game/dialogs.rpy:4290
translate japanese bonus4_1ea38385:

    # s "「I always wanted to have a big brother but I didn't got this chance."
    s "「ずっとお兄ちゃんが欲しくって、でもお兄ちゃんは居ないから…」"

# game/dialogs.rpy:4291
translate japanese bonus4_dcd60152:

    # s "「So, calling you like this... It's almost as if... If I have one..."
    s "「こうやって呼んでると…本当のお兄ちゃんみたいな…」"

# game/dialogs.rpy:4292
translate japanese bonus4_acc2309b:

    # "We blushed together, deeply."
    "僕は頭を掻いた。"

# game/dialogs.rpy:4294
translate japanese bonus4_119e7f5e:

    # "Well, I guess it would be nice for her to have a brother. As long as he doesn't turn crazy easily like Nanami's brother..."
    "Well, I guess it would be nice for her to have a brother. As long as he doesn't turn crazy easily like Nanami's brother..."

# game/dialogs.rpy:4295
translate japanese bonus4_1f716602:

    # hero "「Well... If you still want to call me like this all the time... I won't mind at all, Sakura-chan."
    hero "「もし…そうやって呼ぶのが良いんでしたら、良いですよ…ずっと呼んでいても。」"

# game/dialogs.rpy:4296
translate japanese bonus4_a465601d:

    # hero "「To be honest, I always wanted to have a little sister."
    hero "「それに実は、僕も妹が欲しいってずっと思ってたんです。」"

# game/dialogs.rpy:4297
translate japanese bonus4_b5b85398:

    # s "「But you have a big sister, don't you?"
    s "「でも、お姉さんが居ましたよね？」"

# game/dialogs.rpy:4298
translate japanese bonus4_3f93cb80:

    # hero "「Yeah, but it's not the same thing. Plus, she doesn't live with me anymore..."
    hero "「ええ、でも姉と妹は別物ですよ。それに、今は離れて暮らしてますし…」"

# game/dialogs.rpy:4299
translate japanese bonus4_6fc9312b:

    # "Sakura nodded..."
    "咲良ちゃんは頷いた。"

# game/dialogs.rpy:4303
translate japanese bonus4_bd6e96bc:

    # s "「So, how's your couple with Rika-chan?"
    s "「So, how's your couple with Rika-chan?」"

# game/dialogs.rpy:4304
translate japanese bonus4_a9623e40:

    # hero "「Pretty good! I think I'm going to marry her!"
    hero "「Pretty good! I think I'm going to marry her!」"

# game/dialogs.rpy:4306
translate japanese bonus4_dd27c7e7:

    # s "「Really?! That's great!!"
    s "「Really?! That's great!!」"

# game/dialogs.rpy:4307
translate japanese bonus4_699e47c9:

    # hero "「You think she will say yes?"
    hero "「You think she will say yes?」"

# game/dialogs.rpy:4309
translate japanese bonus4_df94e128:

    # s "「I'm sure she will."
    s "「I'm sure she will.」"

# game/dialogs.rpy:4310
translate japanese bonus4_c15523d1:

    # s "「You know, she really loves you more than it looks."
    s "「You know, she really loves you more than it looks.」"

# game/dialogs.rpy:4311
translate japanese bonus4_f0c8c519:

    # s "「It's just her way of being herself."
    s "「It's just her way of being herself.」"

# game/dialogs.rpy:4312
translate japanese bonus4_10b77e4e:

    # hero "「Yeah, I know..."
    hero "「Yeah, I know...」"

# game/dialogs.rpy:4313
translate japanese bonus4_7f081c6b:

    # "Then I stopped and I asked Sakura."
    "Then I stopped and I asked Sakura."

# game/dialogs.rpy:4314
translate japanese bonus4_f25fd7bb:

    # hero "「Sakura-chan..."
    hero "「Sakura-chan...」"

# game/dialogs.rpy:4315
translate japanese bonus4_7347e2f5:

    # hero "「I would be so honored that you become our future children's godparent!"
    hero "「I would be so honored that you become our future children's godparent!」"

# game/dialogs.rpy:4316
translate japanese bonus4_2662060c:

    # hero "「Would you like to?"
    hero "「Would you like to?」"

# game/dialogs.rpy:4318
translate japanese bonus4_197853e0:

    # "Sakura got surprised and didn't said a word for a while."
    "Sakura got surprised and didn't said a word for a while."

# game/dialogs.rpy:4319
translate japanese bonus4_08e5cd3b:

    # "But her face made a shiny smile, finally."
    "But her face made a shiny smile, finally."

# game/dialogs.rpy:4321
translate japanese bonus4_a89ea5da:

    # s "「Oh yes! Yes, I would love to!"
    s "「Oh yes! Yes, I would love to!」"

# game/dialogs.rpy:4322
translate japanese bonus4_95d8a37f:

    # hero "「Yes! Thank you, Sakura-chan!"
    hero "「Yes! Thank you, Sakura-chan!」"

# game/dialogs.rpy:4323
translate japanese bonus4_189cb5d4:

    # s "「I'm so happy you asked me that!"
    s "「I'm so happy you asked me that!」"

# game/dialogs.rpy:4324
translate japanese bonus4_1170dcb4:

    # hero "「You really are a close friend, Sakura-chan..."
    hero "「You really are a close friend, Sakura-chan...」"

# game/dialogs.rpy:4326
translate japanese bonus4_2fff80d3:

    # hero "「I know we can't technically be brothers and sisters... But then I want you to be a family friend!"
    hero "「I know we can't technically be brothers and sisters... But then I want you to be a family friend!」"

# game/dialogs.rpy:4328
translate japanese bonus4_c58e083a:

    # hero "「I would like so much to have you as a family friend!"
    hero "「I would like so much to have you as a family friend!」"

# game/dialogs.rpy:4329
translate japanese bonus4_02601609:

    # "Sakura suddenly hugged me."
    "Sakura suddenly hugged me."

# game/dialogs.rpy:4330
translate japanese bonus4_8b5ff4e0:

    # s "「Thank you so much, %(stringhero)s!... I mean, [hnick!t]!"
    s "「Thank you so much, %(stringhero)s!... I mean, [hnick!t]!」"

# game/dialogs.rpy:4331
translate japanese bonus4_bdc5b511:

    # "I held her a moment with a smile."
    "I held her a moment with a smile."

# game/dialogs.rpy:4334
translate japanese bonus4_7b0de0eb:

    # s "「So, how's your couple with Nana-chan?"
    s "「So, how's your couple with Nana-chan?」"

# game/dialogs.rpy:4335
translate japanese bonus4_22478f8e:

    # hero "「It's great! She's an amazing girl."
    hero "「It's great! She's an amazing girl.」"

# game/dialogs.rpy:4336
translate japanese bonus4_5ba932be:

    # hero "「We plan to go to her grand-parents at Okinawa for the vacations, for a couple of weeks."
    hero "「We plan to go to her grand-parents at Okinawa for the vacations, for a couple of weeks.」"

# game/dialogs.rpy:4337
translate japanese bonus4_a54e7c2a:

    # s "「That's great!"
    s "「That's great!」"

# game/dialogs.rpy:4338
translate japanese bonus4_d33cd195:

    # s "「What you will do, there?"
    s "「What you will do, there?」"

# game/dialogs.rpy:4339
translate japanese bonus4_15d9f409:

    # hero "「I don't know yet."
    hero "「I don't know yet.」"

# game/dialogs.rpy:4340
translate japanese bonus4_b396e8be:

    # hero "「Since it's Nanami-chan's birth land, she probably knows what's the best."
    hero "「Since it's Nanami-chan's birth land, she probably knows what's the best.」"

# game/dialogs.rpy:4341
translate japanese bonus4_fb2732b4:

    # hero "「So far, she said there's a lot of festivals at this period of the year."
    hero "「So far, she said there's a lot of festivals at this period of the year.」"

# game/dialogs.rpy:4342
translate japanese bonus4_aff5d3d2:

    # s "「You're so lucky!"
    s "「You're so lucky!」"

# game/dialogs.rpy:4343
translate japanese bonus4_a3265a2a:

    # s "「I wish I could go there."
    s "「I wish I could go there.」"

# game/dialogs.rpy:4344
translate japanese bonus4_9597c256:

    # "I nodded, undestanding her feeling."
    "I nodded, undestanding her feeling."

# game/dialogs.rpy:4345
translate japanese bonus4_d8972d72:

    # "Then I got an idea."
    "Then I got an idea."

# game/dialogs.rpy:4346
translate japanese bonus4_3d45bfe0:

    # hero "「Hey... Why not trying to organize a trip for all of us when we will come back?"
    hero "「Hey... Why not trying to organize a trip for all of us when we will come back?」"

# game/dialogs.rpy:4347
translate japanese bonus4_7e322d73:

    # hero "「Like, I'll go there with Nanami in recognition, then when we will come back, we will know what are the great spots we can go."
    hero "「Like, I'll go there with Nanami in recognition, then when we will come back, we will know what are the great spots we can go.」"

# game/dialogs.rpy:4348
translate japanese bonus4_6796bcb2:

    # hero "「And while that, we'll save money all together and Rika will organize the trip for all of us!"
    hero "「And while that, we'll save money all together and Rika will organize the trip for all of us!」"

# game/dialogs.rpy:4350
translate japanese bonus4_0d18a236:

    # s "「That sounds so great!"
    s "「That sounds so great!」"

# game/dialogs.rpy:4351
translate japanese bonus4_52addb72:

    # hero "「I'll call Rika tomorrow to propose it!"
    hero "「I'll call Rika tomorrow to propose it!」"

# game/dialogs.rpy:4352
translate japanese bonus4_8cd12022:

    # hero "「I'm sure it will be alot of fun!"
    hero "「I'm sure it will be alot of fun!」"

# game/dialogs.rpy:4357
translate japanese bonus4_e1a7c00a:

    # "We finally reached Sakura's house."
    "家に着いた。"

# game/dialogs.rpy:4358
translate japanese bonus4_94a9ed42:

    # s "「See you soon and take care... [hnick!t]."
    s "「それでは、また学校で…[hnick!t]。」"

# game/dialogs.rpy:4360
translate japanese bonus4_50eaa374:

    # "Sakura does a maid-like reverence with her little summer dress."
    "咲良ちゃんがメイドっぽく挨拶をした。"

# game/dialogs.rpy:4361
translate japanese bonus4_0f54fbae:

    # hero "「Take care, Sakura-chan!"
    hero "「バイバイ、咲良ちゃん！」"

# game/dialogs.rpy:4363
translate japanese bonus4_68a39d42:

    # hero "「Take care, little sister!"
    hero "「バイバイ、妹ちゃん！」"

# game/dialogs.rpy:4365
translate japanese bonus4_8dcab874:

    # "She giggled and disappeared in her house."
    "彼女は笑って、家の中に帰っていった。"

# game/dialogs.rpy:4366
translate japanese bonus4_6b242521:

    # "That was a fine sunday..."
    "ある晴れた日曜日の事だった…"

# game/dialogs.rpy:4367
translate japanese bonus4_5206326a:

    # "A good start for the vacations..."
    "A good start for the vacations..."

# game/dialogs.rpy:4380
translate japanese previewmsg_5de028fa:

    # centered "{size=+12}The story starts but the preview ends here...{/size}"
    centered "{size=+12}The story starts but the preview ends here...{/size}"

# game/dialogs.rpy:4381
translate japanese previewmsg_f6d47b23:

    # centered "{size=+12}Stay tuned and keep the app in your device to get the free complete update of Kare wa Kanojo in early 2018!{/size}"
    centered "{size=+12}Stay tuned and keep the app in your device to get the free complete update of Kare wa Kanojo in early 2018!{/size}"

translate japanese strings:

    # dialogs.rpy:34
    old "What should I do?"
    new "さて困った…"

    # dialogs.rpy:34
    old "Let's play some online game."
    new "Let's play some online game."

    # dialogs.rpy:34
    old "Let's rewatch some anime VHS."
    new "Let's rewatch some anime VHS."

    # dialogs.rpy:34
    old "Let's study a bit."
    new "Let's study a bit."

    # dialogs.rpy:438
    old "What should I say?"
    new "どうしようかな…"

    # dialogs.rpy:438
    old "The truth: {i}High School Samurai{/i}, an ecchi shonen manga"
    new "少年H漫画のハイスクールサムライにしよう。"

    # dialogs.rpy:438
    old "Not completely the truth: {i}Rosario Maiden{/i}, a seinen manga about vampire dolls"
    new "吸血鬼人形のロザリオメイデンにしよう。"

    # dialogs.rpy:940
    old "What should I do?..."
    new "さて困った…"

    # dialogs.rpy:940
    old "Tell the truth to Sakura"
    new "真実を伝える。"

    # dialogs.rpy:940
    old "Keep the secret and buy it another day"
    new "秘密は秘密のままで。"

    # dialogs.rpy:1609
    old "Please help me! What should I do??"
    new "誰か助けてくれ！僕は一体、どうすればいいんだ？？？"

    # dialogs.rpy:1609
    old "No!... It's a boy! I can't love him, I'm not gay!"
    new "僕は男を愛せない…！僕はホモじゃない！"

    # dialogs.rpy:1609
    old "Whatever! Love doesn't have gender!"
    new "違う…！愛に性別なんて関係無かったんだ！"

    # dialogs.rpy:4107
    old "Call me \"Master\"!"
    new "「ご主人様と呼びたまえ。」"

    # dialogs.rpy:4107
    old "Call me \"Big brother\"!"
    new "「お兄ちゃんって呼んでくれ。」"
