﻿# TODO: Translation updated at 2017-11-07 16:52

translate japanese strings:

    # omake.rpy:83
    old "{b}D.O.B.:{/b} 1978/09/29\n"
    new "{b}D.O.B.:{/b} 1978/09/29\n"

    # omake.rpy:84
    old "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"
    new "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"

    # omake.rpy:85
    old "{b}Height:{/b} 5.4ft\n"
    new "{b}Height:{/b} 5.4ft\n"

    # omake.rpy:86
    old "{b}Weight:{/b} 136 pounds\n"
    new "{b}Weight:{/b} 136 pounds\n"

    # omake.rpy:87
    old "{b}Measurements:{/b} 74-64-83\n"
    new "{b}Measurements:{/b} 74-64-83\n"

    # omake.rpy:88
    old "{b}Blood type:{/b} A\n"
    new "{b}Blood type:{/b} A\n"

    # omake.rpy:89
    old "{b}Favourite manga:{/b} High School Samurai\n"
    new "{b}Favourite manga:{/b} High School Samurai\n"

    # omake.rpy:90
    old "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"
    new "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"

    # omake.rpy:91
    old "{b}Favourite food:{/b} American hamburgers\n"
    new "{b}Favourite food:{/b} American hamburgers\n"

    # omake.rpy:92
    old "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."
    new "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."

    # omake.rpy:115
    old "{b}D.O.B.:{/b} 1979/02/28\n"
    new "{b}D.O.B.:{/b} 1979/02/28\n"

    # omake.rpy:116
    old "{b}P.O.B.:{/b} Kameoka, Kyoto\n"
    new "{b}P.O.B.:{/b} Kameoka, Kyoto\n"

    # omake.rpy:117
    old "{b}Height:{/b} 5.1ft\n"
    new "{b}Height:{/b} 5.1ft\n"

    # omake.rpy:118
    old "{b}Weight:{/b} 121 pounds\n"
    new "{b}Weight:{/b} 121 pounds\n"

    # omake.rpy:119
    old "{b}Measurements:{/b} Unknown\n"
    new "{b}Measurements:{/b} Unknown\n"

    # omake.rpy:120
    old "{b}Blood type:{/b} AB\n"
    new "{b}Blood type:{/b} AB\n"

    # omake.rpy:121
    old "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"
    new "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"

    # omake.rpy:122
    old "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"
    new "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"

    # omake.rpy:123
    old "{b}Favourite food:{/b} Beef yakitori\n"
    new "{b}Favourite food:{/b} Beef yakitori\n"

    # omake.rpy:124
    old "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."
    new "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."

    # omake.rpy:129
    old "{image=chars/s-1-1.png}"
    new "{image=chars/s-1-1.png}"

    # omake.rpy:147
    old "{b}D.O.B.:{/b} 1979/08/05\n"
    new "{b}D.O.B.:{/b} 1979/08/05\n"

    # omake.rpy:148
    old "{b}P.O.B.:{/b} The Village, Osaka\n"
    new "{b}P.O.B.:{/b} The Village, Osaka\n"

    # omake.rpy:149
    old "{b}Height:{/b} 5ft\n"
    new "{b}Height:{/b} 5ft\n"

    # omake.rpy:150
    old "{b}Weight:{/b} 110 pounds\n"
    new "{b}Weight:{/b} 110 pounds\n"

    # omake.rpy:151
    old "{b}Measurements:{/b} 92-64-87\n"
    new "{b}Measurements:{/b} 92-64-87\n"

    # omake.rpy:152
    old "{b}Blood type:{/b} O\n"
    new "{b}Blood type:{/b} O\n"

    # omake.rpy:153
    old "{b}Favourite manga:{/b} Rosario Maiden\n"
    new "{b}Favourite manga:{/b} Rosario Maiden\n"

    # omake.rpy:154
    old "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"
    new "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"

    # omake.rpy:155
    old "{b}Favourite food:{/b} Takoyaki\n"
    new "{b}Favourite food:{/b} Takoyaki\n"

    # omake.rpy:156
    old "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."
    new "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."

    # omake.rpy:161
    old "{image=chars/r-0-4.png}"
    new "{image=chars/r-0-4.png}"

    # omake.rpy:179
    old "{b}D.O.B.:{/b} 1980/10/11\n"
    new "{b}D.O.B.:{/b} 1980/10/11\n"

    # omake.rpy:180
    old "{b}P.O.B.:{/b} Ginoza, Okinawa\n"
    new "{b}P.O.B.:{/b} Ginoza, Okinawa\n"

    # omake.rpy:181
    old "{b}Height:{/b} 4.5ft\n"
    new "{b}Height:{/b} 4.5ft\n"

    # omake.rpy:182
    old "{b}Weight:{/b} 99 pounds\n"
    new "{b}Weight:{/b} 99 pounds\n"

    # omake.rpy:183
    old "{b}Measurements:{/b} 71-51-74\n"
    new "{b}Measurements:{/b} 71-51-74\n"

    # omake.rpy:184
    old "{b}Blood type:{/b} B\n"
    new "{b}Blood type:{/b} B\n"

    # omake.rpy:185
    old "{b}Favourite manga:{/b} Nanda no Ryu\n"
    new "{b}Favourite manga:{/b} Nanda no Ryu\n"

    # omake.rpy:186
    old "{b}Favourite videogame:{/b} Pika Pika Rocket\n"
    new "{b}Favourite videogame:{/b} Pika Pika Rocket\n"

    # omake.rpy:187
    old "{b}Favourite food:{/b} Chanpuruu\n"
    new "{b}Favourite food:{/b} Chanpuruu\n"

    # omake.rpy:188
    old "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."
    new "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."

    # omake.rpy:193
    old "{image=chars/n-1-3.png}"
    new "{image=chars/n-1-3.png}"

    # omake.rpy:85
    old "Opening song \"TAKE MY HEART\""
    new "オープニングソング \"TAKE MY HEART\""

    # omake.rpy:86
    old "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"
    new "歌手: Mew Nekohime\n歌詞: Max le Fou, Masaki Deguchi\n構成とシーケンス: Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"

    # omake.rpy:92
    old "{b}Japanese lyrics:{/b}\n"
    new "{b}歌詞:{/b}\n"

    # omake.rpy:92
    old "{b}Romaji:{/b}\n"
    new "{b}ロマジ:{/b}\n"

    # omake.rpy:124
    old "{b}Translation:{/b}\n"
    new "{b}英訳:{/b}\n"

# ==============================================================================
# DON'T TOUCH THESE STRINGS IN THAT BLOCK!

    # omake.rpy:137
    old "When you take my hand, I feel I could fly"
    new "When you take my hand, I feel I could fly"

    # omake.rpy:138
    old "When I dive into your eyes, I feel I could drown in happiness\n"
    new "When I dive into your eyes, I feel I could drown in happiness\n"

    # omake.rpy:139
    old "We sure look different"
    new "We sure look different"

    # omake.rpy:140
    old "But despite that, my heart beats loud\n"
    new "But despite that, my heart beats loud\n"

    # omake.rpy:141
    old "A boy and a girl"
    new "A boy and a girl"

    # omake.rpy:142
    old "I'm just a human"
    new "I'm just a human"

    # omake.rpy:143
    old "I hope you don't mind"
    new "I hope you don't mind"

    # omake.rpy:144
    old "I can't control my feelings"
    new "I can't control my feelings"

    # omake.rpy:145
    old "Come to me, take my heart"
    new "Come to me, take my heart"

    # omake.rpy:146
    old "I will devote myself to you"
    new "I will devote myself to you"

    # omake.rpy:147
    old "No matter what, I love you"
    new "No matter what, I love you"

# ==============================================================================

    # omake.rpy:278
    old "Chapter 1"
    new "Chapter 1"

    # omake.rpy:278
    old "Complete the first chapter"
    new "Complete the first chapter"

    # omake.rpy:278
    old "Chapter 2"
    new "Chapter 2"

    # omake.rpy:278
    old "Complete the second chapter"
    new "Complete the second chapter"

    # omake.rpy:278
    old "Chapter 3"
    new "Chapter 3"

    # omake.rpy:278
    old "Complete the third chapter"
    new "Complete the third chapter"

    # omake.rpy:278
    old "Chapter 4"
    new "Chapter 4"

    # omake.rpy:278
    old "Complete the fourth chapter"
    new "Complete the fourth chapter"

    # omake.rpy:278
    old "Chapter 5"
    new "Chapter 5"

    # omake.rpy:278
    old "Complete the fifth chapter"
    new "Complete the fifth chapter"

    # omake.rpy:278
    old "Finally together"
    new "Finally together"

    # omake.rpy:278
    old "Finish the game for the first time"
    new "Finish the game for the first time"

    # omake.rpy:278
    old "The perfume of rice fields"
    new "The perfume of rice fields"

    # omake.rpy:278
    old "Get a kiss from Sakura"
    new "Get a kiss from Sakura"

    # omake.rpy:278
    old "It's not that I like you, baka!"
    new "It's not that I like you, baka!"

    # omake.rpy:278
    old "Get a kiss from Rika"
    new "Get a kiss from Rika"

    # omake.rpy:278
    old "A new kind of game"
    new "A new kind of game"

    # omake.rpy:278
    old "Get a kiss from Nanami"
    new "Get a kiss from Nanami"

    # omake.rpy:278
    old "It's a trap!"
    new "It's a trap!"

    # omake.rpy:278
    old "Find the truth about Sakura"
    new "Find the truth about Sakura"

    # omake.rpy:278
    old "Good guy"
    new "Good guy"

    # omake.rpy:278
    old "Tell Sakura the truth about the yukata"
    new "Tell Sakura the truth about the yukata"

    # omake.rpy:278
    old "It's all about the Pentiums, baby"
    new "It's all about the Pentiums, baby"

    # omake.rpy:278
    old "Choose to game at the beginning of the game."
    new "Choose to game at the beginning of the game."

    # omake.rpy:278
    old "She got me!"
    new "She got me!"

    # omake.rpy:278
    old "Help Rika with the festival"
    new "Help Rika with the festival"

    # omake.rpy:278
    old "Grope!"
    new "Grope!"

    # omake.rpy:278
    old "Grope Rika (accidentally)"
    new "Grope Rika (accidentally)"

    # omake.rpy:278
    old "A good prank"
    new "A good prank"

    # omake.rpy:278
    old "Prank your friends with Nanami"
    new "Prank your friends with Nanami"

    # omake.rpy:278
    old "I'm not little!"
    new "I'm not little!"

    # omake.rpy:278
    old "Help Sakura tell the truth to Nanami"
    new "Help Sakura tell the truth to Nanami"

    # omake.rpy:278
    old "Big change of life"
    new "Big change of life"

    # omake.rpy:278
    old "Complete Sakura's route"
    new "Complete Sakura's route"

    # omake.rpy:278
    old "City Rat"
    new "City Rat"

    # omake.rpy:278
    old "Complete Rika's route"
    new "Complete Rika's route"

    # omake.rpy:278
    old "That new girl"
    new "That new girl"

    # omake.rpy:278
    old "Complete Nanami's route"
    new "Complete Nanami's route"

    # omake.rpy:278
    old "Knee-Deep into the 34D"
    new "Knee-Deep into the 34D"

    # omake.rpy:278
    old "Find the secret code in the game"
    new "Find the secret code in the game"
