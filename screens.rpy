﻿# TODO: Translation updated at 2017-11-07 16:52

translate japanese strings:

    # screens.rpy:253
    old "Back"
    new "Back"

    # screens.rpy:254
    old "History"
    new "History"

    # screens.rpy:255
    old "Skip"
    new "Skip"

    # screens.rpy:256
    old "Auto"
    new "オート"

    # screens.rpy:257
    old "Save"
    new "セーブ"

    # screens.rpy:258
    old "Q.save"
    new "クイックセーブ"

    # screens.rpy:259
    old "Q.load"
    new "クイックロード"

    # screens.rpy:260
    old "Settings"
    new "せってい"

    # screens.rpy:302
    old "New game"
    new "はじめから"

    # screens.rpy:320
    old "Load"
    new "つづきから"

    # screens.rpy:323
    old "Bonus"
    new "ボーナス"

    # screens.rpy:329
    old "End Replay"
    new "End Replay"

    # screens.rpy:333
    old "Main menu"
    new "メインメニュー"

    # screens.rpy:335
    old "About"
    new "About"

    # screens.rpy:337
    old "Please donate!"
    new "Please donate!"

    # screens.rpy:342
    old "Help"
    new "Help"

    # screens.rpy:346
    old "Quit"
    new "おわる"

    # screens.rpy:572
    old "Version [config.version!t]\n"
    new "Version [config.version!t]\n"

    # screens.rpy:578
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"

    # screens.rpy:618
    old "Page {}"
    new "Page {}"

    # screens.rpy:618
    old "Automatic saves"
    new "Automatic saves"

    # screens.rpy:618
    old "Quick saves"
    new "Quick saves"

    # screens.rpy:660
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # screens.rpy:660
    old "empty slot"
    new "空き"

    # screens.rpy:680
    old "{#auto_page}A"
    new "{#auto_page}A"

    # screens.rpy:683
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # screens.rpy:751
    old "Display"
    new "ディスプレイ"

    # screens.rpy:752
    old "Windowed"
    new "ウィンドウ"

    # screens.rpy:753
    old "Fullscreen"
    new "フルスクリーン"

    # screens.rpy:757
    old "Rollback Side"
    new "Rollback Side"

    # screens.rpy:758
    old "Disable"
    new "Disable"

    # screens.rpy:759
    old "Left"
    new "Left"

    # screens.rpy:760
    old "Right"
    new "Right"

    # screens.rpy:765
    old "Unseen Text"
    new "見えないテキスト"

    # screens.rpy:766
    old "After choices"
    new "選択肢の後"

    # screens.rpy:767
    old "Transitions"
    new "トランジション"

    # screens.rpy:782
    old "Text speed"
    new "テキストスピード"

    # screens.rpy:786
    old "Auto forward"
    new "Auto forward"

    # screens.rpy:793
    old "Music volume"
    new "BGMのボリューム"

    # screens.rpy:800
    old "Sound volume"
    new "サウンドのボリューム"

    # screens.rpy:806
    old "Test"
    new "テスト"

    # screens.rpy:810
    old "Voice volume"
    new "Voice volume"

    # screens.rpy:821
    old "Mute All"
    new "Mute All"

    # screens.rpy:832
    old "Language"
    new "Language"

    # screens.rpy:958
    old "The dialogue history is empty."
    new "ダイアログの履歴は空です。"

    # screens.rpy:1023
    old "Keyboard"
    new "Keyboard"

    # screens.rpy:1024
    old "Mouse"
    new "Mouse"

    # screens.rpy:1027
    old "Gamepad"
    new "ジョイスティック"

    # screens.rpy:1040
    old "Enter"
    new "Enter"

    # screens.rpy:1041
    old "Advances dialogue and activates the interface."
    new "Advances dialogue and activates the interface."

    # screens.rpy:1044
    old "Space"
    new "Space"

    # screens.rpy:1045
    old "Advances dialogue without selecting choices."
    new "Advances dialogue without selecting choices."

    # screens.rpy:1048
    old "Arrow Keys"
    new "Arrow Keys"

    # screens.rpy:1049
    old "Navigate the interface."
    new "Navigate the interface."

    # screens.rpy:1052
    old "Escape"
    new "Escape"

    # screens.rpy:1053
    old "Accesses the game menu."
    new "Accesses the game menu."

    # screens.rpy:1056
    old "Ctrl"
    new "Ctrl"

    # screens.rpy:1057
    old "Skips dialogue while held down."
    new "Skips dialogue while held down."

    # screens.rpy:1060
    old "Tab"
    new "Tab"

    # screens.rpy:1061
    old "Toggles dialogue skipping."
    new "Toggles dialogue skipping."

    # screens.rpy:1064
    old "Page Up"
    new "Page Up"

    # screens.rpy:1065
    old "Rolls back to earlier dialogue."
    new "Rolls back to earlier dialogue."

    # screens.rpy:1068
    old "Page Down"
    new "Page Down"

    # screens.rpy:1069
    old "Rolls forward to later dialogue."
    new "Rolls forward to later dialogue."

    # screens.rpy:1073
    old "Hides the user interface."
    new "Hides the user interface."

    # screens.rpy:1077
    old "Takes a screenshot."
    new "Takes a screenshot."

    # screens.rpy:1081
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # screens.rpy:1087
    old "Left Click"
    new "Left Click"

    # screens.rpy:1091
    old "Middle Click"
    new "Middle Click"

    # screens.rpy:1095
    old "Right Click"
    new "Right Click"

    # screens.rpy:1099
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Mouse Wheel Up\nClick Rollback Side"

    # screens.rpy:1103
    old "Mouse Wheel Down"
    new "Mouse Wheel Down"

    # screens.rpy:1110
    old "Right Trigger\nA/Bottom Button"
    new "Right Trigger\nA/Bottom Button"

    # screens.rpy:1114
    old "Left Trigger\nLeft Shoulder"
    new "Left Trigger\nLeft Shoulder"

    # screens.rpy:1118
    old "Right Shoulder"
    new "Right Shoulder"

    # screens.rpy:1122
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # screens.rpy:1126
    old "Start, Guide"
    new "Start, Guide"

    # screens.rpy:1130
    old "Y/Top Button"
    new "Y/Top Button"

    # screens.rpy:1133
    old "Calibrate"
    new "Calibrate"

    # screens.rpy:1198
    old "Yes"
    new "はい"

    # screens.rpy:1199
    old "No"
    new "いいえ"

    # screens.rpy:1245
    old "Skipping"
    new "スキップを始める"

    # screens.rpy:1466
    old "Menu"
    new "メニュー"

    # screens.rpy:1522
    old "Picture gallery"
    new "画廊"

    # screens.rpy:1523
    old "Music player"
    new "音楽プレーヤー"

    # screens.rpy:1543
    old "Musics and pictures"
    new "ミュージカルと写真"

    # screens.rpy:1508
    old "Bonus chapters"
    new "ボーナスチャプター"

    # screens.rpy:1511
    old "1 - A casual day at the club"
    new "1 - クラブでのカジュアルな一日"

    # screens.rpy:1516
    old "2 - Questioning sexuality (Sakura's route)"
    new "2 - セクシュアリティを問う（咲良のルート）"

    # screens.rpy:1521
    old "3 - Headline news"
    new "3 - ヘッドラインニュース"

    # screens.rpy:1526
    old "4a - A picnic at the summer (Sakura's route)"
    new "4a - 夏のピクニック（咲良のルート）"

    # screens.rpy:1531
    old "4b - A picnic at the summer (Rika's route)"
    new "4b - 夏のピクニック（梨花のルート）"

    # screens.rpy:1536
    old "4c - A picnic at the summer (Nanami's route)"
    new "4c - 夏のピクニック（七海のルート）"

    # char profiles
    old "Characters profiles"
    new "キャラクタープロフィール"

    # screens.rpy:387
    old "Update Available!"
    new "更新！"

    # screens.rpy:1536
    old "Opening song lyrics"
    new "オープニング歌詞"

        # screens.rpy:1611
    old "Max le Fou - Taichi's Theme"
    new "Max le Fou - 太一のテーマ"

    # screens.rpy:1613
    old "Max le Fou - Sakura's Waltz"
    new "Max le Fou - 咲良のワルツ"

    # screens.rpy:1615
    old "Max le Fou - Rika's theme"
    new "Max le Fou - 梨花のテーマ"

    # screens.rpy:1617
    old "Max le Fou - Of Bytes and Sanshin"
    new "Max le Fou - バイトと三線の"

    # screens.rpy:1619
    old "Max le Fou - Time for School"
    new "Max le Fou - 学校のための時間"

    # screens.rpy:1621
    old "Max le Fou - Sakura's secret"
    new "Max le Fou - 咲良の秘密"

    # screens.rpy:1623
    old "Max le Fou - I think I'm in love"
    new "Max le Fou - 僕は愛にだと思う"

    # screens.rpy:1625
    old "Max le Fou - Darkness of the Village"
    new "Max le Fou - 村の闇"

    # screens.rpy:1627
    old "Max le Fou - Love always win"
    new "Max le Fou - 愛はいつも勝つ"

    # screens.rpy:1629
    old "Max le Fou - Ondo"
    new "Max le Fou - 音頭"

    # screens.rpy:1631
    old "Max le Fou - Happily Ever After"
    new "Max le Fou - 幸せに"

    # screens.rpy:1633
    old "J.S. Bach - Air Orchestral suite #3"
    new "J.S. Bach - Air Orchestral suite #3"

    # screens.rpy:1635
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new "Mew Nekohime - TAKE MY HEART (TV Size)"
# TODO: Translation updated at 2019-04-27 11:10

translate japanese strings:

    # screens.rpy:690
    old "<"
    new "<"

    # screens.rpy:702
    old ">"
    new ">"

    # screens.rpy:1552
    old "Achievements"
    new "Achievements"

