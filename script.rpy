﻿# TODO: Translation updated at 2017-11-07 16:52

# game/script.rpy:220
translate japanese splashscreen_73841dfc:

    # centered "THIS IS AN ALPHA RELEASE\n\nIt's not meant for public.\nPlease do not distribute!{fast}"
    centered "THIS IS AN ALPHA RELEASE\n\nIt's not meant for public.\nPlease do not distribute!{fast}"

# game/script.rpy:251
translate japanese konami_code_e5b259c9:

    # "Max le Fou" "What the heck am I doing here?..."
    "Max le Fou" "私はここ何をやっていますか？..."

# game/script.rpy:287
translate japanese gjconnect_4ae60ea0:

    # "Disconnected."
    "切断されました。"

# game/script.rpy:316
translate japanese gjconnect_e254a406:

    # "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."
    "Game Joltへの接続が成功しました。\n警告：接続は以前に保存されたゲームのカウントには含まれません。"

translate japanese strings:

    # script.rpy:201
    old "Master"
    new "ご主人様"

    # script.rpy:202
    old "Big brother"
    new "お兄ちゃん"

    # script.rpy:272
    old "Disconnect from Game Jolt?"
    new "Game Joltから切断しますか？"

    # script.rpy:272
    old "Yes, disconnect."
    new "はい、切断します。"

    # script.rpy:272
    old "No, return to menu."
    new "いいえ、メニューに戻ります。"

    # script.rpy:334
    old "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"
    new "問題が発生しました。接続に問題がある可能性があります。それともGame Joltが狂っているのか...\n\n再試行する？"

    # script.rpy:334
    old "Yes, try again."
    new "はい、もう一度やり直してください。"

    # script.rpy:352
    old "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    new "認証が失敗したようです。 たぶん、ユーザー名とトークンを正しく書いていないかもしれません...\n\n再試行する？"

    # script.rpy:228
    old "Please enter your name and press Enter:"
    new "あなたの名前を入力し、Enterを押してください："

    # script.rpy:295
    old "Type here your username and press Enter."
    new "ここにユーザー名を入力し、Enterキーを押します。"

    # script.rpy:296
    old "Now, type here your game token and press Enter."
    new "ここにあなたのゲームトークンを入力し、Enterを押してください。"

    # script.rpy:207
    old "Game Jolt trophy obtained!"
    new "Game Joltトロフィーが得られました！"

    # script.rpy:177
    old "Sakura"
    new "咲良"

    # script.rpy:178
    old "Rika"
    new "梨花"

    # script.rpy:179
    old "Nanami"
    new "七海"

    # script.rpy:180
    old "Sakura's mom"
    new "咲良の母"

    # script.rpy:181
    old "Sakura's dad"
    new "咲良の父"

    # script.rpy:182
    old "Toshio"
    new "利夫"

    # script.rpy:188
    old "Taichi"
    new "太一"

    # script.rpy:13
    old "{size=80}Thanks for playing!"
    new "{size=80}遊んでくれてありがとう！"
# TODO: Translation updated at 2019-04-27 11:10

# game/script.rpy:290
translate japanese update_menu_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# game/script.rpy:304
translate japanese gjconnect_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

translate japanese strings:

    # script.rpy:226
    old "Achievement obtained!"
    new "Achievement obtained!"

